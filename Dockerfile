FROM 417625020754.dkr.ecr.eu-west-1.amazonaws.com/lsphp-baseimage-api
RUN mkdir /app
WORKDIR /app
COPY . .
RUN composer update
RUN composer dump-autoload
ENV TZ="Europe/Dublin"
RUN date
#RUN php artisan cache:clear
#RUN composer install
ENTRYPOINT ["php", "-S", "0.0.0.0:8000", "-t", "public"]