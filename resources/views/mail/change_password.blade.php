<!doctype html>
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title>

  </title>
  <!--[if !mso]><!-- -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    body {
      margin: 0;
      padding: 0;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    p {
      display: block;
      margin: 13px 0;
    }

    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100% !important;
        max-width: 100%;
      }

      .mj-column-per-60 {
        width: 60% !important;
        max-width: 60%;
      }

      .mj-column-per-30 {
        width: 30% !important;
        max-width: 30%;
      }

      .mj-column-per-50 {
        width: 50% !important;
        max-width: 50%;
      }
    }

    @media only screen and (max-width:480px) {
      table.mj-full-width-mobile {
        width: 100% !important;
      }

      td.mj-full-width-mobile {
        width: auto !important;
      }
    }
  </style>
  <!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
      <![endif]-->
  <!--[if lte mso 11]>
        <style type="text/css">
          .mj-outlook-group-fix { width:100% !important; }
        </style>
      <![endif]-->

      <!--[if !mso]><!-->
      <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
      <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
      </style>
      <!--<![endif]-->
    </head>

    <body style="background-color:#cccccc;">
      <div style="background-color:#cccccc;">
    <!--[if mso | IE]>
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
          <![endif]-->


          <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">

            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
              <tbody>
                <tr>
                  <td style="direction:ltr;font-size:0px;padding:50px 10px;text-align:center;">
              <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
            <tr>
              <td
                 class="" width="600px"
              >
          
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:580px;" width="580"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
          <![endif]-->


          <div style="margin:0px auto;max-width:580px;">

            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
              <tbody>
                <tr>
                  <td style="border-bottom:2px solid #4C99C9;direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                        <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:580px;"
            >
          <![endif]-->

          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

              <tr>
                <td align="left" style="font-size:0px;padding:0;;padding-left:20px;word-break:break-word;">

                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                    <tbody>
                      <tr>
                        <td style="width:150px;">

                          <img height="auto" src="{{isset($data['logo'])?$data['logo']:''}}" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="150" />

                        </td>
                      </tr>
                    </tbody>
                  </table>

                </td>
              </tr>

            </table>

          </div>

                        <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>

      </div>

              <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      
              </td>
            </tr>
          
            <tr>
              <td
                 class="" width="600px"
              >
          
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:580px;" width="580"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
          <![endif]-->


          <div style="margin:0px auto;max-width:580px;">

            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
              <tbody>
                <tr>
                  <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                        <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="vertical-align:top;width:348px;"
            >
          <![endif]-->

          <div class="mj-column-per-60 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
              <tr>
                <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                  <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">
                    <p>Dear {{$data['candidate_name']}}</p>
                    <p style="line-height:16px">Your {{$data['agency']}} candidate account has now been set up.<br>

                      Please download the {{$data['agency']}} app from the link provided below<br>


                      Once you have downloaded the app, please allow the app to send you notifications. This will ensure that you never miss out on any communications from us.<br>

                      <p>Already an account is existing on this credential, you can login with the existing email id and password or you can reset your password by the following link.</p>
                    </p>
                  </div>
                </td>
              </tr>
              <tr>
                <td align="left" vertical-align="middle" style="font-size:0px;padding:10px 25px;word-break:break-word;">

                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;">
                    <tr>
                      <td align="center" bgcolor="#6ddce6" role="presentation" style="border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#6ddce6;" valign="middle">
                        <p style="display:inline-block;background:#6ddce6;color:#000000;font-family:Helvetica;font-size:13px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;">
                          <a href="https://accounts.logezy.co/forgot-password" style="text-decoration: none;" target="_blank">Click here to set password</a>
                        </p>
                      </td>
                    </tr>
                  </table>

                </td>
              </tr>
            </table>

          </div>

                        <!--[if mso | IE]>
            </td>
          
            <td
               align="center" class="" style="vertical-align:top;width:174px;"
            >
          <![endif]-->

          <div class="mj-column-per-30 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background-color:#f5f3f4;vertical-align:top;" width="100%">

              <tr>
                <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

                  <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">
                    <p style="color:#ec8a96;font-size:16px">Key Features.</p>
                  </div>

                </td>
              </tr>

              <tr>
                <td style="font-size:0px;padding:0 20px;word-break:break-word;">

                  <p style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:100%;">
                  </p>

                                <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:134px;" role="presentation" width="134px"
        >
          <tr>
            <td style="height:0;line-height:0;">
              &nbsp;
            </td>
          </tr>
        </table>
      <![endif]-->


    </td>
  </tr>

  <tr>
    <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">
        <p style="font-size:13px;margin:0">Vacancies</p>
      </div>

    </td>
  </tr>

  <tr>
    <td style="font-size:0px;padding:0 20px;word-break:break-word;">

      <p style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:100%;">
      </p>

                                <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:134px;" role="presentation" width="134px"
        >
          <tr>
            <td style="height:0;line-height:0;">
              &nbsp;
            </td>
          </tr>
        </table>
      <![endif]-->


    </td>
  </tr>

  <tr>
    <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">
        <p style="font-size:13px;margin:0">Bookings</p>
      </div>

    </td>
  </tr>

  <tr>
    <td style="font-size:0px;padding:0 20px;word-break:break-word;">

      <p style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:100%;">
      </p>

                                <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:134px;" role="presentation" width="134px"
        >
          <tr>
            <td style="height:0;line-height:0;">
              &nbsp;
            </td>
          </tr>
        </table>
      <![endif]-->


    </td>
  </tr>

  <tr>
    <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">
        <p style="font-size:13px;margin:0">Digital Timesheet</p>
      </div>

    </td>
  </tr>

  <tr>
    <td style="font-size:0px;padding:0 20px;word-break:break-word;">

      <p style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:100%;">
      </p>

                                <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:134px;" role="presentation" width="134px"
        >
          <tr>
            <td style="height:0;line-height:0;">
              &nbsp;
            </td>
          </tr>
        </table>
      <![endif]-->


    </td>
  </tr>

  <tr>
    <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">
        <p style="font-size:13px;margin:0">Clock in Clock out</p>
      </div>

    </td>
  </tr>

  <tr>
    <td style="font-size:0px;padding:0 20px;word-break:break-word;">

      <p style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:100%;">
      </p>

                                <!--[if mso | IE]>
        <table
           align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:dashed 1px lightgrey;font-size:1px;margin:0px auto;width:134px;" role="presentation" width="134px"
        >
          <tr>
            <td style="height:0;line-height:0;">
              &nbsp;
            </td>
          </tr>
        </table>
      <![endif]-->


    </td>
  </tr>

  <tr>
    <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

      <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">
        <p style="font-size:13px;margin:0">Refer a Friend</p>
      </div>

    </td>
  </tr>

</table>

</div>

                        <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>

      </div>


              <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      
              </td>
            </tr>
          
            <tr>
              <td
                 class="" width="600px"
              >
          
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:580px;" width="580"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
          <![endif]-->


          <div style="background:#f5f3f45c;background-color:#f5f3f45c;margin:0px auto;max-width:580px;">

            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f5f3f45c;background-color:#f5f3f45c;width:100%;">
              <tbody>
                <tr>
                  <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                        <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               align="center" class="" style=""
            >
          <![endif]-->

          <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;">
            <p>Download the App Now</p>
          </div>

                        <!--[if mso | IE]>
            </td>
          
            <td
               class="" style="width:290px;"
            >
          <![endif]-->

          <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                          <!--[if mso | IE]>
        <table
           border="0" cellpadding="0" cellspacing="0" role="presentation"
        >
          <tr>
        
              <td
                 style="vertical-align:top;width:145px;"
              >
            <![endif]-->

            <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">

              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                <tr>
                  <td align="center" style="font-size:0px;padding:0;word-break:break-word;">

                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                      <tbody>
                        <tr>
                          <td style="width:145px;">


                            <a href="{{$data['android_app_url']}}"> <img height="auto" src="https://logezydev.co.uk/assets/img/mail/playstore.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="145" />
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>

                  </td>
                </tr>

              </table>

            </div>

                          <!--[if mso | IE]>
              </td>
              
              <td
                 style="vertical-align:top;width:145px;"
              >
            <![endif]-->

            <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">

              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                <tr>
                  <td align="center" style="font-size:0px;padding:0;word-break:break-word;">

                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                      <tbody>
                        <tr>
                          <td style="width:145px;">
                            <a href="{{$data['ios_app_url']}}">
                              <img height="auto" src="https://logezydev.co.uk/assets/img/mail/appstore.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="145" />
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>

                  </td>
                </tr>

              </table>

            </div>

                          <!--[if mso | IE]>
              </td>
              
          </tr>
          </table>
        <![endif]-->
      </div>

                        <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>

      </div>


              <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      
              </td>
            </tr>
          
            <tr>
              <td
                 class="" width="600px"
              >
          
      <table
         align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:580px;" width="580"
      >
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
          <![endif]-->


          <div style="margin:0px auto;max-width:580px;">

            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
              <tbody>
                <tr>
                  <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
                        <!--[if mso | IE]>
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                
        <tr>
      
            <td
               class="" style="width:580px;"
            >
          <![endif]-->

          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                          <!--[if mso | IE]>
        <table
           border="0" cellpadding="0" cellspacing="0" role="presentation"
        >
          <tr>
        
              <td
                 style="vertical-align:top;width:580px;"
              >
            <![endif]-->

            <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                <tr>
                  <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

                    <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:left;color:#000000;">
                      <p style="line-height:16px">We will shortly send you a helpful video, clearly explaining all the features of the app.</p>
                      <p style="line-height:16px">All future communications with regards to Available Shifts, Your Availability, Shift Bookings and Cancellations will be sent via the {{$data['agency']}} app.</p>
                      <p style="line-height:16px">It is very important that you update your availability frequently via the app, so we can allocate you shifts as soon as our requirements match your availability and preferences.</p>
                      <p style="line-height:16px">We look forward to a rewarding working relationship with you.</p>
                      <p>{{$data['agency']}}</p>
                    </div>

                  </td>
                </tr>

              </table>

            </div>

                          <!--[if mso | IE]>
              </td>
              
          </tr>
          </table>
        <![endif]-->
      </div>

                        <!--[if mso | IE]>
            </td>
          
        </tr>
      
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>

      </div>


              <!--[if mso | IE]>
          </td>
        </tr>
      </table>
      
              </td>
            </tr>
          
                  </table>
                <![endif]-->
              </td>
            </tr>
          </tbody>
        </table>

      </div>


    <!--[if mso | IE]>
          </td>
        </tr>
      </table>
    <![endif]-->


  </div>
</body>

</html>