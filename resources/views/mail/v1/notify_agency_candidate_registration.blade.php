<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Candidate Registered</title>
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            border-collapse: collapse;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        p {
            display: block;
            margin: 13px 0;
        }

        @media only screen and (min-width:480px) {
            .mj-column-per-50 {
                width: 50% !important;
                max-width: 50%;
            }

            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100%;
            }
        }

        @media only screen and (max-width:480px) {
            table.mj-full-width-mobile {
                width: 100% !important;
            }

            td.mj-full-width-mobile {
                width: auto !important;
            }
        }
    </style>
</head>

<body>
    <div style="">
        <div style="background:#fbfbfb;background-color:#fbfbfb;margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="background:#fbfbfb;background-color:#fbfbfb;width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;text-align:center;">
                            <div class="mj-column-per-50 mj-outlook-group-fix"
                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                    style="vertical-align:top;" width="100%">
                                    <tr>
                                        <td align="left"
                                            style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                            <div
                                                style="font-family:helvetica;font-size:16px;line-height:1;text-align:left;color: #5a5959;text-transform: uppercase;">
                                                New candidate registered</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mj-column-per-50 mj-outlook-group-fix"
                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div style="background:#fbfbfb;background-color:#fbfbfb;margin:0px auto;max-width:600px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                style="background:#fbfbfb;background-color:#fbfbfb;width:100%;">
                <tbody>
                    <tr>
                        <td style="direction:ltr;font-size:0px;padding:0px 0;padding-bottom:0;text-align:center;">
                            <div class="mj-column-per-100 mj-outlook-group-fix"
                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                    style="vertical-align:top;" width="100%">
                                    <tr>
                                        <td style="font-size:0px;padding:10px 25px;word-break:break-word;">
                                            <p
                                                style="border-top:solid 4px #0168fa;font-size:1px;margin:0px auto;width:100%;">
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="background:#fbfbfb;background-color:#fbfbfb;margin:0px auto;max-width:600px;">
            <p style="padding-left: 25px;">First Name : {{ $mail_data['candidate_first_name'] }}</p>
            <p style="padding-left: 25px;">Last Name : {{ $mail_data['candidate_last_name'] }}</p>
            <p style="padding-left: 25px;">Address : {{ $mail_data['candidate_address'] }}</p>
            <p style="padding-left: 25px;">Post Code : {{ $mail_data['candidate_post_code'] }}</p>
            <p style="padding-left: 25px;">Job : {{ $mail_data['candidate_postion'] }}</p>
            <p style="padding-left: 25px;">Mobile : {{ $mail_data['candidate_mobile'] }}</p>
            <p style="padding-left: 25px;">Email : {{ $mail_data['candidate_email'] }}</p>
        </div>

    </div>
</body>

</html>
