<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title>

  </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>
  <div style="background:#ffffff;max-width:600px;">
    <p>New message from a candidate.</p>
    <p>Message: {{ $mail_data['message'] }}</p>
    <p>Mobile No: {{ $mail_data['mobile_no'] }}</p>
  </div>
</body>

</html>
