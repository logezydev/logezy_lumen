<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <title>

  </title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
    #outlook a { 
      padding:0;
    }
    body { 
      margin:0;padding:0;
      -webkit-text-size-adjust:100%;
      -ms-text-size-adjust:100%; 
    }
    table, td { 
      border-collapse:collapse;
      mso-table-lspace:0pt;
      mso-table-rspace:0pt; 
    }
    img { 
      border:0;
      height:auto;
      line-height:100%;
      outline:none;
      text-decoration:none;
      -ms-interpolation-mode:bicubic; 
    }
    p { 
      display:block;
      margin:13px 0; 
    }
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
       width:100% !important;
       max-width: 100%; 
     }
   }
 </style>
</head>
<body style="background-color:#eeeeee;">
  <div style="background-color:#eeeeee;">
    <div  style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
      <div style="padding: 25px 25px 0;">
        <img src="{{ isset($data['logo']) ? $data['logo'] : 'https://logezy.co/assets_logezy/website/img/logo.png'}}" style="width:100px" alt="">
      </div>
      <div style="padding: 20px 25px 0;">
       <p style="margin:0;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;text-align:left;color:#555555;    border-top: 2px solid #2196F3;padding-top: 20px;">
         {!! isset($data['mail_body']) ? $data['mail_body'] : '........' !!}
       </p>
     </div>
     <div style="padding: 25px 25px 0;">
      <p style="border-top: 2px dashed #dedede;padding-top: 19px;margin:0;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;color:#555555;">
      </p>
    </div>
    <div style="background:#f5f3f45c;background-color:#f5f3f45c;margin:0px auto;max-width:580px;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f5f3f45c;background-color:#f5f3f45c;width:100%;">
        <tbody>
          <tr>
            <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;">
             <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:center;color:#000000;">
              <p>Download the App Now</p>
            </div>
            <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
             <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                <tbody>
                  <tr>
                    <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                        <tbody>
                          <tr>
                            <td style="width:145px;">
                              <a href="{{ isset($data['android_app_url']) ? $data['android_app_url'] : ''}}" target="_blank" rel="noopener">
                                <img height="auto" src="https://logezydev.co.uk/assets/img/mail/playstore.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="250">
                              </a>
                           </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
              <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                <tbody>
                  <tr>
                    <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
                      <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                        <tbody>
                          <tr>
                            <td style="width:145px;">
                              <a href="{{ isset($data['ios_app_url']) ? $data['ios_app_url'] : ''}}" target="_blank" rel="noopener">
                                <img height="auto" src="https://logezydev.co.uk/assets/img/mail/appstore.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="250">
                              </a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<div style="padding: 25px 25px 40px;">
  <p style="margin:0;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;color:#555555;">You can always reach out to us in case you require any assistance</p>
</div>
</div>
</div>
</body>
</html>
