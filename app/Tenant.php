<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    protected $table = 'tenants';

    /**
     * userTenant
     *
     * @return void
     */
    public function userTenant()
    {
        return $this->hasOne('App\UserTenant', 'id');
    }
}
