<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyShiftAssigned extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->data['title'];
        $this->data['view'] = 'mail.shift_assigned';

        return $this->subject($subject)
                    ->from($this->data['agency_email'],$this->data['agency_name'])
                    // ->text('mail.v1.plain')
                    ->view('mail.shift_assigned');
    }
}
