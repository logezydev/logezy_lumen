<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\VacancyStaffRate;
use App\Tenants\CandidateVacancy;
use App\Tenants\Vacancy;
use App\Tenants\AssignVacancy;
use App\Tenants\AssignVacancyDetail;
use App\Tenants\RoleUser;
use App\Tenants\Helper;
use App\Tenants\Agency;
use App\Tenants\AgencySetting;
use App\Tenants\SignedTimesheet;
use App\Tenants\Day;
use App\Tenants\CustomHoliday;
use App\Tenants\Holiday;
use App\Tenants\CandidateRateCard;
use App\Tenants\CandidateAvailability;
use App\Tenants\TextTemplate;
use App\Tenants\CandidateRestrictedBusinessUnit;
use App\Jobs\PublishAssignedShiftsJob;
use App\Traits\ActivityLog\ActivityCustomLogTrait;
use App\Traits\PushNotificationTrait;
use App\Traits\SMSTraits;
use App\Traits\EmailTraits;
use App\Traits\UserTraits;
use App\User;
use App\Tenants\Notification;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifyShiftAssigned;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Storage;
use App\Helpers\SendMailGun;

class AssignVacancyController extends BaseController
{
    public function assigned_vacancies (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'nullable',
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            // else if ($messages->has('date_to')) {
            //     //Show custom message
            //     return response()->json([
            //       'error'=>[
            //         'status_code' => 400,
            //         'status' => 'Error', 
            //         'message' => $messages->first('date_to')
            //       ]
            //     ],400);
            // }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $date_from = $request->input('date_from');
          $date_to = isset($request->date_to) ? $request->input('date_to') : '';
          $candidate_id = $request->input('candidate_id');
        //   $canVacancy = [];
          $vacancies = 0;
          $unreadNotifications = 0;

          //switching to the database
          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          //checking for candidate access, restrict if candidate has no accesss
          $candidate = DB::table('candidates')->where('id', $candidate_id)->select('id','has_access','is_deleted','employment_types_id', 'user_id')->first();
          if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
          } 
          //checking for candidate status, restrict if candidate is deleted
          if ( $candidate && $candidate->is_deleted == 1 ) {
              return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
          } 

          //getting assigned vacancy details
          $assigned = AssignVacancy::with('assignedDetails', 'timeclockDetails.details')->where( function($query) use($date_from, $date_to) {

            if ($date_from != '' && $date_to != ''){
              $query->whereBetween('on_date', [$date_from, $date_to]);
            } else {
              $query->where('on_date', '>=', $date_from);
            }

          })->where('candidate_id', $candidate_id)
          ->whereIn('status', ['pending', 'approved', 'invoiced'])
          ->where('is_deleted', 0)->where('is_publish', 1)
          ->orderBy('on_date')->orderBy('start_time')->get(['id','code','vacancy_id','candidate_id','on_date','start_time','end_time','break','total_hours','status','notes','is_publish','is_deleted','is_moved_to_vacancies','is_confirm']);

          //to get dashboard count
          if ($date_from != '' && (!isset($date_to) || $date_to == '')){
            // getting today to future vacancy count
            // $todayDate = date("Y-m-d");
            $job_ids = CandidateJob::where('candidate_id', $candidate_id)->pluck('job_id')->all();

            //check agency settings for publish by role
            $is_publish_by_job = Agency::value('is_publish_by_job');

            //checking regions for candidate and business unit
            $regions = DB::table('candidate_regions')->where('candidate_id', $candidate_id)->pluck('region_id')->all();
            $business_units = [];
            if($regions) {
                //get business unit id of candidate with Regions
                $buIDs = DB::table('businessunit_regions')->whereIn('region_id', $regions)->pluck('business_unit_id')->all();
                $business_units = array_unique(array_values($buIDs));
            } else { 
                //Business units id without region
                $buIDs = DB::table('business_units')->leftJoin('businessunit_regions', 'business_units.id', '=', 'businessunit_regions.business_unit_id')->where('business_units.status', 'active')->where('business_units.is_deleted', 0)->whereNull('businessunit_regions.business_unit_id')->pluck('business_units.id')->all();
                $business_units = array_unique(array_values($buIDs));
            }
            $vacancies = DB::table('vacancies')
                    // ->whereRaw("find_in_set($candidate_id, published_ids)")
                    ->where('on_date', '>=', $date_from)
                    ->whereIn('job_id', $job_ids)
                    ->whereRaw('total_numbers > used_count')
                    ->where('is_publish', 1)
                    ->where(function ($query) use($is_publish_by_job, $candidate_id){
                        if ($is_publish_by_job) {
                            $query->where('is_publish_by_job', 1)->orWhereRaw("find_in_set($candidate_id, published_ids)");
                        } 
                        else {
                            $query->whereRaw("find_in_set($candidate_id, published_ids)");
                        }
                    })->whereIn('business_unit_id', $business_units)->count();
                    // ->get(['id', 'published_ids']);

            // foreach ($vacancies as $vacancy) {
            //   if($vacancy->published_ids != '' && in_array( $candidate_id, explode(',', $vacancy->published_ids)) ) {
                // $canVacancy[] = $vacancy->id;
            //   }
            // }
            //getting unread notification count
            $unreadNotifications = DB::table('notifications')->where('role_id', 6)->where('notifiable_id', $candidate->user_id)->where('read_at', NULL)->count();
          }

          //getting today to future assigned shift count
          // $assigns = DB::table('assign_vacancies')
          //             ->whereIn('status', ['pending', 'approved', 'invoiced'])
          //             ->where('candidate_id', $candidate_id)
          //             ->where('is_deleted', 0)
          //             ->where('on_date', '>=', $date_from)
          //             ->where('is_publish', 1)->count();



          //get signed timesheet status for agency app settings
          $agencySettings = DB::table('agency_settings')->where('settings_key', 'timeclock_status')->where('settings_value', 'true')->first(); // fetch agency settings
          if($agencySettings) {
              $settings = json_decode($agencySettings->details, true);
              $signed_timesheet = $settings['signed_timesheet']['status'];
          } else {
              $signed_timesheet = false;
          }
          //business units of the given candidate in the given date ranges
          $business_unit_ids = [];
          if ($date_from != '' && $date_to != ''){
            $business_unit_ids = DB::table('assign_vacancies')
              ->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
              ->whereIn('assign_vacancies.status',['pending', 'invoiced', 'approved', 'rejected'])
              ->where('assign_vacancies.is_deleted', 0)->where('assign_vacancies.is_publish', 1)
              ->where('assign_vacancies.candidate_id', $candidate_id)
              ->whereBetween('assign_vacancies.on_date', [$date_from, $date_to])
              ->pluck('vacancies.business_unit_id')->toArray();
          }

          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success', 
                  'vacancy_count' => $vacancies,
                  'assigned_vacancy_count' => count($assigned), 
                  'unread_notifications_count' => $unreadNotifications,
                  'time_format' => DB::table('agencies')->first()->time_format,
                  'business_unit_ids' => implode(',', array_values(array_unique($business_unit_ids))),
                  'data' => $assigned,
                  'integrate_signed_timesheet' => $signed_timesheet
                ]
              ],200);
      }

    }


    public function assigned_vacancy (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'assign_id' => $request->assign_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'assign_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('assign_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assign_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $assign_id = $request->input('assign_id');
          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');
          //checking for candidate status, restrict if candidate is deleted
          $candidate = Candidate::find($candidate_id);
          //checking for candidate access, restrict if candidate has no accesss
          if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
          } 
          if ( $candidate && $candidate->is_deleted == 1 ) {
              return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
          } 

          $assigned = AssignVacancy::with('assignedDetails', 'timeclockDetails.details.locations')->find($assign_id);

          //get signed timesheet status for agency app settings
          $agencySettings = DB::table('agency_settings')->where('settings_key', 'timeclock_status')->where('settings_value', 'true')->first(); // fetch agency settings
          if($agencySettings) {
              $settings = json_decode($agencySettings->details, true);
              $signed_timesheet = $settings['signed_timesheet']['status'];
          } else {
              $signed_timesheet = false;
          }

          if ($assigned && $assigned->is_deleted == 1) {
            return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' =>  'This shift has been deleted.'
                ]
              ],400);
          }
          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'time_format' => DB::table('agencies')->first()->time_format,
                  'data' => $assigned,
                  'integrate_signed_timesheet' => $signed_timesheet
                ]
              ],200);
      }

    }


    public function assigned_shift (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'assign_id' => $request->assign_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'assign_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('assign_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assign_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $assign_id = $request->input('assign_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $assigned = DB::table('assign_vacancies')
                ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                ->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
                // ->join('assigned_vacancy_details', 'assigned_vacancy_details.assigned_vacancy_id', '=', 'assign_vacancies.id')
                ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                ->join('clients', 'business_units.client_id', '=', 'clients.id')
                ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id', 'vacancies.business_unit_id', 'vacancies.job_id', 'vacancies.shift_id', 'clients.client_name', 'business_units.business_unit_name', 'jobs.name AS job_name', 'jobs.color', 'shifts.shift_name', 'vacancies.on_date', DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 'assign_vacancies.break', 'assign_vacancies.total_hours', 'assign_vacancies.notes', 'assign_vacancies.is_publish', 'assign_vacancies.is_confirm', 'assign_vacancies.status')
                ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                ->where('assign_vacancies.is_deleted', 0)
                ->where('candidates.is_deleted',0)
                ->where('candidates.candidate_status_id', 1)
                ->where('assign_vacancies.id', $assign_id)
                ->get();

          // if ($assigned && $assigned->is_deleted == 1) {
          //   return response()->json([
          //       'error'=>[
          //         'status_code' => 400,
          //         'status' => 'Error', 
          //         'message' =>  'This shift has been deleted.'
          //       ]
          //     ],400);
          // }
          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'time_format' => DB::table('agencies')->first()->time_format,
                  'data' => $assigned
                ]
              ],200);
      }

    }


    public function assigned_vacancy_confirm (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'assigned_vacancy_id' => $request->assigned_vacancy_id,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'assigned_vacancy_id' => 'required', 
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('assigned_vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assigned_vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $assigned_vacancy_id = $request->input('assigned_vacancy_id');
          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $candidate = Candidate::find($candidate_id);
          //checking for candidate access, restrict if candidate has no accesss
          if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
          } 

          $assigned = AssignVacancy::where('id', $assigned_vacancy_id)->where('candidate_id', $candidate_id)->first();
          $assigned->is_confirm = 1;

          if ($assigned->save()) {
            //updating notification
              $vacancy           = Vacancy::find($assigned->vacancy_id); 
              $candidate_name    = Candidate::find($candidate_id)->full_name;
              $user_ids          = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
              $user_ids          = array_unique($user_ids);
              $notify_array = array(
                  'title' => $candidate_name,
                  'type' => "shift_confirmed",
                  'date'       => date("d-m-Y",strtotime($vacancy->on_date)),
                  'body' => "confirmed shift on ".date("d-m-Y",strtotime($vacancy->on_date))." with booking code: ".$assigned->code.", in ".$vacancy->business_unit->business_unit_name,
                   );
      
              if ($user_ids) {
                  foreach ($user_ids as $user_id) {
                      $notification = new Notification;
                      $notification->type             = 'App\Notifications\HelloNotification';
                      $notification->notifiable_type  = 'App\User';
                      $notification->notifiable_id    = $user_id;
                      $notification->object_id        = $assigned->id;
                      $notification->data             = json_encode($notify_array);
                      $notification->resource_ids     = $candidate_id;
                      $notification->save();
                  }
              }
              //send push notifications to admins and managers
              $player_ids = RoleUser::whereIn('role_id', [2,3,4])->whereNotNull('player_id')->where('player_id', '!=', '')->pluck('player_id')->all();
              $player_ids = array_unique($player_ids);
              $template = $candidate_name." confirmed shift on ".date("d-m-Y",strtotime($vacancy->on_date))." with booking code: ".$assigned->code." in ".$vacancy->business_unit->business_unit_name;
              PushNotificationTrait::oneSignalPushMessage($player_ids, $template, array("data_type" => "shift_confirmed"));

              $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('name', 'email')->first();

              $details = EmailTraits::getEmailDetails('Confirm Assigned');
              if($details['enabled']) {
                //get agency details
                $agency = Agency::first();
                //sending request email to the agency
                $mail_content['candidate_name'] = '';
                $mail_content['from_email']     = $details['from_email'] ?? $user->email;
                $mail_content['agency_email']   = $agency->email;
                $mail_content['agency_name']    = $agency->name;
                $mail_content['agency_logo']    = $agency->agency_logo;
                $mail_content['title']          = 'Candidate Confirmed a Vacancy';
                $mail_content['body']           = $candidate_name." confirmed shift on ".date("d-m-Y",strtotime($vacancy->on_date))." with booking code: ".$assigned->code." in ".$vacancy->business_unit->business_unit_name;
                $mail_content['tenant_slug']    = $request->input('subdomain');

                SendMailGun::sendMail('notify_new_message', $mail_content['agency_name'], $mail_content['from_email'], $mail_content["agency_email"], $mail_content['title'], $mail_content, null, null, null);
              }
              
              $loggedData = [
                'role'          => 'candidate',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'candidate_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
              ];
              //Activity Log
              $message = 'Booking - shift has been confirmed with booking code ' . $assigned->code . '.';
              ActivityCustomLogTrait::saveActivityLog('Bookings', 'Update', $message, $assigned->id, $assigned->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
          }

          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success', 
                  'message' => "Thanks for confirming the shift"
                ]
              ],200);
      }

    }


    public function assigned_vacancy_reject (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'assigned_vacancy_id' => $request->assigned_vacancy_id,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'assigned_vacancy_id' => 'required', 
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('assigned_vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assigned_vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $assigned_vacancy_id = $request->input('assigned_vacancy_id');
          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $candidate = Candidate::find($candidate_id);
          //checking for candidate access, restrict if candidate has no accesss
          if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
          } 

          $assigned = AssignVacancy::where('id', $assigned_vacancy_id)->where('candidate_id', $candidate_id)->first();
          $assigned->status = "rejected";
          $assigned->status_by = $candidate->user_id;
          $assigned->status_at = date("Y-m-d H:i:s");

          if ($assigned->save()) {
            //updating notification
              $vacancy           = Vacancy::find($assigned->vacancy_id); 

              //update vacancy
              $latest_applied_count = DB::table('assign_vacancies')->where('vacancy_id', $assigned->vacancy_id)->where('is_deleted', 0)->where('status', 'applied')->count('id');
              $latest_used_count = DB::table('assign_vacancies')->where('vacancy_id', $assigned->vacancy_id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');
              $vacancy->used_count = $latest_used_count;
              $vacancy->applied_count = $latest_applied_count;
              $vacancy->save();

              $candidate_name    = $candidate->full_name;
              $user_ids          = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
              $user_ids          = array_unique($user_ids);
              $notify_array = array(
                  'title' => $candidate_name,
                  'type' => "shift_rejected",
                  'date' => date("d-m-Y",strtotime($vacancy->on_date)),
                  'body' => "Rejected shift on ".date("d-m-Y",strtotime($vacancy->on_date))." with booking code: ".$assigned->code.", in ".$vacancy->business_unit->business_unit_name,
                   );
      
              if ($user_ids) {
                  foreach ($user_ids as $user_id) {
                      $notification = new Notification;
                      $notification->type             = 'App\Notifications\HelloNotification';
                      $notification->notifiable_type  = 'App\User';
                      $notification->notifiable_id    = $user_id;
                      $notification->object_id        = $assigned->id;
                      $notification->data             = json_encode($notify_array);
                      $notification->resource_ids     = $candidate_id;
                      $notification->save();
                  }
              }
              //send push notifications to admins and managers
              $player_ids = RoleUser::whereIn('role_id', [2,3,4])->whereNotNull('player_id')->where('player_id', '!=', '')->pluck('player_id')->all();
              $player_ids = array_unique($player_ids);
              $template = $candidate_name." Rejected shift on ".date("d-m-Y",strtotime($vacancy->on_date))." with booking code: ".$assigned->code." in ".$vacancy->business_unit->business_unit_name;
              PushNotificationTrait::oneSignalPushMessage($player_ids, $template, array("data_type" => "shift_rejected"));
          
              $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('name', 'email')->first();
              $loggedData = [
                'role'          => 'candidate',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'candidate_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
              ];
              //Activity Log
              $message = 'Booking - shift has been rejected with booking code ' . $assigned->code . '.';
              ActivityCustomLogTrait::saveActivityLog('Bookings', 'Update', $message, $assigned->id, $assigned->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
          }

          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success', 
                  'message' => "You've rejected the shift"
                ]
              ],200);
      }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function apply_for_vacancy(Request $request)
    {  

        $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id, 
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'vacancy_id' => 'required', 
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $candidate = Candidate::find($request->candidate_id);
        //checking for candidate access, restrict if candidate has no accesss
        if ( $candidate && $candidate->has_access == 0 ) {
            return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'You have no access to use this account. Please contact the agency.',
                  ]
                ],401);
        } 
        //checking for candidate status, restrict if candidate is deleted
        if ( $candidate && $candidate->is_deleted == 1 ) {
            return response()->json([
              'error'=>[
                'status_code' => 401,
                'status' => 'Error', 
                'message' =>  'Unauthorized',
                'isDeleted'=> true,
                'isUnauthorized'=> true
              ]
            ],401);
        } 

        if ( $candidate->candidate_status_id != 1 ) {
            return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => "Please check and fill your profile to get full access."
                ]
              ],400);
        }
        // checks for existing application
        $alreadyApplied = AssignVacancy::where('candidate_id', $request->candidate_id)
            ->where('vacancy_id', $request->vacancy_id)
            ->where('status', 'applied')
            ->where('is_deleted', 0)->first();
        if ($alreadyApplied) {
            return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => "You're already applied for this slot."
                ]
              ],400);
        }

        $vacancy = Vacancy::find($request->vacancy_id); 

        if ( $vacancy ) {
          //Check vacancy published or not
          if ($vacancy->is_publish == 0) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Sorry for the inconvenience. This vacancy has been canceled. Please contact the agency"
              ]
            ],400);
          }

          //Check vacancy deleted or not
          if ($vacancy->deleted_space != 0) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Sorry for the inconvenience. This vacancy has been deleted by the agency"
              ]
            ],400);
          }

          //Check vacancy rejected by agency or not
          if (isset($vacancy->rejected) && !empty($vacancy->rejected)) {
              $rejected_candidates = collect($vacancy->rejected)->pluck('candidate_id')->all();
              if(in_array($candidate->id, $rejected_candidates)) {
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => "Sorry for the inconvenience. Your job has been canceled. Please contact the agency"
                  ]
                ],400);
              }
          }

          //Check vacancy rejected by candidate or not
          if (isset($vacancy->rejected_candidates) && $vacancy->rejected_candidates != NULL) {
              $rejected_candidates = explode(",", $vacancy->rejected_candidates);
              if(in_array($candidate->id, $rejected_candidates)) {
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => "You rejected this shift earlier."
                  ]
                ],400);
              }
          }

          $todayDate = date("Y-m-d");

          if ( $vacancy->on_date < $todayDate ) { 

              return response()->json([
                'error'=>[
                  'status_code' => 422,
                  'status' => 'Error', 
                  'message' => "Can not apply for the past vacancy."
                ]
              ],422);
          }
          //checking for night shift
          $start = date("H:i", strtotime($vacancy->start_time));
          $end = date("H:i", strtotime($vacancy->end_time));
          $startDate = $vacancy->on_date;
          if ($start >= $end) {
              $endDate = date('Y-m-d', strtotime('+1 day', strtotime($vacancy->on_date)));
          } else {
              $endDate = $vacancy->on_date;
          }

          //Checking before assigning candidate
          $inputArr = array(
              'candidate_id'      => $request->candidate_id,
              'business_unit_id'  => $vacancy->business_unit_id,
              'job_id'            => $vacancy->job_id,
              'on_date'           => $vacancy->on_date,
              'startDate'         => $startDate,
              'endDate'           => $endDate,
              'shift_id'          => $vacancy->shift_id,
              'start_time'        => $start,
              'end_time'          => $end,
              'total_hours'       => $vacancy->total_hours,
          );
          $assignValidations = AssignVacancy::assignValidations($inputArr);

          if ($assignValidations != "false") {
                return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $assignValidations
                    ]
                  ],400);

          } else {

            $request->request->add(['is_publish' => 0]);
            $request->request->add(['status' => 'applied']);

            $assign = AssignVacancy::assign_candidate($request);
            if ($assign) {
              //update vacancy
              $vacancyUpdate = Vacancy::find($vacancy->id);
              $latest_applied_count = DB::table('assign_vacancies')->where('vacancy_id', $vacancy->id)->where('is_deleted', 0)->where('status', 'applied')->count('id');
              $vacancyUpdate->applied_count = $latest_applied_count;
              $vacancyUpdate->save();
              //updating notification
              $candidate_name  = Candidate::find($request->candidate_id)->full_name;
              $user_ids        = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
              $user_ids        = array_unique($user_ids);
              $notify_array = array(
                  'title' => $candidate_name,
                  'type' => "vacancy_applied",
                  'date'       => date("d-m-Y",strtotime($vacancy->on_date)),
                  'body' => "applied for a vacancy on ".date("d-m-Y",strtotime($vacancy->on_date))." with Ref.No: ".$vacancy->code.", in ".$vacancy->business_unit->business_unit_name,
                   );
      
              if ($user_ids) {
                  foreach ($user_ids as $user_id) {
                      $notification = new Notification;
                      $notification->type             = 'App\Notifications\HelloNotification';
                      $notification->notifiable_type  = 'App\User';
                      $notification->notifiable_id    = $user_id;
                      $notification->object_id        = $request->vacancy_id;
                      $notification->data             = json_encode($notify_array);
                      $notification->resource_ids     = $request->vacancy_id;
                      $notification->save();
                  }
              }
              //send push notifications to admins and managers
              $player_ids = RoleUser::whereIn('role_id', [2,3,4])->whereNotNull('player_id')->where('player_id', '!=', '')->pluck('player_id')->all();
              $player_ids = array_unique($player_ids);
              $template = $candidate_name." applied for a vacancy on ".date("d-m-Y",strtotime($vacancy->on_date))." with Ref.No: ".$vacancy->code.", in ".$vacancy->business_unit->business_unit_name;
              PushNotificationTrait::oneSignalPushMessage($player_ids, $template, array("data_type" => "vacancy_applied"));

              //get agency details
              $agency = Agency::first();

              $details = EmailTraits::getEmailDetails('Apply Vacancy');
              
              if($details['enabled']) {
                //sending request email to the agency
                $mail_content['candidate_name'] = '';
                $mail_content['from_email']     = $details['from_email'] ?? $agency->email;
                $mail_content['agency_email']   = $agency->email;
                $mail_content['agency_name']    = $agency->name;
                $mail_content['agency_logo']    = $agency->agency_logo;
                $mail_content['title']          = 'Candidate Applied a Vacancy';
                $mail_content['body']           = $candidate_name." applied for a vacancy on ".date("d-m-Y",strtotime($vacancy->on_date))." with Ref.No: ".$vacancy->code.", in ".$vacancy->business_unit->business_unit_name;
                $mail_content['tenant_slug']    = $request->input('subdomain');

                SendMailGun::sendMail('notify_new_message', $mail_content['agency_name'], $mail_content['from_email'], $mail_content["agency_email"], $mail_content['title'], $mail_content, null, null, null);
              }

              $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('name', 'email')->first();
              
              $loggedData = [
                'role'          => 'candidate',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'candidate_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
              ];
              //Activity Log
              $message = 'Vacancy - new vacancy has been applied with Ref code ' . $vacancy->code . '.';
              ActivityCustomLogTrait::saveActivityLog('Vacancies', 'Update', $message, $vacancy->id, $vacancy->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

              return response()->json([
                    'success'=>[
                      'status_code' => 201,
                      'status' => 'Success', 
                      'message' => "Your application has been sent for approval."
                    ]
                  ],200);
            }
          }   
        }
      }
    }


    //SIGNED TIMESHEETS

    public function get_signed_vacancies (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
          $todayDate = date("Y-m-d");
          $date_from = $request->input('date_from');
          $date_to = $request->input('date_to');
          // $date_to = $todayDate;
          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

            $candidate = Candidate::find($request->candidate_id);
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
                return response()->json([
                      'error'=>[
                        'status_code' => 401,
                        'status' => 'Error', 
                        'message' =>  'You have no access to use this account. Please contact the agency.',
                      ]
                    ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 

              
          $assigned_shifts = AssignVacancy::whereIn('status', ['pending', 'approved', 'invoiced'])->where('is_deleted', 0)
                            ->where(function ($q) use ($date_from, $date_to, $todayDate, $candidate_id) {
                                if ($date_from != '' && $date_to != ''){
                                  $q->whereBetween('on_date', [$date_from, $date_to]);
                                }
                                $q->where('on_date', '<=', $todayDate);
                                if($candidate_id != ''){
                                    $q->where('candidate_id', $candidate_id);
                                }
                            })->get();

          $signed_timesheets = [];

          if ($assigned_shifts) {
            foreach ($assigned_shifts as $assigned) {

              // foreach ($assigned->details as $assign) {

                $assignDetail = AssignVacancyDetail::where('assigned_vacancy_id', $assigned->id)->first();
                $signed = SignedTimesheet::where('assigned_vacancy_id', $assigned->id)->orderBy('id', 'desc')->first();

                if($assigned && $signed){
                  $signed_timesheets[] = array(
                    'id' => $signed->id, 
                    'assigned_vacancy_id' => $signed->assigned_vacancy_id, 
                    'assigned_vacancy_details_id' => $signed->assigned_vacancy_details_id, 
                    'client_name' => $assigned->business_unit->client_name, 
                    'business_unit_name' => $assigned->business_unit_name, 
                    'business_unit_email' => $assigned->business_unit_email, 
                    'business_unit_address' => $assigned->business_unit_address, 
                    'on_date' => $assigned->on_date, 
                    'start_time' => $signed->start_time, 
                    'end_time' => $signed->end_time, 
                    'break' => $signed->break, 
                    'total_hours' => $signed->total_hours, 
                    'candidate_signature' => $signed->candidate_signature, 
                    'authorised_signature' => $signed->authorised_signature, 
                    'authorised_position' => $signed->authorised_position, 
                    'authorised_name' => $signed->authorised_name, 
                    'mileage' => $signed->mileage, 
                    'ref_number' => $signed->ref_number, 
                    'no_of_workers' => $signed->no_of_workers, 
                    'driver_service' => $signed->driver_service, 
                    'travel_expenses' => $signed->travel_expenses, 
                    'had_induction' => $signed->had_induction, 
                    'satisfactory_timekeeping' => $signed->satisfactory_timekeeping, 
                    'dressed_appropriately' => $signed->dressed_appropriately, 
                    'team_player' => $signed->team_player,
                    'standards_of_care' => $signed->standards_of_care,
                    'comments' => $signed->comments, 
                    'mileage_claim' => $signed->mileage_claim,
                    'worker_performance' => $signed->worker_performance,
                  );
                } else {
                  $signed_timesheets[] = array( 
                    'assigned_vacancy_id' => $assigned->id,
                    'assigned_vacancy_details_id' => $assignDetail->id, 
                    'client_name' => $assigned->business_unit->client_name, 
                    'business_unit_name' => $assigned->business_unit_name, 
                    'business_unit_email' => $assigned->business_unit_email, 
                    'business_unit_address' => $assigned->business_unit_address, 
                    'on_date' => $assigned->on_date, 
                    'start_time' => $assigned->start_time, 
                    'end_time' => $assigned->end_time, 
                    'break' => $assigned->break, 
                    'total_hours' => $assigned->total_hours, 
                    'candidate_signature' => '', 
                    'authorised_signature' => '', 
                    'authorised_name' => '', 
                    'authorised_position' => '', 
                    'mileage' => '', 
                    'ref_number' => '', 
                    'no_of_workers' => '', 
                    'driver_service' => '', 
                    'travel_expenses' => '', 
                    'had_induction' => '', 
                    'satisfactory_timekeeping' => '', 
                    'dressed_appropriately' => '', 
                    'team_player' => '',
                    'standards_of_care' => '',
                    'comments' => '', 
                    'mileage_claim' => '',
                    'worker_performance' => '',
                  );
                }
                      
              // }
            }
          }


          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'time_format' => DB::table('agencies')->first()->time_format,
                  'data' => $signed_timesheets
                ]
              ],200);
      }

    }


    //SIGNED TIMESHEETS ON ASSIGNED VACANCY DETAILS ID

    public function get_signed_vacancies_by_id (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'signed_timesheet_id' => $request->signed_timesheet_id,
                        'assigned_vacancy_id' => $request->assigned_vacancy_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'signed_timesheet_id' => 'nullable',
                        'assigned_vacancy_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('signed_timesheet_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('signed_timesheet_id')
                  ]
                ],400);
            }
            else if ($messages->has('assigned_vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assigned_vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
          $signed_timesheet_id = $request->input('signed_timesheet_id');
          $assigned_vacancy_id = $request->input('assigned_vacancy_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

              
          $assigned_shift = AssignVacancy::with('timeclockDetails.details.locations')->find($assigned_vacancy_id);
          $assigned_shift_detail = AssignVacancyDetail::where('assigned_vacancy_id', $assigned_vacancy_id)->first();
          // $assigned_shift = AssignVacancy::find($assigned_vacancy_id);
          // $assigned_shift_detail = AssignVacancyDetail::find($assigned_vacancy_details_id);

          $signed_timesheets = [];

          //Signed timesheet Declaration
          $agencySettings = AgencySetting::where('settings_key', 'signed_timesheet_declaration')->first();

          if($assigned_shift){

            $signed = SignedTimesheet::where('id', $signed_timesheet_id)->orderBy('id', 'desc')->first();

              if($signed){
                $signed_timesheets = array(
                  'id' => $signed->id, 
                  'assigned_vacancy_id' => $signed->assigned_vacancy_id,
                  'assigned_vacancy_details_id' => $signed->assigned_vacancy_details_id, 
                  'client_name' => $assigned_shift->business_unit->client_name, 
                  'business_unit_name' => $assigned_shift->business_unit_name, 
                  'business_unit_email' => $assigned_shift->business_unit_email, 
                  'business_unit_address' => $assigned_shift->business_unit_address, 
                  'on_date' => $assigned_shift->on_date, 
                  'start_time' => $signed->start_time, 
                  'end_time' => $signed->end_time, 
                  'break' => $signed->break, 
                  'total_hours' => $signed->total_hours, 
                  'candidate_signature' => $signed->candidate_signature, 
                  'authorised_signature' => $signed->authorised_signature, 
                  'authorised_name' => $signed->authorised_name, 
                  'authorised_position' => $signed->authorised_position, 
                  'mileage' => $signed->mileage, 
                  'ref_number' => $signed->ref_number, 
                  'no_of_workers' => $signed->no_of_workers, 
                  'driver_service' => $signed->driver_service, 
                  'travel_expenses' => $signed->travel_expenses, 
                  'had_induction' => $signed->had_induction, 
                  'satisfactory_timekeeping' => $signed->satisfactory_timekeeping, 
                  'dressed_appropriately' => $signed->dressed_appropriately, 
                  'team_player' => $signed->team_player,
                  'standards_of_care' => $signed->standards_of_care,
                  'comments' => $signed->comments,
                  'mileage_claim' => $signed->mileage_claim,
                  'worker_performance' => $signed->worker_performance,
                  'signed_timesheet_declaration' => $agencySettings ? $agencySettings->settings_value : NULL,
                  'timeclock_details' => $assigned_shift->timeclockDetails
                );
              } else {
                $signed_timesheets = array( 
                  'assigned_vacancy_id' => $assigned_vacancy_id,
                  'assigned_vacancy_details_id' => $assigned_shift_detail->id, 
                  'client_name' => $assigned_shift->business_unit->client_name, 
                  'business_unit_name' => $assigned_shift->business_unit_name, 
                  'business_unit_email' => $assigned_shift->business_unit_email, 
                  'business_unit_address' => $assigned_shift->business_unit_address, 
                  'on_date' => $assigned_shift->on_date, 
                  'start_time' => $assigned_shift->start_time, 
                  'end_time' => $assigned_shift->end_time, 
                  'break' => $assigned_shift->break, 
                  'total_hours' => $assigned_shift->total_hours, 
                  'candidate_signature' => '', 
                  'authorised_signature' => '', 
                  'authorised_name' => '', 
                  'authorised_position' => '', 
                  'mileage' => '', 
                  'ref_number' => '', 
                  'no_of_workers' => '', 
                  'driver_service' => '', 
                  'travel_expenses' => '', 
                  'had_induction' => '', 
                  'satisfactory_timekeeping' => '', 
                  'dressed_appropriately' => '', 
                  'team_player' => '',
                  'standards_of_care' => '',
                  'comments' => '',
                  'mileage_claim' => '',
                  'worker_performance' => '',
                  'signed_timesheet_declaration' => $agencySettings ? $agencySettings->settings_value : NULL,
                  'timeclock_details' => $assigned_shift->timeclockDetails
                );
              }
          }
              
          $agency = DB::table('agencies')->first();
          //get signed timesheet status for agency app settings
          $agencySettings = DB::table('agency_settings')->where('settings_key', 'timeclock_status')->where('settings_value', 'true')->first(); // fetch agency settings
          if($agencySettings) {
              $settings = json_decode($agencySettings->details, true);
              $signed_timesheet = $settings['signed_timesheet']['status'];
          } else {
              $signed_timesheet = false;
          }

          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success', 
                  'time_format' => $agency->time_format,
                  'time_interval' => $agency->time_interval,
                  'data' => $signed_timesheets,
                  'integrate_signed_timesheet' => $signed_timesheet
                ]
              ],200);
      }

    }


    //UPDATE SIGNED TIMESHEETS

    public function post_signed_vacancies (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'assigned_vacancy_id' => $request->assigned_vacancy_id, 
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'total_hours' => $request->total_hours,
                        'candidate_signature' => $request->candidate_signature,
                        'authorised_signature' => $request->authorised_signature,
                        'authorised_position' => $request->authorised_position,
                        'authorised_name' => $request->authorised_name,
                        'mileage' => $request->mileage, 
                        'ref_number' => $request->ref_number, 
                        'no_of_workers' => $request->no_of_workers, 
                        'driver_service' => $request->driver_service, 
                        'travel_expenses' => $request->travel_expenses, 
                        'had_induction' => $request->had_induction, 
                        'satisfactory_timekeeping' => $request->satisfactory_timekeeping, 
                        'dressed_appropriately' => $request->dressed_appropriately, 
                        'team_player' => $request->team_player,
                        'standards_of_care' => $request->standards_of_care,
                        'comments' => $request->comments,
                        'mileage_claim' => $request->mileage_claim,
                        'worker_performance' => $request->worker_performance,
                        'subdomain' => $request->subdomain
                        ), array(
                        'assigned_vacancy_id' => 'required', 
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'total_hours' => 'required',
                        'candidate_signature' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf|max:2048',
                        'authorised_signature' => 'nullable|mimes:jpeg,png,jpg,gif,svg,pdf|max:2048',
                        'authorised_position' => 'nullable',
                        'authorised_name' => 'required',
                        'mileage' => 'nullable',
                        'ref_number' => 'nullable',
                        'no_of_workers' => 'nullable',
                        'driver_service' => 'nullable',
                        'travel_expenses' => 'nullable',
                        'had_induction' => 'nullable',
                        'satisfactory_timekeeping' => 'nullable',
                        'dressed_appropriately' => 'nullable',
                        'team_player' => 'nullable',
                        'standards_of_care' => 'nullable',
                        'comments' => 'nullable',
                        'mileage_claim' => 'nullable',
                        'worker_performance' => 'nullable',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('assigned_vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assigned_vacancy_id')
                  ]
                ],400);
            }
            // else if ($messages->has('assigned_vacancy_details_id')) {
            //     //Show custom message
            //     return response()->json([
            //       'error'=>[
            //         'status_code' => 400,
            //         'status' => 'Error', 
            //         'message' => $messages->first('assigned_vacancy_details_id')
            //       ]
            //     ],400);
            // }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_hours')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_hours')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_signature')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_signature')
                  ]
                ],400);
            }
            else if ($messages->has('authorised_signature')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('authorised_signature')
                  ]
                ],400);
            }
            else if ($messages->has('authorised_name')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('authorised_name')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $assigned_vacancy_id = $request->assigned_vacancy_id; 
          //getting assigned shift details
          $assign = AssignVacancy::find($assigned_vacancy_id);

          //checking for night shift
          $start = date("H:i", strtotime($request->start_time));
          $end = date("H:i", strtotime($request->end_time));
          $startDate = $assign->on_date;
          if ($start >= $end) {
              $endDate = date('Y-m-d', strtotime('+1 day', strtotime($assign->on_date)));
          } else {
              $endDate = $assign->on_date;
          }
  
          $inputs = array(
              'start_time' => strtotime($startDate .' '.$start),
              'end_time'   => strtotime($endDate .' '.$end),
              'break'      => $request->break ?? 0,
          );
          // $assigned_vacancy_details_id = $request->assigned_vacancy_details_id; 
          $start_time = date( "Y-m-d H:i:s", strtotime($startDate .' '.$start));
          $end_time = date( "Y-m-d H:i:s", strtotime($endDate .' '.$end));
          $break = $request->break;
          $total_hours = calcTotalHours($inputs);
          $candidate_signature = $request->candidate_signature;
          $authorised_signature = $request->authorised_signature;
          $authorised_name = $request->authorised_name;
          $comments = $request->comments;
          $mileage_claim = $request->mileage_claim;
          $worker_performance = $request->worker_performance;
          //validation

          // $assigned_vacancy_id = $request->assigned_vacancy_details_id; 
          $candidate = Candidate::find($assign->candidate_id);
          //checking for candidate access, restrict if candidate has no accesss
          if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
          } 
          $assigned_vacancy_details_id = AssignVacancyDetail::where('assigned_vacancy_id', $assigned_vacancy_id)->first()->id;
          $vacancy = vacancy::find($assign->vacancy_id);
          //checking for approved or invoiced shifts
          if ( $assign && ($assign->status == "approved" || $assign->status == "invoiced") ) { 

              return response()->json([
                'error'=>[
                  'status_code' => 422,
                  'status' => 'Error', 
                  'message' => "This timesheet cannot be processed as the shift has been ".$assign->status." by the agency."
                ]
              ],422);

          }
          //checking for start time
          $currentTime = date("Y-m-d H:i:s");
          if ( strtotime($assign->start_time) > strtotime($currentTime) ) { 

              return response()->json([
                'error'=>[
                  'status_code' => 422,
                  'status' => 'Error', 
                  'message' => "Timesheet cannot be processed as the shift has not yet started."
                ]
              ],422);

          }
          //checking for future shifts
          $todayDate = date("Y-m-d");
          if ( $assign->on_date > $todayDate ) {  

              return response()->json([
                'error'=>[
                  'status_code' => 422,
                  'status' => 'Error', 
                  'message' => "Timesheet processing is not possible because the shift has not yet begun."
                ]
              ],422);

          }
          //image updation 
            $domain              = $request->input('subdomain');

            if ($request->has('candidate_signature') && ($request->candidate_signature != '' || $request->candidate_signature != null) ) {

                $file1                = $request->file('candidate_signature');
                // inserting new image
                $file_name1 = time().$file1->getClientOriginalName(); 

                $image_name1 = $domain.'/'.$file_name1;
                $path1 = Storage::disk('s3')->put(
                            $image_name1, 
                            file_get_contents($request->file('candidate_signature')), 
                           'public');

                $image_url1 = Storage::disk('s3')->url($image_name1);

                $candidate_signature_image = $image_url1;

            }
            if ($request->has('authorised_signature') && ($request->authorised_signature != '' || $request->authorised_signature != null) ) {

                $file2                = $request->file('authorised_signature');
                // inserting new image
                $file_name2 = time().$file2->getClientOriginalName(); 

                $image_name2 = $domain.'/'.$file_name2;
                $path = Storage::disk('s3')->put(
                            $image_name2, 
                            file_get_contents($request->file('authorised_signature')), 
                           'public');

                $image_url2 = Storage::disk('s3')->url($image_name2);

                $authorised_signature_image = $image_url2;

            }

          $signed = SignedTimesheet::where('assigned_vacancy_id', $assigned_vacancy_id)->first();

          if ($signed) {
            //delete the existing image
            if ($signed->candidate_signature != '') {

                $splitUrl1              = explode('/', $signed->candidate_signature);
                $array_reverse1        = array_reverse($splitUrl1);
                $candidate_signature_url       = $array_reverse1[0];

                if(Storage::disk('s3')->exists($domain.'/'.$candidate_signature_url)) {
                    Storage::disk('s3')->delete($domain.'/'.$candidate_signature_url);
                }
            }
            if ($signed->authorised_signature != '') {

                $splitUrl2              = explode('/', $signed->authorised_signature);
                $array_reverse2         = array_reverse($splitUrl2);
                $authorised_signature_url       = $array_reverse2[0];

                if(Storage::disk('s3')->exists($domain.'/'.$authorised_signature_url)) {
                    Storage::disk('s3')->delete($domain.'/'.$authorised_signature_url);
                }
            }
            $signed->assigned_vacancy_id = $assigned_vacancy_id;
            $signed->assigned_vacancy_details_id = $assigned_vacancy_details_id;
            $signed->start_time             = $start_time;
            $signed->end_time               = $end_time;
            $signed->break                  = $request->break;
            // $signed->total_hours            = $request->total_hours ?? 0;
            $signed->total_hours            = $total_hours;
            $signed->candidate_signature    = $candidate_signature_image;
            $signed->authorised_signature   = isset($authorised_signature_image) ? $authorised_signature_image : '';
            $signed->authorised_position    = isset($request->authorised_position) ? $request->authorised_position : '';
            $signed->authorised_name        = $request->authorised_name;
            $signed->comments               = $request->comments;
            $signed->mileage                = !empty($request->mileage) ? $request->mileage : 0.00;
            $signed->ref_number             = !empty($request->ref_number) ? $request->ref_number : '';
            $signed->no_of_workers          = !empty($request->no_of_workers) ? $request->no_of_workers : 0;
            $signed->driver_service         = !empty($request->driver_service) ? $request->driver_service : 'no';
            $signed->travel_expenses        = !empty($request->travel_expenses) ? $request->travel_expenses : 0.00;
            $signed->had_induction          = !empty($request->had_induction) ? $request->had_induction : 'no';
            $signed->satisfactory_timekeeping      = !empty($request->satisfactory_timekeeping) ? $request->satisfactory_timekeeping : 'no';
            $signed->dressed_appropriately         = !empty($request->dressed_appropriately) ? $request->dressed_appropriately : 'no';
            $signed->team_player         = !empty($request->team_player) ? $request->team_player : 'no';
            $signed->standards_of_care         = !empty($request->standards_of_care) ? $request->standards_of_care : 'no';
            if (isset($request->mileage_claim) && !empty($request->mileage_claim)) {
              $signed->mileage_claim = $request->mileage_claim;
            }
            if (isset($request->worker_performance) && !empty($request->worker_performance)) {
              $signed->worker_performance = $request->worker_performance;
            }
            if ($assign->status == "approved") {
              $signed->is_approve = 1;
            }
            $signed->save();

            //updating notification
            $candidate_name  = Candidate::find($assign->candidate_id)->full_name;
            $user_ids        = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
            $user_ids        = array_unique($user_ids);
            $notify_array = array(
                'title' => $candidate_name,
                'type' => "signed_timesheet",
                'date' => date("d-m-Y",strtotime($assign->on_date)),
                'body' => "has updated the timesheet for ".date("d-m-Y",strtotime($assign->on_date)),
                 );
            if ($user_ids) {
                foreach ($user_ids as $user_id) {
                    $notification = new Notification;
                    $notification->type             = 'App\Notifications\HelloNotification';
                    $notification->notifiable_type  = 'App\User';
                    $notification->notifiable_id    = $user_id;
                    $notification->object_id        = $assign->id;
                    $notification->data             = json_encode($notify_array);
                    $notification->resource_ids     = $signed->id;
                    $notification->save();
                }
            }
            //send push notifications to admins and managers
            $player_ids = RoleUser::whereIn('role_id', [2,3,4])->whereNotNull('player_id')->where('player_id', '!=', '')->pluck('player_id')->all();
            $player_ids = array_unique($player_ids);
            $template = $candidate_name." has updated the timesheet for ".date("d-m-Y",strtotime($assign->on_date));
            PushNotificationTrait::oneSignalPushMessage($player_ids, $template, array("data_type" => "signed_timesheet"));

            $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('name', 'email')->first();
            $loggedData = [
              'role'          => 'candidate',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'candidate_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];
            //Activity Log
            $message = 'Signed Timesheet - timesheet has been updated with booking code: ' . $assign->code . ', start time: '. $start .', end time: '.  $end .', break: '.$signed->break. ', total hours: '.$signed->total_hours;
            ActivityCustomLogTrait::saveActivityLog('Signed Timesheets', 'Update', $message, $assign->id, $assign->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json([
                  'success'=>[
                    'status_code' => 201,
                    'status' => 'Success', 
                    'message' => "Timesheet updated successfully."
                  ]
                ],201);

          } else {

            $newSigned = new SignedTimesheet;
            $newSigned->assigned_vacancy_id = $assigned_vacancy_id;
            $newSigned->assigned_vacancy_details_id = $assigned_vacancy_details_id;
            $newSigned->start_time             = $start_time;
            $newSigned->end_time               = $end_time;
            $newSigned->break                  = $request->break;
            // $newSigned->total_hours            = $request->total_hours ?? 0;
            $newSigned->total_hours            = $total_hours;
            $newSigned->candidate_signature    = $candidate_signature_image;
            $newSigned->authorised_signature   = isset($authorised_signature_image) ? $authorised_signature_image : '';
            $newSigned->authorised_position    = isset($request->authorised_position) ? $request->authorised_position : '';
            $newSigned->authorised_name        = $request->authorised_name;
            $newSigned->comments               = $request->comments;
            $newSigned->mileage                = !empty($request->mileage) ? $request->mileage : 0.00;
            $newSigned->ref_number             = !empty($request->ref_number) ? $request->ref_number : '';
            $newSigned->no_of_workers          = !empty($request->no_of_workers) ? $request->no_of_workers : 0;
            $newSigned->driver_service         = !empty($request->driver_service) ? $request->driver_service : 'no';
            $newSigned->travel_expenses        = !empty($request->travel_expenses) ? $request->travel_expenses : 0.00;
            $newSigned->had_induction          = !empty($request->had_induction) ? $request->had_induction : 'no';
            $newSigned->satisfactory_timekeeping      = !empty($request->satisfactory_timekeeping) ? $request->satisfactory_timekeeping : 'no';
            $newSigned->dressed_appropriately         = !empty($request->dressed_appropriately) ? $request->dressed_appropriately : 'no';
            $newSigned->team_player         = !empty($request->team_player) ? $request->team_player : 'no';
            $newSigned->standards_of_care         = !empty($request->standards_of_care) ? $request->standards_of_care : 'no';
            if (isset($request->mileage_claim) && !empty($request->mileage_claim)) {
              $newSigned->mileage_claim = $request->mileage_claim;
            }
            if (isset($request->worker_performance) && !empty($request->worker_performance)) {
              $newSigned->worker_performance = $request->worker_performance;
            }
            if ($assign->status == "approved") {
              $newSigned->is_approve = 1;
            }
            $newSigned->save();

            //updating notification
            $candidate_name  = Candidate::find($assign->candidate_id)->full_name;
            $agency_email    = Agency::first()->email;
            $user_ids        = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
            $user_ids        = array_unique($user_ids);

            $notify_array = array(
                'title' => $candidate_name,
                'type' => "signed_timesheet",
                'date' => date("d-m-Y",strtotime($assign->on_date)),
                'body' => "has submitted the timesheet for ".date("d-m-Y",strtotime($assign->on_date)),
                 );
            if ($user_ids) {
                foreach ($user_ids as $user_id) {
                    $notification = new Notification;
                    $notification->type             = 'App\Notifications\HelloNotification';
                    $notification->notifiable_type  = 'App\User';
                    $notification->notifiable_id    = $user_id;
                    $notification->object_id        = $assign->id;
                    $notification->data             = json_encode($notify_array);
                    $notification->resource_ids     = $newSigned->id;
                    $notification->save();
                }
            }
            //send push notifications to admins and managers
            $player_ids = RoleUser::whereIn('role_id', [2,3,4])->whereNotNull('player_id')->where('player_id', '!=', '')->pluck('player_id')->all();
            $player_ids = array_unique($player_ids);
            $template = $candidate_name." has submitted the timesheet for ".date("d-m-Y",strtotime($assign->on_date));
            PushNotificationTrait::oneSignalPushMessage($player_ids, $template, array("data_type" => "signed_timesheet"));

            $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('name', 'email')->first();
            $loggedData = [
              'role'          => 'candidate',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'candidate_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];
            //Activity Log
            $message = 'Signed Timesheet - timesheet has been submitted with booking code ' . $assign->code . '.';
            ActivityCustomLogTrait::saveActivityLog('Signed Timesheets', 'Update', $message, $assign->id, $assign->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json([
                  'success'=>[
                    'status_code' => 201,
                    'status' => 'Success', 
                    'message' => "Timesheet submitted successfully."
                  ]
                ],201);

          }

      }

    }

    ##############################################################
    #######################ASSIGN SECTION#########################
    ##############################################################
    /**
     * @param Request $request
     * 
     * @return void
     */
    public function assign_multiple_candidates(Request $request){

      $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id, 
                        'candidate_ids' => $request->candidate_ids,
                        'subdomain' => $request->subdomain,
                        'is_publish' => $request->is_publish,
                        'text' => $request->text,
                        'push' => $request->push,
                        'email' => $request->email,
                        ), array(
                        'vacancy_id' => 'required', 
                        'candidate_ids' => 'required',
                        'subdomain' => 'required',
                        'is_publish' => 'nullable|boolean',
                        'text' => 'nullable|boolean',
                        'push' => 'nullable|boolean',
                        'email' => 'nullable|boolean',
                    )
                );

        if ($validator->fails()) {

              $messages = $validator->errors();
              //Determining If Messages Exist For A Field
              if ($messages->has('vacancy_id')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('vacancy_id')
                    ]
                  ],400);
              }
              else if ($messages->has('candidate_ids')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('candidate_ids')
                    ]
                  ],400);
              }
              else if ($messages->has('subdomain')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('subdomain')
                    ]
                  ],400);
              }
              else if ($messages->has('is_publish')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('is_publish')
                    ]
                  ],400);
              }
              else if ($messages->has('text')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('text')
                    ]
                  ],400);
              }
              else if ($messages->has('push')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('push')
                    ]
                  ],400);
              }
              else if ($messages->has('email')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('email')
                    ]
                  ],400);
              }


        } else {

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $candidate_ids = explode(",", $request->candidate_ids);

          $vacancy = Vacancy::find($request->vacancy_id);

          $errResponse = [];
          $sucResponse = [];
          $assignCandidates = [];
          //checking for night shift
          $start = date("H:i", strtotime($vacancy->start_time));
          $end = date("H:i", strtotime($vacancy->end_time));
          $startDate = $vacancy->on_date;
          if ($start >= $end) {
              $endDate = date('Y-m-d', strtotime('+1 day', strtotime($vacancy->on_date)));
          } else {
              $endDate = $vacancy->on_date;
          }
          
          $publishedShifts = [];
          $notify_data = [];
          $bookingCodes = [];

          foreach( $candidate_ids as $candidate_id){

              //Checking before assigning candidate
              $inputArr = array(
                  'candidate_id'      => $candidate_id,
                  'business_unit_id'  => $vacancy->business_unit_id,
                  'job_id'            => $vacancy->job_id,
                  'on_date'           => $vacancy->on_date,
                  'startDate'         => $startDate,
                  'endDate'           => $endDate,
                  'shift_id'          => $vacancy->shift_id,
                  'start_time'        => $start,
                  'end_time'          => $end,
                  'total_hours'       => $vacancy->total_hours,
              );

              $assignValidations = AssignVacancy::assignValidations($inputArr);

              //getting candidate on id
              $candidate = Candidate::where('id', $candidate_id)->first();

              if ($assignValidations != "false") {
                  array_push($errResponse, $assignValidations);
              } else {
                  $request->request->add(['candidate_id' => $candidate_id]);
                  $request->request->add(['status' => 'pending']);
                  $assign = AssignVacancy::assign_candidate($request);
                  if (!$assign) {
                      array_push($errResponse, "Can not assign candidates greater than the vacancy space.");
                  } else {
                    array_push($bookingCodes, $assign->code);
                    array_push($assignCandidates, $candidate->full_name);
                    if ($assign && $assign->status == 'pending' && $assign->is_publish == 1) {

                        Notification::candidate_shift_assign($candidate, $assign, $vacancy);

                        $notification_services['email'] = 0; 
                        $notification_services['text']  = 0; 
                        $notification_services['push']  = 0; 
        
                        // Check if push notification is selected
                        if($request->push == 1){ 
                            $notification_services['push'] = 1; 
                            PushNotificationTrait::shiftAssign($assign, $candidate);
                        }
                        // Check if text notification is selected
                        if($request->text == 1){ 
                            $notification_services['text']  = 1; 
                            SMSTraits::vacancyAssign($assign, $candidate, $request->subdomain);
                        }
                        // Check if text notification is selected
                        if($request->email == 1){ 
                            $candidateEmail = RoleUser::where('role_id', 6)->where('user_id', $candidate->user_id)->first()->email;
                            $notification_services['email']  = 1;
                            $assign_mail_variables  = [
                                '[candidate_name]'  => $assign->candidate_name,
                                '[client]'          => $assign->vacancy->client,
                                '[business_unit]'   => $assign->business_unit,
                                '[address]'         => $assign->vacancy->address_line,
                                '[date]'            => $assign->on_date,
                                '[shift_start]'     => $assign->start_time,
                                '[shift_end]'       => $assign->end_time,
                                '[detail]'          => $assign->notes, 
                                '[booking_code]'    => $assign->code,
                                '[agency_name]'     => $assign->vacancy->agency,
                            ];
                            $agency = Agency::first();
                            $mail_content                 = EmailTraits::getMessegeConetent($assign_mail_variables,'Assign Vacancy');
                            $mail_content['agency_email'] = $agency->email;
                            $mail_content['agency_name']  = $agency->name;
                            $mail_content['agency_logo']  = $agency->agency_logo;
                            
                            // dispatch(new PublishAssignedShiftsJob($mail_content));
                        } 
                    //     array_push($publishedShifts, $assign);

                    //     $notify = Notification::AddPublishedShiftNotication($assign);

                    //     if($notify){
                    //         array_push($notify_data, $notify);
                    //     }
                    }
                  }
      
                  // array_push($sucResponse, $candidate->full_name.'  Successfully  assigned.');
              }
          }

          if ($assignCandidates) {
            array_push($sucResponse, implode(", ",$assignCandidates).'  successfully  assigned');
          }

          if(count($bookingCodes) > 0){
            $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
            $loggedData = [
                'role'          => 'manager',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'manager_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
            ];
            //Activity Log
            $message = 'New booking(s) '. (count($bookingCodes) > 1 ? ' have ' : ' has '). 'been created with code(s) ' . implode(', ', $bookingCodes) . '.';
            ActivityCustomLogTrait::saveActivityLog('Bookings', 'Create', $message, '', $bookingCodes, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
          }

          return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $errResponse
              ],
              'success'=>[
                'status_code' => 201,
                'status' => 'Success', 
                'message' => $sucResponse
              ],
            ]);
        }

    }

    ##############################################################
    #######################ACCEPT APPLIED CANDIDATES SECTION######
    ##############################################################
    /**
     * @param Request $request
     * 
     * @return void
     */
    public function accept_applied_candidates(Request $request){

      $validator = Validator::make(
                        array(
                        'assign_ids' => $request->assign_ids,
                        'subdomain' => $request->subdomain,
                        'is_publish' => $request->is_publish,
                        'text' => $request->text,
                        'push' => $request->push,
                        'email' => $request->email,
                        ), array(
                        'assign_ids' => 'required',
                        'subdomain' => 'required',
                        'is_publish' => 'nullable|boolean',
                        'text' => 'nullable|boolean',
                        'push' => 'nullable|boolean',
                        'email' => 'nullable|boolean',
                    )
                );

        if ($validator->fails()) {

              $messages = $validator->errors();
              //Determining If Messages Exist For A Field
              if ($messages->has('assign_ids')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('assign_ids')
                    ]
                  ],400);
              }
              else if ($messages->has('subdomain')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('subdomain')
                    ]
                  ],400);
              }
              else if ($messages->has('is_publish')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('is_publish')
                    ]
                  ],400);
              }
              else if ($messages->has('text')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('text')
                    ]
                  ],400);
              }
              else if ($messages->has('push')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('push')
                    ]
                  ],400);
              }
              else if ($messages->has('email')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('email')
                    ]
                  ],400);
              }


        } else {

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $assignCandidates = [];
          $errResponse = [];
          $sucResponse = [];
          $assign_ids = explode(',', $request->assign_ids);

          if (count($assign_ids) > 0) {
            $assignVacancyDetails = AssignVacancy::find($assign_ids[0]);
            if ($assignVacancyDetails) {
              $vacancy = Vacancy::find($assignVacancyDetails->vacancy_id);
              if ($vacancy) {
                if ($vacancy->space_left < count($assign_ids) ) {
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => "Cannot assign candidates greater than the space !"
                    ]
                  ],400);
                }
              }
            }
          }
          $bookingCodes = [];
          /* changing pending timesheet to approved */
          foreach ($assign_ids as $assign_vacancy_id) {

              $assignVacancy = AssignVacancy::find($assign_vacancy_id);
              if ($assignVacancy->status == "applied") {
                $vacancy = Vacancy::find($assignVacancy->vacancy_id);
                $vacancyDetails = DB::table('vacancies')->find($assignVacancy->vacancy_id);

                //Checking before assigning candidate
                $inputArr = array(
                    'candidate_id'      => $assignVacancy->candidate_id,
                    'business_unit_id'  => $vacancy->business_unit_id,
                    'job_id'            => $vacancy->job_id,
                    'on_date'           => $vacancy->on_date,
                    'startDate'         => date("Y-m-d", strtotime($vacancyDetails->start_time)),
                    'endDate'           => date("Y-m-d", strtotime($vacancyDetails->end_time)),
                    'shift_id'          => $vacancy->shift_id,
                    'start_time'        => $vacancy->start_time,
                    'end_time'          => $vacancy->end_time,
                    'total_hours'       => $vacancy->total_hours,
                );
                $assignValidations = AssignVacancy::assignValidations($inputArr);
                
                if ($assignValidations != "false") {
                    array_push($errResponse, $assignValidations);
                } else {
                    array_push($bookingCodes, $assignVacancy->code);
                    //getting candidate on id
                    $candidate = Candidate::where('id', $assignVacancy->candidate_id)->first();
                    //updating assign vacancy
                    $assignVacancy->status = "pending";
                    $assignVacancy->created_by = Auth::id();
                    $assignVacancy->is_publish = isset($request->is_publish) ? $request->is_publish : $assignVacancy->is_publish;

                    if ($assignVacancy->save()){
                      //finding latest applied count
                      $latest_applied_count = DB::table('assign_vacancies')->where('vacancy_id', $assignVacancy->vacancy_id)->where('is_deleted', 0)->where('status', 'applied')->count('id');
                      //finding latest assigned count
                      $latest_used_count = DB::table('assign_vacancies')->where('vacancy_id', $assignVacancy->vacancy_id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');
                      //reduce vacancy by one
                      $vacancy->used_count = $latest_used_count;
                      $vacancy->applied_count = $latest_applied_count;
                      $vacancy->save();

                      array_push($assignCandidates, $assignVacancy->candidate_name);

                      if ($assignVacancy && $assignVacancy->status == 'pending' && $assignVacancy->is_publish == 1) {

                          $notification_services['email'] = 0; 
                          $notification_services['text']  = 0; 
                          $notification_services['push']  = 0; 
          
                          // Check if push notification is selected
                          if($request->push == 1){ 
                              $notification_services['push'] = 1; 
                              PushNotificationTrait::shiftAssign($assignVacancy, $candidate);
                          }
                          // Check if text notification is selected
                          if($request->text == 1){ 
                              $notification_services['text']  = 1; 
                              SMSTraits::vacancyAssign($assignVacancy, $candidate, $request->subdomain);
                          }
                      }
                    }
                }
              }
          }

          if ($assignCandidates) {
            array_push($sucResponse, implode(", ",$assignCandidates).'  successfully  assigned');
          }
          
          if(count($bookingCodes) > 0 && count($assignCandidates) > 0){
            $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
            $loggedData = [
                'role'          => 'manager',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'manager_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
            ];
            //Activity Log
            $message = 'Bookings - '. implode(", ", $assignCandidates).(count($assignCandidates) > 1 ? ' have ' : ' has '). 'been assigned with code(s) ' . implode(', ', $bookingCodes) . '.';
            ActivityCustomLogTrait::saveActivityLog('Bookings', 'Update', $message, '', $assignCandidates, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
          }

          return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $errResponse
              ],
              'success'=>[
                'status_code' => 201,
                'status' => 'Success', 
                'message' => $sucResponse
              ],
            ]);
        }

    }
    ##########################################################
    ####################UPDATE SHIFT##########################
    ##########################################################
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \App\Models\AssignVacancy $assignVacancy
     *
     * @return \Illuminate\Http\Response
     */
    public function update_or_reassign_shift(Request $request)
    {

      $validator = Validator::make(
                        array(
                        'assign_id' => $request->assign_id, 
                        'reassign_candidate_id' => $request->reassign_candidate_id, 
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_hours' => $request->total_hours,
                        'subdomain' => $request->subdomain,
                        'is_publish' => $request->is_publish,
                        'text' => $request->text,
                        'push' => $request->push,
                        'email' => $request->email,
                        ), array(
                        'assign_id' => 'required|numeric',
                        'reassign_candidate_id' => 'nullable|numeric',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable|numeric',
                        'notes' => 'nullable',
                        'total_hours' => 'required',
                        'subdomain' => 'required',
                        'is_publish' => 'nullable|boolean',
                        'text' => 'nullable|boolean',
                        'push' => 'nullable|boolean',
                        'email' => 'nullable|boolean',
                    )
                );

        if ($validator->fails()) {

              $messages = $validator->errors();
              //Determining If Messages Exist For A Field
              if ($messages->has('assign_id')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('assign_id')
                    ]
                  ],400);
              }
              else if ($messages->has('reassign_candidate_id')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('reassign_candidate_id')
                    ]
                  ],400);
              }
              else if ($messages->has('start_time')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('start_time')
                    ]
                  ],400);
              }
              else if ($messages->has('end_time')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('end_time')
                    ]
                  ],400);
              }
              else if ($messages->has('break')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('break')
                    ]
                  ],400);
              }
              else if ($messages->has('notes')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('notes')
                    ]
                  ],400);
              }
              else if ($messages->has('total_hours')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('total_hours')
                    ]
                  ],400);
              }
              else if ($messages->has('subdomain')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('subdomain')
                    ]
                  ],400);
              }


        } else {

          // return response()->json([
          //         'error'=>[
          //           'status_code' => 400,
          //           'status' => 'Error', 
          //           'message' => "Temporarily not available !"
          //         ]
          //       ], 400);

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');
          
          $assign = AssignVacancy::find($request->assign_id);
          $vacancy = Vacancy::find($assign->vacancy_id);
          $request->request->add(['vacancy_id' => $assign->vacancy_id]);

          $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
          $loggedData = [
              'role'          => 'manager',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'manager_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
          ];
          ############Reassign candidate#########
          if ($request->reassign_candidate_id) {

            //getting candidate on id
            $candidate = Candidate::where('id', $request->reassign_candidate_id)->first();
            $request->request->add(['candidate_id' => $request->reassign_candidate_id]);
            $request->request->add(['on_date' => $assign->on_date]);
            $request->request->add(['status' => "pending"]);

            if ($assign->status == "approved" || $assign->status == "invoiced") {
              return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => "Can not reassign an approved shift."
                  ]
                ], 400);
            } 

            if ($assign->candidate_id == $request->reassign_candidate_id) {
              return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => "Candidates should not be same for reassigning shift !"
                  ]
                ], 400);
            } 

            if ($candidate->candidate_status_id != 1) {
              return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => "Please select an active candidate."
                  ]
                ], 400);
            } 
            //checking for night shift
            $start = date("H:i", strtotime($request->start_time));
            $end = date("H:i", strtotime($request->end_time));
            $startDate = $request->on_date;
            if ($start >= $end) {
                $endDate = date('Y-m-d', strtotime('+1 day', strtotime($request->on_date)));
            } else {
                $endDate = $request->on_date;
            }

            //Checking before assigning candidate
            $inputArr = array(
                'candidate_id'      => $request->reassign_candidate_id,
                'business_unit_id'  => $vacancy->business_unit_id,
                'job_id'            => $vacancy->job_id,
                'on_date'           => $request->on_date,
                'startDate'         => $startDate,
                'endDate'           => $endDate,
                'shift_id'          => $vacancy->shift_id,
                'start_time'        => $request->start_time,
                'end_time'          => $request->end_time,
                'total_hours'       => isset($request->total_hours) ? $request->total_hours : $vacancy->total_hours,
            );
            $assignValidations = AssignVacancy::assignValidations($inputArr);
            
            if ($assignValidations != "false") {
              return response()->json([
                  'error'=>[
                    'status_code' => 422,
                    'status' => 'Error', 
                    'message' => $assignValidations
                  ]
                ], 422);
                // return response()->json(['errors' => ['exist' => [$assignValidations]]], 422);
            } else {
                //Delete the shift
                $assignVacancy = AssignVacancy::find($request->assign_id);
                $assignVacancy->is_deleted = 1;
                $assignVacancy->deleted_by = Auth::id();
                $assignVacancy->deleted_at = Carbon::now();
                $assignVacancy->reason_to_delete = "Deleted assign shift and reassigned to another candidate'";
                //move to open shift
                $assignVacancy->is_moved_to_vacancies = 1;
                $assignVacancy->save();
                // Update the vacancy table total_numbers
                $vacancy->used_count = $vacancy->used_count - 1;
                $vacancy->save();
                
                //reassign the shift
                $assign = AssignVacancy::assign_candidate($request);

                if (!$assign) {
                  return response()->json([
                    'error'=>[
                      'status_code' => 422,
                      'status' => 'Error', 
                      'message' => "Cannot assign candidates greater than the vacancy space."
                    ]
                  ], 422);
                    // return response()->json(['errors' => ['exist' => ["Cannot assign candidates greater than the vacancy space."]]], 422);
                } else {

                    if ($assign->status == 'pending' && $assign->is_publish == 1) {
                      $notification_services['email'] = 0; 
                      $notification_services['text']  = 0; 
                      $notification_services['push']  = 0; 

                      // Check if push notification is selected
                      if($request->push == 1){ 
                          $notification_services['push'] = 1; 
                          PushNotificationTrait::shiftAssign($assign, $candidate);
                      }
                      // Check if text notification is selected
                      if($request->text == 1){ 
                          $notification_services['text']  = 1; 
                          SMSTraits::vacancyAssign($assign, $candidate, $request->subdomain);
                      }
                    }
                    $candidate_name = DB::table('candidates')->where('id', $assign->candidate_id)->pluck('full_name')->first();
                    //Activity Log
                    $message = 'Booking - reassigned the candidate: '. $candidate_name .' with booking code '.$assign->code .'.';
                    ActivityCustomLogTrait::saveActivityLog('Bookings', 'Create', $message, $assign->id, $assign->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
                    
                    return response()->json([
                      'success'=>[
                        'status_code' => 201,
                        'status' => 'success', 
                        'message' => "shift reassigned successfully"
                      ]
                    ],201);
                }
            }
          } else {
            //getting assigned shift on id
            $assign = AssignVacancy::find($request->assign_id);
            //getting staff rate on employment id
            $candidate = Candidate::where('id', $assign->candidate_id)->first();
            //checking for night shift
            $vacancy = Vacancy::find($assign->vacancy_id);
            $start = date("H:i", strtotime($request->start_time));
            $end = date("H:i", strtotime($request->end_time));
            $daycheck = date('a', strtotime($request->end_time));
            if ($start >= $end) {
                $endDate = date('Y-m-d', strtotime('+1 day', strtotime($vacancy->on_date)));
            } else {
                $endDate = date('Y-m-d', strtotime($vacancy->on_date));
            }//calculate total hours
            $inputs = array(
                'on_date'    => $vacancy->on_date,
                'start_time' => $request->start_time,
                'end_time'   => $request->end_time,
                'break'      => $request->break,
            );
            $totalHours = Helper::getTotalHours($inputs);

            // $assign->candidate_id = $request->candidate_id;
            // $assign->status = $request->status;
            if (isset($request->is_publish)) {
              if ($assign->is_publish != 1) {
                $assign->is_publish = $request->is_publish;
              }
            }
            $assign->created_by = Auth::id();
            $assign->notes = $request->notes;
            $assign->start_time       = date( "Y-m-d H:i:s", strtotime($vacancy->on_date. ' '.$request->start_time));
            $assign->end_time         = date( "Y-m-d H:i:s", strtotime($endDate. ' '.$request->end_time) );
            if (isset($request->break)) {
                $assign->break = $request->break;
            }
            $assign->total_hours      = $totalHours;
            $oldShift = AssignVacancy::find($request->assign_id);
            
            //updating for Holiday Pay if anyone of the start , end time or break is not equal
            if ( $request->break != $oldShift->break || $assign->total_hours != $oldShift->total_hours ) {
                //getting holiday pay by business unit id and job id from vacancy
                $holidayPay = getHolidayPayByBuAndJob($assign);
                if($holidayPay){
                    $assign->holiday_pay_percentage = $holidayPay->holiday_pay_percentage;
                    $assign->holiday_pay_rate       = $holidayPay->holiday_pay_amount;
                    $assign->holiday_pay_frequency  = $holidayPay->holiday_pay_frequency;
                    //checking frequency
                    if ($holidayPay->holiday_pay_frequency == "flat") {
                        $assign->holiday_pay_amount     = $holidayPay->holiday_pay_amount;
                    } else {
                        $assign->holiday_pay_amount     = $assign->total_hours * $holidayPay->holiday_pay_amount;
                    }
                    if ($oldShift->holiday_pay_status == "paid") {
                        $assign->holiday_pay_status     = 'amended';
                    } else if ($oldShift->holiday_pay_status == "amended") {
                        $assign->holiday_pay_status     = 'amended';
                    } else {
                        $assign->holiday_pay_status     = 'due';
                    }
                    $assign->holiday_pay_updated_at = date("Y-m-d H:i:s");
                }
            }
            $assign->save();
            //getting new ratecards or vacancy details
            $rateCards = [];
            if ( $assign->start_time != $oldShift->start_time || $assign->end_time != $oldShift->end_time ) {

                $request->request->add(['on_date' => date('Y-m-d', strtotime($vacancy->on_date))]);
                $request->request->add(['business_unit_id' => $vacancy->business_unit_id]);
                $request->request->add(['job_id' => $vacancy->job_id]);
                $request->request->add(['shift_id' => $vacancy->shift_id]);
                
                $rateCards = splitTimeRangeIntoSlots($request);

                if (count($rateCards)>0) {
                    //delete entries on vacancy id if exists
                    $detailIDs = AssignVacancyDetail::where('assigned_vacancy_id', $assign->id)->pluck('id')->all();
                    if ($detailIDs) AssignVacancyDetail::destroy($detailIDs);
                }
                $previousValue = null;
                foreach ($rateCards as $rateCard) {
                    $staff_rate = '';
                    if( $rateCard['staff_rates'] ) {
                        foreach($rateCard['staff_rates'] as $staffRate) {
                            if( $staffRate->employment_type_id == $candidate->employment_types_id ){
                                $staff_rate = $staffRate->staff_rate;
                                break;
                            }
                        }
                    } 
                    $staff_rate_amount = $staff_rate != '' ? $staff_rate : 0;
                    $candidate_staff_rate = 0;
                    if ($rateCard['day_id'] != 8) {
                        $candidateRateCards = CandidateRateCard::where('business_unit_id', $vacancy->business_unit_id)
                                    ->where('job_id', $vacancy->job_id)
                                    ->where('day_id', $rateCard['day_id'])
                                    ->where('shift_id', $rateCard['shift_id'])
                                    // ->where('start_time', '<', date( "H:i:s", strtotime($rateCard['end_time'])) )
                                    // ->where('end_time', '>', date( "H:i:s", strtotime($rateCard['start_time'])) )
                                    ->where('employment_type_id', $candidate->employment_types_id)
                                    ->where('candidate_id', $candidate->id)
                                    ->get();  
                        $candidate_rate = '';
                        if (count($candidateRateCards)>0) {
                            foreach ($candidateRateCards as $rate_card) {
                                $start = date("H:i", strtotime($rate_card->start_time));
                                $end = date("H:i", strtotime($rate_card->end_time));
                                // $date = date('Y-m-d', strtotime($vacancy->on_date));
                                $week = date("W", strtotime($vacancy->on_date));
                                if($previousValue) {
                                  $week = $previousValue>$rate_card->day_id ? $week+1 : $week;
                                }
                                $year = date("Y", strtotime($vacancy->on_date));
                                $date = date('Y-m-d', strtotime($year."W".$week.$rate_card->day_id));
                                if ($start >= $end) {
                                    $startDate = $date;
                                    $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                                }
                                else {
                                    $startDate = $date;
                                    $endDate = $date;
                                }
                                if ( strtotime($startDate.' '.$rate_card->start_time) < strtotime($rateCard['end_time']) && strtotime($rateCard['start_time']) < strtotime($endDate.' '.$rate_card->end_time) ) {
                                    $candidate_rate = $rate_card;
                                }
                            } 
                        }
                        $candidate_staff_rate = $candidate_rate == '' ? 0 : $candidate_rate->staff_rate;
                        $previousValue = $rateCard['day_id'];
                    } 
                    //check whether the rate is flat or hourly
                    if ($rateCard['rate_type'] == "flat_rate") {
                        $client_pay_amount   = !empty($rateCard['client_rate']) ? $rateCard['client_rate'] : 0;
                        $staff_pay_amount    = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
                    } else {
                        $client_pay_amount   = ($rateCard['total_hours']) * (!empty($rateCard['client_rate']) ? $rateCard['client_rate'] : 0);
                        $staff_pay_amount    = ($rateCard['total_hours']) * (($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount);
                    }
        
                    $assign_details = new AssignVacancyDetail();
                    $assign_details->assigned_vacancy_id = $assign->id;
                    $assign_details->start_time          = date( "Y-m-d H:i:s", strtotime($rateCard['start_time'])) ?? null;
                    $assign_details->end_time            = date( "Y-m-d H:i:s", strtotime($rateCard['end_time'])) ?? null;
                    $assign_details->total_hours         = $rateCard['total_hours'];
                    $assign_details->rate_type           = $rateCard['rate_type'];
                    $assign_details->client_rate         = !empty($rateCard['client_rate']) ? $rateCard['client_rate'] : 0;
                    $assign_details->staff_rate          = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
                    $assign_details->client_pay_amount   = $client_pay_amount;
                    $assign_details->staff_pay_amount    = $staff_pay_amount;
                    $assign_details->break               = $rateCard['break'];
                    $assign_details->save();
                }
                //updating assigned detail id
                $assignedDetail = AssignVacancyDetail::where('assigned_vacancy_id', $assign->id)->first();
                if ($assignedDetail) {
                    SignedTimesheet::where('assigned_vacancy_id', $assign->id)->update(['assigned_vacancy_details_id' => $assignedDetail->id]);
                }
            } else {
                if ( $oldShift->break != $request->break) {
                    $rateCards = AssignVacancyDetail::where('assigned_vacancy_id', $assign->id)->orderBy('id', 'asc')->get();
                    $max = '';
                    $index = '';
                    if (count($rateCards)>0) {
                        $hoursArr = [];
                        foreach($rateCards as $rateCard) {
                            $hoursArr[$rateCard->id] = $rateCard->total_hours;
                        }
                        $max = max($hoursArr);
                        $index = array_search($max, $hoursArr);
                        foreach ($rateCards as $detail) {
                            if ($index == $detail->id) {
                                if ( $detail->break != $request->break) {
                                    $assign_details = AssignVacancyDetail::find($detail->id);
                                    $assign_details_data = DB::table('assigned_vacancy_details')->where('id', $detail->id)->first();
                                    //calculate total hours
                                    $inputArr = array(
                                        'start_time' => strtotime($assign_details_data->start_time),
                                        'end_time'   => strtotime($assign_details_data->end_time) ,
                                        'break'      => $request->break,
                                    );
                                    $assign_details->total_hours         = calcTotalHours($inputArr) ?? 0;
                                    $rateType = $assign_details->rate_type != NULL ? $assign_details->rate_type : $vacancy->rate_type;
                                    if ($rateType == "hourly_rate") {
                                        $assign_details->client_pay_amount   = $assign_details->total_hours * $assign_details->client_rate;
                                        $assign_details->staff_pay_amount    = $assign_details->total_hours * $assign_details->staff_rate;
                                    } else {
                                        $assign_details->client_pay_amount   = $assign_details->client_rate;
                                        $assign_details->staff_pay_amount    = $assign_details->staff_rate;
                                    }
                                    $assign_details->break               = $request->break;
                                    $assign_details->save();
                                }
                            }
                        }
                    }
                }
            }

            $logArray = [];
            if(strcmp($assign->start_time, $oldShift->start_time) != 0){
              array_push($logArray, ['key' => 'start time', 'old_value' => date( "H:i:s", strtotime($oldShift->start_time)), 'new_value' => date( "H:i:s", strtotime($assign->start_time))]);
            }
            if(strcmp($assign->end_time, $oldShift->end_time) != 0){
              array_push($logArray, ['key' => 'end time', 'old_value' => date( "H:i:s", strtotime($oldShift->end_time)), 'new_value' => date( "H:i:s", strtotime($assign->end_time))]);
            }
            if(strcmp($assign->break, $oldShift->break) != 0){
              array_push($logArray, ['key' => 'break', 'old_value' => $oldShift->break.' min', 'new_value' => $assign->break.' min']);
            }
            if(strcmp($assign->total_hours, $oldShift->total_hours) != 0){
              array_push($logArray, ['key' => 'total hours', 'old_value' => $oldShift->total_hours. ' hrs', 'new_value' => $assign->total_hours.' hrs']);
            }
            if(strcmp($assign->notes, $oldShift->notes) != 0){
              array_push($logArray, ['key' => 'notes', 'old_value' => $oldShift->notes, 'new_value' => $assign->notes]);
            }

            //Activity Log
            foreach($logArray as $value){
              $message = 'Booking - ' . $assign->code."'s " .$value['key'] .' has been updated from '. ($value['old_value'] ? $value['old_value'] : 'null') .' to '. ($value['new_value'] ? $value['new_value'] : 'null'). '.';
              ActivityCustomLogTrait::saveActivityLog('Bookings', 'Update', $message, $assign->id, $assign->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
            }

            return response()->json([
              'success'=>[
                'status_code' => 201,
                'status' => 'success', 
                'message' => "shift details updated successfully"
              ]
            ],201);
          }
          ###################################################################
          // //getting assigned shift on id
          // $assign = AssignVacancy::find($request->assign_id);
          // //checking for night shift
          // $vacancy = Vacancy::find($assign->vacancy_id);
          // $vacancyDetails = DB::table('vacancies')->find($assign->vacancy_id);
          // $start = date("H:i", strtotime($request->start_time));
          // $end = date("H:i", strtotime($request->end_time));
          // if ($start >= $end) {
          //     $endDate = date('Y-m-d', strtotime('+1 day', strtotime($vacancyDetails->on_date)));
          // } else {
          //     $endDate = $vacancyDetails->on_date;
          // }
          // //getting staff rate on employment id
          // $candidate = Candidate::where('id', $assign->candidate_id)->first();
          // $vacancy_staff_rate = VacancyStaffRate::where('employment_type_id', $candidate->employment_types_id)
          //                 ->where('vacancy_id', $assign->vacancy_id)
          //                 ->first();
          // ############Reassign candidate#########
          // if ($request->reassign_candidate_id) {

          //   if ($assign->status == "approved" || $assign->status == "invoiced") {
          //     return response()->json([
          //         'error'=>[
          //           'status_code' => 400,
          //           'status' => 'Error', 
          //           'message' => "Can not reassign an approved shift."
          //         ]
          //       ], 400);
          //   } 

          //   if ($assign->candidate_id == $request->reassign_candidate_id) {
          //     return response()->json([
          //         'error'=>[
          //           'status_code' => 400,
          //           'status' => 'Error', 
          //           'message' => "Candidates should not be same for reassigning shift !"
          //         ]
          //       ], 400);
          //   } 

          //   if ($candidate->candidate_status_id != 1) {
          //     return response()->json([
          //         'error'=>[
          //           'status_code' => 400,
          //           'status' => 'Error', 
          //           'message' => "Please select an active candidate."
          //         ]
          //       ], 400);
          //   } 
          //   //Checking before assigning the new candidate
          //   $inputArr = array(
          //       'candidate_id'      => $request->reassign_candidate_id,
          //       'business_unit_id'  => $vacancyDetails->business_unit_id,
          //       'job_id'            => $vacancyDetails->job_id,
          //       'on_date'           => $vacancyDetails->on_date,
          //       'startDate'         => $vacancyDetails->on_date,
          //       'endDate'           => $endDate,
          //       'shift_id'          => $vacancyDetails->shift_id,
          //       'start_time'        => $start,
          //       'end_time'          => $end,
          //       'total_hours'       => $request->total_hours,
          //   );

          //   $assignValidations = AssignVacancy::assignValidations($inputArr);

          //   if ($assignValidations != "false") {
          //     return response()->json([
          //         'error'=>[
          //           'status_code' => 400,
          //           'status' => 'Error', 
          //           'message' => $assignValidations
          //         ]
          //       ], 400);
          //   } else {
          //     $assign->candidate_id = $request->reassign_candidate_id;
          //     $assign->status = 'pending';
          //     // $assign->is_publish = isset($request->is_publish) ? $request->is_publish : 0;
          //   }
          //   #################################
          //   $assign->created_by = Auth::id();
          //   $assign->notes = $request->notes;
          //   $assign->save();
          // }
          // //getting staff rate on employment id
          // $candidate = Candidate::where('id', $assign->candidate_id)->first();
          // $vacancy_staff_rate = VacancyStaffRate::where('employment_type_id', $candidate->employment_types_id)
          //                 ->where('vacancy_id', $assign->vacancy_id)
          //                 ->first();
          // $staff_rate_amount = (!$vacancy_staff_rate) ? 0 : $vacancy_staff_rate->staff_rate;                
          // //getting Individual staff rate
          // $day_id = Day::where('day', date('l',strtotime($vacancyDetails->on_date)))->first()->id;   
          // //#check for custom holiday
          // $custom_holiday = CustomHoliday::where('business_unit_id', $vacancyDetails->business_unit_id)->where('holiday_date', $vacancyDetails->on_date)->where('is_deleted',0)->first();
          // //#check for holiday
          // $holiday = Holiday::where('on_date', $vacancyDetails->on_date)->where('is_deleted',0)->first();  
          // $candidate_staff_rate = 0;
          // //checking for holidays, if yes then taking holiday rates. No , then taking candidate rates
          // if (!$custom_holiday && !$holiday) {  
          //   $candidate_rate = CandidateRateCard::where('business_unit_id', $vacancyDetails->business_unit_id)
          //                       ->where('job_id', $vacancyDetails->job_id)
          //                       ->where('day_id', $day_id)
          //                       ->where('shift_id', $vacancyDetails->shift_id)
          //                       ->where('employment_type_id', $candidate->employment_types_id)
          //                       ->where('candidate_id', $assign->candidate_id)
          //                       ->first();  
          //   $candidate_staff_rate = (!$candidate_rate) ? 0 : $candidate_rate->staff_rate;
          // }
          // //calculate total hours
          // $inputs = array(
          //     'on_date'    => $vacancyDetails->on_date,
          //     'start_time' => $request->start_time,
          //     'end_time'   => $request->end_time,
          //     'break'      => $request->break,
          // );
          // $totalHours = Helper::getTotalHours($inputs);
          // //check whether the rate is flat or hourly
          // if ($vacancyDetails->rate_type == "flat_rate") {
          //   $client_pay_amount   = !empty($vacancyDetails->client_rate) ? $vacancyDetails->client_rate : 0;
          //   $staff_pay_amount    = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
          // } else {
          //   $client_pay_amount   = ($totalHours) * (!empty($vacancyDetails->client_rate) ? $vacancyDetails->client_rate : 0);
          //   $staff_pay_amount    = ($totalHours) * (($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount);
          // }
          // //updating details
          // $assign_details = AssignVacancyDetail::where('assigned_vacancy_id', $request->assign_id)->first();
          // $assign_details->start_time        = isset($request->start_time) ? date('Y-m-d', strtotime($vacancyDetails->on_date)).' '.date('H:i:s', strtotime($request->start_time)) : null;
          // $assign_details->end_time          = isset($request->end_time) ? date('Y-m-d', strtotime($endDate)).' '.date('H:i:s', strtotime($request->end_time)) : null;
          // $assign_details->total_hours       = $totalHours ?? 0;
          // $assign_details->break             = isset($request->break) ? $request->break : 0;
          // $assign_details->client_rate       = $vacancyDetails->client_rate ?? 0;
          // $assign_details->staff_rate        = $vacancy_staff_rate ? $vacancy_staff_rate->staff_rate : 0;
          // $assign_details->client_pay_amount = $client_pay_amount;
          // $assign_details->staff_pay_amount  = $staff_pay_amount;
          // $assign_details->save();
          // //Updating Candidate notification
          // // $this->notifyCandidate(Candidate::find($request->candidate_id), $assign, $vacancy);
          // //check for publishing shifts
          // if ( $assign->is_publish == 1) {

          //     $notification_services['email'] = 0; 
          //     $notification_services['text']  = 0; 
          //     $notification_services['push']  = 0; 

          //     // Check if push notification is selected
          //     if($request->push == 1){ 
          //         $notification_services['push'] = 1; 
          //         PushNotificationTrait::shiftAssign($assign, $candidate);
          //     }
          //     // Check if text notification is selected
          //     if($request->text == 1){ 
          //         $notification_services['text']  = 1; 
          //         SMSTraits::vacancyAssign($assign, $candidate, $request->subdomain);
          //     }
          //     // Check if text notification is selected
          //     if($request->email == 1){ 
          //         $candidateEmail = RoleUser::where('role_id', 6)->where('user_id', $candidate->user_id)->first()->email;
          //         $notification_services['email']  = 1;
          //         $assign_mail_variables  = [
          //             '[candidate_name]'  => $assign->candidate_name,
          //             '[client]'          => $assign->vacancy->client,
          //             '[business_unit]'   => $assign->business_unit,
          //             '[address]'         => $assign->vacancy->address_line,
          //             '[date]'            => $assign->on_date,
          //             '[shift_start]'     => $assign->start_time,
          //             '[shift_end]'       => $assign->end_time,
          //             '[detail]'          => $assign->notes, 
          //             '[booking_code]'    => $assign->code,
          //             '[agency_name]'     => $assign->vacancy->agency,
          //         ];
          //         $agency = Agency::first();
          //         $mail_content                 = EmailTraits::getMessegeConetent($assign_mail_variables,'Assign Vacancy');
          //         $mail_content['agency_email'] = $agency->email;
          //         $mail_content['agency_name']  = $agency->name;
          //         $mail_content['agency_logo']  = $agency->agency_logo;
                  
          //         // dispatch(new PublishAssignedShiftsJob($mail_content));
          //     } 
          // }
          
          // if ($request->reassign_candidate_id) {
          //   return response()->json([
          //     'success'=>[
          //       'status_code' => 201,
          //       'status' => 'success', 
          //       'message' => "shift reassigned successfully"
          //     ]
          //   ],201);
          // } else {
          //   return response()->json([
          //     'success'=>[
          //       'status_code' => 201,
          //       'status' => 'success', 
          //       'message' => "shift details updated successfully"
          //     ]
          //   ],201);
          // }
        }
    }
    
    ##########################################################
    ####################DELETE SHIFT##########################
    ##########################################################
    /**
     * delete the specified resource in storage.
     *
     * @param \Illuminate\Http\Request  $request
     * @param \App\Models\AssignVacancy $assignVacancy
     *
     * @return \Illuminate\Http\Response
     */

    public function delete_shifts(Request $request)
    {

      $validator = Validator::make(
                        array(
                        'assign_ids' => $request->assign_ids, 
                        'reason_type' => $request->reason_type,
                        'reason' => $request->reason,
                        'subdomain' => $request->subdomain,
                        'is_publish' => $request->is_publish,
                        'text' => $request->text,
                        'push' => $request->push,
                        'email' => $request->email,
                        ), array(
                        'assign_ids' => 'required',
                        'reason_type' => 'required|numeric',
                        'reason' => 'required',
                        'subdomain' => 'required',
                        'is_publish' => 'nullable|boolean',
                        'text' => 'nullable|boolean',
                        'push' => 'nullable|boolean',
                        'email' => 'nullable|boolean',
                    )
                );

        if ($validator->fails()) {

              $messages = $validator->errors();
              //Determining If Messages Exist For A Field
              if ($messages->has('assign_ids')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('assign_ids')
                    ]
                  ],400);
              }
              else if ($messages->has('reason_type')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('reason_type')
                    ]
                  ],400);
              }
              else if ($messages->has('reason')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('reason')
                    ]
                  ],400);
              }
              else if ($messages->has('subdomain')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('subdomain')
                    ]
                  ],400);
              }


        } else {

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $reason_type = $request->has('reason_type') ? $request->reason_type : 0;
          $subdomain = $request->input('subdomain');
          $assign_ids = explode(',', $request->assign_ids);
          /* deleting timesheet */
          foreach ($assign_ids as $assign_vacancy_id) {
              $assignVacancy = AssignVacancy::find($assign_vacancy_id);
              $assignVacancy->is_deleted = 1;
              $assignVacancy->deleted_by = Auth::id();
              $assignVacancy->deleted_at = date("Y-m-d H:i:s");
              $assignVacancy->reason_to_delete = $request->reason;
              if ($reason_type == 1) {
                  $assignVacancy->is_moved_to_vacancies = 1;
              }
              $assignVacancy->save();
              //move to open shift
              $vacancy = Vacancy::find($assignVacancy->vacancy_id);
              if ($reason_type == 1) {
                  // Update the vacancy table total_numbers
                  $latest_used_count = DB::table('assign_vacancies')->where('vacancy_id', $assignVacancy->vacancy_id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');
                  $vacancy->used_count = $latest_used_count;
              }
              //update vacancy
              $latest_applied_count = DB::table('assign_vacancies')->where('vacancy_id', $assignVacancy->vacancy_id)->where('is_deleted', 0)->where('status', 'applied')->count('id');
              $vacancy->applied_count = $latest_applied_count;
              $vacancy->save();

              $signedTimesheet = SignedTimesheet::where('assigned_vacancy_id', $assignVacancy->id)->first();
              if($signedTimesheet){
                  $signedTimesheet->is_deleted   = 1;
                  $signedTimesheet->deleted_at   = Carbon::now();
                  $signedTimesheet->save();
              }

              $currentUrl = $request->url();
              $baseUrl = url('/');
              $relativeUrl = str_replace($baseUrl, '', $currentUrl);
              $url = explode('/', $relativeUrl);
              
              if(isset($url[2]) && $url[2] == 'agency'){
                $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
                $loggedData = [
                  'role'          => 'manager',
                  'tenant'        => $request->input('subdomain'),
                  'dashboardType' => 'manager_app',
                  'user_name'     => $user->name,
                  'email'         => $user->email,
                ];
                //Activity Log
                $message = 'Booking - '. 'shift has been deleted '. ($reason_type == 1 ? 'and moved to open shift ' : '').'with booking code ' . $assignVacancy->code . '.';
                ActivityCustomLogTrait::saveActivityLog('Bookings', 'Update', $message, $assignVacancy->id, $assignVacancy->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
              }

              if ( $assignVacancy->is_publish == 1 && $request->is_publish == 1) {
                  //Updating Candidate notification
                  // $todayDate = date("Y-m-d");
                  // if($vacancy->on_date >= $todayDate){
                  //     $candidate = Candidate::find($assignVacancy->candidate_id);
                  //     Notification::candidate_shift_cancel($candidate, $assignVacancy, $vacancy);
                  // }
                  $candidate = Candidate::find($assignVacancy->candidate_id);
                  Notification::candidate_shift_cancel($candidate, $assignVacancy, $vacancy);

                  $notification_services['email'] = 0; 
                  $notification_services['text']  = 0; 
                  $notification_services['push']  = 0; 
                  /* Check if push notification is selected  */
                  if($request->push == 1){ 
                      $notification_services['push'] = 1; 
                      PushNotificationTrait::shiftCancel($assignVacancy);
                  }
                  /* Check if text notification is selected  */
                  if($request->text == 1){ 
                      $notification_services['text']  = 1; 
                      SMSTraits::vacancyAssignRemove($assignVacancy, $subdomain);
                  }
                  /* Check if text notification is selected  */
                  if($request->email == 1){ 
                      $notification_services['email']  = 1;
                  }

                  return response()->json([
                    'success'=>[
                      'status_code' => 201,
                      'status' => 'success', 
                      'message' => "shift deleted and notified successfully"
                    ]
                  ],201);
                  
              } else {

                  return response()->json([
                    'success'=>[
                      'status_code' => 201,
                      'status' => 'success', 
                      'message' => "shift deleted successfully"
                    ]
                  ],201);

              }
          }
        }
    }
    
}
