<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\AgencyNotificationSetting;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;

class AgencyNotificationSettingController extends BaseController
{

  public function getMobileNotifications (Request $request) 
  {

    $validator = Validator::make(
      array(
        'subdomain' => $request->subdomain
      ), array(
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {

      config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
      DB::reconnect('mysql');

      $agencysettings = AgencyNotificationSetting::all();

      $agencyMobileNotifications = [
          "candidate_availability_web_alerts" => 0,
          "candidate_apply_job_web_alerts" => 0,
          "candidate_confirm_shift_web_alerts" => 0,
          "candidate_signing_timesheet_web_alerts" => 0,
          "candidate_reject_job_web_alerts" => 0,
          "candidate_delete_account_alerts" => 0,
      ];

      if (count($agencysettings)>0) {
          foreach ($agencysettings as $agencysetting) {
              if ($agencysetting->settings_key == "candidate_availability_web_alerts") {
                  $agencyMobileNotifications['candidate_availability_web_alerts'] = $agencysetting->settings_value;
              }
              if ($agencysetting->settings_key == "candidate_apply_job_web_alerts") {
                  $agencyMobileNotifications['candidate_apply_job_web_alerts'] = $agencysetting->settings_value;
              }
              if ($agencysetting->settings_key == "candidate_confirm_shift_web_alerts") {
                  $agencyMobileNotifications['candidate_confirm_shift_web_alerts'] = $agencysetting->settings_value;
              }
              if ($agencysetting->settings_key == "candidate_signing_timesheet_web_alerts") {
                  $agencyMobileNotifications['candidate_signing_timesheet_web_alerts'] = $agencysetting->settings_value;
              }
              if ($agencysetting->settings_key == "candidate_reject_job_web_alerts") {
                $agencyMobileNotifications['candidate_reject_job_web_alerts'] = $agencysetting->settings_value;
              }
              if ($agencysetting->settings_key == "candidate_delete_account_alerts") {
                  $agencyMobileNotifications['candidate_delete_account_alerts'] = $agencysetting->settings_value;
              }
          }
      }

      return response()->json([
        'success'=>[
          'status_code' => 200,
          'status' => 'Success', 
          'data' => $agencyMobileNotifications
        ]
      ],200);
    }
    
  }
  
}
