<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\AppManagement;
use App\Tenant;
use App\User;
use App\UserTenant;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;

class AppController extends BaseController
{


  public function add_app_version (Request $request) 
  {

    $validator = Validator::make(
      array(
        'platform' => $request->platform,
        'version' => $request->version,
        'force_update' => $request->force_update,
        'feature' => $request->feature,
        'subdomain' => $request->subdomain
      ), array(
        'platform' => 'required',
        'version' => 'required',
        'force_update' => 'required|boolean',
        'feature' => 'required',
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('platform')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('platform')
          ]
        ],400);
      }
      else if ($messages->has('version')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('version')
          ]
        ],400);
      }
      else if ($messages->has('force_update')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('force_update')
          ]
        ],400);
      }
      else if ($messages->has('feature')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('feature')
          ]
        ],400);
      }
      else if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {
        $tenant_id = Tenant::where('slug', $request->subdomain)->first()->id;

        if ($tenant_id) {

            AppManagement::where('tenant_id', $tenant_id)
                        ->where('app_type', 'candidate')
                        ->where('platform', $request->platform)
                        ->update(['is_current' => 0]);

            $AppManagement = new AppManagement;
            $AppManagement->tenant_id = $tenant_id;
            $AppManagement->app_type = 'candidate';
            $AppManagement->platform = $request->platform;
            $AppManagement->version = $request->version;
            $AppManagement->is_current = 1;
            $AppManagement->force_update = $request->force_update;
            $AppManagement->feature = $request->feature;
            $AppManagement->save();

            return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success', 
                  'data' => "Version added successfully"
                ]
              ],200);

        } else {
          return response()->json([
            'error'=>[
              'status_code' => 400,
              'status' => 'Error', 
              'message' => "No agency found !"
            ]
          ],400);
        }
    }
    
  }
  
  public function get_app_version (Request $request) 
  {

    $validator = Validator::make(
      array(
        'platform' => $request->platform,
        'subdomain' => $request->subdomain
      ), array(
        'platform' => 'required',
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('platform')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('platform')
          ]
        ],400);
      }
      else if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {

      $tenant_id = Tenant::where('slug', $request->subdomain)->first()->id;

      if ($tenant_id) {

        $AppManagement = AppManagement::where('tenant_id', $tenant_id)->where('platform', $request->platform)->where('is_current', 1)->orderBy('id', 'desc')->first();

        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'data' => $AppManagement
          ]
        ],200);

      } else {

        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => "No versions found !"
          ]
        ],400);

      }
    }
    
  }
  
}
