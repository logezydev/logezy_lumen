<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\BusinessUnit;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\VacancyStaffRate;
use App\Tenants\Vacancy;
use App\Tenants\AssignVacancy;
use App\Tenants\AssignVacancyDetail;
use App\Tenants\RoleUser;
use App\Tenants\RoleUserDetail;
use App\Tenants\Helper;
use App\Tenants\Agency;
use App\Tenants\SignedTimesheet;
use App\Tenants\Day;
use App\Tenants\Job;
use App\Tenants\ClientJob;
use App\Tenants\Shift;
use App\Tenants\CandidateRateCard;
use App\Tenants\CandidateAvailability;
use App\Tenants\CandidateRestrictedBusinessUnit;
use App\User;
use App\Tenants\Notification;
use App\Traits\PushNotificationTrait;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Storage;

class HelperController extends Controller
{
    /**
     * Gets the candidate statuses.
     *
     * @return     <type>  The businesses.
     */
    public function getCandidateStatuses(Request $request) {

        $validator = Validator::make(
                        array(
                        'subdomain' => $request->subdomain
                        ), array(
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $candidate_statuses = DB::table('candidate_statuses')->where('is_deleted',0)->orderBy('id', 'ASC')
            ->get();
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $candidate_statuses
              ]
            ],200);
        }
    }

    /**
     * Gets the times.
     *
     * @return     <type>  The time array.
     */
    public function getTimes(Request $request) {

        $validator = Validator::make(
                        array(
                        'subdomain' => $request->subdomain
                        ), array(
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $timeArr = [];
            $agency = DB::table('agencies')->first();
            $time_interval = $agency->time_interval;
            if ($agency->time_format == 24) {

                for ($i=0;$i<=1440;$i = $i + $time_interval) {
                    $hours = floor($i / 60);
                    $minutes = ($i % 60);
                    $timeArr[] = date("H:i", strtotime($hours.':'.$minutes) );
                }

            } else {

                for ($i=0;$i<=1440;$i = $i + $time_interval) {
                    $hours = floor($i / 60);
                    $minutes = ($i % 60);
                    $timeArr[] = date("h:i A", strtotime($hours.':'.$minutes) );
                }

            }
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $timeArr
              ]
            ],200);
        }
    }
    /**
     * Gets the BusinessesForClients.
     *
     * @return     <type>  The businesses.
     */
    public function getBusinessesForClients(Request $request) {

        $validator = Validator::make(
                        array(
                        'role_user_id' => $request->role_user_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'role_user_id' => 'required',
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('role_user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('role_user_id')
                  ]
                ],400);
            } else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            //getting business units from role user id
            $roleUser = RoleUser::where('id', $request->role_user_id)->where('status', 'active')->where('is_deleted', 0)->whereIn('role_id',[5,7])->first();
            $business_units = [];
            if ($roleUser) {
              if ($roleUser->role_id == 7) {
                $role_users_details = RoleUserDetail::where('role_user_id', $roleUser->id)->first();
                if ($role_users_details && $role_users_details->business_unit_ids) {
                    $business_units = explode(",", $role_users_details->business_unit_ids);
                } else {
                  $client = DB::table('clients')->where('user_id', $roleUser->user_id)->pluck('id');
                  $business_units = DB::table('business_units')->where('client_id', $client)->where('status', 'active')->where('is_deleted', 0)->pluck('id')->all();
                }
              } else {
                $business_units = DB::table('business_units')->where('user_id', $roleUser->user_id)->pluck('id')->all();
              }
            }

            $businessUnits = BusinessUnit::whereIn('id',$business_units)->where('status','active')->where('is_deleted',0)->orderBy('business_unit_name', 'ASC')->get();
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $businessUnits
              ]
            ],200);
        }
    }
    /**
     * Gets the businesses.
     *
     * @return     <type>  The businesses.
     */
    public function getBusinesses(Request $request) {

        $validator = Validator::make(
                        array(
                        'region_id' => $request->region_id,
                        'business_unit_ids' => $request->business_unit_ids,
                        'subdomain' => $request->subdomain
                        ), array(
                        'region_id' => 'nullable',
                        'business_unit_ids' => 'nullable',
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('region_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('region_id')
                  ]
                ],400);
            } else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $business_units = [];
            if(isset($request->region_id)) {
                $buIDs = DB::select("SELECT `business_unit_id` from `businessunit_regions` where `region_id` = ".$request->region_id."");
                $business_units = count($buIDs)>0 ? array_column($buIDs, 'business_unit_id') : [];
            }
            $business_unit_ids = isset($request->business_unit_ids) ? explode(",", $request->business_unit_ids) : [];

            // $businessUnits = BusinessUnit::where('status','active')
            $businessUnits = DB::table('business_units')
            ->join('clients', 'clients.id', '=', 'business_units.client_id')
            ->where(function($query) use ($request, $business_units, $business_unit_ids) {
              if (isset($request->region_id)) {
                $query->whereIn('business_units.id', $business_units);
              }
              if ($business_unit_ids) {
                $query->whereIn('business_units.id', $business_unit_ids);
              }
            })
            ->where('business_units.is_deleted', 0)->where('business_units.status', 'active')
            ->orderBy('business_units.business_unit_name', 'ASC')
            ->select(
                'business_units.id', 
                'clients.client_name', 
                'business_units.business_unit_name', 
                'business_units.client_id', 
                'business_units.break'
            )->get();
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $businessUnits
              ]
            ],200);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getShiftsByBusinessunit(Request $request){

        $validator = Validator::make(
                        array(
                        'business_unit_id' => $request->business_unit_id, 
                        'subdomain' => $request->subdomain
                        ), array(
                        'business_unit_id' => 'required', 
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('business_unit_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');
            /* fetching shifts in json format */
            $shifts = Shift::where('business_unit_id',$request->business_unit_id)
                    ->where('status','active')->where('is_deleted',0)
                    ->whereIn('shift_name',['Day', 'Night'])
                    ->select('id','shift_name', DB::raw('TIME_FORMAT(start_time, "'.timeFormat().'") as start_time'), DB::raw('TIME_FORMAT(end_time, "'.timeFormat().'") as end_time') )
                    ->orderBy('id', 'ASC')
                    ->get();

            $agency = DB::table('agencies')->first();
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'time_format' => $agency->time_format,
                'time_interval' => $agency->time_interval,
                'data' => $shifts
              ]
            ],200);
        }
        
    }

    /**
     * Get all Jobs belong to given client ser id
     *
     * @param      \Illuminate\Http\Request  $request  The request
     */
    public function getClientJobs(Request $request) {

        $validator = Validator::make(
                        array(
                        'business_unit_id' => $request->business_unit_id, 
                        'subdomain' => $request->subdomain
                        ), array(
                        'business_unit_id' => 'required', 
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('business_unit_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $client_id = BusinessUnit::find($request->business_unit_id)->client_id;
            
            $job_ids = ClientJob::where('client_id', $client_id)->where('is_deleted', 0)->where('status', 'active')->pluck('job_id')->all();
            
            $withoutIndexJob_ids = array_values($job_ids);

            $jobs = Job::whereIn('id', $withoutIndexJob_ids)->where('is_deleted', 0)->where('status', 'active')->orderBy('name', 'ASC')->get();
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $jobs
              ]
            ],200);
        }
    }

    /**
     * Get all Bu belong to given job id
     *
     * @param      \Illuminate\Http\Request  $request  The request
     */
    public function getBuByJobId(Request $request) {

        $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id, 
                        'job_id' => $request->job_id, 
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'nullable', 
                        'job_id' => 'required', 
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('job_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $restriceted_business_units = [];
            if ($request->candidate_id != ''){
                $restriceted_business_units = CandidateRestrictedBusinessUnit::where('candidate_id', $request->candidate_id)->pluck('business_unit_id')->all();
            }

            $Client_ids = DB::table('client_jobs')->where('job_id', $request->job_id)->where('is_deleted', 0)->where('status', 'active')->pluck('client_id')->all();
            
            $withoutIndexClient_ids = array_values($Client_ids);

            $businessUnits = DB::table('business_units')
            ->join('clients', 'clients.id', '=', 'business_units.client_id')
            ->whereRaw("business_units.client_id IN ('".implode("','",$withoutIndexClient_ids)."')")
            ->whereRaw("business_units.id NOT IN ('".implode("','",$restriceted_business_units)."')")
            ->where('business_units.is_deleted', 0)->where('business_units.status', 'active')
            ->orderBy('business_units.business_unit_name', 'ASC')
            ->get([
              'business_units.id', 
              'clients.client_name',
              'business_units.business_unit_name', 
              'business_units.break'
            ]);
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $businessUnits
              ]
            ],200);
        }
    }

    /**
     * Get all Jobs belong to given user id
     *
     * @param      \Illuminate\Http\Request  $request  The request
     */
    public function get_candidate_jobs(Request $request) {
        $validator = Validator::make(
                array(
                'candidate_id' => $request->candidate_id, 
                'subdomain' => $request->subdomain
                ), array(
                'candidate_id' => 'required', 
                'subdomain' => 'required'
            )
        );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $job_ids = DB::table('candidate_jobs')->where('candidate_id', $request->candidate_id)->where('is_deleted', 0)->where('status', 'active')->pluck('job_id')->all();
            $withoutIndexJob_ids = array_unique(array_values($job_ids));
            $jobs = DB::table('jobs')->whereRaw("id IN ('".implode("','",$withoutIndexJob_ids)."')")->where('status','active')->where('is_deleted',0)->orderBy('name', 'ASC')->get();
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $jobs
              ]
            ],200);
        }
    }

    /**
     * Get all Bu belong to given job id
     *
     * @param      \Illuminate\Http\Request  $request  The request
     */
    public function getCandidatesByStatus(Request $request) {

        $validator = Validator::make(
                        array(
                        'subdomain' => $request->subdomain,
                        'candidate_status_id' => $request->candidate_status_id,
                        'search' => $request->search
                        ), array(
                        'subdomain' => 'required',
                        'candidate_status_id' => 'nullable|integer',
                        'search' => 'nullable'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_status_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_status_id')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $jobs = [];
            $name = '';
            $candidateIds = [];
            $candidates = [];
            $candidate_status_id = isset($request->candidate_status_id) ? $request->candidate_status_id : '';

            $canArr = DB::table('candidates')->where('candidates.is_deleted',0)
                    ->where(function($query) use ($request) {
                        if ($request->candidate_status_id) {
                            $query->where('candidates.candidate_status_id', $request->candidate_status_id);
                        }
                        $query->where('candidates.candidate_status_id', '!=', 6);
                        $query->where('candidates.candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'));
                    })->where(function($query) use($request) {
                      if(!empty($request->search)) {
                          // $query->whereLike(['client_name','formatted_number','email','ref_code','address_line','mobile'],$request->search);
                          $query->where('candidates.full_name', 'like', "%{$request->search}%")->orWhere('candidates.place', 'like', "%{$request->search}%")->orWhere('candidates.post_code', 'like', "%{$request->search}%")->orWhere('candidates.candidate_code', 'like', "%{$request->search}%")->orWhere('candidates.formatted_mobile_number', 'like', "%{$request->search}%");
                      }
                  })->orderBy('candidates.first_name')->paginate(20);

            if (count($canArr) > 0) {
                foreach ($canArr as $can) {
                    $job_ids = DB::table('candidate_jobs')->where('candidate_id', $can->id)->pluck('job_id')->all();
                    $job_names = DB::table('jobs')->whereIn('id', $job_ids)->pluck('name')->all();
                    $candidates[] = array(
                        'id' => $can->id, 
                        'full_name' => $can->full_name, 
                        'weekly_hours' => $can->weekly_hours, 
                        'profile_img' => $can->profile_img, 
                        'job_ids' => $job_ids ? implode(',', $job_ids) : '', 
                        'job_names' => $job_names ? implode(',', $job_names) : '', 
                    );
                }
            }

            // $candidates = DB::table('candidates')
            //             ->join('candidate_jobs', function($join) use ($jobs)
            //             {
            //                 $join->on('candidates.id', '=', 'candidate_jobs.candidate_id');
            //                 if ($jobs) {
            //                     $join->whereIn('candidate_jobs.job_id', $jobs);
            //                 }
            //             })
            //             ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
            //             ->where(function($query) use ($name, $candidateIds)
            //             {
            //                 if ($name) {
            //                     $query->where('candidates.full_name', 'like',  '%' . $name .'%')->get();
            //                 }
            //                 if ($candidateIds) {
            //                     $query->whereIn('candidates.id', $candidateIds)->get();
            //                 }
            //             })->where('candidates.is_deleted',0)->where('candidates.candidate_status_id', $candidate_status_id)
            //             ->select('candidates.id','candidates.full_name', 'candidates.weekly_hours', 'candidates.profile_img', DB::raw("GROUP_CONCAT(jobs.id SEPARATOR ',') as `job_ids`"), DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
            //             ->orderBy('candidates.first_name')
            //             ->groupBy('candidates.id')
            //             ->get();
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $candidates
              ]
            ],200);
        }
    }


    /**
     * Get all candidates to given job id
     *
     * @param      \Illuminate\Http\Request  $request  The request
     */
    public function getCandidatesByJob(Request $request) {

        $validator = Validator::make(
                        array(
                        'job_id' => $request->job_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'job_id' => 'required',
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('job_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_id')
                  ]
                ],400);
            }
            //Determining If Messages Exist For A Field
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $jobs = [];
            $name = '';
            $candidateIds = DB::table('candidate_jobs')->where('job_id', $request->job_id)->pluck('candidate_id')->all() ?? [];

            $candidates = DB::table('candidates')
                        ->whereIn('candidates.id', $candidateIds)
                        ->where('candidates.is_deleted',0)->where('candidates.candidate_status_id', 1)
                        ->select('candidates.id','candidates.full_name', 'candidates.profile_img')
                        ->orderBy('candidates.first_name')
                        ->get();
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $candidates
              ]
            ],200);
        }
    }

    /**
     * calculate_total_hours
     * @param  Request $request [description]
     * @return [float]           [description]
     */
    public function calculateTotalHours(Request $request){

        $validator = Validator::make(
                        array(
                        'on_date' => $request->on_date, 
                        'start_time' => $request->start_time, 
                        'end_time' => $request->end_time, 
                        'subdomain' => $request->subdomain
                        ), array(
                        'on_date' => 'required', 
                        'start_time' => 'required', 
                        'end_time' => 'required', 
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('on_date')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('on_date')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $input = $request->all();
           
            $totalHours = Helper::getTotalHours($input);
            
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $totalHours
              ]
            ],200);
        }
    }



    /**
     * candidates_for_publish_vacancy
     */
    public function candidates_for_publish_vacancy (Request $request) {
        $validator = Validator::make(
                        array(
                        'business_unit_id' => $request->business_unit_id,
                        'job_id' => $request->job_id,
                        'shift_id' => $request->shift_id,
                        'on_date' => $request->on_date,
                        'vacancy_id' => $request->vacancy_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'business_unit_id' => 'required',
                        'job_id' => 'required',
                        'shift_id' => 'required',
                        'on_date' => 'required',
                        'vacancy_id' => 'required',
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('business_unit_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            //Determining If Messages Exist For A Field
            else if ($messages->has('job_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_id')
                  ]
                ],400);
            }
            //Determining If Messages Exist For A Field
            else if ($messages->has('shift_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('shift_id')
                  ]
                ],400);
            }
            //Determining If Messages Exist For A Field
            else if ($messages->has('on_date')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('on_date')
                  ]
                ],400);
            }
            //Determining If Messages Exist For A Field
            else if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            //Determining If Messages Exist For A Field
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');
            $input = array(
                'business_unit_id' => $request->business_unit_id,
                'job_id' => $request->job_id,
                'shift_id' => $request->shift_id,
                'on_date' => $request->on_date,
                'id' => $request->vacancy_id,
            );

            $candidates = Helper::getCandidatesForVacancyPublish($input);

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $candidates
              ]
            ],200);
        }
    }



    /**
     * candidates_for_publish_vacancy
     */
    public function sendOneSignalNotification (Request $request) {
        $playersId = array(
            '9c57870e-220b-482a-b317-5560f2279374'
        );
        $message = "Testing one sognal push notification";
        $additionalData = array(
            'foo' => 'bar'
        );
        $response = PushNotificationTrait::oneSignalPushMessage($playersId, $message, $additionalData);

        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'data' => $response
          ]
        ],200);
    }

}
