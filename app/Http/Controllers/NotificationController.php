<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\Vacancy;
use App\Tenants\AssignVacancy;
use App\Tenants\Notification;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;

class NotificationController extends BaseController
{
    public function get_notifications (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $candidate = Candidate::find($candidate_id);
          //delete notifications prior to 30 days
          DB::table('notifications')->where('role_id', 6)->where('notifiable_id', $candidate->user_id)->where( 'created_at', '<', date('Y-m-d 00:00:00', strtotime("-30 days")))
             ->delete();
          $notifications = Notification::where('role_id', 6)->where('notifiable_id', $candidate->user_id)->orderBy('created_at', 'desc')->paginate(30)->toArray();

          $notifyArr = [];

          if (count($notifications['data']) > 0) {
            $collection = collect($notifications['data']);
            $notifyArr = $collection->map(function ($row) {
                        $notifyData = json_decode($row['data']);
                        $array = [
                          'id'=> $row['id'],
                          'object_id'=> $row['object_id'],
                          'object_date'=> $notifyData->date ?? '',
                          'notification_type' => $notifyData->type,
                          'text'=> $notifyData->body,
                          'read_at' => $row['read_at'] ? date("d-m-Y h:i A",strtotime($row['read_at'])) : '',
                          'created_at' => $row['created_at'] ? date("d-m-Y h:i A",strtotime($row['created_at'])) : '',
                        ];
                        return $array;
                      });
            $notifications['data'] = $notifyArr;
          }

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'notifications' => $notifications
              ]
            ],200);

      }
    }

    public function getAgencyNotifications (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'user_id' => $request->user_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'user_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('user_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          //delete notifications prior to 30 days
          DB::table('notifications')->where('notifiable_id', $request->user_id)->where( 'created_at', '<', date('Y-m-d 00:00:00', strtotime("-30 days")))
             ->delete();

          $notifications = Notification::where('role_id', NULL)->where('notifiable_id', $request->user_id)->orderBy('created_at', 'desc')->paginate(30)->toArray();

          $notifyArr = [];

          if (count($notifications['data']) > 0) {
            $collection = collect($notifications['data']);
            $notifyArr = $collection->map(function ($row) {
                        $notifyData = json_decode($row['data']);
                        if ($notifyData != null){
                          $array = [
                            'id'=> $row['id'],
                            'object_id'=> $row['object_id'],
                            'object_date'=> $notifyData->date ?? '',
                            'notification_type' => $notifyData->type ?? '',
                            'text'=> $notifyData->title.' '.$notifyData->body ,
                            'read_at' => $row['read_at'] ? date("d-m-Y h:i A",strtotime($row['read_at'])) : '',
                            'created_at' => $row['created_at'] ? date("d-m-Y h:i A",strtotime($row['created_at'])) : '',
                          ];
                          return $array;
                        } else {
                          $array = [
                            'id'=> $row['id'],
                            'object_id'=> $row['object_id'],
                            'object_date'=> NULL,
                            'notification_type' => NULL,
                            'text'=> NULL,
                            'read_at' => $row['read_at'] ? date("d-m-Y h:i A",strtotime($row['read_at'])) : '',
                            'created_at' => $row['created_at'] ? date("d-m-Y h:i A",strtotime($row['created_at'])) : '',
                          ];
                          return $array;

                        }
                      });
            $notifications['data'] = $notifyArr;
          }

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'notifications' => $notifications
              ]
            ],200);

      }
      
    }

    public function view_notification (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'notification_id' => $request->notification_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'notification_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('notification_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('notification_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $notification_id = $request->input('notification_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $Viewed = DB::table('notifications')
              ->where('id', $notification_id)
              ->update(['read_at' => Carbon::now()]);

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => "Viewed"
              ]
            ],200);

      }  
      
    }

    public function delete_notification (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'notification_id' => $request->notification_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'notification_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('notification_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('notification_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $notification_id = $request->input('notification_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $deleted = DB::table('notifications')
              ->where('id', $notification_id)
              ->delete();

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => "Deleted"
              ]
            ],200);

      }  
      
    }

    public function delete_all_notifications (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'user_id' => $request->user_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'user_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('user_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $user_id = $request->input('user_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $deleted = DB::table('notifications')
              ->where('notifiable_id', $user_id)
              ->where('role_id', 6)
              ->delete();

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => "Deleted all notifications"
              ]
            ],200);

      }  
      
    }

    public function delete_all_notifications_agency (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'user_id' => $request->user_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'user_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('user_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $user_id = $request->input('user_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $deleted = DB::table('notifications')
              ->where('notifiable_id', $user_id)
              ->delete();

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => "Deleted all notifications"
              ]
            ],200);

      }  
      
    }
    
}
