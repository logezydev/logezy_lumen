<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Scheduler;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;

class ScheduleController extends BaseController
{

  /**
   * Gets the schedule slots.
   *
   * @return     <type>  The schedule.
   */
  public function fetchScheduleSlots(Request $request) {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to, 
                        'subdomain' => $request->subdomain
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required', 
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                    //Show custom message
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => $messages->first('subdomain')
                      ]
                    ],400);
            }


      } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $candidate_assignedShifts = Scheduler::fetchAssignedVacancies($request);

        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'time_format' => DB::table('agencies')->first()->time_format,
            'data' => $candidate_assignedShifts
          ]
        ],200);
      }
  }

  /**
   * Gets Bookings
   *
   * @return     <type>  The schedule.
   */
  public function getBookingsFromToday(Request $request) {

      $validator = Validator::make(
                        array(
                        'subdomain' => $request->subdomain
                        ), array(
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $candidate_assignedShifts = Scheduler::getBookings($request);

        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'time_format' => DB::table('agencies')->first()->time_format,
            'data' => $candidate_assignedShifts
          ]
        ],200);
      }
  }


}
