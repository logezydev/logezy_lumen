<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\UserToken;
use App\Tenant;
use App\UserTenant;
use App\OtpVerification;
use App\Tenants\Role;
use App\Tenants\RoleUser;
use App\Tenants\RoleUserDetail;
use App\Tenants\Agency;
use App\Tenants\Candidate;
use App\Tenants\Notification;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Carbon\Carbon;
use App\Jobs\OtpSendingJob;
use App\Helpers\SendMailGun;
use App\Jobs\NewCandidateRegisterJob;
use Illuminate\Database\DatabaseManager;
use App\Mail\SendPasswordReset;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Traits\ActivityLog\ActivityCustomLogTrait;

class UserController extends BaseController
{
    /**
     * authenticate.
     *
     * @param mixed $request
     */
    public function authenticate(Request $request)
    {
        // $this->validate($request, 
        //   [
        //     'email' => 'required', 
        //     'password' => 'required',
        //     'subdomain' => 'required'
        //   ]);

        $validator = Validator::make(
                        array(
                        'email' => $request->email, 
                        'password' => $request->password,
                        'subdomain' => $request->subdomain
                        ), array(
                        'email' => 'required|email', 
                        'password' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
            else if ($messages->has('password')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('password')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            if (Hash::check($request->input('password'), User::where('email', $request->input('email'))->pluck('password')->first())) {

              $tenant = Tenant::where('slug', $request->input('subdomain'))->first();

              $token = base64_encode(Str::random(40));
              $user = User::where('email', $request->input('email'))->where('status', 'active')->first();

              if($token && $user){

                $user_tenant = UserTenant::where('user_id', $user->id)->where('tenant_id', $tenant->id)->first();

                if($user_tenant){
                    //setting token
                    // $user->update(['remember_token' => $token]);
                    $userToken = UserToken::where('user_id', $user->id)->where('tenant_role_id', 6)->where('tenant_id', $tenant->id)->first();
                    if ($userToken) {
                      $userToken->update(['remember_token' => $token]);
                    } else {
                      $userTokenObj = new UserToken;
                      $userTokenObj->user_id = $user->id;
                      $userTokenObj->tenant_role_id = 6;
                      $userTokenObj->tenant_id = $tenant->id;
                      $userTokenObj->remember_token = $token;
                      $userTokenObj->save();
                    }
                  
                    //reconnecting database
                    config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
                    DB::reconnect('mysql');
                    $candidate = Candidate::where('user_id', $user->id)->with('jobs')->with('employment_type')->first();

                    if ($candidate) {

                      if ( $candidate->is_deleted == 1 ) {
                          return response()->json([
                              'error'=>[
                                'status_code' => 400,
                                'status' => 'Error', 
                                'message' => "There are no active records on this email address and password"
                              ]
                            ],400);
                      } else if ( $candidate->has_access == 0 ) {
                          return response()->json([
                              'error'=>[
                                'status_code' => 400,
                                'status' => 'Error', 
                                'message' => "You have no access to use this account. Please contact the agency."
                              ]
                            ],400);
                      } else {

                          $loggedData = [
                            'role'          => 'candidate',
                            'tenant'        => $request->input('subdomain'),
                            'dashboardType' => 'candidate_app',
                            'user_name'     => $user->name,
                            'email'         => $user->email,
                          ];

                          //Activity Log
                          $message = 'Candidate - '.$candidate->full_name .' has been logged in.';
                          ActivityCustomLogTrait::saveActivityLog('Candidates', 'Update', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

                          /*
                          logedin details 
                          **/
                          $role_id = Role::where('name', 'candidate')->pluck('id')->first();
                          $login = RoleUser::where('user_id', $user->id)->where('role_id',$role_id)->first();
                          //checking tenant role status
                          if ($login && ($login->status != 'active' || $login->is_deleted != 0)) {
                            return response()->json([
                              'error'=>[
                                'status_code' => 400,
                                'status' => 'Error', 
                                'message' => 'It appears that your account is either inactive or has been blocked. Please contact your agency.'
                              ]
                            ],400);
                          }
                          $login->login_at = Carbon::now()->toDateTimeString();
                          $login->save();
                    
                          return response()->json([
                            'success'=>[
                              'status_code' => 200,
                              'status' => 'Success', 
                              'token' => $token,
                              'subdomain' => $request->input('subdomain'),
                              'agency_email' => Agency::first()->email,
                              'tenant_id' => $tenant->id,
                              'role_user_id' => $login->id,
                              'user_details' => $user,
                              'candidate_data'=> $candidate
                            ]
                          ],200);
                      }

                    } else {
                      
                      return response()->json([
                        'error'=>[
                          'status_code' => 400,
                          'status' => 'Error', 
                          'message' => 'Your required information is missing (Job or Employment type). Please contact the agency.'
                        ]
                      ],400);

                    }

                } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'User not found.'
                      ]
                    ],400);
                }
                
              } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No user found.'
                      ]
                    ],400);
              }

            } else {
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'Invalid email or password'
                      ]
                    ],400);
            }
      }
            
    }
    /**
     * authenticate.
     *
     * @param mixed $request
     */
    public function authenticateAgency(Request $request)
    {
        // $this->validate($request, 
        //   [
        //     'email' => 'required', 
        //     'password' => 'required',
        //     'subdomain' => 'required'
        //   ]);

        $validator = Validator::make(
                        array(
                        'email' => $request->email, 
                        'password' => $request->password,
                        'role_id' => $request->role_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'email' => 'required|email', 
                        'password' => 'required',
                        'role_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
            else if ($messages->has('password')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('password')
                  ]
                ],400);
            }
            else if ($messages->has('role_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('role_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            if (Hash::check($request->input('password'), User::where('email', $request->input('email'))->pluck('password')->first())) {

              $tenant = Tenant::where('slug', $request->input('subdomain'))->first();
              $token = base64_encode(Str::random(40));
              $user = User::where('email', $request->input('email'))->where('status', 'active')->first();

              if($token && $user && $tenant){

                $user_tenant = UserTenant::where('user_id', $user->id)->where('tenant_id', $tenant->id)->first();

                $subscription = DB::table('subscriptions')->where('tenant_id', $tenant->id)->first(); 

                if($user_tenant){
                    //setting token
                    // $user->update(['remember_token' => $token]);
                    $userToken = UserToken::where('user_id', $user->id)->where('tenant_role_id', $request->role_id)->where('tenant_id', $tenant->id)->first();
                    if ($userToken) {
                      $userToken->update(['remember_token' => $token]);
                    } else {
                      $userTokenObj = new UserToken;
                      $userTokenObj->user_id = $user->id;
                      $userTokenObj->tenant_role_id = $request->role_id;
                      $userTokenObj->tenant_id = $tenant->id;
                      $userTokenObj->remember_token = $token;
                      $userTokenObj->save();
                    }
                    //reconnecting database
                    config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
                    DB::reconnect('mysql');
                    $roleUser = RoleUser::where('user_id', $user->id)->where('role_id',$request->role_id)->first();
                    //checking tenant role status
                    if ($roleUser && ($roleUser->status != 'active' || $roleUser->is_deleted != 0)) {
                      return response()->json([
                        'error'=>[
                          'status_code' => 400,
                          'status' => 'Error', 
                          'message' => 'It appears that your account is either inactive or has been blocked. Please contact your agency.'
                        ]
                      ],400);
                    }

                    $roleUser->login_at = Carbon::now()->toDateTimeString();
                    $roleUser->save();

                    $loggedData = [
                      'role'          => 'manager',
                      'tenant'        => $request->input('subdomain'),
                      'dashboardType' => 'manager_app',
                      'user_name'     => $user->name,
                      'email'         => $user->email,
                    ];

                    //Activity Log
                    $message = 'Manager - user has been logged in.';
                    ActivityCustomLogTrait::saveActivityLog('Managers', 'Update', $message, $roleUser->id, $roleUser->user_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

                  
                    return response()->json([
                            'success'=>[
                              'status_code' => 200,
                              'status' => 'Success', 
                              'token' => $token,
                              'subdomain' => $request->input('subdomain'),
                              // 'agency_email' => Agency::first()->email,
                              'tenant_id' => $tenant->id,
                              'tenant_name' => $tenant->name,
                              'role_user_id' => $roleUser->id,
                              'subscription' => $subscription,
                              'user_details' => $user,
                            ]
                          ],200);

                } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No agency found.'
                      ]
                    ],400);
                }
                
              } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No user found.'
                      ]
                    ],400);
              }

            } else {
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'Invalid email or password'
                      ]
                    ],400);
            }
      }
            
    }

    /**
     * authenticate.
     *
     * @param mixed $request
     */
    public function authenticateAgencyWithoutPassword(Request $request)
    {
        // $this->validate($request, 
        //   [
        //     'email' => 'required', 
        //     'password' => 'required',
        //     'subdomain' => 'required'
        //   ]);

        $validator = Validator::make(
                        array(
                        'email' => $request->email, 
                        // 'password' => $request->password,
                        'role_id' => $request->role_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'email' => 'required|email', 
                        // 'password' => 'required',
                        'role_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
            // else if ($messages->has('password')) {
            //     //Show custom message
            //     return response()->json([
            //       'error'=>[
            //         'status_code' => 400,
            //         'status' => 'Error', 
            //         'message' => $messages->first('password')
            //       ]
            //     ],400);
            // }
            else if ($messages->has('role_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('role_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            if (User::where('email', $request->input('email'))->pluck('email')->first()) {

              $tenant = Tenant::where('slug', $request->input('subdomain'))->first();
              $token = base64_encode(Str::random(40));
              $user = User::where('email', $request->input('email'))->first();

              if($token && $user && $tenant){

                $user_tenant = UserTenant::where('user_id', $user->id)->where('tenant_id', $tenant->id)->first();

                if($user_tenant){
                    //setting token
                    // $user->update(['remember_token' => $token]);
                    $userToken = UserToken::where('user_id', $user->id)->where('tenant_role_id', $request->role_id)->where('tenant_id', $tenant->id)->first();
                    if ($userToken) {
                      $userToken->update(['remember_token' => $token]);
                    } else {
                      $userTokenObj = new UserToken;
                      $userTokenObj->user_id = $user->id;
                      $userTokenObj->tenant_role_id = $request->role_id;
                      $userTokenObj->tenant_id = $tenant->id;
                      $userTokenObj->remember_token = $token;
                      $userTokenObj->save();
                    }
                    //reconnecting database
                    config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
                    DB::reconnect('mysql');
                    $roleUser = RoleUser::where('user_id', $user->id)->where('role_id',$request->role_id)->first();
                    $roleUser->login_at = Carbon::now()->toDateTimeString();
                    $roleUser->save();
                  
                    return response()->json([
                            'success'=>[
                              'status_code' => 200,
                              'status' => 'Success', 
                              'token' => $token,
                              'subdomain' => $request->input('subdomain'),
                              // 'agency_email' => Agency::first()->email,
                              'tenant_id' => $tenant->id,
                              'tenant_name' => $tenant->name,
                              'role_user_id' => $roleUser->id,
                              'user_details' => $user,
                            ]
                          ],200);

                } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No agency found.'
                      ]
                    ],400);
                }
                
              } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No user found.'
                      ]
                    ],400);
              }

            } else {
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'Invalid email'
                      ]
                    ],400);
            }
      }
            
    }
    /**
     * authenticate.
     *
     * @param mixed $request
     */
    public function authenticateCandidateWithoutPassword(Request $request)
    {
        // $this->validate($request, 
        //   [
        //     'email' => 'required', 
        //     'password' => 'required',
        //     'subdomain' => 'required'
        //   ]);

        $validator = Validator::make(
                        array(
                        'email' => $request->email, 
                        'subdomain' => $request->subdomain
                        ), array(
                        'email' => 'required|email', 
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            if (User::where('email', $request->input('email'))->pluck('email')->first()) {

              $tenant = Tenant::where('slug', $request->input('subdomain'))->first();

              $token = base64_encode(Str::random(40));
              $user = User::where('email', $request->input('email'))->first();

              if($token && $user){

                $user_tenant = UserTenant::where('user_id', $user->id)->where('tenant_id', $tenant->id)->first();

                if($user_tenant){
                    //setting token
                    // $user->update(['remember_token' => $token]);
                    $userToken = UserToken::where('user_id', $user->id)->where('tenant_role_id', 6)->where('tenant_id', $tenant->id)->first();
                    if ($userToken) {
                      $userToken->update(['remember_token' => $token]);
                    } else {
                      $userTokenObj = new UserToken;
                      $userTokenObj->user_id = $user->id;
                      $userTokenObj->tenant_role_id = 6;
                      $userTokenObj->tenant_id = $tenant->id;
                      $userTokenObj->remember_token = $token;
                      $userTokenObj->save();
                    }
                  
                    //reconnecting database
                    config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
                    DB::reconnect('mysql');
                    $candidate = Candidate::where('user_id', $user->id)->with('jobs')->with('employment_type')->first();

                    if ($candidate) {

                      if ( $candidate->is_deleted == 1 ) {
                          return response()->json([
                              'error'=>[
                                'status_code' => 400,
                                'status' => 'Error', 
                                'message' => "Your account has been deleted. Please contact the agency."
                              ]
                            ],400);
                      } else if ( $candidate->has_access == 0 ) {
                          return response()->json([
                              'error'=>[
                                'status_code' => 400,
                                'status' => 'Error', 
                                'message' => "You have no access to use this account. Please contact the agency."
                              ]
                            ],400);
                      } else {

                          /*
                          logedin details 
                          **/
                          $role_id = Role::where('name', 'candidate')->pluck('id')->first();
                          $login = RoleUser::where('user_id', $user->id)->where('role_id',$role_id)->first();
                          $login->login_at = Carbon::now()->toDateTimeString();
                          $login->save();
                    
                          return response()->json([
                            'success'=>[
                              'status_code' => 200,
                              'status' => 'Success', 
                              'token' => $token,
                              'subdomain' => $request->input('subdomain'),
                              'agency_email' => Agency::first()->email,
                              'tenant_id' => $tenant->id,
                              'role_user_id' => $login->id,
                              'user_details' => $user,
                              'candidate_data'=> $candidate
                            ]
                          ],200);
                      }

                    } else {
                      
                      return response()->json([
                        'error'=>[
                          'status_code' => 400,
                          'status' => 'Error', 
                          'message' => 'No user found.'
                        ]
                      ],400);

                    }

                } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No user found.'
                      ]
                    ],400);
                }
                
              } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No user found.'
                      ]
                    ],400);
              }

            } else {
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'Invalid email or password'
                      ]
                    ],400);
            }
      }
            
    }

    /**
     * authenticate.
     *
     * @param mixed $request
     */
    public function authenticateClientBu(Request $request)
    {
        // $this->validate($request, 
        //   [
        //     'email' => 'required', 
        //     'password' => 'required',
        //     'subdomain' => 'required'
        //   ]);

        $validator = Validator::make(
                        array(
                        'email' => $request->email, 
                        'password' => $request->password,
                        // 'role_id' => $request->role_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'email' => 'required|email', 
                        'password' => 'required',
                        // 'role_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
            else if ($messages->has('password')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('password')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            if (Hash::check($request->input('password'), User::where('email', $request->input('email'))->pluck('password')->first())) {

              $tenant = Tenant::where('slug', $request->input('subdomain'))->first();
              $token = base64_encode(Str::random(40));
              $user = User::where('email', $request->input('email'))->first();

              if($token && $user && $tenant){

                $user_tenant = UserTenant::where('user_id', $user->id)->where('tenant_id', $tenant->id)->first();
                
                if($user_tenant){
                    //setting token
                    // $user->update(['remember_token' => $token]);
                    
                    $userToken = UserToken::where('user_id', $user->id)->where('tenant_role_id', 7)->where('tenant_id', $tenant->id)->first();
                    if ($userToken) {
                      $userToken->update(['remember_token' => $token]);
                    } else {
                      $userTokenObj = new UserToken;
                      $userTokenObj->user_id = $user->id;
                      $userTokenObj->tenant_role_id = 7;
                      $userTokenObj->tenant_id = $tenant->id;
                      $userTokenObj->remember_token = $token;
                      $userTokenObj->save();
                    }
                    //reconnecting database
                    config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
                    DB::reconnect('mysql');
                    //checking Roles
                    $roleUser = RoleUser::where('user_id', $user->id)->where('status', 'active')->where('is_deleted', 0)->whereIn('role_id',[5,7])->first();
                    if ($roleUser) {
                      if ($roleUser->role_id == 7) {
                        $role_users_details = RoleUserDetail::where('role_user_id', $roleUser->id)->first();
                        if ($role_users_details && $role_users_details->business_unit_ids) {
                            $business_units = explode(",", $role_users_details->business_unit_ids);
                        } else {
                          $client = DB::table('clients')->where('user_id', $roleUser->user_id)->pluck('id');
                          $business_units = DB::table('business_units')->where('client_id', $client)->where('status', 'active')->where('is_deleted', 0)->pluck('id')->all();
                        }
                      } else {
                        $business_units = DB::table('business_units')->where('user_id', $roleUser->user_id)->pluck('id')->all();
                      }
                      $roleUser->login_at = Carbon::now()->toDateTimeString();
                      $roleUser->save();
                    
                      return response()->json([
                              'success'=>[
                                'status_code' => 200,
                                'status' => 'Success', 
                                'token' => $token,
                                'subdomain' => $request->input('subdomain'),
                                'business_units' => $business_units,
                                'tenant_id' => $tenant->id,
                                'tenant_name' => $tenant->name,
                                'role_user_id' => $roleUser->id,
                                'user_details' => $user,
                              ]
                            ],200);
                    } else {
                      return response()->json([
                        'error'=>[
                          'status_code' => 400,
                          'status' => 'Error', 
                          'message' => 'No roles found.'
                        ]
                      ],400);
                    }

                } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No agency found.'
                      ]
                    ],400);
                }
                
              } else{
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'No user found.'
                      ]
                    ],400);
              }

            } else {
                    return response()->json([
                      'error'=>[
                        'status_code' => 400,
                        'status' => 'Error', 
                        'message' => 'Invalid email or password'
                      ]
                    ],400);
            }
      }
            
    }

    public function rolesByUserEmail (Request $request) 
    {
        $validator = Validator::make(
                          array(
                          'email' => $request->email, 
                          'subdomain' => $request->subdomain, 
                          ), array(
                          'email' => 'required|email', 
                          'subdomain' => 'required', 
                      )
                  );

        if ($validator->fails()) {

              $messages = $validator->errors();
              //Determining If Messages Exist For A Field
              if ($messages->has('email')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('email')
                    ]
                  ],400);
              }
              else if ($messages->has('subdomain')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('subdomain')
                    ]
                  ],400);
              }
        } else {
          $user_id = User::where('email', $request->input('email'))->pluck('id');

          if (count($user_id) > 0) {
            //reconnecting database
            config(['database.connections.mysql.database' => 'logezy_'.$request->subdomain,'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $roleUsers  = RoleUser::whereIn('role_id', [2,3,4])->where('user_id', $user_id)->get();

            if ( count($roleUsers) > 0 ) {

              return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success', 
                  'data' => $roleUsers
                ]
              ],200);

            } else {
            
              return response()->json([
                      'error'=>[
                        'status_code' => 401,
                        'status' => 'Unauthorized', 
                        'message' => "You don't have access to continue."
                      ]
                    ],401);
            }

          } else {
            
            return response()->json([
                    'error'=>[
                      'status_code' => 404,
                      'status' => 'Not Found', 
                      'message' => "No user found on this email address."
                    ]
                  ],404);
          }


        } 
    }


    /*
    post forgot password
    **/
    public function forgot_password(Request $request)
    {
      $validator = Validator::make(
                      array(
                      'email' => $request->email,
                      ), array(
                      'email' => 'required',
                  )
              );
      if ($validator->fails()) {
        $messages = $validator->errors();
        //Determining If Messages Exist For A Field
        if ($messages->has('email')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $messages->first('email')
              ]
            ],400);
        }
      } else {
          $user = User::where('email', $request->email)->first();

          if($user){
            $current_date_time = date('Y-m-d H:i:s');
            $timeafter15mins = date("Y-m-d H:i:s",strtotime("+15 minutes", strtotime($current_date_time)));
            //delete existing 
            $otp = OtpVerification::where('user_id', $user->id)->delete();
            //create new otp
            $otp = new OtpVerification;
            $otp->user_id = $user->id;
            $otp->otp = rand(100000, 999999);
            $otp->expired_at = $timeafter15mins;
            $otp->save();

            if ($otp) {

              $mail_content = [
                  'title' => 'OTP Verification',
                  'agency' => "Logezy",
                  'otp' => $otp->otp,
                  'user_email' => $request->email,
              ];
              SendMailGun::sendMail('otp-sending', 'Logezy', 'info@logezy.co.uk', $request->email, 'OTP Verification', $mail_content, null, null, null);
              // dispatch(new OtpSendingJob($mail_content));

              return response()->json([
                  'success'=>[
                    'status_code' => 200,
                    'status' => 'Success', 
                    'user_id' => $user->id,
                    'message' => "The OTP has been sent to your email address."
                  ]
                ],200);
            }
          } else{
            return response()->json([
                  'error'=>[
                    'status_code' => 302,
                    'status' => 'Error', 
                    'message' => "The email address that you've entered doesn't match with any account."
                  ]
                ],302);
          }
      }
    }
    /**
     * [reset password].
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function verify_otp(Request $request)
    {
      $validator = Validator::make(
                      array(
                      'otp' => $request->otp,
                      'user_id' => $request->user_id,
                      ), array(
                      'otp' => 'required',
                      'user_id' => 'required',
                  )
              );
      if ($validator->fails()) {
        $messages = $validator->errors();
        //Determining If Messages Exist For A Field
        if ($messages->has('otp')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $messages->first('otp')
              ]
            ],400);
        }
        else if ($messages->has('user_id')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $messages->first('user_id')
              ]
            ],400);
        }
      } else {
        $otp = OtpVerification::where('user_id', $request->user_id)->where('otp', $request->otp)->first();
        // return $request->all();
        if ($otp ) {
          $current_date_time = strtotime(date('Y-m-d H:i:s'));
          $expired_at = strtotime($otp->expired_at);

          if ($current_date_time > $expired_at) {
            return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' => "OTP is expired, Please try again"
                ]
              ],401);
          } else if ($otp->otp != $request->otp) {
            return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' => "OTP is not matching"
                ]
              ],401);
          } else {
            $otp->verify = 1;
            $otp->save();
            return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'user_id' => $request->user_id,
                  'message' => "OTP is verified successfully"
                ]
              ],200);

          }
        } else {
            return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' => "OTP is not valid"
                ]
              ],401);
        }
      }
    }

    /**
     * [reset password].
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function password_reset(Request $request)
    {
      $validator = Validator::make(
                      array(
                      'otp' => $request->otp,
                      'user_id' => $request->user_id,
                      'password' => $request->password,
                      'confirm_password' => $request->confirm_password,
                      ), array(
                      'otp' => 'required',
                      'user_id' => 'required',
                      'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                      'confirm_password' => 'required|same:password',
                  )
              );
      $user_id = $request->user_id;
      if ($validator->fails()) {
        $messages = $validator->errors();
        //Determining If Messages Exist For A Field
        if ($messages->has('otp')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $messages->first('otp')
              ]
            ],400);
        } else if ($messages->has('user_id')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $messages->first('user_id')
              ]
            ],400);
        } else if ($messages->has('password')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Your password must be at least 8 characters and contains a combination of uppercase, lowercase letter, a special character and number are required."
              ]
            ],400);
        } else if ($messages->has('confirm_password')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $messages->first('confirm_password')
              ]
            ],400);
        }
      } else {
         $otp = OtpVerification::where('user_id', $request->user_id)->where('otp', $request->otp)->first();
        // return $request->all();
        if ($otp ) {
          $current_date_time = strtotime(date('Y-m-d H:i:s'));
          $expired_at = strtotime($otp->expired_at);

          if ($current_date_time > $expired_at) {
            return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' => "OTP is expired, Please try again"
                ]
              ],401);
          } else if ($otp->verify != 1) {
            return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' => "OTP is not verified. Please try again."
                ]
              ],401);
          } else {
            $user = User::where('id', $request->user_id)->first();
            // return $request->all();
            if ($user) {
              $user->password = Hash::make($request->password);
              if ($user->save()) {
                $otp = OtpVerification::where('user_id', $request->user_id)->delete();
                return response()->json([
                    'success'=>[
                      'status_code' => 200,
                      'status' => 'Success', 
                      'message' => "Your password has been successfully reset."
                    ]
                  ],200);
              }
            } else {
              return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => "No user found !"
                  ]
                ],400);
            }
          }
        } else {
            return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' => "Something went wrong!. Please try again."
                ]
              ],401);
        }
      }
    }

    /**
     * [change password].
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function change_password(Request $request)
    {
      $validator = Validator::make(
                      array(
                      'user_id' => $request->user_id,
                      'current_password' => $request->current_password,
                      'password' => $request->password,
                      'confirm_password' => $request->confirm_password,
                      ), array(
                      'user_id' => 'required',
                      'current_password' => 'required',
                      'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                      'confirm_password' => 'required|same:password',
                  )
              );
      //get user
      $user = User::where('id', $request->user_id)->first();
      //return error if no user found
      if(!$user){
        return response()->json([
            'error'=>[
              'status_code' => 400,
              'status' => 'Error', 
              'message' => "No user found !"
            ]
          ],400);
      }

      //old password validation
      if (!Hash::check($request->current_password, $user->password)) {
        return response()->json([
            'error'=>[
              'status_code' => 400,
              'status' => 'Error', 
              'message' => "Your current password is incorrect"
            ]
          ],400);
      } 
      
      //normal validation
      if ($validator->fails()) {
        $messages = $validator->errors();
        //Determining If Messages Exist For A Field
        if ($messages->has('user_id')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $messages->first('user_id')
              ]
            ],400);
        } else if ($messages->has('password')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Your password must be at least 8 characters and contains a combination of uppercase, lowercase letter, a special character and number are required."
              ]
            ],400);
        } else if ($messages->has('confirm_password')) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $messages->first('confirm_password')
              ]
            ],400);
        }
      } else {
        // return $request->all();
        if ($user) {
          $user->password = Hash::make($request->password);
          $user->remember_token = '';
          if ($user->save()) {
            $userToken = UserToken::where('user_id', $user->id)->first();
            $userToken->update(['remember_token' => '']);
            return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success', 
                  'message' => "Your password has been successfully reset."
                ]
              ],200);
          } else {
            return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' => "Something went wrong!. Please try again."
                ]
              ],401);
          }
        } 
      }
    }

    /**
    * Logout API
    **/
    public function logout(Request $request)
    {
      // $this->validate($request, 
      //     [
      //       'user_id' => 'required', 
      //     ]);

      $validator = Validator::make(
                        array(
                        // 'candidate_id' => $request->candidate_id,
                        'user_id' => $request->user_id,
                        // 'subdomain' => $request->subdomain
                        ), array(
                        // 'candidate_id' => 'required',
                        'user_id' => 'required',
                        // 'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('user_id')
                  ]
                ],400);
            } 
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $user = User::find($request->input('user_id'));
          // $user->update(['remember_token' => '']);
          //remove token
          if( $request->header('Authorization') !== "" ){
            $userToken = UserToken::where('remember_token', explode(' ', $request->header('Authorization'))[1])->first();
            if($userToken) {
              $userToken->update(['remember_token' => '']);
            }
          }
          //checking for subdomain
          if ($request->has('subdomain')) {
            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');
            //checking for candidate id 
            if ($request->has('candidate_id')) {
              $candidate_id = $request->input('candidate_id');
              $candidate = Candidate::find($candidate_id);
              $candidate->firebase_uin      = '';
              $candidate->save();
              //removing firebase and Player ID for stoping push notification to the app
              $roleUser = RoleUser::where('user_id', $request->input('user_id'));
              $roleUser->update(['firebase_uin' => '', 'player_id' => '']);

            }

            $loggedData = [
              'tenant'        => $request->input('subdomain'),
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];
            //Manager App
            if(isset($request->role_user_id)){
              $loggedData['role']          = 'manager';
              $loggedData['dashboardType'] = 'manager_app';

              $roleUser = RoleUser::where('user_id', $user->id)->select('id', 'user_name')->first();
              $logName = 'Manager';
              $logEntity = 'Managers';
              $subjectId = $roleUser->id;
              $objectId = $roleUser->user_name;

            //Candidate App
            }else if(isset($request->candidate_id)){
              $loggedData['role']          = 'candidate';
              $loggedData['dashboardType'] = 'candidate_app';
              $logName = 'Candidate';
              $logEntity = 'Candidates';
              $subjectId = $candidate->id;
              $objectId = $candidate->full_name;
            }

            //Activity Log
            $message = $logName.' - '.$objectId .' has been logged out.';
            ActivityCustomLogTrait::saveActivityLog($logEntity, 'Update', $message, $subjectId, $objectId, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
          }
          return response()->json([
                    'success'=>[
                      'status_code' => 200,
                      'status' => 'Success', 
                      'message' => "Logged out successfully"
                    ]
                  ],200);
      }

    }
    

    /**
    * Delete Candidate account API
    **/
    public function deleteCandidateAccount(Request $request)
    {

      $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        // 'user_id' => $request->user_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        // 'user_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            // if ($messages->has('user_id')) {
            //     //Show custom message
            //     return response()->json([
            //       'error'=>[
            //         'status_code' => 400,
            //         'status' => 'Error', 
            //         'message' => $messages->first('user_id')
            //       ]
            //     ],400);
            // } 
            // else 
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          //checking for candidate id and subdomain
          if ($request->has('candidate_id') && $request->has('subdomain')) {
            $candidate_id = $request->input('candidate_id');
  
            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');
  
            $candidate = Candidate::find($candidate_id);

            $historyLog = [ 
                "title" => "Self Deleted",
                "description" => "Self deleted from Candidate App",
                "date" => \Carbon\Carbon::now()->format('d.m.Y'),
                "time" => \Carbon\Carbon::now()->format('g:i a'),
            ];

            $historyLogs = $candidate->history_logs ?? [];

            array_unshift($historyLogs, $historyLog);
  
            $candidate->history_logs = $historyLogs;
            $candidate->firebase_uin = '';
            $candidate->is_deleted = 1;
            $candidate->deleted_by = $candidate->user_id;
            $candidate->deleted_at = Carbon::now()->toDateTimeString();
            $candidate->reason_to_delete = 'Self deleted';
            $candidate->save();

            $user_ids          = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
            $user_ids          = array_unique($user_ids);
            $notify_array = array(
                'title' => $candidate->full_name,
                'type' => "candidate_self_delete",
                'body' => 'Candidate -'.$candidate->full_name .' has been self deleted.',
                  );
    
            if ($user_ids) {
                foreach ($user_ids as $user_id) {
                    $notification = new Notification;
                    $notification->type             = 'App\Notifications\HelloNotification';
                    $notification->notifiable_type  = 'App\User';
                    $notification->notifiable_id    = $user_id;
                    $notification->object_id        = $candidate->id;
                    $notification->data             = json_encode($notify_array);
                    $notification->resource_ids     = $candidate->id;
                    $notification->save();
                }
            }

            //get agency details
            $agency = Agency::first();
            //sending request email to the agency
            $mail_content['candidate_name'] = '';
            $mail_content['agency_email']   = $agency->email;
            $mail_content['agency_name']    = $agency->name;
            $mail_content['agency_logo']    = $agency->agency_logo;
            $mail_content['title']          = 'Candidate Account Deleted';
            $mail_content['body']           = 'Candidate -'.$candidate->full_name . ' has been self deleted.';
            $mail_content['tenant_slug']    = $request->input('subdomain');

            SendMailGun::sendMail('notify_new_message', $mail_content['agency_name'], $mail_content['agency_email'], $mail_content["agency_email"], $mail_content['title'], $mail_content, null, null, null);

            $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('id', 'name', 'email')->first();

            $loggedData = [
              'role'          => 'candidate',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'candidate_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];

            //Activity Log
            $message = 'Candidate -'.$candidate->full_name .' has been self deleted.';
            ActivityCustomLogTrait::saveActivityLog('Candidates', 'Delete', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json([
                    'success'=>[
                      'status_code' => 200,
                      'status' => 'Success', 
                      'message' => "Account deleted successfully"
                    ]
                  ],200);
          }
          return response()->json([
                    'success'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => "Something went wrong"
                    ]
                  ],400);
      }

    }
    
}
