<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\VacancyStaffRate;
use App\Tenants\CandidateVacancy;
use App\Tenants\Vacancy;
use App\Tenants\AssignVacancy;
use App\Tenants\AssignVacancyDetail;
use App\Tenants\CandidateHolidayPayDeduction;
use App\Tenants\RoleUser;
use App\Tenants\RoleUserDetail;
use App\Tenants\Helper;
use App\Tenants\Agency;
use App\Tenants\AgencySetting;
use App\Tenants\SignedTimesheet;
use App\Tenants\Day;
use App\Tenants\CustomHoliday;
use App\Tenants\Holiday;
use App\Tenants\CandidateRateCard;
use App\Tenants\CandidateAvailability;
use App\Tenants\TextTemplate;
use App\Tenants\CandidateRestrictedBusinessUnit;
use App\Jobs\PublishAssignedShiftsJob;
use App\Traits\PushNotificationTrait;
use App\Traits\SMSTraits;
use App\Traits\EmailTraits;
use App\Traits\UserTraits;
use App\User;
use App\Tenants\Notification;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifyShiftAssigned;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Storage;

class AssignVacancyController extends BaseController
{
    public function assigned_vacancies (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'role_user_id' => $request->role_user_id,
                        'subdomain' => $request->subdomain,
                        'page' => $request->page
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'role_user_id' => 'required',
                        'subdomain' => 'required',
                        'page' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('role_user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('role_user_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('page')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('page')
                  ]
                ],400);
            }


      } else {

          $date_from = $request->input('date_from');
          $date_to = isset($request->date_to) ? $request->input('date_to') : '';
          // $business_units = array_map("intval", explode(",", $request->business_units));
          $shifts = isset($request->shifts)&& !empty($request->shifts) ? array_map("intval", explode(",", $request->shifts)) : [];
          $jobs = isset($request->jobs) && !empty($request->jobs) ? array_map("intval", explode(",", $request->jobs)) : [];

          //switching to the database
          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');     

        //getting business units from role user id
        $roleUser = RoleUser::where('id', $request->role_user_id)->where('status', 'active')->where('is_deleted', 0)->whereIn('role_id',[5,7])->first();
        $business_units = [];
        if ($roleUser) {
          if ($roleUser->role_id == 7) {
            $role_users_details = RoleUserDetail::where('role_user_id', $roleUser->id)->first();
            if ($role_users_details && $role_users_details->business_unit_ids) {
                $business_units = explode(",", $role_users_details->business_unit_ids);
            } else {
              $client = DB::table('clients')->where('user_id', $roleUser->user_id)->pluck('id');
              $business_units = DB::table('business_units')->where('client_id', $client)->where('status', 'active')->where('is_deleted', 0)->pluck('id')->all();
            }
          } else {
            $business_units = DB::table('business_units')->where('user_id', $roleUser->user_id)->pluck('id')->all();
          }
        }   

          //getting assigned vacancy details
          $assigned_shifts = DB::table('assign_vacancies')
                ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                ->join('vacancies', function($join) use ($shifts, $jobs)
                 {
                    $join->on('vacancies.id', '=', 'assign_vacancies.vacancy_id');
                    if ($shifts) {
                        $join->whereIn('vacancies.shift_id', $shifts);
                    }
                    if ($jobs) {
                        $join->whereIn('vacancies.job_id', $jobs);
                    }
                 })
                ->where(function($query) use ($date_from, $date_to)
                    {
                        if ($date_from != '' && $date_to != ''){
                          $query->whereBetween('assign_vacancies.on_date', [$date_from, $date_to]);
                        } else {
                          $query->where('assign_vacancies.on_date', '>=', $date_from);
                        }
                    })
                ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                ->join('clients', 'business_units.client_id', '=', 'clients.id')
                ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                ->select(
                  'candidates.id as candidate_id', 
                  'assign_vacancies.code', 
                  'candidates.full_name', 
                  'candidates.profile_img', 
                  'candidates.candidate_status_id',
                  'assign_vacancies.is_publish', 
                  'assign_vacancies.id AS assign_id', 
                  'vacancies.business_unit_id', 
                  'vacancies.job_id', 
                  'vacancies.shift_id', 
                  'clients.client_name', 
                  'business_units.business_unit_name', 
                  'jobs.name AS job_name', 
                  'jobs.color', 
                  'shifts.shift_name', 
                  'assign_vacancies.on_date', 
                  DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  
                  DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 
                  'assign_vacancies.break', 
                  'assign_vacancies.total_hours', 
                  'assign_vacancies.notes', 
                  'assign_vacancies.is_publish', 
                  'assign_vacancies.is_confirm', 
                  'assign_vacancies.status')
                ->whereIn('vacancies.business_unit_id', $business_units)
                ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                ->where('assign_vacancies.is_publish', 1)
                ->where('assign_vacancies.is_deleted', 0)
                ->where('candidates.is_deleted',0)
                ->where('candidates.candidate_status_id', 1)
                ->orderBy('assign_vacancies.on_date')
                ->orderBy('candidates.first_name')
                ->paginate(15);
          
          $assigned_shifts=$assigned_shifts->toArray();

          $assignedVacancies = collect($assigned_shifts['data'])->groupBy(['on_date','full_name']);

          return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success',
            'time_format' => DB::table('agencies')->first()->time_format,
            'current_page' => count($assigned_shifts['data'])>0 ? $assigned_shifts['current_page'] : 0,
            'total_results' => $assigned_shifts['total'],
            'data' => $assignedVacancies
          ]
          ],200);
      }

    }

    public function timesheets (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'role_user_id' => $request->role_user_id,
                        'subdomain' => $request->subdomain,
                        'page' => $request->page
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'role_user_id' => 'required',
                        'subdomain' => 'required',
                        'page' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('role_user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('role_user_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('page')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('page')
                  ]
                ],400);
            }


      } else {

          $date_from = $request->input('date_from');
          $date_to = isset($request->date_to) ? $request->input('date_to') : '';
          // $business_units = array_map("intval", explode(",", $request->business_units));
          $shifts = isset($request->shifts)&& !empty($request->shifts) ? array_map("intval", explode(",", $request->shifts)) : [];
          $jobs = isset($request->jobs) && !empty($request->jobs) ? array_map("intval", explode(",", $request->jobs)) : [];

          //switching to the database
          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');      

        //getting business units from role user id
        $roleUser = RoleUser::where('id', $request->role_user_id)->where('status', 'active')->where('is_deleted', 0)->whereIn('role_id',[5,7])->first();
        $business_units = [];
        if ($roleUser) {
          if ($roleUser->role_id == 7) {
            $role_users_details = RoleUserDetail::where('role_user_id', $roleUser->id)->first();
            if ($role_users_details && $role_users_details->business_unit_ids) {
                $business_units = explode(",", $role_users_details->business_unit_ids);
            } else {
              $client = DB::table('clients')->where('user_id', $roleUser->user_id)->pluck('id');
              $business_units = DB::table('business_units')->where('client_id', $client)->where('status', 'active')->where('is_deleted', 0)->pluck('id')->all();
            }
          } else {
            $business_units = DB::table('business_units')->where('user_id', $roleUser->user_id)->pluck('id')->all();
          }
        } 

          //getting assigned vacancy details
          $assigned_shifts = DB::table('assign_vacancies')
                ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                ->join('vacancies', function($join) use ($shifts, $jobs)
                 {
                    $join->on('vacancies.id', '=', 'assign_vacancies.vacancy_id');
                    if ($shifts) {
                        $join->whereIn('vacancies.shift_id', $shifts);
                    }
                    if ($jobs) {
                        $join->whereIn('vacancies.job_id', $jobs);
                    }
                 })
                ->where(function($query) use ($date_from, $date_to)
                    {
                        if ($date_from != '' && $date_to != ''){
                          $query->whereBetween('assign_vacancies.on_date', [$date_from, $date_to]);
                        } else {
                          $query->where('assign_vacancies.on_date', '>=', $date_from);
                        }
                    })
                ->leftJoin('signed_timesheets', 'signed_timesheets.assigned_vacancy_id', '=', 'assign_vacancies.id')
                ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                ->join('clients', 'business_units.client_id', '=', 'clients.id')
                ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                ->select(
                  'candidates.id as candidate_id', 
                  'assign_vacancies.code', 
                  'candidates.full_name', 
                  'candidates.profile_img', 
                  'candidates.candidate_status_id',
                  'assign_vacancies.is_publish', 
                  'assign_vacancies.id AS assign_id', 
                  'vacancies.business_unit_id', 
                  'vacancies.job_id', 
                  'vacancies.shift_id', 
                  'clients.client_name', 
                  'business_units.business_unit_name', 
                  'jobs.name AS job_name', 
                  'jobs.color', 
                  'shifts.shift_name', 
                  'assign_vacancies.on_date', 
                  DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  
                  DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 
                  'assign_vacancies.break', 
                  'assign_vacancies.total_hours', 
                  'assign_vacancies.notes', 
                  'assign_vacancies.is_publish', 
                  'assign_vacancies.is_confirm', 
                  'assign_vacancies.status', 
                  'signed_timesheets.id as signed_timesheet_id', 
                  'signed_timesheets.is_approve as signed_timesheet_approval')
                ->whereIn('vacancies.business_unit_id', $business_units)
                ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                ->where('assign_vacancies.is_publish', 1)
                ->where('assign_vacancies.is_deleted', 0)
                ->where('candidates.is_deleted',0)
                ->where('candidates.candidate_status_id', 1)
                ->orderBy('candidates.first_name')
                ->orderBy('assign_vacancies.on_date')
                ->paginate(15);
          
          $assigned_shifts=$assigned_shifts->toArray();

          $assignedVacancies = collect($assigned_shifts['data'])->groupBy(['on_date','full_name']);

          return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success',
            'time_format' => DB::table('agencies')->first()->time_format,
            'current_page' => count($assigned_shifts['data'])>0 ? $assigned_shifts['current_page'] : 0,
            'total_results' => $assigned_shifts['total'],
            'data' => $assignedVacancies
          ]
          ],200);
      }

    }


    public function assigned_vacancy (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'assign_id' => $request->assign_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'assign_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('assign_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assign_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $assign_id = $request->input('assign_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');
          

          $assigned = DB::table('assign_vacancies')
                ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                ->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
                ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                ->join('clients', 'business_units.client_id', '=', 'clients.id')
                ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                ->select(
                  'candidates.id as candidate_id', 
                  'assign_vacancies.code', 
                  'candidates.full_name', 
                  'candidates.profile_img', 
                  'candidates.candidate_status_id',
                  'assign_vacancies.is_publish', 
                  'assign_vacancies.id AS assign_id', 
                  'vacancies.business_unit_id', 
                  'vacancies.job_id', 
                  'vacancies.shift_id', 
                  'clients.client_name', 
                  'business_units.business_unit_name', 
                  'jobs.name AS job_name', 
                  'jobs.color', 
                  'shifts.shift_name', 
                  'assign_vacancies.on_date', 
                  DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  
                  DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 
                  'assign_vacancies.break', 
                  'assign_vacancies.total_hours', 
                  'assign_vacancies.notes', 
                  'assign_vacancies.is_publish', 
                  'assign_vacancies.is_confirm', 
                  'assign_vacancies.is_deleted', 
                  'assign_vacancies.status')
                ->where('assign_vacancies.id', $assign_id)
                ->where('assign_vacancies.is_publish', 1)
                ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                ->first();

          if ($assigned && $assigned->is_deleted == 1) {
            return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' =>  'This shift has been deleted.'
                ]
              ],400);
          }
          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'time_format' => DB::table('agencies')->first()->time_format,
                  'data' => $assigned
                ]
              ],200);
      }

    }


    public function timesheet (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'assign_id' => $request->assign_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'assign_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('assign_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assign_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $assign_id = $request->input('assign_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');
          

          $assigned = DB::table('assign_vacancies')
                ->leftJoin('signed_timesheets', 'signed_timesheets.assigned_vacancy_id', '=', 'assign_vacancies.id')
                ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                ->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
                ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                ->join('clients', 'business_units.client_id', '=', 'clients.id')
                ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                ->select(
                  'candidates.id as candidate_id', 
                  'assign_vacancies.code',   
                  'candidates.full_name', 
                  'candidates.profile_img', 
                  'candidates.candidate_status_id',
                  'assign_vacancies.is_publish', 
                  'assign_vacancies.id AS assign_id', 
                  'vacancies.business_unit_id', 
                  'vacancies.job_id', 
                  'vacancies.shift_id', 
                  'clients.client_name', 
                  'business_units.business_unit_name', 
                  'jobs.name AS job_name', 
                  'jobs.color', 
                  'shifts.shift_name', 
                  'assign_vacancies.on_date', 
                  DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  
                  DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 
                  'assign_vacancies.break', 
                  'assign_vacancies.total_hours', 
                  'assign_vacancies.notes', 
                  'assign_vacancies.is_publish', 
                  'assign_vacancies.is_confirm', 
                  'assign_vacancies.is_deleted', 
                  'assign_vacancies.status',
                  'signed_timesheets.id as signed_timesheet_id', 
                  'signed_timesheets.is_approve as signed_timesheet_approval')
                ->where('assign_vacancies.id', $assign_id)
                ->where('assign_vacancies.is_publish', 1)
                ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                ->first();

          if ($assigned && $assigned->is_deleted == 1) {
            return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' =>  'This shift has been deleted.'
                ]
              ],400);
          }
          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'time_format' => DB::table('agencies')->first()->time_format,
                  'data' => $assigned
                ]
              ],200);
      }

    }


    //SIGNED TIMESHEETS ON ASSIGNED VACANCY DETAILS ID

    public function signed_timesheet (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'signed_timesheet_id' => $request->signed_timesheet_id,
                        'assign_id' => $request->assign_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'signed_timesheet_id' => 'required',
                        'assign_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('signed_timesheet_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('signed_timesheet_id')
                  ]
                ],400);
            }
            else if ($messages->has('assign_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assign_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
          $signed_timesheet_id = $request->input('signed_timesheet_id');
          $assigned_vacancy_id = $request->input('assign_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $signed_timesheet = [];
              
          $assigned_shift = AssignVacancy::find($assigned_vacancy_id);
          //Signed timesheet Declaration
          $agencySettings = AgencySetting::where('settings_key', 'signed_timesheet_declaration')->first();

          if($assigned_shift){

            $signed = SignedTimesheet::where('id', $signed_timesheet_id)->where('assigned_vacancy_id', $assigned_vacancy_id)->first();

              if($signed){
                $signed_timesheet = array(
                  'id' => $signed->id, 
                  'assigned_vacancy_id' => $signed->assigned_vacancy_id,
                  'assigned_vacancy_details_id' => $signed->assigned_vacancy_details_id, 
                  'client_name' => $assigned_shift->business_unit->client_name, 
                  'candidate_name' => $assigned_shift->candidate_name, 
                  'job_title' => $assigned_shift->job_title, 
                  'job_color' => $assigned_shift->job_color, 
                  'shift_type' => $assigned_shift->shift_type, 
                  'business_unit_name' => $assigned_shift->business_unit_name, 
                  'business_unit_email' => $assigned_shift->business_unit_email, 
                  'business_unit_address' => $assigned_shift->business_unit_address, 
                  'on_date' => $assigned_shift->on_date, 
                  'start_time' => $signed->start_time, 
                  'end_time' => $signed->end_time, 
                  'break' => $signed->break, 
                  'total_hours' => $signed->total_hours, 
                  'signed_timesheet_approval' => $signed->is_approve == 0 ? 'pending' : 'approved', 
                  'candidate_signature' => $signed->candidate_signature, 
                  'authorised_signature' => $signed->authorised_signature, 
                  'authorised_name' => $signed->authorised_name, 
                  'mileage' => $signed->mileage, 
                  'ref_number' => $signed->ref_number, 
                  'no_of_workers' => $signed->no_of_workers, 
                  'driver_service' => $signed->driver_service, 
                  'travel_expenses' => $signed->travel_expenses, 
                  'had_induction' => $signed->had_induction, 
                  'satisfactory_timekeeping' => $signed->satisfactory_timekeeping, 
                  'dressed_appropriately' => $signed->dressed_appropriately, 
                  'team_player' => $signed->team_player,
                  'standards_of_care' => $signed->standards_of_care,
                  'comments' => $signed->comments,
                  'mileage_claim' => $signed->mileage_claim,
                  'worker_performance' => $signed->worker_performance,
                  'signed_timesheet_declaration' => $agencySettings ? $agencySettings->settings_value : NULL
                );
              }
          }
              
          $agency = DB::table('agencies')->first();

          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success', 
                  'time_format' => $agency->time_format,
                  'time_interval' => $agency->time_interval,
                  'data' => $signed_timesheet
                ]
              ],200);
      }

    }

    /**
    *  Approve Timesheets 
    */
    public function approveTimesheet(Request $request)
    {

      $validator = Validator::make(
                        array(
                        'assign_id' => $request->assign_id, 
                        'subdomain' => $request->subdomain,
                        ), array(
                        'assign_id' => 'required',
                        'subdomain' => 'required',
                    )
                );

        if ($validator->fails()) {

              $messages = $validator->errors();
              //Determining If Messages Exist For A Field
              if ($messages->has('assign_id')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('assign_id')
                    ]
                  ],400);
              }
              else if ($messages->has('subdomain')) {
                  //Show custom message
                  return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => $messages->first('subdomain')
                    ]
                  ],400);
              }


        } else {

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');


          $assign_id = $request->assign_id;

          //getting assigned shift on id
          $assign = AssignVacancy::find($assign_id);
          $oldShift = DB::table('assign_vacancies')->find($assign_id);

          $assign->status     = "approved";
          $assign->approved_by = Auth::id();
          $assign->approved_at = date('Y-m-d H:i:s');

          if ( $assign->is_holidaypay_counted == 0 && $assign->holiday_pay_amount != 0 ) {
            $assign->is_holidaypay_counted = 1;
            $holiday_pay = DB::table('candidate_holiday_pays')
            ->where('candidate_id', $assign->candidate_id)->first();
            //adding amount to total amount
            $newTotal = ($holiday_pay ? $holiday_pay->total_amount : 0) + $assign->holiday_pay_amount;
            //update log
            $input = [
                'type' => "add",
                'candidate_id' => $assign->candidate_id,
                'requested_amount' => $assign->holiday_pay_amount,
                'amount_when_deducting' => $holiday_pay ? $holiday_pay->total_amount : 0,
                'initiated_by' => Auth::id(),
                'status' => "approved"
            ];
            //inserting addition details
            CandidateHolidayPayDeduction::create($input);
            //update total
            DB::table('candidate_holiday_pays')->updateOrInsert(['candidate_id' => $assign->candidate_id], ['total_amount' => $newTotal, 'candidate_id' => $assign->candidate_id]);
          }
          //saving data
          if ($assign->save()) {
            //update signed timesheet
            $signedTimesheet = SignedTimesheet::where("assigned_vacancy_id", $assign->id)->first();
            if($signedTimesheet){
                $signedTimesheet->start_time    = date( "Y-m-d H:i:s", strtotime($oldShift->start_time));
                $signedTimesheet->end_time      = date( "Y-m-d H:i:s", strtotime($oldShift->end_time));
                $signedTimesheet->break         = $oldShift->break;
                $signedTimesheet->total_hours   = $oldShift->total_hours;
                $signedTimesheet->approved_by   = Auth::id();
                $signedTimesheet->is_approve    = 1;
                $signedTimesheet->save();
            }
          }

          return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'message' => "Timesheet approved successfully"
                ]
              ],200);

        }
    }


    /**
    *  Approve Signed Timesheets 
    */
    public static function approveSignedTimesheet(Request $request) {
      $validator = Validator::make(
                        array(
                        'signed_timesheet_id' => $request->signed_timesheet_id,
                        'assign_id' => $request->assign_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'signed_timesheet_id' => 'required',
                        'assign_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('signed_timesheet_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('signed_timesheet_id')
                  ]
                ],400);
            }
            else if ($messages->has('assign_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assign_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        $signed_timesheet_id = $request->input('signed_timesheet_id');
        $assigned_vacancy_id = $request->input('assign_id');

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $signedTimesheet     = DB::table('signed_timesheets')->where('assigned_vacancy_id', $assigned_vacancy_id)->where('id', $signed_timesheet_id)->first();

        $vacancy_id = AssignVacancy::where('id',$assigned_vacancy_id)->pluck('vacancy_id');

        //check whether the rate is flat or hourly
        $vacancy = DB::table('vacancies')->find($vacancy_id);
        

        //get assign data before updating
        $oldAssignVacancy = DB::table('assign_vacancies')->find($assigned_vacancy_id);
        $candidate = Candidate::where('id', $oldAssignVacancy->candidate_id)->first();

        $assign = AssignVacancy::where('id',$assigned_vacancy_id)->first();
        $assign->start_time          = date( "Y-m-d H:i:s", strtotime($signedTimesheet->start_time));
        $assign->end_time            = date( "Y-m-d H:i:s", strtotime($signedTimesheet->end_time));
        $assign->total_hours         = $signedTimesheet->total_hours;
        $assign->break               = $signedTimesheet->break;
        $assign->status              = "approved";
        $assign->approved_by         = Auth::id();
        $assign->approved_at         = date('Y-m-d H:i:s');
        $assign->is_publish          = 1;
        //updating for Holiday Pay if anyone of the total hours or break is not equal
        if ( $signedTimesheet->break != $oldAssignVacancy->break || $signedTimesheet->total_hours != $oldAssignVacancy->total_hours ) {
            //getting holiday pay by business unit id and job id from assign
            $holidayPay = getHolidayPayByBuAndJob($assign);
            if($holidayPay){
                //update deduct log
                if($assign->holiday_pay_amount > 0 &&  $assign->is_holidaypay_counted == 1) {
                    $holiday_pay = DB::table('candidate_holiday_pays')->where('candidate_id', $assign->candidate_id)->first();
                    $input = [
                        'type' => "deduct",
                        'candidate_id' => $assign->candidate_id,
                        'requested_amount' => $assign->holiday_pay_amount,
                        'deducted_date' => date("Y-m-d H:i:s"),
                        'amount_when_deducting' => $holiday_pay ? $holiday_pay->total_amount : 0,
                        'deducted_by' => Auth::id(),
                        'status' => "approved",
                        'initiated_by' => Auth::id(),
                    ];
                    //inserting deduction details
                    CandidateHolidayPayDeduction::create($input);
                    //deducting amount from total amount
                    $newTotal = ($holiday_pay ? $holiday_pay->total_amount : 0) - $assign->holiday_pay_amount;
                    //update total
                    DB::table('candidate_holiday_pays')->updateOrInsert(['candidate_id' => $assign->candidate_id], ['total_amount' => $newTotal, 'candidate_id' => $assign->candidate_id]);
                }
                $assign->holiday_pay_percentage = $holidayPay->holiday_pay_percentage;
                $assign->holiday_pay_rate       = $holidayPay->holiday_pay_amount;
                $assign->holiday_pay_frequency  = $holidayPay->holiday_pay_frequency;
                //checking frequency
                if ($holidayPay->holiday_pay_frequency == "flat") {
                    $assign->holiday_pay_amount     = $holidayPay->holiday_pay_amount;
                } else {
                    $assign->holiday_pay_amount     = $signedTimesheet->total_hours * $holidayPay->holiday_pay_amount;
                }
                if ($oldAssignVacancy->holiday_pay_status == "paid") {
                    $assign->holiday_pay_status     = 'amended';
                } else if ($oldAssignVacancy->holiday_pay_status == "amended") {
                    $assign->holiday_pay_status     = 'amended';
                } else {
                    $assign->holiday_pay_status     = 'due';
                }
                $assign->holiday_pay_updated_at = date("Y-m-d H:i:s");
                //update add log
                if($assign->holiday_pay_amount > 0 &&  $assign->is_holidaypay_counted == 1) {
                    $holiday_pay = DB::table('candidate_holiday_pays')->where('candidate_id', $assign->candidate_id)->first();
                    //adding amount to total amount
                    $newTotal = ($holiday_pay ? $holiday_pay->total_amount : 0) + $assign->holiday_pay_amount;
                    //update log
                    $input = [
                        'type' => "add",
                        'candidate_id' => $assign->candidate_id,
                        'requested_amount' => $assign->holiday_pay_amount,
                        'amount_when_deducting' => $holiday_pay ? $holiday_pay->total_amount : 0,
                        'initiated_by' => Auth::id(),
                        'status' => "approved"
                    ];
                    //inserting addition details
                    CandidateHolidayPayDeduction::create($input);
                    //update total
                    DB::table('candidate_holiday_pays')->updateOrInsert(['candidate_id' => $assign->candidate_id], ['total_amount' => $newTotal, 'candidate_id' => $assign->candidate_id]);
                }
            }
        }
        if ( $assign->is_holidaypay_counted == 0 && $assign->holiday_pay_amount != 0 ) {
            $assign->is_holidaypay_counted = 1;
            $holiday_pay = DB::table('candidate_holiday_pays')
            ->where('candidate_id', $assign->candidate_id)->first();
            //adding amount to total amount
            $newTotal = ($holiday_pay ? $holiday_pay->total_amount : 0) + $assign->holiday_pay_amount;
            //update log
            $input = [
                'type' => "add",
                'candidate_id' => $assign->candidate_id,
                'requested_amount' => $assign->holiday_pay_amount,
                'amount_when_deducting' => $holiday_pay ? $holiday_pay->total_amount : 0,
                'initiated_by' => Auth::id(),
                'status' => "approved"
            ];
            //inserting addition details
            CandidateHolidayPayDeduction::create($input);
            //update total
            DB::table('candidate_holiday_pays')->updateOrInsert(['candidate_id' => $assign->candidate_id], ['total_amount' => $newTotal, 'candidate_id' => $assign->candidate_id]);
        }
        $assign->save();


        //updating signed timesheet status to approved
        SignedTimesheet::where('id', $signed_timesheet_id)->where('assigned_vacancy_id', $assigned_vacancy_id)->update(['is_approve' => 1, 'approved_by' => Auth::id()]);
        //updating details
        if ( $signedTimesheet->start_time != $oldAssignVacancy->start_time || $signedTimesheet->end_time != $oldAssignVacancy->end_time ) {

            $inputArr = [
              'on_date' => date('Y-m-d', strtotime($vacancy->on_date)),
              'start_time' => $signedTimesheet->start_time,
              'end_time' => $signedTimesheet->end_time,
              'break' => $signedTimesheet->break,
              'business_unit_id' => $vacancy->business_unit_id,
              'job_id' => $vacancy->job_id,
              'shift_id' => $vacancy->shift_id
            ];
            
            $rateCards = splitTimeRangeIntoSlots($inputArr);

            if (count($rateCards)>0) {
                //delete entries on vacancy id if exists
                $detailIDs = AssignVacancyDetail::where('assigned_vacancy_id', $assign->id)->pluck('id')->all();
                if ($detailIDs) AssignVacancyDetail::destroy($detailIDs);
            }
            $previousValue = null;
            foreach ($rateCards as $rateCard) {
                $staff_rate = '';
                if( $rateCard['staff_rates'] ) {
                    foreach($rateCard['staff_rates'] as $staffRate) {
                        if( $staffRate->employment_type_id == $candidate->employment_types_id ){
                            $staff_rate = $staffRate->staff_rate;
                            break;
                        }
                    }
                }
                $staff_rate_amount = $staff_rate != '' ? $staff_rate : 0;
                $candidate_staff_rate = 0;
                if ($rateCard['day_id'] != 8) {
                    $candidateRateCards = CandidateRateCard::where('business_unit_id', $vacancy->business_unit_id)
                                ->where('job_id', $vacancy->job_id)
                                ->where('day_id', $rateCard['day_id'])
                                ->where('shift_id', $rateCard['shift_id'])
                                // ->where('start_time', date( "H:i:s", strtotime($rateCard['end_time'])) )
                                // ->where('end_time', date( "H:i:s", strtotime($rateCard['start_time'])) )
                                ->where('employment_type_id', $candidate->employment_types_id)
                                ->where('candidate_id', $candidate->id)
                                ->get();   
                    $candidate_rate = '';
                    if (count($candidateRateCards)>0) {
                        foreach ($candidateRateCards as $rate_card) {
                            $start = date("H:i", strtotime($rate_card->start_time));
                            $end = date("H:i", strtotime($rate_card->end_time));
                            // $date = date('Y-m-d', strtotime($vacancy->on_date));
                            $week = date("W", strtotime($vacancy->on_date));
                            if($previousValue) {
                                $week = $previousValue>$rate_card->day_id ? $week+1 : $week;
                            }
                            $year = date("Y", strtotime($vacancy->on_date));
                            $date = date('Y-m-d', strtotime($year."W".$week.$rate_card->day_id));
                            if ($start >= $end) {
                                $startDate = $date;
                                $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                            }
                            else {
                                $startDate = $date;
                                $endDate = $date;
                            }
                            if ( strtotime($startDate.' '.$rate_card->start_time) < strtotime($rateCard['end_time']) && strtotime($rateCard['start_time']) < strtotime($endDate.' '.$rate_card->end_time) ) {
                                $candidate_rate = $rate_card;
                            }
                        } 
                    }
                    $candidate_staff_rate = $candidate_rate == '' ? 0 : $candidate_rate->staff_rate;
                    $previousValue = $rateCard['day_id'];
                }
                //check whether the rate is flat or hourly
                if ($rateCard['rate_type'] == "flat_rate") {
                    $client_pay_amount   = !empty($rateCard['client_rate']) ? $rateCard['client_rate'] : 0;
                    $staff_pay_amount    = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
                } else {
                    $client_pay_amount   = ($rateCard['total_hours']) * (!empty($rateCard['client_rate']) ? $rateCard['client_rate'] : 0);
                    $staff_pay_amount    = ($rateCard['total_hours']) * (($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount);
                }
    
                $assign_details = new AssignVacancyDetail();
                $assign_details->assigned_vacancy_id = $assign->id;
                $assign_details->start_time          = date( "Y-m-d H:i:s", strtotime($rateCard['start_time'])) ?? null;
                $assign_details->end_time            = date( "Y-m-d H:i:s", strtotime($rateCard['end_time'])) ?? null;
                $assign_details->total_hours         = $rateCard['total_hours'];
                $assign_details->rate_type           = $rateCard['rate_type'];
                $assign_details->client_rate         = !empty($rateCard['client_rate']) ? $rateCard['client_rate'] : 0;
                $assign_details->staff_rate          = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
                $assign_details->client_pay_amount   = $client_pay_amount;
                $assign_details->staff_pay_amount    = $staff_pay_amount;
                $assign_details->break               = $rateCard['break'];
                $assign_details->save();
            }
            //updating assigned detail id
            $assignedDetail = AssignVacancyDetail::where('assigned_vacancy_id', $assigned_vacancy_id)->first();
            if ($assignedDetail) {
                SignedTimesheet::where('id', $signed_timesheet_id)->update(['assigned_vacancy_details_id' => $assignedDetail->id]);
            }
        } else {
            if ( $oldAssignVacancy->break != $signedTimesheet->break) {
                $rateCards = AssignVacancyDetail::where('assigned_vacancy_id', $assign->id)->orderBy('id', 'asc')->get();
                $max = '';
                $index = '';
                if (count($rateCards)>0) {
                    $hoursArr = [];
                    foreach($rateCards as $rateCard) {
                        $hoursArr[$rateCard->id] = $rateCard->total_hours;
                    }
                    $max = max($hoursArr);
                    $index = array_search($max, $hoursArr);
                    foreach ($rateCards as $detail) {
                        if ($index == $detail->id) {
                            if ( $detail->break != $signedTimesheet->break) {
                                $assign_details = AssignVacancyDetail::find($detail->id);
                                $assign_details_data = DB::table('assigned_vacancy_details')->where('id', $detail->id)->first();
                                //calculate total hours
                                $inputArr = array(
                                    'start_time' => strtotime($assign_details_data->start_time),
                                    'end_time'   => strtotime($assign_details_data->end_time) ,
                                    'break'      => $signedTimesheet->break,
                                );
                                $assign_details->total_hours         = calcTotalHours($inputArr) ?? 0;
                                $rateType = $assign_details->rate_type != NULL ? $assign_details->rate_type : $vacancy->rate_type;
                                if ($rateType == "hourly_rate") {
                                    $assign_details->client_pay_amount   = $assign_details->total_hours * $assign_details->client_rate;
                                    $assign_details->staff_pay_amount    = $assign_details->total_hours * $assign_details->staff_rate;
                                } else {
                                    $assign_details->client_pay_amount   = $assign_details->client_rate;
                                    $assign_details->staff_pay_amount    = $assign_details->staff_rate;
                                }
                                $assign_details->break               = $signedTimesheet->break;
                                $assign_details->save();
                            }
                        }
                    }
                }
            }
        }
        
        return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'message' => "Signed Timesheet approved successfully"
                ]
              ],200);

      }
    }
    
}
