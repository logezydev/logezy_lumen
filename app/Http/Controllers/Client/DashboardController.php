<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Dashboard;
use App\Tenants\Candidate;
use App\Tenants\Agency;
use App\Tenants\AssignVacancy;
use App\Tenants\RoleUser;
use App\Tenants\RoleUserDetail;
use App\UserTenant;
use App\Tenant;
use Carbon\Carbon;
use DB;

class DashboardController extends BaseController
{
    /**
     * Display a listing of the dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboardDetails(Request $request) {

        $validator = Validator::make(
                        array(
                        'role_user_id' => $request->role_user_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'role_user_id' => 'required',
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('role_user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('role_user_id')
                  ]
                ],400);
            } else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            } 

        } else {

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            //getting business units from role user id
            $roleUser = RoleUser::where('id', $request->role_user_id)->where('status', 'active')->where('is_deleted', 0)->whereIn('role_id',[5,7])->first();
            $business_units = [];
            if ($roleUser) {
              if ($roleUser->role_id == 7) {
                $role_users_details = RoleUserDetail::where('role_user_id', $roleUser->id)->first();
                if ($role_users_details && $role_users_details->business_unit_ids) {
                    $business_units = explode(",", $role_users_details->business_unit_ids);
                } else {
                  $client = DB::table('clients')->where('user_id', $roleUser->user_id)->pluck('id');
                  $business_units = DB::table('business_units')->where('client_id', $client)->where('status', 'active')->where('is_deleted', 0)->pluck('id')->all();
                }
              } else {
                $business_units = DB::table('business_units')->where('user_id', $roleUser->user_id)->pluck('id')->all();
              }
            }  

            // $business_units = array_map("intval", explode(",", $request->business_units));
            $currWeek = Dashboard::getCurrentWeek();

            $openVacancies      = DB::table('vacancies')
                                    ->where('on_date', '>=', $currWeek['monday'])
                                    ->where('on_date', '<=', $currWeek['sunday'])
                                    ->whereIn('business_unit_id', $business_units)
                                    ->where('deleted_space', 0)
                                    ->sum(DB::raw('total_numbers - used_count')); 
            $assignedVacancies  = AssignVacancy::whereHas('vacancy', function($query) use($business_units){
                                        $query->whereIn('business_unit_id', $business_units);
                                    })->where('on_date', '>=', $currWeek['monday'])
                                    ->where('on_date', '<=', $currWeek['sunday'])
                                    ->whereIn('status',['pending', 'approved'])
                                    ->where('is_publish',1)->where('is_deleted',0)->count();
            $invoicedShifts     = AssignVacancy::whereHas('vacancy', function($query) use($business_units){
                                        $query->whereIn('business_unit_id', $business_units);
                                    })->where('on_date', '>=', $currWeek['monday'])
                                    ->where('on_date', '<=', $currWeek['sunday'])
                                    ->where('status','invoiced')
                                    ->where('is_publish',1)->where('is_deleted',0)->count();

            $deletedShifts      = AssignVacancy::whereHas('vacancy', function($query) use($business_units){
                                        $query->whereIn('business_unit_id', $business_units);
                                    })->where('on_date', '>=', $currWeek['monday'])
                                    ->where('on_date', '<=', $currWeek['sunday'])
                                    ->where('is_publish',1)->where('is_deleted',1)->count();
            //getting vacancies
            $vacancies = DB::table('vacancies')
                    ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                    ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                    ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                    ->whereBetween('vacancies.on_date', [$currWeek['monday'], $currWeek['sunday']])
                    ->whereIn('vacancies.business_unit_id', $business_units)
                    ->whereRaw('vacancies.total_numbers > vacancies.used_count')
                    ->orderBy('vacancies.on_date', 'asc')
                    ->select('vacancies.id',
                    'vacancies.code',
                    'vacancies.on_date',
                    DB::raw('DATE_FORMAT(vacancies.start_time, "'.dateTimeFormat().'") as start_time'),
                    DB::raw('DATE_FORMAT(vacancies.end_time, "'.dateTimeFormat().'") as end_time'),
                    'vacancies.break',
                    'vacancies.total_hours',
                    'business_units.business_unit_name',
                    'jobs.name AS job_title',
                    'jobs.color',
                    'shifts.shift_name AS shift_type',
                    'vacancies.is_publish',
                    'vacancies.used_count AS assigned_count',
                    DB::raw( 'vacancies.total_numbers - vacancies.used_count as space_left'),
                    'vacancies.total_numbers AS total_space')->paginate(5);
            $vacancies=$vacancies->toArray();
            
        }
                    
        return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success', 
              'openVacancies' => $openVacancies,
              'assignedVacancies' => $assignedVacancies,
              'invoicedShifts' => $invoicedShifts,
              'deletedShifts' => $deletedShifts,
              'vacancies' => $vacancies['data']
            ]
          ],200);
    }
}
