<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\Day;
use App\Tenants\CustomHoliday;
use App\Tenants\Holiday;
use App\Tenants\CandidateVacancy;
use App\Tenants\Vacancy;
use App\Tenants\RoleUser;
use App\Tenants\RoleUserDetail;
use App\Tenants\Agency;
use App\Tenants\Helper;
use App\Tenants\AssignVacancy;
use App\Tenants\CandidateRateCard;
use App\Tenants\EmploymentType;
use App\Tenants\VacancyDetail;
use App\Tenants\VacancyStaffRate;
use App\Jobs\PublishAssignedShiftsJob;
use App\Traits\PushNotificationTrait;
use App\Traits\SMSTraits;
use App\Traits\EmailTraits;
use App\Traits\UserTraits;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;

class VacancyController extends BaseController
{

    /**
     * Store new vacancy
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function addVacancies(Request $request) {

      $validator = Validator::make(
                        array(
                        'business_unit_id' => $request->business_unit_id,
                        'job_id' => $request->job_id,
                        'dates' => $request->dates,
                        'shift_id' => $request->shift_id,
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_numbers' => $request->total_numbers,
                        'subdomain' => $request->subdomain
                        ), array(
                        'business_unit_id' => 'required',
                        'job_id' => 'required',
                        'dates' => 'required',
                        'shift_id' => 'required',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'notes' => 'nullable',
                        'total_numbers' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('business_unit_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('job_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_id')
                  ]
                ],400);
            }
            else if ($messages->has('dates')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('dates')
                  ]
                ],400);
            }
            else if ($messages->has('shift_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('shift_id')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_numbers')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_numbers')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
        try {
          $datesArr = explode(",", $request->dates);
          $res = [];
          if ($datesArr) {
            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');
            foreach ($datesArr as $date) {
              $request->request->add(['on_date' => $date]);
              $res[] = Vacancy::addVacancy($request);
            }
          }

          return response()->json([
            'success'=>[
              'status_code' => 201,
              'status' => 'Success', 
              'message' => count($datesArr) == 1 ? "Vacancy created successfully" : "Vacancies created successfully",
              'data' => $res
            ]
          ],201);
        } catch (\Illuminate\Database\QueryException $ex) {

            return response()->json([
              'Error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $ex->getMessage()
              ]
            ],400);
        }
      }

    }


    /**
     * Update vacancy
     *
     * @param      \Illuminate\Http\Request  $request  The request
     * @param      \App\Models\Vacancy       $vacancy  The vacancy
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function update_vacancy(Request $request) {

      $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id,
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_hours' => $request->total_hours,
                        'subdomain' => $request->subdomain
                        ), array(
                        'vacancy_id' => 'required',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'notes' => 'nullable',
                        'total_hours' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_hours')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_hours')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $vacancy = Vacancy::find($request->vacancy_id);
        //checking for night shift
        $start = date("H:i", strtotime($request->start_time));
        $end = date("H:i", strtotime($request->end_time));
        if ($start >= $end) {
            $endDate = date('Y-m-d', strtotime('+1 day', strtotime($vacancy->on_date)));
        } else {
            $endDate = $vacancy->on_date;
        }
        //calculate total hours
        $inputs = array(
            'on_date'    => $vacancy->on_date,
            'start_time' => strtotime($vacancy->on_date. ' '.$request->start_time),
            'end_time'   => strtotime($endDate. ' '.$request->end_time) ,
            'break'      => $request->break ?? 0,
        );
        $totalHours = calcTotalHours($inputs);
        //fetching rates
        // $inputArr = array(
        //     'business_unit_id'  => $vacancy->business_unit_id,
        //     'job_id'            => $vacancy->job_id,
        //     'on_date'           => $vacancy->on_date,
        //     'shift_id'          => $vacancy->shift_id,
        // );
        // $rates = Helper::getClientStaffRates($inputArr);

        // if( isset($request->client_rate) && ( $request->client_rate == null || $request->client_rate == "0.00") ) {
        //     $clientRate = !empty($rates->client_rate) ? $rates->client_rate : 0;
        // } else {
        //     $clientRate = $request->client_rate;
        // }
        // $staffRates  = [];
        // if( $rates ) {
        //     $staffRates  = $rates->staff_rate_cards ?? [];
        // }
        // $vacancy->shift_id         = isset($request->shift_id) ? $request->shift_id : null;
        $vacancy->start_time       = isset($request->start_time) ? date( "Y-m-d H:i:s", strtotime($vacancy->on_date. ' '.$request->start_time)) : null;
        $vacancy->end_time         = isset($request->end_time) ? date( "Y-m-d H:i:s", strtotime($endDate. ' '.$request->end_time)) : null;
        // $vacancy->on_date          = isset($request->on_date) ? date( "Y-m-d", strtotime($request->on_date) ) : null;
        $vacancy->break            = isset($request->break) ? $request->break : 0;
        $vacancy->total_hours      = $totalHours ?? 0;
        $vacancy->client_rate      = 0;
        $vacancy->notes            = isset($request->notes) ? $request->notes : null;
        if (isset($request->space_left) ) {
            $space = (int)$vacancy->total_numbers - (int)$vacancy->used_count;
            if($space != $request->space_left) {
                $new_space = ((int)$request->space_left - (int)$space)+(int)$vacancy->total_numbers;
                $vacancy->total_numbers    = $new_space;
            }
        }
        $oldVacancy = DB::table('vacancies')->find($request->vacancy_id);
        //Populating vacancy staff rates
        if($vacancy->save()) {
          if ( ($oldVacancy->break != $request->break) || (strtotime($oldVacancy->on_date. ' '.$request->start_time) == strtotime($oldVacancy->start_time) && strtotime($endDate. ' '.$request->end_time) == strtotime($oldVacancy->end_time)) ) {

                    $request->request->add(['on_date' => date('Y-m-d', strtotime($oldVacancy->on_date))]);
                    $request->request->add(['business_unit_id' => $vacancy->business_unit_id]);
                    $request->request->add(['job_id' => $vacancy->job_id]);
                    $request->request->add(['shift_id' => $vacancy->shift_id]);
                    //find matching ratecards
                    $rateCards = splitTimeRangeIntoSlots($request);
                    // dd($rateCards);
                    //delete entries on vacancy id if exists
                    $detailIDs = VacancyDetail::where('vacancy_id', $vacancy->id)->pluck('id')->all();
                    if ($detailIDs) VacancyDetail::destroy($detailIDs);
                    $staffrateIDs = VacancyStaffRate::where('vacancy_id', $vacancy->id)->pluck('id')->all();
                    if ($staffrateIDs) VacancyStaffRate::destroy($staffrateIDs);

                    //adding vacancy details
                    foreach ($rateCards as $rateCard) {
                        $vacancyDetail                   = new VacancyDetail;
                        $vacancyDetail->vacancy_id       = $vacancy->id;
                        $vacancyDetail->day_id           = $rateCard['day_id'];
                        $vacancyDetail->shift_id         = $rateCard['shift_id'];
                        $vacancyDetail->rate_type        = $rateCard['rate_type'];
                        $vacancyDetail->start_time       = date( "Y-m-d H:i:s", strtotime($rateCard['start_time'])) ?? null;
                        $vacancyDetail->end_time         = date( "Y-m-d H:i:s", strtotime($rateCard['end_time'])) ?? null;
                        $vacancyDetail->break            = $rateCard['break'] ?? 0;
                        $vacancyDetail->total_hours      = $rateCard['total_hours'] ?? 0;
                        $vacancyDetail->client_rate      = $rateCard['client_rate'] ?? 0;
                        $vacancyDetail->save();
                        //Populating vacancy staff rates
                        if( $rateCard['staff_rates'] ) {
                            foreach($rateCard['staff_rates'] as $staffRate) {
                                // if(isset($staffRate['value'])){
                                    $vacancy_staff_rate                     = new VacancyStaffRate;
                                    $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                    $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                    $vacancy_staff_rate->employment_type_id = $staffRate->employment_type_id;
                                    $vacancy_staff_rate->staff_rate         = $staffRate->staff_rate;
                                    $vacancy_staff_rate->save();
                                // }
                            }
                        } else {
                            $staffRates = EmploymentType::where('status','active')->where('is_deleted',0)->get();
                            foreach ($staffRates as $staffRate) {
                                // if(isset($staffRate['value'])){
                                    $vacancy_staff_rate                     = new VacancyStaffRate;
                                    $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                    $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                    $vacancy_staff_rate->employment_type_id = $staffRate->id;
                                    $vacancy_staff_rate->staff_rate         = $staffRate->value;
                                    $vacancy_staff_rate->save();
                                // }
                            }
                        }
                    }
                }
            // $staffRateArr    = isset($request->staff_rates) ? $request->staff_rates : [];
            // if($staffRateArr) {
            //     $checkEmpty = [];
            //     foreach($staffRateArr as $staffRate) {
            //         if( VacancyStaffRate::find($staffRate['id']) && $staffRate['staff_rate'] != '0.00' ){
            //             $checkEmpty [] = $staffRate;
            //             $vacancy_staff_rate                     = VacancyStaffRate::find($staffRate['id']);
            //             $vacancy_staff_rate->vacancy_id         = $id;
            //             $vacancy_staff_rate->employment_type_id = $staffRate['employment_type_id'];
            //             $vacancy_staff_rate->staff_rate         = $staffRate['staff_rate'];
            //             $vacancy_staff_rate->save();
            //         }
            //     }
            //     if (!$checkEmpty) {
            //         if( $staffRates ) {
            //             foreach($staffRateArr as $staffRate) {
            //                 $vacancy_staff_rate = VacancyStaffRate::find($staffRate['id']);
            //                 $vacancy_staff_rate->delete();
            //             }
            //             foreach($staffRates as $staffRate) {
            //                 // if(isset($staffRate['value'])){
            //                     $vacancy_staff_rate                     = new VacancyStaffRate;
            //                     $vacancy_staff_rate->vacancy_id         = $vacancy->id;
            //                     $vacancy_staff_rate->employment_type_id = $staffRate['id'];
            //                     $vacancy_staff_rate->staff_rate         = $staffRate['value'];
            //                     $vacancy_staff_rate->save();
            //                 // }
            //             }
            //         }
            //     }
            // }
        }

        return response()->json([
          'success'=>[
            'status_code' => 201,
            'status' => 'Success', 
            'message' => "Vacancy updated successfully"
          ]
        ],201);
      }

    }


    public function vacancy_list (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'role_user_id' => $request->role_user_id,
                        'subdomain' => $request->subdomain,
                        'page' => $request->page
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'role_user_id' => 'required',
                        'subdomain' => 'required',
                        'page' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('role_user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('role_user_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('page')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('page')
                  ]
                ],400);
            }


      } else {

        $date_from = $request->input('date_from');
        $date_to = $request->input('date_to');
        // $business_units = array_map("intval", explode(",", $request->business_units));

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        //getting business units from role user id
        $roleUser = RoleUser::where('id', $request->role_user_id)->where('status', 'active')->where('is_deleted', 0)->whereIn('role_id',[5,7])->first();
        $business_units = [];
        if ($roleUser) {
          if ($roleUser->role_id == 7) {
            $role_users_details = RoleUserDetail::where('role_user_id', $roleUser->id)->first();
            if ($role_users_details && $role_users_details->business_unit_ids) {
                $business_units = explode(",", $role_users_details->business_unit_ids);
            } else {
              $client = DB::table('clients')->where('user_id', $roleUser->user_id)->pluck('id');
              $business_units = DB::table('business_units')->where('client_id', $client)->where('status', 'active')->where('is_deleted', 0)->pluck('id')->all();
            }
          } else {
            $business_units = DB::table('business_units')->where('user_id', $roleUser->user_id)->pluck('id')->all();
          }
        } 

        
        //getting vacancies
        $vacancies = DB::table('vacancies')
                ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                ->whereBetween('vacancies.on_date', [$date_from, $date_to])
                // ->whereRaw("vacancies.business_unit_id IN ('".$business_units."')")
                ->whereIn('vacancies.business_unit_id', $business_units)
                ->whereRaw('vacancies.total_numbers > vacancies.used_count')
                ->orderBy('vacancies.on_date', 'asc')
                ->select('vacancies.id',
                'vacancies.code',
                'vacancies.on_date',
                DB::raw('DATE_FORMAT(vacancies.start_time, "'.dateTimeFormat().'") as start_time'),
                DB::raw('DATE_FORMAT(vacancies.end_time, "'.dateTimeFormat().'") as end_time'),
                'vacancies.break',
                'vacancies.total_hours',
                'business_units.client_id',
                'vacancies.business_unit_id',
                'vacancies.job_id',
                'vacancies.shift_id',
                'business_units.business_unit_name',
                'business_units.reference_code',
                'business_units.email',
                'business_units.formatted_number',
                'business_units.address_line',
                'business_units.description',
                'jobs.name AS job_title',
                'jobs.color',
                'shifts.shift_name AS shift_type',
                'vacancies.is_publish',
                'vacancies.notes',
                'vacancies.applied_count',
                'vacancies.used_count AS assigned_count',
                'vacancies.total_numbers AS total_space')->paginate(15);
                
        $vacancies=$vacancies->toArray();

        return response()->json([
        'success'=>[
          'status_code' => 200,
          'status' => 'Success',
          'time_format' => DB::table('agencies')->first()->time_format,
          'current_page' => count($vacancies['data'])>0 ? $vacancies['current_page'] : 0,
          'total_results' => $vacancies['total'],
          'data' => $vacancies['data']
        ]
        ],200);

      }
    }


    public function vacancy (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'vacancy_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
          $vacancy_id = $request->input('vacancy_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          //getting vacancies
            $vacancy = DB::table('vacancies')
                    ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                    ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                    ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                    ->where('vacancies.id', $vacancy_id)
                    ->select('vacancies.id',
                    'vacancies.code',
                    'vacancies.on_date',
                    DB::raw('DATE_FORMAT(vacancies.start_time, "'.dateTimeFormat().'") as start_time'),
                    DB::raw('DATE_FORMAT(vacancies.end_time, "'.dateTimeFormat().'") as end_time'),
                    'vacancies.break',
                    'vacancies.total_hours',
                    'business_units.client_id',
                    'vacancies.business_unit_id',
                    'vacancies.job_id',
                    'vacancies.shift_id',
                    'business_units.business_unit_name',
                    'business_units.reference_code',
                    'business_units.email',
                    'business_units.formatted_number',
                    'business_units.address_line',
                    'business_units.description',
                    'jobs.name AS job_title',
                    'jobs.color',
                    'shifts.shift_name AS shift_type',
                    'vacancies.is_publish',
                    'vacancies.notes',
                    'vacancies.applied_count',
                    'vacancies.used_count AS assigned_count',
                    'vacancies.total_numbers AS total_space')->first();
          

          return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success',
              'time_format' => DB::table('agencies')->first()->time_format,
              'data' => $vacancy
            ]
          ],200);

      }
    }

}
