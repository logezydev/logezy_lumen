<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\SmsDevice;
use App\Tenants\Agency;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use GuzzleHttp;
use Illuminate\Database\DatabaseManager;

class SmsController extends BaseController
{
  public function register_mobile(Request $request)
  {
    $validator = Validator::make(
      array(
        'mobile_number' => $request->mobile_number,
        'device_token' => $request->device_token,
      ),
      array(
        // 'mobile_number' => 'required|unique:sms_devices',
        'mobile_number' => 'required',
        'device_token' => 'required',
      )
    );

    if ($validator->fails()) {
      $messages = $validator->errors();
      //Determining If Messages Exist For A Field
      if ($messages->has('mobile_number')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('mobile_number')
          ]
        ], 400);
      } else if ($messages->has('device_token')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('device_token')
          ]
        ], 400);
      }
    } else {
      $mobile_number = $request->input('mobile_number');
      $device_token = $request->input('device_token');

      $Sms = SmsDevice::where('mobile_number', $mobile_number)->first();

      if ($Sms) {
        $Sms->device_token = $device_token;
        $Sms->save();
        if ($Sms->id) {
          return response()->json([
            'success' => [
              'status_code' => 201,
              'status' => 'Success',
              'data' => "Mobile number updated successfully"
            ]
          ], 201);
        } else {
          return response()->json([
            'error' => [
              'status_code' => 400,
              'status' => 'Error',
              'data' => "Something went wrong"
            ]
          ], 400);
        }
      } else {
      
        $SmsDevice = new SmsDevice;
        $SmsDevice->mobile_number = $mobile_number;
        $SmsDevice->device_token = $device_token;
        $SmsDevice->save();
  
        if ($SmsDevice->id) {
          return response()->json([
            'success' => [
              'status_code' => 201,
              'status' => 'Success',
              'data' => "Mobile number registered successfully"
            ]
          ], 201);
        } else {
          return response()->json([
            'error' => [
              'status_code' => 400,
              'status' => 'Error',
              'data' => "Something went wrong"
            ]
          ], 400);
        }
      }
    }
  }

  public function send_sms(Request $request)
  {

    $validator = Validator::make(
      array(
        'subdomain' => $request->subdomain,
        'mobile_number' => $request->mobile_number,
        'message' => $request->message,
        'fcm_token' => $request->fcm_token,
      ),
      array(
        'subdomain' => 'required',
        'mobile_number' => 'required',
        'message' => 'required',
        'fcm_token' => 'required',
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
      //Determining If Messages Exist For A Field
      if ($messages->has('subdomain')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('subdomain')
          ]
        ], 400);
      } else if ($messages->has('mobile_number')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('mobile_number')
          ]
        ], 400);
      } else if ($messages->has('message')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('message')
          ]
        ], 400);
      } else if ($messages->has('fcm_token')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('fcm_token')
          ]
        ], 400);
      }
    } else {

      $subdomain = $request->input('subdomain');
      $mobile_number = $request->input('mobile_number');
      $message = $request->input('message');
      $fcm_token = $request->input('fcm_token');

      config(['database.connections.mysql.database' => 'logezy_' . $subdomain, 'database.default' => 'mysql']);
      DB::reconnect('mysql');

      $agency = Agency::first();
      $agency_mobile = $agency->mobile_number != "" ? $agency->mobile_number : '';

      config(['database.connections.mysql.database' => 'logezycore', 'database.default' => 'mysql']);
      DB::reconnect('mysql');

      if ($agency_mobile) {

        $regNo = SmsDevice::where('mobile_number', $agency_mobile)->first();

        if ($regNo && $regNo->device_token) {

          $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

          $notification = [
            "title" => "Ionic 4 Notification",
            "body" => "This notification sent from POSTMAN using Firebase HTTP protocol",
            "sound" => "default",
            "click_action" => "FCM_PLUGIN_ACTIVITY",
            "icon" => "fcm_push_icon",
            "email" => $agency->email
          ];

          $data = [
            "mobileNo" => $mobile_number,
            "message" => $message
          ];

          $fcmData = [
            "notification" => $notification,
            "data" => $data,
            "to" => $regNo->device_token,
            "priority" => "high",
            "restricted_package_name" => ""
          ];


          $headers = [
            'Authorization: ' . $fcm_token . '',
            'Content-Type: application/json'
          ];


          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $fcmUrl);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmData));
          $result = curl_exec($ch);
          curl_close($ch);


          $result = json_decode($result);

          if ($result->success == 1) {
            return response()->json([
              'success' => [
                'status_code' => 200,
                'status' => 'Success',
                'data' => "Message send successfully"
              ]
            ], 200);
          } else {
            return response()->json([
              'success' => [
                'status_code' => 400,
                'status' => 'Success',
                'data' => "Something went wrong"
              ]
            ], 400);
          }
          return response()->json([
            'success' => [
              'status_code' => 201,
              'status' => 'Success',
              'data' => "Message send successfully"
            ]
          ], 201);
        } else {
          return response()->json([
            'error' => [
              'status_code' => 400,
              'status' => 'Error',
              'data' => "Agency mobile number is not registered"
            ]
          ], 400);
        }
      } else {
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'data' => "Please add agency mobile number"
          ]
        ], 400);
      }
    }
  }
}
