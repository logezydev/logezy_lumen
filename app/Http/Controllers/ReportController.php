<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Report;
use App\Tenants\AssignVacancy;
use App\Tenants\AssignVacancyDetail;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Storage;

class ReportController extends BaseController
{
  public function get_payroll_reports (Request $request) {

    $validator = Validator::make(
                      array(
                      'week' => $request->week, 
                      'month' => $request->month, 
                      'year' => $request->year,
                      'candidate_id' => $request->candidate_id,
                      'subdomain' => $request->subdomain
                      ), array(
                      "week" => "nullable|numeric|min:1|max:54", 
                      "month" => "nullable|date_format:m", 
                      "year" => "required|date_format:Y",
                      "candidate_id" => "required",
                      "subdomain" => "required"
                  )
              );

    if ($validator->fails()) {

          $messages = $validator->errors();
          //Determining If Messages Exist For A Field
          if ($messages->has('week')) {
              //Show custom message
              return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => $messages->first('week')
                ]
              ],400);
          }
          else if ($messages->has('month')) {
              //Show custom message
              return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => $messages->first('month')
                ]
              ],400);
          }
          else if ($messages->has('year')) {
              //Show custom message
              return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => $messages->first('year')
                ]
              ],400);
          }
          else if ($messages->has('candidate_id')) {
              //Show custom message
              return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => $messages->first('candidate_id')
                ]
              ],400);
          }
          else if ($messages->has('subdomain')) {
              //Show custom message
              return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => $messages->first('subdomain')
                ]
              ],400);
          }


    } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $candidate_id = $request->candidate_id;
        $week = $request->week;
        $month = $request->month;
        $year = $request->year;

        if($request->has('week') && !empty($week) && $request->has('year') && !empty($year)){
          $weekStartEnd = $this->getWeek(sprintf("%02d", $week), $year);
          return Report::getWeeklyReports($weekStartEnd, $candidate_id);
        } else {
          $timestamp    = strtotime($year.'-'.$month.'-01');
          $first = date('Y-m-03', $timestamp);
          $first_week = date('W', strtotime($first));
          $last  = date('Y-m-t', $timestamp); 
          $last_week  = date('W', strtotime($last)); 
          $shiftDetails = [];
          if($first_week == 52 && $first_week>$last_week) {
            $first_week = 01;
          }
          for($i=$first_week;$i<=$last_week;$i++) {
              $weekStartEnd = $this->getWeek(sprintf("%02d", $i), $year);
              $from = date("d-m-Y", strtotime($weekStartEnd[0]));
              $to = date("d-m-Y", strtotime($weekStartEnd[1]));
              $shiftDetails[$from.' to '.$to] = Report::getWeeklyReports($weekStartEnd, $candidate_id);
          }
          return $shiftDetails;
        }
    }

  }
  public static function getWeek($week, $year) {
    return [date("Y-m-d", strtotime("{$year}-W{$week}-1")), date("Y-m-d", strtotime("{$year}-W{$week}-7"))];
  }

}
