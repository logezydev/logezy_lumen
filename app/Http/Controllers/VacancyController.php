<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\Day;
use App\Tenants\CustomHoliday;
use App\Tenants\Holiday;
use App\Tenants\CandidateVacancy;
use App\Tenants\Vacancy;
use App\Tenants\RoleUser;
use App\Tenants\Agency;
use App\Tenants\Shift;
use App\Tenants\Helper;
use App\Tenants\AssignVacancy;
use App\Tenants\CandidateRateCard;
use App\Tenants\EmploymentType;
use App\Tenants\VacancyDetail;
use App\Tenants\VacancyStaffRate;
use App\Tenants\Notification;
use App\Jobs\PublishAssignedShiftsJob;
use App\Traits\ActivityLog\ActivityCustomLogTrait;
use App\Traits\PushNotificationTrait;
use App\Traits\SMSTraits;
use App\Traits\EmailTraits;
use App\Traits\UserTraits;
use App\Helpers\SendMailGun;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;

class VacancyController extends BaseController
{

    /**
     * Store new vacancy
     */
    public function addVacancies(Request $request) {

      $validator = Validator::make(
                        array(
                        'business_unit_id' => $request->business_unit_id,
                        'job_id' => $request->job_id,
                        'dates' => $request->dates,
                        'shift_id' => $request->shift_id,
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_numbers' => $request->total_numbers,
                        'subdomain' => $request->subdomain
                        ), array(
                        'business_unit_id' => 'required',
                        'job_id' => 'required',
                        'dates' => 'required',
                        'shift_id' => 'required',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'notes' => 'nullable',
                        'total_numbers' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('business_unit_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('job_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_id')
                  ]
                ],400);
            }
            else if ($messages->has('dates')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('dates')
                  ]
                ],400);
            }
            else if ($messages->has('shift_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('shift_id')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_numbers')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_numbers')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
        try {
          $datesArr = explode(",", $request->dates);

          if ($datesArr) {
            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');
            foreach ($datesArr as $date) {
              $request->request->add(['on_date' => $date]);
              Vacancy::addVacancy($request);
            }
          }

          return response()->json([
            'success'=>[
              'status_code' => 201,
              'status' => 'Success', 
              'message' => "Vacancy created successfully"
            ]
          ],201);
        } catch (\Illuminate\Database\QueryException $ex) {

            return response()->json([
              'Error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $ex->getMessage()
              ]
            ],400);
        }
      }

    }

    /**
     * Show the resource
     */
    public function show($subdomain, $vacancy_id) {
        return $vacancy = Vacancy::find($vacancy_id);
    }

    /**
     * Update vacancy
     */
    public function update_vacancy(Request $request) {

      $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id,
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_hours' => $request->total_hours,
                        'subdomain' => $request->subdomain
                        ), array(
                        'vacancy_id' => 'required',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'notes' => 'nullable',
                        'total_hours' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_hours')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_hours')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $vacancy = Vacancy::find($request->vacancy_id);
        //checking for night shift
        $start = date("H:i", strtotime($request->start_time));
        $end = date("H:i", strtotime($request->end_time));
        if ($start >= $end) {
            $endDate = date('Y-m-d', strtotime('+1 day', strtotime($vacancy->on_date)));
        } else {
            $endDate = $vacancy->on_date;
        }
        //calculate total hours
        $inputs = array(
            'on_date'    => $vacancy->on_date,
            'start_time' => strtotime($vacancy->on_date. ' '.$request->start_time),
            'end_time'   => strtotime($endDate. ' '.$request->end_time) ,
            'break'      => $request->break ?? 0,
        );
        $totalHours = calcTotalHours($inputs);
        //fetching rates
        // $inputArr = array(
        //     'business_unit_id'  => $vacancy->business_unit_id,
        //     'job_id'            => $vacancy->job_id,
        //     'on_date'           => $vacancy->on_date,
        //     'shift_id'          => $vacancy->shift_id,
        // );
        // $rates = Helper::getClientStaffRates($inputArr);

        // if( isset($request->client_rate) && ( $request->client_rate == null || $request->client_rate == "0.00") ) {
        //     $clientRate = !empty($rates->client_rate) ? $rates->client_rate : 0;
        // } else {
        //     $clientRate = $request->client_rate;
        // }
        // $staffRates  = [];
        // if( $rates ) {
        //     $staffRates  = $rates->staff_rate_cards ?? [];
        // }
        // $vacancy->shift_id         = isset($request->shift_id) ? $request->shift_id : null;
        $vacancy->start_time       = isset($request->start_time) ? date( "Y-m-d H:i:s", strtotime($vacancy->on_date. ' '.$request->start_time)) : null;
        $vacancy->end_time         = isset($request->end_time) ? date( "Y-m-d H:i:s", strtotime($endDate. ' '.$request->end_time)) : null;
        // $vacancy->on_date          = isset($request->on_date) ? date( "Y-m-d", strtotime($request->on_date) ) : null;
        $vacancy->break            = isset($request->break) ? $request->break : 0;
        $vacancy->total_hours      = $totalHours ?? 0;
        $vacancy->client_rate      = 0;
        $vacancy->notes            = isset($request->notes) ? $request->notes : null;
        if (isset($request->space_left) ) {
            $space = (int)$vacancy->total_numbers - (int)$vacancy->used_count;
            if($space != $request->space_left) {
                $new_space = ((int)$request->space_left - (int)$space)+(int)$vacancy->total_numbers;
                $vacancy->total_numbers    = $new_space;
            }
        }
        $oldVacancy = DB::table('vacancies')->find($request->vacancy_id);
        //Populating vacancy staff rates
        if($vacancy->save()) {
          if ( ($oldVacancy->break != $request->break) || (strtotime($oldVacancy->on_date. ' '.$request->start_time) == strtotime($oldVacancy->start_time) && strtotime($endDate. ' '.$request->end_time) == strtotime($oldVacancy->end_time)) ) {

                    $request->request->add(['on_date' => date('Y-m-d', strtotime($oldVacancy->on_date))]);
                    $request->request->add(['business_unit_id' => $vacancy->business_unit_id]);
                    $request->request->add(['job_id' => $vacancy->job_id]);
                    $request->request->add(['shift_id' => $vacancy->shift_id]);
                    //find matching ratecards
                    $rateCards = splitTimeRangeIntoSlots($request);
                    // dd($rateCards);
                    //delete entries on vacancy id if exists
                    $detailIDs = VacancyDetail::where('vacancy_id', $vacancy->id)->pluck('id')->all();
                    if ($detailIDs) VacancyDetail::destroy($detailIDs);
                    $staffrateIDs = VacancyStaffRate::where('vacancy_id', $vacancy->id)->pluck('id')->all();
                    if ($staffrateIDs) VacancyStaffRate::destroy($staffrateIDs);

                    //adding vacancy details
                    foreach ($rateCards as $rateCard) {
                        $vacancyDetail                   = new VacancyDetail;
                        $vacancyDetail->vacancy_id       = $vacancy->id;
                        $vacancyDetail->day_id           = $rateCard['day_id'];
                        $vacancyDetail->shift_id         = $rateCard['shift_id'];
                        $vacancyDetail->rate_type        = $rateCard['rate_type'];
                        $vacancyDetail->start_time       = date( "Y-m-d H:i:s", strtotime($rateCard['start_time'])) ?? null;
                        $vacancyDetail->end_time         = date( "Y-m-d H:i:s", strtotime($rateCard['end_time'])) ?? null;
                        $vacancyDetail->break            = $rateCard['break'] ?? 0;
                        $vacancyDetail->total_hours      = $rateCard['total_hours'] ?? 0;
                        $vacancyDetail->client_rate      = $rateCard['client_rate'] ?? 0;
                        $vacancyDetail->save();
                        //Populating vacancy staff rates
                        if( $rateCard['staff_rates'] ) {
                            foreach($rateCard['staff_rates'] as $staffRate) {
                                // if(isset($staffRate['value'])){
                                    $vacancy_staff_rate                     = new VacancyStaffRate;
                                    $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                    $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                    $vacancy_staff_rate->employment_type_id = $staffRate->employment_type_id;
                                    $vacancy_staff_rate->staff_rate         = $staffRate->staff_rate;
                                    $vacancy_staff_rate->save();
                                // }
                            }
                        } else {
                            $staffRates = EmploymentType::where('status','active')->where('is_deleted',0)->get();
                            foreach ($staffRates as $staffRate) {
                                // if(isset($staffRate['value'])){
                                    $vacancy_staff_rate                     = new VacancyStaffRate;
                                    $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                    $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                    $vacancy_staff_rate->employment_type_id = $staffRate->id;
                                    $vacancy_staff_rate->staff_rate         = $staffRate->value;
                                    $vacancy_staff_rate->save();
                                // }
                            }
                        }
                    }
                }
            // $staffRateArr    = isset($request->staff_rates) ? $request->staff_rates : [];
            // if($staffRateArr) {
            //     $checkEmpty = [];
            //     foreach($staffRateArr as $staffRate) {
            //         if( VacancyStaffRate::find($staffRate['id']) && $staffRate['staff_rate'] != '0.00' ){
            //             $checkEmpty [] = $staffRate;
            //             $vacancy_staff_rate                     = VacancyStaffRate::find($staffRate['id']);
            //             $vacancy_staff_rate->vacancy_id         = $id;
            //             $vacancy_staff_rate->employment_type_id = $staffRate['employment_type_id'];
            //             $vacancy_staff_rate->staff_rate         = $staffRate['staff_rate'];
            //             $vacancy_staff_rate->save();
            //         }
            //     }
            //     if (!$checkEmpty) {
            //         if( $staffRates ) {
            //             foreach($staffRateArr as $staffRate) {
            //                 $vacancy_staff_rate = VacancyStaffRate::find($staffRate['id']);
            //                 $vacancy_staff_rate->delete();
            //             }
            //             foreach($staffRates as $staffRate) {
            //                 // if(isset($staffRate['value'])){
            //                     $vacancy_staff_rate                     = new VacancyStaffRate;
            //                     $vacancy_staff_rate->vacancy_id         = $vacancy->id;
            //                     $vacancy_staff_rate->employment_type_id = $staffRate['id'];
            //                     $vacancy_staff_rate->staff_rate         = $staffRate['value'];
            //                     $vacancy_staff_rate->save();
            //                 // }
            //             }
            //         }
            //     }
            // }
        }

        return response()->json([
          'success'=>[
            'status_code' => 201,
            'status' => 'Success', 
            'message' => "Vacancy updated successfully"
          ]
        ],201);
      }

    }
    /**
     * Assign candidate on multiple dates
     */
    public function assign_multiple_dates(Request $request)
    {

      $validator = Validator::make(
                        array(
                        'business_unit_id' => $request->business_unit_id,
                        'job_id' => $request->job_id,
                        'candidate_id' => $request->candidate_id,
                        'dates' => $request->dates,
                        'shift_id' => $request->shift_id,
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_hours' => $request->total_hours,
                        'subdomain' => $request->subdomain,
                        'is_publish' => $request->is_publish,
                        'text' => $request->text,
                        'push' => $request->push,
                        'email' => $request->email,
                        ), array(
                        'business_unit_id' => 'required|numeric',
                        'job_id' => 'required|numeric',
                        'candidate_id' => 'required|numeric',
                        'dates' => 'required',
                        'shift_id' => 'required|numeric',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'notes' => 'nullable',
                        'total_hours' => 'required',
                        'subdomain' => 'required',
                        'is_publish' => 'nullable|boolean',
                        'text' => 'nullable|boolean',
                        'push' => 'nullable|boolean',
                        'email' => 'nullable|boolean',
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('business_unit_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('job_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('dates')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('dates')
                  ]
                ],400);
            }
            else if ($messages->has('shift_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('shift_id')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_hours')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_hours')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('is_publish')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('is_publish')
                  ]
                ],400);
            }
            else if ($messages->has('text')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('text')
                  ]
                ],400);
            }
            else if ($messages->has('push')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('push')
                  ]
                ],400);
            }
            else if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }


      } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $errResponse = [];
        $sucResponse = [];
        $assignDates = [];
        $shift       = [];
        $notify_data = [];
        $publishedShifts = [];
        $candidateDetails = Candidate::find($request->candidate_id);

        $datesArr = explode(",", $request->dates);
        if ($datesArr) {
          foreach( $datesArr as $date){
              //checking for night shift
              $start = date("H:i", strtotime($request->start_time));
              $end = date("H:i", strtotime($request->end_time));
              $startDate = $date;
              if ($start >= $end) {
                  $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
              } else {
                  $endDate = $date;
              }

              //Checking before assigning candidate
              $inputArr = array(
                  'candidate_id'      => $request->candidate_id,
                  'business_unit_id'  => $request->business_unit_id,
                  'job_id'            => $request->job_id,
                  'on_date'           => $date,
                  'startDate'         => $startDate,
                  'endDate'           => $endDate,
                  'shift_id'          => $request->shift_id,
                  'start_time'        => $start,
                  'end_time'          => $end,
                  'total_hours'       => $request->total_hours,
              );
              $assignValidations = AssignVacancy::assignValidations($inputArr);

              if ($assignValidations != "false") {
                  array_push($errResponse, $assignValidations);
              } else {
                  $request->request->add(['on_date' => $date]);
                  $request->request->add(['total_numbers' => 1]);
                  $vacancy_id = Vacancy::addVacancy($request);
                  if ($vacancy_id != '') {
                      $request->request->add(['vacancy_id' => $vacancy_id]);
                      $request->request->add(['status' => 'pending']);
                      $assignDetails = AssignVacancy::assign_candidate($request);
                      if ($assignDetails) {
                          array_push($assignDates, date("d-m-Y", strtotime($date)));

                          $notification_services['email'] = 0; 
                          $notification_services['text']  = 0; 
                          $notification_services['push']  = 0; 
      
                          // Check if push notification is selected
                          if($request->push == 1){ 
                              $notification_services['push'] = 1; 
                              PushNotificationTrait::shiftAssign($assignDetails, $candidateDetails);
                          }
                          // Check if text notification is selected
                          if($request->text == 1){ 
                              $notification_services['text']  = 1; 
                              SMSTraits::vacancyAssign($assignDetails, $candidateDetails, $request->subdomain);
                          }
                          // Check if text notification is selected
                          if($request->email == 1){ 
                              $notification_services['email']  = 1;
                              $candidateEmail = RoleUser::where('role_id', 6)->where('user_id', $candidateDetails->user_id)->first()->email;
                              $notification_services['email']  = 1;
                              $assign_mail_variables  = [
                                  '[candidate_name]'  => $assignDetails->candidate_name,
                                  '[client]'          => $assignDetails->vacancy->client,
                                  '[business_unit]'   => $assignDetails->business_unit,
                                  '[address]'         => $assignDetails->vacancy->address_line,
                                  '[date]'            => $assignDetails->on_date,
                                  '[shift_start]'     => $assignDetails->start_time,
                                  '[shift_end]'       => $assignDetails->end_time,
                                  '[detail]'          => $assignDetails->notes, 
                                  '[booking_code]'    => $assignDetails->code,
                                  '[agency_name]'     => $assignDetails->vacancy->agency,
                              ];
                              $agency = Agency::first();
                              $mail_content                 = EmailTraits::getMessegeConetent($assign_mail_variables,'Assign Vacancy');
                              $mail_content['agency_email'] = $agency->email;
                              $mail_content['agency_name']  = $agency->name;
                              $mail_content['agency_logo']  = $agency->agency_logo;
                              
                              // dispatch(new PublishAssignedShiftsJob($mail_content));
                          }
                          
                          // array_push($shift, json_decode(json_encode($assignDetails), true));

                          // if ( ( isset($request->is_publish) && $request->is_publish == 1 ) && isset($request->notify) ) {
          
                          //     array_push($publishedShifts, $assignDetails);
          
                          //     $notify = Notification::AddPublishedShiftNotication($assignDetails);
                              
                          // }

                      } else {
                          array_push($errResponse, 'Can not assign '.$candidateDetails->full_name.'. Something went wrong!.');
                      }
                  } else {
                      array_push($errResponse, 'Can not assign '.$candidateDetails->full_name.'. Something went wrong!.');
                  }
              }
          }
        }

        if ($assignDates) {
          array_push($sucResponse, $candidateDetails->full_name.'  Successfully  assigned on '.implode(", ",$assignDates));
        }

      return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $errResponse
          ],
          'success'=>[
            'status_code' => 201,
            'status' => 'Success', 
            'message' => $sucResponse
          ],
        ]);
        // return ['error' => $errResponse,'success' => $sucResponse];
      }
    }

    /**
     * Vacancies
     */
    public function getVacancies (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain,
                        'shift' => $request->shift,
                        'region' => $request->region,
                        'business_unit' => $request->business_unit
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'candidate_id' => 'required',
                        'subdomain' => 'required',
                        'shift' => 'nullable',
                        'region' => 'nullable',
                        'business_unit' => 'nullable'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        if ($request->has('pagination') && $request->pagination == 1 && !isset($request->page)) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Please specify the page"
              ]
            ],400);
        }

          $date_from = $request->input('date_from');
          $date_to = $request->input('date_to');
          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

            $candidate = DB::table('candidates')->where('id', $candidate_id)->select('id','has_access','is_deleted','employment_types_id', 'candidate_status_id')->first();
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 

            if ( $candidate->candidate_status_id != 1 ) {
                return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => "Please check and fill your profile to get full access."
                    ]
                  ],400);
            }
            //getting candidate job id's
            $job_ids = CandidateJob::where('candidate_id', $candidate_id)->pluck('job_id')->all();

            $shifts = isset($request->shift) && !empty($request->shift) ? Shift::where("shift_name", $request->shift)->pluck('id')->all() : [];
            $business_unit_id = isset($request->business_unit_id) && !empty($request->business_unit_id) ? $request->business_unit_id : '';
            //region
            $business_units = [];

            if(isset($request->region_id)) {
                $buIDs = DB::select("SELECT `business_unit_id` from `businessunit_regions` where `region_id` = ".$request->region_id."");
                $business_units = count($buIDs)>0 ? array_column($buIDs, 'business_unit_id') : [];
            } else {
                $regions = DB::table('candidate_regions')->where('candidate_id', $candidate_id)->pluck('region_id')->all();
                if($regions) {
                    //get business unit id of candidate with Regions
                    $buIDs = DB::table('businessunit_regions')->whereIn('region_id', $regions)->pluck('business_unit_id')->all();
                    $business_units = array_unique(array_values($buIDs));
                } else { 
                    //Business units id without region
                    $buIDs = DB::table('business_units')->leftJoin('businessunit_regions', 'business_units.id', '=', 'businessunit_regions.business_unit_id')->where('business_units.status', 'active')->where('business_units.is_deleted', 0)->whereNull('businessunit_regions.business_unit_id')->pluck('business_units.id')->all();
                    $business_units = array_unique(array_values($buIDs));
                }
            }
            
            //closed vacancies
            $closedVacancies = DB::table('assign_vacancies')
                ->whereIn('status',['pending', 'invoiced', 'approved', 'rejected'])
                ->where('is_deleted', 0)
                ->where('candidate_id', $candidate_id)
                ->whereBetween('on_date', [$date_from, $date_to])
                ->pluck('vacancy_id')->all();
            //getting agency seettings for publish vacancy by job role
            $is_publish_by_job = Agency::value('is_publish_by_job');

            $vacancies = DB::table('vacancies')
                    ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                    ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                    ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                    ->whereBetween('vacancies.on_date', [$date_from, $date_to])
                    ->whereIn('vacancies.job_id', $job_ids)
                    ->whereRaw('vacancies.total_numbers > vacancies.used_count')
                    // ->whereRaw("find_in_set($candidate_id, vacancies.published_ids)")
                    ->orderBy('vacancies.on_date', 'asc')->where('vacancies.is_publish', 1)
                    ->where(function($query) use ($shifts, $business_unit_id, $request, $business_units, $closedVacancies) {
                        if (isset($shifts) && !empty($shifts)) {
                            $query->whereIn('vacancies.shift_id', $shifts);
                        }
                        if (isset($business_unit_id) && !empty($business_unit_id)) {
                            $query->where('vacancies.business_unit_id', $business_unit_id);
                        }
                        // if (isset($request->region_id)) {
                        //     $query->whereIn('vacancies.business_unit_id', $business_units);
                        // }
                        if (!empty($closedVacancies)) {
                            $query->whereNotIn('vacancies.id', $closedVacancies);
                        }
                    })
                    ->where(function ($query) use($is_publish_by_job, $candidate_id){
                        if ($is_publish_by_job) {
                            $query->where('vacancies.is_publish_by_job', 1)->orWhereRaw("find_in_set($candidate_id, published_ids)");
                        } 
                        else {
                            $query->whereRaw("find_in_set($candidate_id, vacancies.published_ids)");
                        }
                    })
                    ->whereIn('vacancies.business_unit_id', $business_units)
                    ->where('is_publish', 1)
                    ->select('vacancies.id',
                    'vacancies.code',
                    'vacancies.on_date',
                    DB::raw('DATE_FORMAT(vacancies.start_time, "'.dateTimeFormat().'") as start_time'),
                    DB::raw('DATE_FORMAT(vacancies.end_time, "'.dateTimeFormat().'") as end_time'),
                    'vacancies.break',
                    'vacancies.total_hours',
                    'business_units.client_id',
                    'vacancies.business_unit_id',
                    'vacancies.job_id',
                    'vacancies.shift_id',
                    'business_units.business_unit_name',
                    'business_units.reference_code',
                    'business_units.email',
                    'business_units.formatted_number',
                    'business_units.address_line',
                    'business_units.latitude',
                    'business_units.longitude',
                    'business_units.profile_pdf',
                    'business_units.description',
                    'jobs.name AS job_title',
                    'jobs.color',
                    'shifts.shift_name AS shift_type',
                    'vacancies.is_publish',
                    'vacancies.is_publish_by_job',
                    'vacancies.notes',
                    'vacancies.applied_count',
                    'vacancies.used_count AS assigned_count',
                    DB::raw( 'vacancies.total_numbers - vacancies.used_count as space_left'),
                    'vacancies.total_numbers AS total_space');

            if (isset($request->pagination) && $request->pagination == 1) {
                $vacancies = $vacancies->paginate(10);
                $current_page = $vacancies->currentPage();
                $vacancies = $vacancies->getCollection();
                //gets only vacancy id
                $vacancyIds = collect($vacancies)->pluck('id');
            } else {
                $vacancies = $vacancies->get();
                $current_page = 0;
                //gets only vacancy id
                $vacancyIds = collect($vacancies)->pluck('id');
            }

            //get assigned and applied data with vacancy ids

            // $candidateAssignDatas = DB::table('assign_vacancies')
            //     ->select('id', 'code', 'candidate_id', 'vacancy_id', 'status')
            //     ->whereIn('vacancy_id', $vacancyIds)
            //     ->where('candidate_id', $candidate_id)
            //     ->whereIn('status',['pending', 'invoiced', 'approved', 'rejected'])
            //     ->where('is_deleted', 0)->get();
            $candidateVacanciesApplied = DB::table('assign_vacancies')
                ->select('id', 'candidate_id', 'vacancy_id')
                ->where('status', 'applied')
                ->where('is_deleted', 0)
                ->whereIn('vacancy_id', $vacancyIds)
                ->get();
            $candidateVacancyDetails = VacancyDetail::whereIn('vacancy_id', $vacancyIds)->get();  
            $candidateVacancyStaffRates = VacancyStaffRate::where('employment_type_id', $candidate->employment_types_id)
                ->whereIn('vacancy_id', $vacancyIds)
                ->get();
            $candidateRates = CandidateRateCard::where('employment_type_id', $candidate->employment_types_id)
                ->where('candidate_id', $candidate->id)
                ->get();


            //collect datas

            // $assignDatas = collect($candidateAssignDatas);
            $vacanciesApplied = collect($candidateVacanciesApplied);
            $vacanciesDetails = collect($candidateVacancyDetails);
            $vacanciesStaffRates = collect($candidateVacancyStaffRates);
            $candidateRates = collect($candidateRates);

            $vacancies->map(function ($vacancy, $k) use($vacanciesApplied, $vacanciesDetails, $vacanciesStaffRates, $candidateRates, $candidate) { 
                // $assigned = $assignDatas->where('vacancy_id', $vacancy->id)->first();
                // $vacancy->assigned_status = $assigned ? $assigned->status : null;
                // $vacancy->assigned_vacancies = $assigned ? $assigned : [];
                $vacancy->assigned_status = null;
                $vacancy->assigned_vacancies = [];
                $vacancy->applied = $vacanciesApplied->where('vacancy_id', $vacancy->id)->values(); 

                //staff rate calculation
                $details = $vacanciesDetails->where('vacancy_id', $vacancy->id)->sortBy('id')->values();  
                $staffRateArr = [];
                if ($candidate->employment_types_id == '') {
                    $staffRateArr[] = array(
                              'employment_type_id'=> '',
                              'title'=> '',
                              'start_time'=> '',
                              'end_time'=> '',
                              'break'=> '',
                              'staff_rate'=> 0
                            );
                } else {
                    if (count($details)>0) {
                        $previousValue = null;
                        foreach($details as $detail){
                            $vacancydetails = $vacanciesDetails->where('id', $detail->id)->first();
                            $staff_rate = $vacanciesStaffRates->where('vacancy_id', $vacancy->id)
                                            ->where('vacancy_detail_id', $detail->id)
                                            ->first();
                            $staff_rate_amount = (!$staff_rate) ? 0 : $staff_rate->staff_rate;
                            $candidate_staff_rate = 0;
                            if ($vacancydetails->day_id != 8) {
                                $candidateRateCards = $candidateRates->where('business_unit_id', $vacancy->business_unit_id)
                                            ->where('job_id', $vacancy->job_id)
                                            ->where('day_id', $vacancydetails->day_id)
                                            ->where('shift_id', $vacancydetails->shift_id)
                                            ->values(); 
                                $candidate_rate = '';
                                if (count($candidateRateCards)>0) {
                                    foreach ($candidateRateCards as $rateCard) {
                                        $start = date("H:i", strtotime($rateCard->start_time));
                                        $end = date("H:i", strtotime($rateCard->end_time));
                                        // $date = date('Y-m-d', strtotime($vacancy->on_date));
                                        $week = date("W", strtotime($vacancy->on_date));
                                        if($previousValue) {
                                          $week = $previousValue>$rateCard->day_id ? $week+1 : $week;
                                        }
                                        $year = date("Y", strtotime($vacancy->on_date));
                                        $date = date('Y-m-d', strtotime($year."W".$week.$rateCard->day_id));
                                        if ($start >= $end) {
                                            $startDate = $date;
                                            $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                                        }
                                        else {
                                            $startDate = $date;
                                            $endDate = $date;
                                        }
                                        if ( strtotime($startDate.' '.$rateCard->start_time) < strtotime($vacancydetails->end_time) && strtotime($vacancydetails->start_time) < strtotime($endDate.' '.$rateCard->end_time) ) {
                                            $candidate_rate = $rateCard;
                                        }
                                    } 
                                }
                                $candidate_staff_rate = $candidate_rate == '' ? 0 : $candidate_rate->staff_rate;
                                $previousValue = $vacancydetails->day_id;
                            }
                            $staffRateArr[] = array(
                              'employment_type_id'=>$candidate->employment_types_id,
                              'title'=> (!$staff_rate) ? '' : $staff_rate->employment_type,
                              'start_time'=> $vacancydetails->start_time,
                              'end_time'=> $vacancydetails->end_time,
                              'break'=> $vacancydetails->break,
                              'staff_rate'=> ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount
                            );
                        }
                    } else {
                        $staffRateArr[] = array(
                            'employment_type_id'=> '',
                            'title'=> '',
                            'start_time'=> '',
                            'end_time'=> '',
                            'break'=> '',
                            'staff_rate'=> 0
                        );
                    }  
                }  
                $vacancy->staff_rates = $staffRateArr;
                //business unit array
                $vacancy->business_unit = array(
                    'id'=> $vacancy->business_unit_id,
                    'reference_code'=> $vacancy->reference_code,
                    'business_unit_name'=> $vacancy->business_unit_name,
                    'formatted_number'=> $vacancy->formatted_number,
                    'email'=> $vacancy->email,
                    'address_line'=> $vacancy->address_line,
                    'latitude'=> $vacancy->latitude,
                    'longitude'=> $vacancy->longitude,
                    'profile_pdf'=> $vacancy->profile_pdf,
                    'description'=> $vacancy->description,
                    'client_id'=> $vacancy->client_id,
                );

                return $vacancy;
            });

            // if (isset($request->pagination) && $request->pagination == 1) {
            //     $vacancies = $vacancies->getCollection();
            // } else {
            //     $vacancies = $vacancies;
            // }

            //business units of the given candidate in the given date ranges
            $business_unit_ids = DB::table('vacancies')
                    ->whereBetween('on_date', [$date_from, $date_to])
                    ->whereIn('job_id', $job_ids)
                    ->whereRaw('total_numbers > used_count')
                    ->where('is_publish', 1)
                    ->where(function ($query) use($is_publish_by_job, $candidate_id){
                        if ($is_publish_by_job) {
                            $query->where('is_publish_by_job', 1)->orWhereRaw("find_in_set($candidate_id, published_ids)");
                        } 
                        else {
                            $query->whereRaw("find_in_set($candidate_id, published_ids)");
                        }
                    })
                    ->whereIn('business_unit_id', $business_units)
                    ->pluck('business_unit_id')
                    ->toArray();
            return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'time_format' => DB::table('agencies')->first()->time_format,
                  'current_page' => $current_page,
                  'business_unit_ids' => implode(',', array_values(array_unique($business_unit_ids))),
                  'data' => $vacancies
                ]
            ],200);

      }
    }

    /**
     * Vacancy
     */
    public function getVacancy (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'vacancy_id' => $request->vacancy_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'vacancy_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
          $candidate_id = $request->input('candidate_id');
          $vacancy_id = $request->input('vacancy_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $candidate = DB::table('candidates')->where('id', $candidate_id)->select('id','has_access','is_deleted','employment_types_id')->first();
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
          //checking for candidate status, restrict if candidate is deleted
          if ( $candidate && $candidate->is_deleted == 1 ) {
              return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' =>  'Unauthorized',
                  'isDeleted'=> true,
                  'isUnauthorized'=> true
                ]
              ],401);
          } 

          $job_ids = CandidateJob::where('candidate_id', $candidate_id)->pluck('job_id')->all();
          
          if ($job_ids){

            $vacancy = DB::table('vacancies')
                    ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                    ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                    ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                    ->where('vacancies.id', $vacancy_id)
                    ->whereIn('vacancies.job_id', $job_ids)
                    ->select('vacancies.id',
                    'vacancies.code',
                    'vacancies.on_date',
                    DB::raw('DATE_FORMAT(vacancies.start_time, "'.dateTimeFormat().'") as start_time'),
                    DB::raw('DATE_FORMAT(vacancies.end_time, "'.dateTimeFormat().'") as end_time'),
                    'vacancies.break',
                    'vacancies.total_hours',
                    'business_units.client_id',
                    'vacancies.business_unit_id',
                    'vacancies.job_id',
                    'vacancies.shift_id',
                    'business_units.business_unit_name',
                    'business_units.reference_code',
                    'business_units.email',
                    'business_units.formatted_number',
                    'business_units.address_line',
                    'business_units.latitude',
                    'business_units.longitude',
                    'business_units.profile_pdf',
                    'business_units.description AS bu_description',
                    'jobs.name AS job_title',
                    'jobs.color',
                    'shifts.shift_name AS shift_type',
                    'vacancies.is_publish',
                    // 'vacancies.is_publish_by_job',
                    'vacancies.description',
                    'vacancies.notes',
                    'vacancies.applied_count',
                    'vacancies.used_count',
                    'vacancies.total_numbers',
                    'vacancies.deleted_space',
                    'vacancies.rate_type',
                    'vacancies.has_splitrate',
                    'vacancies.published_ids',
                    'vacancies.rejected_candidates',
                    DB::raw( 'vacancies.total_numbers - vacancies.used_count as space_left'),
                    'vacancies.total_numbers AS total_space')->first();

            if ($vacancy){
                // $vacancy = Vacancy::where('id', $vacancy_id)->whereIn('job_id', $job_ids)->first();
                // $vacancy = Vacancy::where('id', $vacancy_id)->whereIn('job_id', $job_ids)->where('is_publish', 1)->first();

                $assignVacancy = DB::table('assign_vacancies')->where('vacancy_id', $vacancy_id)->where('candidate_id',$candidate_id)->whereIn('status',['applied', 'pending', 'invoiced', 'approved'])->select('status')->first();
                //checking that if vacancy is covered or not
                $coveredVacancy = DB::table('vacancies')->where('id', $vacancy_id)->whereIn('job_id', $job_ids)->whereRaw('total_numbers <= used_count')->where('is_publish', 1)->select('id')->first();

                if($assignVacancy){
                  $vacancy->assigned_status = $assignVacancy->status;
                } else if($coveredVacancy){
                  $vacancy->assigned_status = 'covered';
                } else {
                  $vacancy->assigned_status = null;
                }

                //Check vacancy rejected by candidate or not
                if (isset($vacancy->rejected_candidates) && $vacancy->rejected_candidates != NULL) {
                    $rejected_candidates = explode(",", $vacancy->rejected_candidates);
                    $vacancy->rejected_status = in_array($candidate_id, $rejected_candidates) ? true : false;
                } else {
                    $vacancy->rejected_status = false;
                }

                //Applied candidates
                $vacancy->applied = DB::table('assign_vacancies')
                    ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                    ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id')
                    ->where('assign_vacancies.status', 'applied')
                    ->where('assign_vacancies.is_deleted', 0)
                    ->where('candidates.candidate_status_id', 1)
                    ->where('assign_vacancies.vacancy_id', $vacancy->id)
                    ->groupBy('assign_vacancies.id')
                    ->get();

                //business unit array
                $vacancy->business_unit = array(
                    'id'=> $vacancy->business_unit_id,
                    'reference_code'=> $vacancy->reference_code,
                    'business_unit_name'=> $vacancy->business_unit_name,
                    'formatted_number'=> $vacancy->formatted_number,
                    'email'=> $vacancy->email,
                    'address_line'=> $vacancy->address_line,
                    'latitude'=> $vacancy->latitude,
                    'longitude'=> $vacancy->longitude,
                    'profile_pdf'=> $vacancy->profile_pdf,
                    'description'=> $vacancy->bu_description,
                    'client_id'=> $vacancy->client_id,
                );

                //getting Individual staff rate
                $details = VacancyDetail::where('vacancy_id', $vacancy->id)->orderBy('id', 'asc')->get();  
                $staffRateArr = [];
                if ($candidate->employment_types_id == '') {
                    $staffRateArr[] = array(
                              'employment_type_id'=> '',
                              'title'=> '',
                              'start_time'=> '',
                              'end_time'=> '',
                              'break'=> '',
                              'staff_rate'=> 0
                            );
                } else {
                    if (count($details)>0) {
                        $previousValue = null;
                        foreach($details as $detail){
                            $vacancydetails = DB::table('vacancy_details')->where('id', $detail->id)->first();
                            $staff_rate = VacancyStaffRate::where('employment_type_id', $candidate->employment_types_id)
                                            ->where('vacancy_id', $vacancy->id)
                                            ->where('vacancy_detail_id', $vacancydetails->id)
                                            ->first();
                            $staff_rate_amount = (!$staff_rate) ? 0 : $staff_rate->staff_rate;
                            $candidate_staff_rate = 0;
                            if ($vacancydetails->day_id != 8) {
                                $candidateRateCards = CandidateRateCard::where('business_unit_id', $vacancy->business_unit_id)
                                            ->where('job_id', $vacancy->job_id)
                                            ->where('day_id', $vacancydetails->day_id)
                                            ->where('shift_id', $vacancydetails->shift_id)
                                            // ->where('start_time', '<', date( "H:i:s", strtotime($vacancydetails->end_time)) )
                                            // ->where('end_time', '>', date( "H:i:s", strtotime($vacancydetails->start_time)) )
                                            ->where('employment_type_id', $candidate->employment_types_id)
                                            ->where('candidate_id', $candidate->id)
                                            ->get();  
                                $candidate_rate = '';
                                if (count($candidateRateCards)>0) {
                                    foreach ($candidateRateCards as $rate_card) {
                                        $start = date("H:i", strtotime($rate_card->start_time));
                                        $end = date("H:i", strtotime($rate_card->end_time));
                                        // $date = date('Y-m-d', strtotime($vacancy->on_date));
                                        $week = date("W", strtotime($vacancy->on_date));
                                        if($previousValue) {
                                          $week = $previousValue>$rate_card->day_id ? $week+1 : $week;
                                        }
                                        $year = date("Y", strtotime($vacancy->on_date));
                                        $date = date('Y-m-d', strtotime($year."W".$week.$rate_card->day_id));
                                        if ($start >= $end) {
                                            $startDate = $date;
                                            $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                                        }
                                        else {
                                            $startDate = $date;
                                            $endDate = $date;
                                        }
                                        if ( strtotime($startDate.' '.$rate_card->start_time) < strtotime($vacancydetails->end_time) && strtotime($vacancydetails->start_time) < strtotime($endDate.' '.$rate_card->end_time) ) {
                                            $candidate_rate = $rate_card;
                                        }
                                    } 
                                }
                                $candidate_staff_rate = $candidate_rate == '' ? 0 : $candidate_rate->staff_rate;
                                $previousValue = $vacancydetails->day_id; 
                            }
                            $staffRateArr[] = array(
                                  'employment_type_id'=>$candidate->employment_types_id,
                                  'title'=>EmploymentType::find($candidate->employment_types_id)->title,
                                  'start_time'=> $detail->start_time,
                                  'end_time'=> $detail->end_time,
                                  'break'=> $vacancydetails->break,
                                  'staff_rate'=> ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount
                                );
                        }
                    } else {
                        $staffRateArr[] = array(
                              'employment_type_id'=> '',
                              'title'=> '',
                              'start_time'=> '',
                              'end_time'=> '',
                              'break'=> '',
                              'staff_rate'=> 0
                            );
                    }
                }     

                $vacancy->staff_rates = $staffRateArr;

                //enabling BU extra notes for old candidate app
                $enabledBuNotesAgencies = [
                'annicare',
                'medsolve_healthcare'
                ];
                if (in_array($request->input('subdomain'), $enabledBuNotesAgencies)) {
                    if($vacancy->bu_description) {
                      $vacancy->notes = strip_tags($vacancy->bu_description.' - '.$vacancy->notes);
                    }
                }
            } else{
                $vacancy = [];
            }

          } else {
            $vacancy = [];
          }
          

          return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success',
              'time_format' => DB::table('agencies')->first()->time_format,
              'data' => $vacancy
            ]
          ],200);

      }
    }

    /**
     * Vacancy dates with open and assigned
     * */
    public function vacancy_dates (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $date_from = $request->input('date_from');
          $date_to = $request->input('date_to');
          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

            $candidate = Candidate::find($request->candidate_id);
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 

            if ( $candidate->candidate_status_id != 1 ) {
                return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => "Please check and fill your profile to get full access."
                    ]
                  ],400);
            }
            //getting candidate job id's
            $job_ids = CandidateJob::where('candidate_id', $candidate_id)->pluck('job_id')->all();

            $assigned = DB::table('assign_vacancies')
                            ->whereBetween('on_date', [$date_from, $date_to])
                            ->where('candidate_id', $candidate_id)
                            ->where('is_publish', 1)
                            ->whereIn('status',['pending', 'invoiced', 'approved'])
                            ->orderBy('on_date', 'asc')
                            ->where('is_deleted', 0);
            $assignedVacancyIDs = $assigned->pluck('vacancy_id')->all();

            $openVacancies = DB::table('vacancies')
                    ->whereBetween('on_date', [$date_from, $date_to])
                    ->whereIn('job_id', $job_ids)
                    ->whereRaw("find_in_set($candidate_id, published_ids)")
                    ->whereRaw('total_numbers > used_count')
                    ->whereNotIn('id', $assignedVacancyIDs)
                    ->orderBy('on_date', 'asc')->where('is_publish', 1)
                    ->pluck('on_date')->all();

            
            $assignedVacancies = $assigned->pluck('on_date')->all();

            return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success',
              'data' => [
                'open' => $openVacancies,
                'assigned' => $assignedVacancies
              ]
            ]
            ],200);

      }
    }

    /**
     * Reject Vacancy by candidate
     * */
    public function reject_vacancy (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'vacancy_id' => $request->vacancy_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'vacancy_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
        $candidate_id = $request->input('candidate_id');
        $vacancy_id = $request->input('vacancy_id');

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $candidate = Candidate::find($candidate_id);
        //checking for candidate access, restrict if candidate has no accesss
        if ( $candidate && $candidate->has_access == 0 ) {
          return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' =>  'You have no access to use this account. Please contact the agency.',
                ]
              ],401);
        } 
        //checking for candidate status, restrict if candidate is deleted
        if ( $candidate && $candidate->is_deleted == 1 ) {
          return response()->json([
            'error'=>[
              'status_code' => 401,
              'status' => 'Error', 
              'message' =>  'Unauthorized',
              'isDeleted'=> true,
              'isUnauthorized'=> true
            ]
          ],401);
        } 
        $vacancy = Vacancy::where('id', $vacancy_id)->first();

        //Check vacancy published or not
        if ($vacancy->is_publish == 0) {
          return response()->json([
            'error'=>[
              'status_code' => 400,
              'status' => 'Error', 
              'message' => "Sorry for the inconvenience. This vacancy has been canceled. Please contact the agency"
            ]
          ],400);
        }
        
        //Check vacancy rejected by agency or not
        if (isset($vacancy->rejected) && !empty($vacancy->rejected)) {
          $rejected_candidates = collect($vacancy->rejected)->pluck('candidate_id')->all();
          if(in_array($candidate->id, $rejected_candidates)) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Sorry for the inconvenience. Your job has been canceled. Please contact the agency"
              ]
            ],400);
          }
        }

        //Check vacancy rejected by candidate or not
        if (isset($vacancy->rejected_candidates) && $vacancy->rejected_candidates != NULL) {
          $rejected_candidates = explode(",", $vacancy->rejected_candidates);
          if(in_array($candidate->id, $rejected_candidates)) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "You rejected this shift earlier."
              ]
            ],400);
          }
        }
        //get removed the given candidate from the existing publsihed cadidate list
        $array1 = Array($candidate_id);
        $array2 = explode(',', $vacancy->published_ids);
        $array3 = array_diff($array2, $array1);

        //adding rejected candidates
        $rejectedArray = explode(',', $vacancy->rejected_candidates);
        $newRejectedArray = array_merge($array1, $rejectedArray);
        $newRejectedArray = array_unique($newRejectedArray);
        
        $vacancy->rejected_candidates = implode(',', $newRejectedArray);
        $vacancy->published_ids = implode(',', $array3);
        $vacancy->save();

        $user_ids        = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
        $user_ids        = array_unique($user_ids);
        $notify_array = array(
            'title' => $candidate->full_name,
            'type' => "vacancy_rejected",
            'date'       => date("d-m-Y",strtotime($vacancy->on_date)),
            'body' => "rejected a vacancy on ".date("d-m-Y",strtotime($vacancy->on_date))." with Ref.No: ".$vacancy->code.", in ".$vacancy->business_unit->business_unit_name,
              );

        if ($user_ids) {
            foreach ($user_ids as $user_id) {
                $notification = new Notification;
                $notification->type             = 'App\Notifications\HelloNotification';
                $notification->notifiable_type  = 'App\User';
                $notification->notifiable_id    = $user_id;
                $notification->object_id        = $request->vacancy_id;
                $notification->data             = json_encode($notify_array);
                $notification->resource_ids     = $candidate->id;
                $notification->save();
            }
        }

        //get agency details
        $agency = Agency::first();
        //sending request email to the agency
        $mail_content['candidate_name'] = '';
        $mail_content['agency_email']   = $agency->email;
        $mail_content['agency_name']    = $agency->name;
        $mail_content['agency_logo']    = $agency->agency_logo;
        $mail_content['title']          = 'Rejected a Vacancy';
        $mail_content['body']           = 'Candidate -'.$candidate->full_name . ' has rejected a vacancy on '.date("d-m-Y",strtotime($vacancy->on_date)).' with Ref.No: '.$vacancy->code.', in '.$vacancy->business_unit->business_unit_name;
        $mail_content['tenant_slug']    = $request->input('subdomain');

        SendMailGun::sendMail('notify_new_message', $mail_content['agency_name'], $mail_content['agency_email'], $mail_content["agency_email"], $mail_content['title'], $mail_content, null, null, null);

        $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('name', 'email')->first();
        $loggedData = [
          'role'          => 'candidate',
          'tenant'        => $request->input('subdomain'),
          'dashboardType' => 'candidate_app',
          'user_name'     => $user->name,
          'email'         => $user->email,
        ];
        //Activity Log
        $message = 'Vacancy - '.$vacancy->code ."'s ".' shift has been rejected.';
        ActivityCustomLogTrait::saveActivityLog('Vacancies', 'Update', $message, $vacancy->id, $vacancy->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

        return response()->json([
        'success'=>[
          'status_code' => 201,
          'status' => 'Success',
          'message' => "Rejected successfully"
        ]
        ],201);

      }
    }


    //deprecated
    public function vacancy_list (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain,
                        'shift' => $request->shift,
                        'region' => $request->region,
                        'business_unit' => $request->business_unit
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'candidate_id' => 'required',
                        'subdomain' => 'required',
                        'shift' => 'nullable',
                        'region' => 'nullable',
                        'business_unit' => 'nullable'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        if ($request->has('pagination') && $request->pagination == 1 && !isset($request->page)) {
            //Show custom message
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Please specify the page"
              ]
            ],400);
        }

          $date_from = $request->input('date_from');
          $date_to = $request->input('date_to');
          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

            $candidate = Candidate::find($request->candidate_id);
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 

            if ( $candidate->candidate_status_id != 1 ) {
                return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => "Please check and fill your profile to get full access."
                    ]
                  ],400);
            }
            //getting candidate job id's
              $job_ids = CandidateJob::where('candidate_id', $candidate_id)->pluck('job_id')->all();

              // $vacancies = CandidateVacancy::whereDoesntHave('assignedVacancies', function ($query) use ($candidate_id) {
              //                       if ($candidate_id) {
              //                       $query->where('candidate_id',$candidate_id);
              //                       }
              //                       $query->whereIn('status',['pending', 'invoiced', 'approved']);
              //                       $query->where('is_deleted', 0);
              //                   })->where( function($query) use($date_from, $date_to, $job_ids) {

              //                   if ($date_from != '' && $date_to != ''){
              //                     $query->whereBetween('on_date', [$date_from, $date_to]);
              //                   }

              //                   if ($job_ids != ''){
              //                     $query->whereIn('job_id', $job_ids);
              //                   }

              //                   $query->whereRaw('total_numbers > used_count');

              //                 })->whereRaw("find_in_set($candidate_id, published_ids)")
              //                 ->orderBy('on_date', 'asc')->where('is_publish', 1)->get();

            $shifts = isset($request->shift) && !empty($request->shift) ? Shift::where("shift_name", $request->shift)->pluck('id')->all() : [];
            $business_unit_id = isset($request->business_unit_id) && !empty($request->business_unit_id) ? $request->business_unit_id : '';
            //region
            $business_units = [];
            if(isset($request->region_id)) {
                $buIDs = DB::select("SELECT `business_unit_id` from `businessunit_regions` where `region_id` = ".$request->region_id."");
                $business_units = count($buIDs)>0 ? array_column($buIDs, 'business_unit_id') : [];
            }

            $vacancies = DB::table('vacancies')
                    ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
                    ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
                    ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
                    ->whereBetween('vacancies.on_date', [$date_from, $date_to])
                    ->whereIn('vacancies.job_id', $job_ids)
                    ->whereRaw('vacancies.total_numbers > vacancies.used_count')
                    ->whereRaw("find_in_set($candidate_id, vacancies.published_ids)")
                    ->orderBy('vacancies.on_date', 'asc')->where('vacancies.is_publish', 1)
                    ->where(function($query) use ($shifts, $business_unit_id, $request, $business_units) {
                        if (isset($shifts) && !empty($shifts)) {
                            $query->whereIn('vacancies.shift_id', $shifts);
                        }
                        if (isset($business_unit_id) && !empty($business_unit_id)) {
                            $query->where('vacancies.business_unit_id', $business_unit_id);
                        }
                        if (isset($request->region_id)) {
                            $query->whereIn('vacancies.business_unit_id', $business_units);
                        }
                    })
                    ->select('vacancies.id',
                    'vacancies.code',
                    'vacancies.on_date',
                    DB::raw('DATE_FORMAT(vacancies.start_time, "'.dateTimeFormat().'") as start_time'),
                    DB::raw('DATE_FORMAT(vacancies.end_time, "'.dateTimeFormat().'") as end_time'),
                    'vacancies.break',
                    'vacancies.total_hours',
                    'business_units.client_id',
                    'vacancies.business_unit_id',
                    'vacancies.job_id',
                    'vacancies.shift_id',
                    'business_units.business_unit_name',
                    'business_units.reference_code',
                    'business_units.email',
                    'business_units.formatted_number',
                    'business_units.address_line',
                    'business_units.latitude',
                    'business_units.longitude',
                    'business_units.profile_pdf',
                    'business_units.description',
                    'jobs.name AS job_title',
                    'jobs.color',
                    'shifts.shift_name AS shift_type',
                    'vacancies.is_publish',
                    'vacancies.notes',
                    'vacancies.applied_count',
                    'vacancies.used_count AS assigned_count',
                    DB::raw( 'vacancies.total_numbers - vacancies.used_count as space_left'),
                    'vacancies.total_numbers AS total_space');

            if (isset($request->pagination) && $request->pagination == 1) {
                $vacancies = $vacancies->paginate(10);
                $current_page = $vacancies->currentPage();
            } else {
                $vacancies = $vacancies->get();
                $current_page = 0;
            }

            $vacancy_list = [];
            if ($vacancies) {
                foreach ($vacancies as $vacancy) {
                    //checking assigned or not
                    $assign = DB::table('assign_vacancies')
                            ->where('vacancy_id', $vacancy->id)
                            ->where('candidate_id', $candidate_id)
                            ->whereIn('status',['pending', 'invoiced', 'approved', 'rejected'])
                            ->where('is_deleted', 0)->first();
                    $vacancy->assigned_status = $assign ? $assign->status : null;
                    $vacancy->assigned_vacancies = $assign ? $assign : [];
                    $vacancy->applied = DB::table('assign_vacancies')
                    ->select('id', 'candidate_id')
                    ->where('status', 'applied')
                    ->where('is_deleted', 0)
                    ->where('vacancy_id', $vacancy->id)
                    ->get();

                    if(empty($assign)){   
                        //staff rate calculation
                        $details = VacancyDetail::where('vacancy_id', $vacancy->id)->orderBy('id', 'asc')->get();  
                        $staffRateArr = [];
                        if ($candidate->employment_types_id == '') {
                            $staffRateArr[] = array(
                                      'employment_type_id'=> '',
                                      'title'=> '',
                                      'start_time'=> '',
                                      'end_time'=> '',
                                      'break'=> '',
                                      'staff_rate'=> 0
                                    );
                        } else {
                            if (count($details)>0) {
                                $previousValue = null;
                                foreach($details as $detail){
                                    $vacancydetails = VacancyDetail::find($detail->id);
                                    $staff_rate = VacancyStaffRate::where('employment_type_id', $candidate->employment_types_id)
                                                    ->where('vacancy_id', $vacancy->id)
                                                    ->where('vacancy_detail_id', $vacancydetails->id)
                                                    ->first();
                                    $staff_rate_amount = (!$staff_rate) ? 0 : $staff_rate->staff_rate;
                                    $candidate_staff_rate = 0;
                                    if ($vacancydetails->day_id != 8) {
                                        $candidateRateCards = CandidateRateCard::where('business_unit_id', $vacancy->business_unit_id)
                                                    ->where('job_id', $vacancy->job_id)
                                                    ->where('day_id', $vacancydetails->day_id)
                                                    ->where('shift_id', $vacancydetails->shift_id)
                                                    // ->where('start_time', '<', date( "H:i:s", strtotime($vacancydetails->end_time)) )
                                                    // ->where('end_time', '>', date( "H:i:s", strtotime($vacancydetails->start_time)) )
                                                    ->where('employment_type_id', $candidate->employment_types_id)
                                                    ->where('candidate_id', $candidate->id)
                                                    ->get(); 
                                        $candidate_rate = '';
                                        if (count($candidateRateCards)>0) {
                                            foreach ($candidateRateCards as $rateCard) {
                                                $start = date("H:i", strtotime($rateCard->start_time));
                                                $end = date("H:i", strtotime($rateCard->end_time));
                                                // $date = date('Y-m-d', strtotime($vacancy->on_date));
                                                $week = date("W", strtotime($vacancy->on_date));
                                                if($previousValue) {
                                                  $week = $previousValue>$rateCard->day_id ? $week+1 : $week;
                                                }
                                                $year = date("Y", strtotime($vacancy->on_date));
                                                $date = date('Y-m-d', strtotime($year."W".$week.$rateCard->day_id));
                                                if ($start >= $end) {
                                                    $startDate = $date;
                                                    $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                                                }
                                                else {
                                                    $startDate = $date;
                                                    $endDate = $date;
                                                }
                                                if ( strtotime($startDate.' '.$rateCard->start_time) < strtotime($vacancydetails->end_time) && strtotime($vacancydetails->start_time) < strtotime($endDate.' '.$rateCard->end_time) ) {
                                                    $candidate_rate = $rateCard;
                                                }
                                            } 
                                        }
                                        $candidate_staff_rate = $candidate_rate == '' ? 0 : $candidate_rate->staff_rate;
                                        $previousValue = $vacancydetails->day_id;
                                    }
                                    $staffRateArr[] = array(
                                          'employment_type_id'=>$candidate->employment_types_id,
                                          'title'=>EmploymentType::find($candidate->employment_types_id)->title,
                                          'start_time'=> $vacancydetails->start_time,
                                          'end_time'=> $vacancydetails->end_time,
                                          'break'=> $vacancydetails->break,
                                          'staff_rate'=> ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount
                                        );
                                }
                            } else {
                                $staffRateArr[] = array(
                                      'employment_type_id'=> '',
                                      'title'=> '',
                                      'start_time'=> '',
                                      'end_time'=> '',
                                      'break'=> '',
                                      'staff_rate'=> 0
                                    );
                            }  
                        }  
                        $vacancy->staff_rates = $staffRateArr;
                        //business unit array
                        $vacancy->business_unit = array(
                                      'id'=> $vacancy->business_unit_id,
                                      'reference_code'=> $vacancy->reference_code,
                                      'business_unit_name'=> $vacancy->business_unit_name,
                                      'formatted_number'=> $vacancy->formatted_number,
                                      'email'=> $vacancy->email,
                                      'address_line'=> $vacancy->address_line,
                                      'latitude'=> $vacancy->latitude,
                                      'longitude'=> $vacancy->longitude,
                                      'profile_pdf'=> $vacancy->profile_pdf,
                                      'description'=> $vacancy->description,
                                      'client_id'=> $vacancy->client_id,
                                    );

                        $vacancy_list[] = $vacancy;

                    }
                }
            }

            return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success',
              'time_format' => DB::table('agencies')->first()->time_format,
              'current_page' => $current_page,
              'data' => $vacancy_list
            ]
            ],200);

      }
    }

    //deprecated
    public function vacancy (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'vacancy_id' => $request->vacancy_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'vacancy_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
          $candidate_id = $request->input('candidate_id');
          $vacancy_id = $request->input('vacancy_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

          $candidate = Candidate::find($candidate_id);
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
          //checking for candidate status, restrict if candidate is deleted
          if ( $candidate && $candidate->is_deleted == 1 ) {
              return response()->json([
                'error'=>[
                  'status_code' => 401,
                  'status' => 'Error', 
                  'message' =>  'Unauthorized',
                  'isDeleted'=> true,
                  'isUnauthorized'=> true
                ]
              ],401);
          } 

          $job_ids = CandidateJob::where('candidate_id', $candidate_id)->pluck('job_id')->all();
          $vacancy = [];

          if ($job_ids){

            $assignVacancy = AssignVacancy::where('vacancy_id', $vacancy_id)->where('candidate_id',$candidate_id)->whereIn('status',['applied', 'pending', 'invoiced', 'approved'])->first();
            $vacancy = Vacancy::where('id', $vacancy_id)->whereIn('job_id', $job_ids)->first();
            // $vacancy = Vacancy::where('id', $vacancy_id)->whereIn('job_id', $job_ids)->where('is_publish', 1)->first();
            //checking that if vacancy is covered or not
            $coveredVacancy = Vacancy::where('id', $vacancy_id)->whereIn('job_id', $job_ids)->whereRaw('total_numbers <= used_count')->where('is_publish', 1)->first();

            if($assignVacancy){
              $vacancy['assigned_status'] = $assignVacancy->status;
            } else if($coveredVacancy){
              $vacancy['assigned_status'] = 'covered';
            } else {
              $vacancy['assigned_status'] = null;
            }

            //Check vacancy rejected by candidate or not
            if (isset($vacancy->rejected_candidates) && $vacancy->rejected_candidates != NULL) {
                $rejected_candidates = explode(",", $vacancy->rejected_candidates);
                $vacancy['rejected_status'] = in_array($candidate_id, $rejected_candidates) ? true : false;
            } else {
                $vacancy['rejected_status'] = false;
            }

            // //getting Individual staff rate
            // $day_id = Day::where('day', date('l',strtotime($vacancy->on_date)))->first()->id;  
            // $candidateStaffRateArr = [];

            // //#check for custom holiday
            // $custom_holiday = CustomHoliday::where('business_unit_id', $vacancy->business_unit_id)->where('holiday_date', $vacancy->on_date)->where('is_deleted',0)->first();
            // //#check for holiday
            // $holiday = Holiday::where('on_date', $vacancy->on_date)->where('is_deleted',0)->first();  
            // $candidate_staff_rate = 0;
            // //checking for holidays, if yes then taking holiday rates. No , then taking candidate rates
            // if (!$custom_holiday && !$holiday) { 
            //   $candidate_rate = CandidateRateCard::where('business_unit_id', $vacancy->business_unit_id)->where('job_id', $vacancy->job_id)
            //                 ->where('day_id', $day_id)->where('shift_id', $vacancy->shift_id)->where('employment_type_id', $candidate->employment_types_id)
            //                 ->where('candidate_id', $request->candidate_id)->first();
            //   if ($candidate_rate) {           
            //       $candidateStaffRateArr[] = array(
            //           'id'=>$candidate_rate->id, 
            //           'employment_type_id'=>$candidate_rate->employment_type_id,
            //           'title'=>EmploymentType::find($candidate_rate->employment_type_id)->title,
            //           'staff_rate'=>$candidate_rate->staff_rate
            //         );
            //   }
            // }

            // //getting vacancy staff rate
            // $staffrates = VacancyStaffRate::where('vacancy_id', $vacancy->id)->where('employment_type_id', $candidate->employment_types_id)->first();
            // $staffRateArr = [];
            // if($staffrates){
            //     $staffRateArr[] = array(
            //         'id'=>$staffrates->id, 
            //         'employment_type_id'=>$staffrates->employment_type_id,
            //         'title'=>EmploymentType::find($staffrates->employment_type_id)->title,
            //         'staff_rate'=>$staffrates->staff_rate
            //       );
            // }
            $details = VacancyDetail::where('vacancy_id', $vacancy->id)->orderBy('id', 'asc')->get();  
            $staffRateArr = [];
            if ($candidate->employment_types_id == '') {
                $staffRateArr[] = array(
                          'employment_type_id'=> '',
                          'title'=> '',
                          'start_time'=> '',
                          'end_time'=> '',
                          'break'=> '',
                          'staff_rate'=> 0
                        );
            } else {
                if (count($details)>0) {
                    $previousValue = null;
                    foreach($details as $detail){
                        $vacancydetails = DB::table('vacancy_details')->where('id', $detail->id)->first();
                        $staff_rate = VacancyStaffRate::where('employment_type_id', $candidate->employment_types_id)
                                        ->where('vacancy_id', $vacancy->id)
                                        ->where('vacancy_detail_id', $vacancydetails->id)
                                        ->first();
                        $staff_rate_amount = (!$staff_rate) ? 0 : $staff_rate->staff_rate;
                        $candidate_staff_rate = 0;
                        if ($vacancydetails->day_id != 8) {
                            $candidateRateCards = CandidateRateCard::where('business_unit_id', $vacancy->business_unit_id)
                                        ->where('job_id', $vacancy->job_id)
                                        ->where('day_id', $vacancydetails->day_id)
                                        ->where('shift_id', $vacancydetails->shift_id)
                                        // ->where('start_time', '<', date( "H:i:s", strtotime($vacancydetails->end_time)) )
                                        // ->where('end_time', '>', date( "H:i:s", strtotime($vacancydetails->start_time)) )
                                        ->where('employment_type_id', $candidate->employment_types_id)
                                        ->where('candidate_id', $candidate->id)
                                        ->get();  
                            $candidate_rate = '';
                            if (count($candidateRateCards)>0) {
                                foreach ($candidateRateCards as $rate_card) {
                                    $start = date("H:i", strtotime($rate_card->start_time));
                                    $end = date("H:i", strtotime($rate_card->end_time));
                                    // $date = date('Y-m-d', strtotime($vacancy->on_date));
                                    $week = date("W", strtotime($vacancy->on_date));
                                    if($previousValue) {
                                      $week = $previousValue>$rate_card->day_id ? $week+1 : $week;
                                    }
                                    $year = date("Y", strtotime($vacancy->on_date));
                                    $date = date('Y-m-d', strtotime($year."W".$week.$rate_card->day_id));
                                    if ($start >= $end) {
                                        $startDate = $date;
                                        $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                                    }
                                    else {
                                        $startDate = $date;
                                        $endDate = $date;
                                    }
                                    if ( strtotime($startDate.' '.$rate_card->start_time) < strtotime($vacancydetails->end_time) && strtotime($vacancydetails->start_time) < strtotime($endDate.' '.$rate_card->end_time) ) {
                                        $candidate_rate = $rate_card;
                                    }
                                } 
                            }
                            $candidate_staff_rate = $candidate_rate == '' ? 0 : $candidate_rate->staff_rate;
                            $previousValue = $vacancydetails->day_id; 
                        }
                        $staffRateArr[] = array(
                              'employment_type_id'=>$candidate->employment_types_id,
                              'title'=>EmploymentType::find($candidate->employment_types_id)->title,
                              'start_time'=> $detail->start_time,
                              'end_time'=> $detail->end_time,
                              'break'=> $vacancydetails->break,
                              'staff_rate'=> ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount
                            );
                    }
                } else {
                    $staffRateArr[] = array(
                          'employment_type_id'=> '',
                          'title'=> '',
                          'start_time'=> '',
                          'end_time'=> '',
                          'break'=> '',
                          'staff_rate'=> 0
                        );
                }
            }     

            $vacancy['staff_rates'] = $staffRateArr;

            //enabling BU extra notes for old candidate app
            $enabledBuNotesAgencies = [
            'annicare',
            'medsolve_healthcare'
            ];
            if (in_array($request->input('subdomain'), $enabledBuNotesAgencies)) {
                if($vacancy->business_unit->description) {
                  $vacancy->notes = strip_tags($vacancy->business_unit->description.' - '.$vacancy->notes);
                }
            }


          }
          

          return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success',
              'time_format' => DB::table('agencies')->first()->time_format,
              'data' => $vacancy
            ]
          ],200);

      }
    }
    
}
