<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Dashboard;
use App\Tenants\Candidate;
use App\Tenants\Agency;
use App\Tenants\Notification;
use App\Tenants\RoleUser;
use App\UserTenant;
use App\Tenant;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDashboardDetails(Request $request) {

        $validator = Validator::make(
                        array(
                        'user_id' => $request->user_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'user_id' => 'required|integer',
                        'subdomain' => 'required'
                    )
                );


        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('user_id')
                  ]
                ],400);
            } else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            } 

        } else {
			$userTenants = UserTenant::where('user_id', $request->user_id)->select('user_id', 'tenant_id')->get();
			if (count($userTenants)>0) {
				foreach ($userTenants as $userTenant) {
					$tenant = Tenant::find($userTenant->tenant_id);

		            config(['database.connections.mysql.database' => 'logezy_'.$tenant->slug,'database.default'=>'mysql']);
		            DB::reconnect('mysql');
		            $userTenant->tenant_name = $tenant->name;
		            $userTenant->subdomain 	= $tenant->slug;
		            $userTenant->user_roles = RoleUser::where('user_id', $request->user_id)->whereIn('role_id', [3,4])->select('user_id', 'role_id')->get();
		            config(['database.connections.mysql.database' => 'logezycore','database.default'=>'mysql']);
		            DB::reconnect('mysql');
				}
			} 

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');



			// $agencyName 			= Agency::getAgencyDetails()->name;
			// $upcomingBirthdayCount	= DB::table('candidates')->whereRaw("DAYOFYEAR(dob) BETWEEN $start AND $end")->where('candidates.is_deleted',0)->where('candidates.candidate_status_id', 1)->count();
			// $upcomingBirthdays		= DB::table('candidates')->whereRaw("DAYOFYEAR(dob) BETWEEN $start AND $end")->where('candidates.is_deleted',0)->where('candidates.candidate_status_id', 1)->select('id', 'full_name', 'profile_img')->get();
	  		// $allClient              = Dashboard::getAllClientsCount(); 
			// $activeClient           = Dashboard::getActiveClientsCount();
			// $inactiveClient         = Dashboard::getInactiveAllClientsCount();
			// $AllCandidate           = Dashboard::getAllCandidateCount(); 
			// $activeCandidate        = Dashboard::getActiveCandidateCount();
			// $inactiveCandidate      = Dashboard::getInactiveCandidateCount();
			// $allBusinessUnit        = Dashboard::getAllBusinessUnitCount(); 
			// $activeBusinessUnit     = Dashboard::getActiveBusinessUnitCount();
			// $inactiveBusinessUnit   = Dashboard::getInactiveBusinessUnitCount();
			// $allDocuments           = Dashboard::getAllDocumentsCount(); 
			// $expiredDocuments       = Dashboard::getExpiredDocumentsCount();
			// $expiresindays          = Dashboard::getExpireDocumentsWithinDaysCount();
			$openVacancies          = Dashboard::getOpenVacanciesCount();  
			$appliedVacancies       = Dashboard::getAppliedVacanciesCount(); 
			$assignedVacancies      = Dashboard::getAssignedVacanciesCount(); 
			$deletedShifts          = Dashboard::getDeletedShiftsCount(); 
			$totalTimesheetHours    = number_format((float) Dashboard::getTotalTimesheetHours(), 2, '.', '');
			// $approvedTimesheetHours = number_format((float) Dashboard::getApprovedTimesheetHours(), 2, '.', '');
			// $invoicedTimesheetHours = number_format((float) Dashboard::getInvoicedTimesheetHours(), 2, '.', '');
			// $pendingTimesheetHours  = number_format((float) Dashboard::getPendingTimesheetHours(), 2, '.', '');
			$totalChargeRate        = number_format((float) Dashboard::getTotalChargeRate(), 2, '.', '');
			$totalPayRate           = number_format((float) Dashboard::getTotalPayRate(), 2, '.', '');
			if (count($userTenants)>1) {
				$hasMultipleBranches = 1;
			} else {
				$hasMultipleBranches = 0;
			}
			// $totalInvoices      	= Dashboard::getTotalInvoice(); 
			// $paidInvoices      	    = Dashboard::getPaidInvoice(); 
			// $duelessthanInvoices    = Dashboard::getInvoiceOverDueLessthanThirty(); 
			// $duegreaterthanInvoices = Dashboard::getInvoiceOverDueGreaterthanThirty(); 
	  		// $sales  				= Dashboard::getLastTwelveMonthsSales()['monthlysales']; 
	  		// $salesMaxValue          = $this->roundUpToAny(Dashboard::getLastTwelveMonthsSales()['salesMaxValue']);
			// $months  				= Dashboard::getLastTwelveMonthsSales()['months']; 
			// $customers 				= Dashboard::getTopFiveCustomers();
			// $topCustomers  		    = Dashboard::getLastTwelveMonthsTopCustomers()['topCustomers'];
			// $topCustomersMaxValue   = $this->roundUpToAny(Dashboard::getLastTwelveMonthsTopCustomers()['maxValue']);
			// $candidates 		    = Dashboard::getTopFiveCandidates();
			// $topCandidates		    = Dashboard::getLastTwelveMonthsTopCandidates()['topCandidates']; 
			// $topCandidatesMaxValue  = $this->roundUpToAny(Dashboard::getLastTwelveMonthsTopCandidates()['maxValue']); 
			// $lastTwelthsMonth 	    = date('M-Y',strtotime(Dashboard::getLastTwelveMonthsDate()));
			// $lastMonth 				= date('M-Y',strtotime(Dashboard::getLastdayPreviousMonth())); 
			$unreadNotifications = Notification::where('notifiable_id', $request->user_id)->where('read_at', NULL)->orderBy('created_at', 'desc')->count();
		}
                    
	    return response()->json([
	        'success'=>[
	          'status_code' => 200,
	          'status' => 'Success', 
	          'openVacancies' => $openVacancies,
	          'appliedVacancies' => $appliedVacancies,
	          'assignedVacancies' => $assignedVacancies,
	          'deletedShifts' => $deletedShifts,
	          'totalTimesheetHours' => $totalTimesheetHours,
	          'totalChargeRate' => $totalChargeRate,
	          'totalPayRate' => $totalPayRate,
	          'unreadNotifications' => $unreadNotifications,
	          'hasMultipleBranches' => $hasMultipleBranches,
	          'branches' => $userTenants,
	        ]
	      ],200);
	}
	/**
	 * Get total sales amount and last twelve months
	 */
 //    public function getLastTwelveMonthDetails(){   
	// 	$data = array();
	// 	$data['totalsalesamount'] = Dashboard::getLastTwelveMonthsTotalSalesAmount();
	// 	$lastTwelthsMonth 	      = date('M-Y',strtotime(Dashboard::getLastTwelveMonthsDate()));
 //        $lastMonth 				  = date('M-Y',strtotime(Dashboard::getLastdayPreviousMonth())); 
	// 	$data['lasttwelvemonths'] = $lastTwelthsMonth.' - '.$lastMonth;
	// 	return $data;
	// }
	
 //    public function roundUpToAny($n,$x=100) { 
 //        if($n > 100) {
 //            $val = (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x; 
 //            return floor($val);
 //        }else{
 //            return '100';
 //        }
 //    }
}
