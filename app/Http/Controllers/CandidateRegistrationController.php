<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use DB;
use Config;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
/* Models */
use App\User;
use App\Tenant;
use App\UserTenant;
use App\Tenants\Role;
use App\Tenants\RoleUser;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\WorkExperience;
use App\Tenants\DocumentCategory;
use App\Tenants\CandidateDocument;
use App\Tenants\CandidateReferral;
use App\Traits\ActivityLog\ActivityCustomLogTrait;

class CandidateRegistrationController extends BaseController
{
    public function __construct()
    {
    }

    /**
     * getCurrentTenant
     *
     * @param  mixed $subdomain
     *
     * @return void
     */
    protected static function getCurrentTenant($subdomain)
    {
        Config(['database.connections.mysql.database' => 'logezycore', 'database.default' => 'mysql']);
        DB::reconnect('mysql');

        return Tenant::where('slug', $subdomain)->first(['id', 'name', 'db_name']);
    }

    /**
     * getComplianceDocuments.
     *
     * @param mixed $request
     *
     * user_job_ids, candidate_id
     */
    public function getComplianceDocuments(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'subdomain' => 'required',
                'job_ids' => 'required',
                ]);

            if ($validator->fails()) :

            if ($validator->errors()->has('subdomain')) :
                return response()->json(
                   ['error' => [
                    'status_code' => 400,
                    'status' => 'Error',
                    'message' => $validator->errors()->first('subdomain'), ]], 400); 
            elseif ($validator->errors()->has('job_ids')) :
                return response()->json(
                   ['error' => [
                    'status_code' => 400,
                    'status' => 'Error',
                    'message' => $validator->errors()->first('job_ids'), ]], 400);
            endif; else :

            Config(['database.connections.mysql.database' => $this->getCurrentTenant($request->subdomain)->db_name, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $document_category = DocumentCategory::whereHas('documents.documentJob', function ($query) use ($request) {
                $query->whereIn('job_id', explode(',', $request->job_ids));
            })->with(['documents.uploadedDocuments' => function ($query) use ($request) {
                $query->where(['candidate_id' => $request->candidate_id, 'is_latest' => 1]);
            }])->orderBy('position', 'ASC')->get();

            return response()->json(
                ['success' => [
                 'status_code' => 200,
                 'status' => 'Success',
                 'data' => $document_category, ]], 200);
            endif;
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                ['error' => [
                 'status_code' => 500,
                 'status' => 'Error',
                 'message' => $ex->getMessage(), ]], 500);
        }
    }

    /**
     * returnUsersByEmail
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function returnUsersByEmail(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), ['email' => 'required']);

            if ($validator->fails()) :
                    if ($validator->errors()->has('email')) :
                       return response()->json(
                           ['error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('email'), ]], 400);
            endif; else :

            Config(['database.connections.mysql.database' => 'logezycore', 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            return response()->json(
                    ['success' => [
                     'status_code' => 200,
                     'status' => 'Success',
                     'data' => User::where('email', 'like', '%'.$request->email.'%')->get(['id', 'email']), ]], 200);
            endif;
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
            ['error' => [
             'status_code' => 500,
             'status' => 'Error',
             'message' => $ex->getMessage(), ]], 500);
        }
    }

    /**
     * getCandidateByUserId
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function getCandidateByUserId(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), ['subdomain' => 'required', 'user_id' => 'required']);

            if ($validator->fails()) :

                if ($validator->errors()->has('subdomain') || $validator->errors()->has('user_id')) :

                   return response()->json(
                       ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors(), ]], 400);

            endif; else :

            Config(['database.connections.mysql.database' => 'logezycore', 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $user_tenant = UserTenant::where('user_id', $request->user_id)->first();

            Config(['database.connections.mysql.database' => $this->getCurrentTenant($request->subdomain)->db_name, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $role_user = RoleUser::query();
            $user_exists = $role_user->where('user_id', $request->user_id)->whereRaw('role_id = 6')->exists();
            $role_user_id = $role_user->where('user_id', $request->user_id)->first(['id']);

            return response()->json(
                ['success' => [
                 'status_code' => 200,
                 'status' => 'Success',
                 'data' => compact('user_tenant', 'user_exists', 'role_user_id'), ]], 200);

            endif;
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                ['error' => [
                 'status_code' => 500,
                 'status' => 'Error',
                 'message' => $ex->getMessage(), ]], 500);
        }
    }

    /**
     * createCandidateUser
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function createCandidateUser(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'subdomain' => 'required',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|email|unique:users|max:255',
                'mobile_prefix' => 'required|max:5',
                'mobile_number' => 'required|max:15',
                'address_line1' => 'required|max:255',
                'place' => 'required|max:255',
                'gender' => 'required',
                'post_code' => 'required|max:16',
                'indemnity_insurance' => 'required',
                'employment_type_id' => 'required',
                'job_ids' => 'required',
                'password' => 'required|string|min:8|regex:/[a-z]/|regex:/[A-Z]/|regex:/[0-9]/|regex:/[@$!%*#?&]/',
                'password_confirm' => 'required|same:password',
                ]);

            if ($validator->fails()) :

                if ($validator->errors()->has('subdomain')) :
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('subdomain'), ]], 400); 
                elseif ($validator->errors()->has('first_name')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('first_name'), ]], 400); 
                elseif ($validator->errors()->has('last_name')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('last_name'), ]], 400); 
                elseif ($validator->errors()->has('email')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('email'), ]], 400); 
                elseif ($validator->errors()->has('mobile_prefix')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('mobile_prefix'), ]], 400); 
                elseif ($validator->errors()->has('mobile_number')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('mobile_number'), ]], 400); 
                elseif ($validator->errors()->has('address_line1')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('address_line1'), ]], 400); 
                elseif ($validator->errors()->has('place')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('place'), ]], 400); 
                elseif ($validator->errors()->has('gender')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('gender'), ]], 400); 
                elseif ($validator->errors()->has('post_code')):
                       return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('post_code'), ]], 400); 
               elseif ($validator->errors()->has('indemnity_insurance')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('indemnity_insurance'), ]], 400); 
                elseif ($validator->errors()->has('employment_type_id')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('employment_type_id'), ]], 400); 
                elseif ($validator->errors()->has('job_ids')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('job_ids'), ]], 400); 
                elseif ($validator->errors()->has('password')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('password'), ]], 400); 
                elseif ($validator->errors()->has('password_confirm')):
                        return response()->json(
                            ['error' => [
                             'status_code' => 400,
                             'status' => 'Error',
                             'message' => $validator->errors()->first('password_confirm'), ]], 400);
            endif; 

        else :

            Config(['database.connections.mysql.database' => 'logezycore', 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $user = User::create([
                'name' => $request->first_name.' '.$request->last_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'status' => 'inactive',
            ]);

            UserTenant::create([
                'user_id' => $user->id,
                'tenant_id' => $this->getCurrentTenant($request->subdomain)->id,
            ]);

            Config(['database.connections.mysql.database' => $this->getCurrentTenant($request->subdomain)->db_name, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            RoleUser::create([
                 'user_id' => $user->id,
                 'role_id' => Role::where('name', 'candidate')->first()->id,
            ]);

            $candidate = Candidate::create([
                'user_id' => $user->id,
                'employment_types_id' => $request->employment_type_id,
                'candidate_status_id' => 6,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'full_name' => $request->first_name.' '.$request->last_name,
                'candidate_code' => 'CAND'.$user->id,
                'address_line1' => $request->address_line1,
                'address_line2' => $request->address_line2,
                'post_code' => $request->post_code,
                'place' => $request->place,
                'gender' => $request->gender,
                'mobile_prefix' => $request->mobile_prefix,
                'mobile_number' => $request->mobile_number,
                'formatted_mobile_number' => $request->mobile_prefix.' '.$request->mobile_number,
                'phone_prefix' => $request->phone_prefix,
                'phone_number' => $request->phone_number,
                'nmc_nurse_type' => $request->nmc_nurse_type,
                'nmc_pin_number' => $request->nmc_pin_number,
                'candidate_status_id' => 6,
                'created_by' => $user->id,
                'has_access' => 0,
            ]);

            foreach (explode(',', $request->job_ids)  as $job_id) :
                CandidateJob::create([
                    'candidate_id' => $candidate->id,
                    'job_id' => $job_id,
                    'status' => 'active',
                    'created_by' => $user->id,
                  ]);
            endforeach;

            $loggedData = [
                'role'          => 'candidate',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'candidate_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
            ];

            //Activity Log
            $message = 'New Candidate '.$candidate->full_name .' has been signed up.';
            ActivityCustomLogTrait::saveActivityLog('Candidates', 'Create', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData 

            $referral_code = Str::random(8);

            CandidateReferral::create([
                'candidate_id' => $candidate->id,
                'referral_code' => $referral_code,
              ]);

            $response = file_get_contents('https://api-ssl.bitly.com/v3/shorten?access_token='.env('BITLY_ACCESS_TOKEN', '').'&longUrl=https://'.$request->subdomain.'.logezy.co/referral/'.$referral_code);

            $referral_url = json_decode($response)->data->url;

            return response()->json(
                        ['success' => [
                         'status_code' => 200,
                         'status' => 'Success',
                         'data' => ['user_id' => $user->id, 'candidate_id' => $candidate->id], 'referral_url' => $referral_url, ]], 200);
            endif;
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                ['error' => [
                 'status_code' => 500,
                 'status' => 'Error',
                 'message' => $ex->getMessage(), ]], 500);
        }
    }

    /**
     * uploadComplianceCertificate
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function uploadComplianceDocument(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'subdomain' => 'required',
                'document_id' => 'required',
                'file' => 'required|mimes:jpeg,png,jpg,gif,svg,pdf,doc|max:2048',
                'candidate_id' => 'required',
                'user_id' => 'required',
               ]);

            if ($validator->fails()) :

                if ($validator->errors()->has('subdomain')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('subdomain'), ]], 400); 
            elseif ($validator->errors()->has('document_id')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('document_id'), ]], 400); 
            elseif ($validator->errors()->has('file')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('file'), ]], 400); 
            elseif ($validator->errors()->has('candidate_id')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('candidate_id'), ]], 400); 
            elseif ($validator->errors()->has('user_id')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('user_id'), ]], 400);

            endif; 

        else:

            Config(['database.connections.mysql.database' => $this->getCurrentTenant($request->subdomain)->db_name, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $file_name = $request->subdomain.'/'.time().$request->file('file')->getClientOriginalName();
            $s3_response = Storage::disk('s3')->put($file_name, file_get_contents($request->file('file')), 'public');

            if ($s3_response):
            CandidateDocument::create([
                'document_id' => $request->document_id,
                'issue_date' => Carbon::parse($request->issue_date),
                'expiry_date' => $request->expiry_date ? Carbon::parse($request->expiry_date) : Carbon::now()->addMonths(3),
                'status' => 'inactive',
                'file' => Storage::disk('s3')->url($file_name),
                'candidate_id' => $request->candidate_id,
                'created_by' => $request->user_id,
              ]);

            $user = DB::table('logezycore.users')->where('id', $request->user_id)->select('id', 'name', 'email')->first();
            $loggedData = [
              'role'          => 'candidate',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'candidate_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];
            $document = DB::table('documents')->where('id', $request->document_id)->pluck('title')->first();
            //Activity Log
            $message = 'Candidate Document - ' . $document . ' document has been uploaded.';
            ActivityCustomLogTrait::saveActivityLog('Candidate Documents', 'Update', $message, $request->document_id, $document, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json(
                ['success' => [
                 'status_code' => 200,
                 'status' => 'Success',
                 'data' => ['s3_file_url' => Storage::disk('s3')->url($file_name)], ]], 200); else:
                return response()->json(
                    ['error' => [
                     'status_code' => 400,
                     'status' => 'Error',
                     'message' => 'error while uploading file', ]], 400);
            endif;

            endif;
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                ['error' => [
                 'status_code' => 500,
                 'status' => 'Error',
                 'message' => $ex->getMessage(), ]], 500);
        }
    }

    /**
     * workExperience
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function workExperience(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'subdomain' => 'required',
                'work_experiences' => 'required|array',
                'work_experiences.*.company_name' => 'required',
                'work_experiences.*.address_line1' => 'required',
                'work_experiences.*.designation' => 'required',
                'work_experiences.*.start_date' => 'required',
                'work_experiences.*.end_date' => 'required',
                'candidate_id' => 'required',
                'user_id' => 'required',
              ]);

            if ($validator->fails()) :

                if ($validator->errors()->has('subdomain')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('subdomain'), ]], 400); 
            elseif ($validator->errors()->has('work_experiences')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('work_experiences'), ]], 400); 
            elseif ($validator->errors()->has('work_experiences.*.company_name')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('work_experiences.*.company_name'), ]], 400); 
            elseif ($validator->errors()->has('work_experiences.*.address_line1')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('work_experiences.*.address_line1'), ]], 400); 
            elseif ($validator->errors()->has('work_experiences.*.designation')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('work_experiences.*.designation'), ]], 400); 
            elseif ($validator->errors()->has('work_experiences.*.start_date')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('work_experiences.*.start_date'), ]], 400); 
            elseif ($validator->errors()->has('work_experiences.*.end_date')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('work_experiences.*.end_date'), ]], 400); 
            elseif ($validator->errors()->has('candidate_id')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('candidate_id'), ]], 400); 
            elseif ($validator->errors()->has('user_id')) :
                    return response()->json(
                        ['error' => [
                         'status_code' => 400,
                         'status' => 'Error',
                         'message' => $validator->errors()->first('user_id'), ]], 400);
            endif; 

        else:

            Config(['database.connections.mysql.database' => $this->getCurrentTenant($request->subdomain)->db_name, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            WorkExperience::where('candidate_id', $request->candidate_id)->delete();
            foreach ($request->work_experiences as $work_experience):
               WorkExperience::create([
                'candidate_id' => $request->candidate_id,
                'company_name' => $work_experience['company_name'],
                'address_line1' => $work_experience['address_line1'],
                'address_line2' => array_key_exists('address_line2', $work_experience) ? $work_experience['address_line2'] : null,
                'post_code' => array_key_exists('post_code', $work_experience) ? $work_experience['post_code'] : null,
                'place' => array_key_exists('place', $work_experience) ? $work_experience['place'] : null,
                'designation' => $work_experience['designation'],
                'start_date' => Carbon::parse($work_experience['start_date']),
                'end_date' => Carbon::parse($work_experience['end_date']),
                'desc' => array_key_exists('desc', $work_experience) ? $work_experience['desc'] : null,
                'created_by' => $request->user_id,
                ]);
            endforeach;

            $candidate = Candidate::find($request->candidate_id);
            $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('id', 'name', 'email')->first();
            $loggedData = [
              'role'          => 'candidate',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'candidate_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];
            //Activity Log
            $message = 'Candidate - work experience has been updated.';
            ActivityCustomLogTrait::saveActivityLog('Candidates', 'Update', $message, '', '', $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json(
                ['success' => [
                 'status_code' => 200,
                 'status' => 'Success',
                 'data' =>  $candidate, ]], 200);

            endif;
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                ['error' => [
                 'status_code' => 500,
                 'status' => 'Error',
                 'message' => $ex->getMessage(), ]], 500);
        }
    }
}
