<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\SmsDevice;
use App\Tenants\Agency;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use GuzzleHttp;
use Illuminate\Database\DatabaseManager;

class SubscriptionController extends BaseController
{
  public function getSubscriptionDetails(Request $request)
  {
    $validator = Validator::make(
      array(
        'subscription_id' => $request->subscription_id,
      ),
      array(
        'subscription_id' => 'required',
      )
    );

    if ($validator->fails()) {
      $messages = $validator->errors();
      //Determining If Messages Exist For A Field
      if ($messages->has('subscription_id')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('subscription_id')
          ]
        ], 400);
      } 

    } else {

      $subscription_id = $request->input('subscription_id');

      $subscription = DB::table('subscriptions')->where('subscription_id', $subscription_id)->first(); 

      if ($subscription) {

        $integration = DB::table('integrations')->where('slug', 'zoho')->first(); 

        $oauthtoken = $integration ? $integration->access_token : null;
        $organzationID =  env("ZOHO_ORGANIZATION_ID", null);
        $zohoSubscriptionApi =  env("ZOHO_SUBSCRIPTION_API", null);
        // return $oauthtoken ."|".$organzationID ."|".$zohoSubscriptionApi;

        if($oauthtoken && $organzationID && $zohoSubscriptionApi){

          $url = $zohoSubscriptionApi.'/api/v1/subscriptions/'.$subscription_id;


          $headers = [
            'X-com-zoho-subscriptions-organizationid: ' . $organzationID . '',
            'Authorization: Zoho-oauthtoken ' . $oauthtoken . ''
          ];


          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          // curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmData));
          $result = curl_exec($ch);
          curl_close($ch);

          $result = json_decode($result);


          //getting invoices
          $query = "filter_by=Status.All";
          ($subscription_id ? $query = $query."&subscription_id=".$subscription_id : "");
          $requestURL = $zohoSubscriptionApi."/api/v1/invoices?".$query;

          $ch1 = curl_init();
          curl_setopt($ch1, CURLOPT_URL, $requestURL);
          // curl_setopt($ch1, CURLOPT_POST, true);
          curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
          // curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($fcmData));
          $result1 = curl_exec($ch1);
          curl_close($ch1);
          $result1 = json_decode($result1);

            return response()->json([
              'success' => [
                'status_code' => 200,
                'status' => 'Success',
                'data' => ['subscription' => $result, 'invoices' => $result1]
              ]
            ], 200);
        } else {

          return response()->json([
            'error' => [
              'status_code' => 400,
              'status' => 'Error',
              'message' => 'No integration found or Env variables missing'
            ]
          ], 400);
      }

      } else {

          return response()->json([
            'error' => [
              'status_code' => 400,
              'status' => 'Error',
              'message' => 'No subscription added'
            ]
          ], 400);
      }
    }
  }

  public function send_sms(Request $request)
  {

    $validator = Validator::make(
      array(
        'subdomain' => $request->subdomain,
        'mobile_number' => $request->mobile_number,
        'message' => $request->message,
        'fcm_token' => $request->fcm_token,
      ),
      array(
        'subdomain' => 'required',
        'mobile_number' => 'required',
        'message' => 'required',
        'fcm_token' => 'required',
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
      //Determining If Messages Exist For A Field
      if ($messages->has('subdomain')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('subdomain')
          ]
        ], 400);
      } else if ($messages->has('mobile_number')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('mobile_number')
          ]
        ], 400);
      } else if ($messages->has('message')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('message')
          ]
        ], 400);
      } else if ($messages->has('fcm_token')) {
        //Show custom message
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'message' => $messages->first('fcm_token')
          ]
        ], 400);
      }
    } else {

      $subdomain = $request->input('subdomain');
      $mobile_number = $request->input('mobile_number');
      $message = $request->input('message');
      $fcm_token = $request->input('fcm_token');

      config(['database.connections.mysql.database' => 'logezy_' . $subdomain, 'database.default' => 'mysql']);
      DB::reconnect('mysql');

      $agency = Agency::first();
      $agency_mobile = $agency->mobile_number != "" ? $agency->mobile_number : '';

      config(['database.connections.mysql.database' => 'logezycore', 'database.default' => 'mysql']);
      DB::reconnect('mysql');

      if ($agency_mobile) {

        $regNo = SmsDevice::where('mobile_number', $agency_mobile)->first();

        if ($regNo && $regNo->device_token) {

          $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

          $notification = [
            "title" => "Ionic 4 Notification",
            "body" => "This notification sent from POSTMAN using Firebase HTTP protocol",
            "sound" => "default",
            "click_action" => "FCM_PLUGIN_ACTIVITY",
            "icon" => "fcm_push_icon",
            "email" => $agency->email
          ];

          $data = [
            "mobileNo" => $mobile_number,
            "message" => $message
          ];

          $fcmData = [
            "notification" => $notification,
            "data" => $data,
            "to" => $regNo->device_token,
            "priority" => "high",
            "restricted_package_name" => ""
          ];


          $headers = [
            'Authorization: ' . $fcm_token . '',
            'Content-Type: application/json'
          ];


          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $fcmUrl);
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
          curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmData));
          $result = curl_exec($ch);
          curl_close($ch);


          $result = json_decode($result);

          if ($result->success == 1) {
            return response()->json([
              'success' => [
                'status_code' => 200,
                'status' => 'Success',
                'data' => "Message send successfully"
              ]
            ], 200);
          } else {
            return response()->json([
              'success' => [
                'status_code' => 400,
                'status' => 'Success',
                'data' => "Something went wrong"
              ]
            ], 400);
          }
          return response()->json([
            'success' => [
              'status_code' => 201,
              'status' => 'Success',
              'data' => "Message send successfully"
            ]
          ], 201);
        } else {
          return response()->json([
            'error' => [
              'status_code' => 400,
              'status' => 'Error',
              'data' => "Agency mobile number is not registered"
            ]
          ], 400);
        }
      } else {
        return response()->json([
          'error' => [
            'status_code' => 400,
            'status' => 'Error',
            'data' => "Please add agency mobile number"
          ]
        ], 400);
      }
    }
  }
}
