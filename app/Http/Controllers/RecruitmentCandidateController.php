<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

use App\Mail\ChangePasswordMail;
use App\Mail\NewCandidateRegistered;
use App\Mail\NotyfyAgencyCandidateRegistered;

/* Models */
use App\User;
use App\Tenant;
use App\UserRole;
use App\UserTenant;
use App\Tenants\Role;
use App\Tenants\RoleUser;
use App\Tenants\Candidate;
use App\Tenants\Job;
use App\Tenants\EmploymentType;
use App\Tenants\CandidateJob;
use App\Tenants\Document;
use App\Tenants\CandidateDocument;
use App\Tenants\RecruitmentCandidate;
use App\Tenants\RecruitmentBuiltForm;
use App\Tenants\CandidateApplicationFormDetail;
use App\Tenants\Agency;
use App\Tenants\RecruitmentJob;
use App\Tenants\AgencySetting;
use App\Jobs\NewCandidateRegisterJob;
use App\Helpers\SendMailGun;

use App\Rules\IsValidPassword;
use App\Rules\NotAllowedDomains;

use App\Traits\ActivityLog\ActivityCustomLogTrait;
use App\Traits\EmailTraits;

class RecruitmentCandidateController extends BaseController
{
    /**
     * getGeneralDetails.
     *
     * @param mixed $request
     *
     * @return void
     */
    public function getGeneralDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subdomain' => 'required'
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has('subdomain')) {
                return response()->json(
                    ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors()->first('subdomain'), 
                    ]], 400); 
            }
        }

        try {
            config(['database.connections.mysql.database' => 'logezy_' . $request->input('subdomain'), 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $jobs = Job::where(['status' => 'active', 'is_deleted' => 0])->orderBy('name', 'asc')->get(['id', 'color', 'name']);

            $employmentTypes = EmploymentType::where(['status' => 'active', 'is_deleted' => 0])->get(['id', 'title']);

            $hasRecruitmentJobs = AgencySetting::where('settings_key', 'recruitment_jobs_in_sign_up_form')->pluck('settings_value')->first() ?? false;

            if ($hasRecruitmentJobs === 'true') {
                $jobs = RecruitmentJob::where([
                    'status' => 'active',
                    'is_published' => 1,
                    'is_deleted' => 0
                ])->orderBy('title')->get(['id', 'title']);
            }

            return response()->json([
                'success' => [
                    'status_code' => 200,
                    'status' => 'Success',
                    'data' => [
                        'jobs' => $jobs,
                        'employmentTypes' => $employmentTypes,
                    ]],
                ], 200);

        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json([
                'error' => [
                    'status_code' => 500,
                    'status' => 'Error',
                    'message' => $ex->getMessage(),
                ],
            ], 500);
        }
    }

    /**
     * registerCandidate
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function registerCandidate(Request $request)
    {
        $regexName = '/^[0-9\W]*([a-zA-Z])+([\sa-zA-Z0-9\&\.\/\-,()&\'])*$/';
        $regexEmail = '/^([+_a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,20})$/';

        $validator = Validator::make($request->all(), [
            'subdomain' => 'required',
            'first_name' => ['required', 'regex:' . $regexName],
            'last_name' => ['required', 'regex:' . $regexName],
            'email' => ['required', 'email', 'regex:' . $regexEmail, new NotAllowedDomains],
            'job_id' => 'required',
            'password' => ['required', new isValidPassword(), 'same:password_confirm'],
            'password_confirm' => ['required'],
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has('subdomain')) {
                return response()->json(
                    ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors()->first('subdomain'), 
                    ]], 400); 
            } elseif ($validator->errors()->has('first_name')) {
                return response()->json(
                    ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors()->first('first_name'), 
                    ]], 400);
            } elseif ($validator->errors()->has('last_name')) {
                return response()->json(
                    ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors()->first('last_name'), 
                    ]], 400);
            } elseif ($validator->errors()->has('email')) {
                return response()->json(
                    ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors()->first('email'), 
                    ]], 400);
            } elseif ($validator->errors()->has('job_id')) {
                return response()->json(
                    ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors()->first('job_id'), 
                    ]], 400);
            } elseif ($validator->errors()->has('password')) {
                return response()->json(
                    ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors()->first('password'), 
                    ]], 400);
            } elseif ($validator->errors()->has('password_confirm')) {
                return response()->json(
                    ['error' => [
                        'status_code' => 400,
                        'status' => 'Error',
                        'message' => $validator->errors()->first('password_confirm'), 
                    ]], 400);
            }
        }

        try {
            $user = User::where('email', $request->email)->first();
            $tenant = Tenant::where('slug', $request->subdomain)->first();
            $newUser = FALSE;

            if ($user === NULL) {
                $user = new User();
                $user->name = $request->first_name . ' ' . $request->last_name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->status = 'active';
                $user->save();

                $userTenant = new UserTenant();
                $userTenant->user_id = $user->id;
                $userTenant->tenant_id = $tenant->id;
                $userTenant->save();

                $userRole = new UserRole();
                $userRole->user_id = $user->id;
                $userRole->role_id = 2;
                $userRole->save();

                $newUser = TRUE;
            } else {
                 if (UserTenant::where(['user_id' => $user->id, 'tenant_id' => $tenant->id])->exists() === FALSE) {
                    $userTenant = new UserTenant();
                    $userTenant->user_id = $user->id;
                    $userTenant->tenant_id = $tenant->id;
                    $userTenant->save();
                }
                if (UserRole::where(['user_id' => $user->id, 'role_id' => 2])->exists() === FALSE) {
                    $userRole = new UserRole();
                    $userRole->user_id = $user->id;
                    $userRole->role_id = 2;
                    $userRole->save();
                }
            }

            User::where('email', $request->email)->update(['password' => Hash::make($request->password)]);

            Config(['database.connections.mysql.database' => 'logezy_' . $request->input('subdomain'), 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $roles = Role::where('name', 'candidate')->first();

            if (RoleUser::where(['user_id' => $user->id, 'role_id' => $roles->id])->first()) {
                if (Candidate::where(['user_id' => $user->id, 'is_deleted' => 1])->exists()) {
                    return response()->json([
                      'error' => [
                        'status_code' => 403,
                        'status' => 'Error', 
                        'message' => "Please get in touch with the agency as there was already a candidate on this email address who was deleted."
                    ]], 403);
                }

                return response()->json([
                  'error' => [
                    'status_code' => 403,
                    'status' => 'Error', 
                    'message' => "Already an account exists on this email, you can login with the existing email id and password or you can reset your password by the forgot password option."
                ]], 403);
            }

            $roleUser = new RoleUser();
            $roleUser->user_id = $user->id;
            $roleUser->role_id = $roles->id;
            $roleUser->user_name = $user->name;
            $roleUser->email = $user->email;
            $roleUser->save();
            
            $candidateLatest = Candidate::latest()->first();
            $candidate = new Candidate();
            $candidate->user_id = $user->id;
            $candidate->first_name = $request->first_name;
            $candidate->last_name = $request->last_name;
            $candidate->full_name = $request->first_name . ' ' . $request->last_name;
            // $candidate->candidate_code = candidateCode($request->subdomain, $tenant->id, $candidateLatest ? $candidateLatest->id + 1 : 1);
            $candidate->address_line1 = $request->address ?? NULL;
            $candidate->post_code = $request->postcode ?? NULL;
            $candidate->place = $request->place ?? NULL; 
            $candidate->mobile_prefix = $request->mobile_prefix ?? NULL;
            $candidate->mobile_number = $request->mobile_number ?? NULL;
            $candidate->formatted_mobile_number = (isset($request->mobile_prefix) && isset($request->mobile_number)) ? "{$request->mobile_prefix} {$request->mobile_number}" : NULL;
            $candidate->candidate_status_id = 6;
            $candidate->created_by = $user->id;
            $candidate->has_access = 1;

            $historyLog = [[ 
                "title" => "Account Created",
                "description" => "Self registered through Recruitment (Mobile App)",
                "date" => Carbon::now()->format('d.m.Y'),
                "time" => Carbon::now()->format('g:i a'),
            ]];

            $candidate->history_logs = $historyLog;

            $candidate->save();
            
            //update candidate code
            $candidateCode = candidateCode($request->subdomain, $tenant->id, $candidate->id);
            $candidate->update(['candidate_code' => $candidateCode]);

            $recruitmentCandidate = new RecruitmentCandidate();
            $recruitmentCandidate->candidate_id = $candidate->id;
            $recruitmentCandidate->created_by = $user->id;
            $recruitmentCandidate->save();

            $jobId = $request->job_id;
            $hasRecruitmentJobs = AgencySetting::where('settings_key', 'recruitment_jobs_in_sign_up_form')->pluck('settings_value')->first() ?? false;

            if ($hasRecruitmentJobs === 'true') {
                $jobId = RecruitmentJob::where('id', $request->job_id)->first()->job_id ?? $request->job_id;
            }

            $candidateJob = new CandidateJob();
            $candidateJob->candidate_id = $candidate->id;
            $candidateJob->job_id = $jobId;
            $candidateJob->status = 'active';
            $candidateJob->save();

            $form = RecruitmentBuiltForm::whereHas('recruitmentJobsForm', function ($query) use ($request, $jobId) {
                $query->where('job_id', $jobId);
            })->first();

            if ($form) {
                $candidate->candidateApplicationFormDetail()->create([
                    'form_id' => $form->id,
                    'form_data' => json_encode((object) []),
                    'form_completed' => false,
                    'created_by' => $user->id
                ]);
            }

            $agency = Agency::first();
            $job = Job::find($jobId)->name;

            $signInUrl = "https://" . $request->input('subdomain') . ".logezy.co/signin";

            $replaceContent = [
                '[candidate_name]' => $candidate->full_name ?? "--",
                '[candidate_email]' => $request->email ?? "--",
                '[candidate_password]' => $request->password ?? "--",
                '[sign_in_url]' => $signInUrl ?? "--",
                '[agency_name]' => $agency->name ?? "--",
            ];

            $template = \App\Tenants\RecruitmentEmailSettings::where('template', 'candidate_registration')->first();

            $mailBody = $template->body;
            foreach ($replaceContent as $key => $value) {
                $mailBody = Str::replaceArray($key, [$value], $mailBody);
            }

            $details = EmailTraits::getRecruitmentEmailDetails('candidate_registration');

            $mailContent = [
                'title' => 'Registered successfully',
                'agency' => $agency->name,
                'agency_email' => $agency->email,
                'sender_email' => $details['sender_email'] ?? $agency->email,
                'candidate_first_name' => $request->first_name,
                'candidate_last_name' => $request->last_name,
                'candidate_address' => $candidate->address_line1,
                'candidate_post_code' => $candidate->post_code,
                'candidate_postion' => $job,
                'candidate_mobile' => $candidate->formatted_mobile_number,
                'candidate_name' => $request->first_name . ' ' . $request->last_name,
                'candidate_email' => $request->email,
                'password' => $request->password,
                'mail_body' => $mailBody,
                'logo' => $agency->agency_logo,
                'ios_app_url' => $agency->ios_app_url,
                'android_app_url' => $agency->android_app_url,
                'tenant_slug'   => $request->input('subdomain')
            ];

            switch ($newUser) {
                case TRUE:
                    if($details['enabled']){
                        SendMailGun::sendMail('recruitment-candidate-signup', $agency->name, $mailContent['sender_email'], $request->email, 'Registered successfully', $mailContent, null, null, null);
                        // Mail::to($request->email)->send(new NewCandidateRegistered($mailContent));
                    }                        
                break;

                case FALSE:
                SendMailGun::sendMail('mobile-app-change-password', $agency->name, $agency->email, $request->email, 'Change Password', $mailContent, null, null, null);
                // Mail::to($request->email)->send(new ChangePasswordMail($mailContent));
                break;
                
                default:
                    if($details['enabled']){
                        SendMailGun::sendMail('recruitment-candidate-signup', $agency->name, $mailContent['sender_email'], $request->email, 'Registered successfully', $mailContent, null, null, null);
                        // Mail::to($request->email)->send(new NewCandidateRegistered($mailContent));
                    }
                break;
            }

            $mailContent["title"] = "New candidate registered";

            if (is_null($template->recipients_emails)) {
                $emailArray = [$agency->email];
            } else {
                $emailArray = explode(",", $template->recipients_emails);
            }

            foreach ($emailArray as $email) {
                SendMailGun::sendMail('notify-agency-candidate-registration', $agency->name, $agency->email, $email, 'Registered successfully', $mailContent, null, null, null);
                // Mail::to($email)->send(new NotyfyAgencyCandidateRegistered($mailContent));
            }

            $loggedData = [
                'role'          => 'candidate',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'candidate_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
            ];

            //Activity Log
            $message = 'New Candidate '.$candidate->full_name .' has been signed up.';
            ActivityCustomLogTrait::saveActivityLog('Candidates', 'Create', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData 

            return response()->json([
                'success' => [
                    'status_code' => 200,
                    'status' => 'Success',
                    'data' => [
                        'user_id' => $user->id,
                        'candidate_id' => $candidate->id,
                    ]]
                ], 200);

        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json([
                'error' => [
                    'status_code' => 500,
                    'status' => 'Error',
                    'message' => $ex->getMessage(),
                ],
            ], 500);
        }
    }

    /**
     * getAvailableJobs
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function getAvailableJobs(Request $request)
    {
        try {
            config(['database.connections.mysql.database' => 'logezy_' . $request->input('subdomain'), 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $recruitmentJob = RecruitmentJob::query();
            $recruitmentJob = $recruitmentJob->with('Job')->where(['status' => 'active', 'is_deleted' => 0]);
            if ($request->exists('job_id')) {
                $recruitmentJob = $recruitmentJob->whereIn('job_id', json_decode($request->job_ids));
            }
            if ($request->exists('limit')) {
                $recruitmentJob = $recruitmentJob->limit($request->limit);
            }
            $recruitmentJob = $recruitmentJob->orderBy('id', 'DESC')->get();
            return response()->json(
                [
                    'success' => [
                        'status_code' => 200,
                        'status' => 'Success',
                        'data' => $recruitmentJob,
                    ],
                ],
                200
            );
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json($ex->getMessage());
        }
    }

    /**
     * getBuiltForm.
     *
     * @param mixed $request
     */
    public function getBuiltForm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subdomain' => 'required',
            "candidate_id" => "required",
            'job_id' => 'required',
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has('subdomain')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('subdomain'),
                        ],
                    ],
                    400
                );
            }
            if ($validator->errors()->has('candidate_id')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('candidate_id'),
                        ],
                    ],
                    400
                );
            }
            if ($validator->errors()->has('job_id')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('job_id'),
                        ],
                    ],
                    400
                );
            }
        }

        try {
            config(['database.connections.mysql.database' => 'logezy_' . $request->subdomain, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $formId = CandidateApplicationFormDetail::where([
                'candidate_id' => $request->candidate_id,
            ])->pluck('form_id')->first();

            if ($formId) {
                $builtForm = RecruitmentBuiltForm::where('id', $formId)->first();

                if ($builtForm) {
                    return response()->json(
                        [
                            'success' => [
                                'status_code' => 200,
                                'status' => 'Success',
                                'form_id' => $builtForm->id,
                                'data' => $builtForm->elements,
                            ],
                        ],
                        200
                    );
                }
            }

            $builtForm = RecruitmentBuiltForm::where('status', 'active')->whereHas('recruitmentJobsForm', function ($query) use ($request) {
                $query->where('job_id', $request->job_id);
            })->first();

            if ($builtForm) {
                return response()->json(
                    [
                        'success' => [
                            'status_code' => 200,
                            'status' => 'Success',
                            'form_id' => $builtForm->id,
                            'data' => $builtForm->elements,
                        ],
                    ],
                    200
                );
            }

            return response()->json(
                [
                    'error' => [
                        'status_code' => 403,
                        'status' => 'Error',
                        'message' => 'No form found for this job.',
                    ],
                ],
                403
            );
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                [
                    'error' => [
                        'status_code' => 500,
                        'status' => 'Error',
                        'message' => $ex->getMessage(),
                    ],
                ],
                500
            );
        }
    }

    /**
     * getCandidateAdditional
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function getCandidateAdditional(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subdomain' => 'required',
            'candidate_id' => 'required',
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has('subdomain')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('subdomain'),
                        ],
                    ],
                    400
                );
            }
            if ($validator->errors()->has('candidate_id')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('candidate_id'),
                        ],
                    ],
                    400
                );
            }
        }

        try {
            config(['database.connections.mysql.database' => 'logezy_' . $request->subdomain, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $form_data = (object) [];
            $formData = CandidateApplicationFormDetail::where('candidate_id', $request->candidate_id)->pluck('form_data')->first();
            $candidate_documents = CandidateDocument::where(["candidate_id" => $request->candidate_id, 'is_latest' => 1])->get();

            if ($formData) {
                $form_data = json_decode($formData);
            }

            return response()->json(
                [
                    'success' => [
                        'status_code' => 200,
                        'status' => 'Success',
                        'data' => compact('form_data', 'candidate_documents'),
                    ],
                ],
                200
            );
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                [
                    'error' => [
                        'status_code' => 500,
                        'status' => 'Error',
                        'message' => $ex->getMessage(),
                    ],
                ],
                500
            );
        }
    }

    /**
     * storeCandidateAdditional
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function storeCandidateAdditional(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subdomain' => 'required',
            'candidate_id' => 'required',
            'form_id' => 'required',
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has('subdomain')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('subdomain'),
                        ],
                    ],
                    400
                );
            }
            if ($validator->errors()->has('candidate_id')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('candidate_id'),
                        ],
                    ],
                    400
                );
            }
            if ($validator->errors()->has('form_id')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('form_id'),
                        ],
                    ],
                    400
                );
            }
        }

        try {
            config(['database.connections.mysql.database' => 'logezy_' . $request->subdomain, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $form_data = $request->form_data;

            if (is_array($request->form_data)) {
                $form_data = json_encode($request->form_data);
            }

            $additional = CandidateApplicationFormDetail::updateOrCreate([
                'candidate_id' => $request->candidate_id,
                'form_id' => $request->form_id
            ], [
                'form_data' => $form_data,
                'updated_platform' => 'mobile'
            ]);

            if ($request->has('form_completed') ? $request->form_completed === true : false) {
                
                $additional->update(['form_completed' => 1]);

                $candidateData = Candidate::where("id", $request->candidate_id)->select('id', 'user_id', 'full_name')->first();
                $candidate = $candidateData->full_name;

                $userIds = RoleUser::whereIn('role_id', [2, 3, 4])->pluck('user_id')->all();
                $userIds = array_unique($userIds);

                $notify = [
                    'title' => "Candidate submitted application form in recruitment",
                    'type' => "candidate_submitted_application_form_in_recruitment",
                    'body' => "The application form for " . $request->candidate_name ?? $candidate . " has been submitted in recruitment."
                ];

                if ($userIds) {
                    foreach ($userIds as $userId) {
                        $notification                  = new \App\Tenants\Notification;
                        $notification->type            = 'App\Notifications\HelloNotification';
                        $notification->notifiable_type = 'App\User';
                        $notification->notifiable_id   = $userId;
                        $notification->data            = json_encode($notify);
                        $notification->save();
                    }
                }                

                $details = EmailTraits::getRecruitmentEmailDetails('candidate_form_submit');
                if($details['enabled']) {
                    $agency = Agency::first();
                    
                    $mailContent["title"] = "Application form submitted";
                    $mailContent["agency_logo"] = $agency->agency_logo;
                    $mailContent["agency_email"] = $agency->email;
                    $mailContent["sender_email"] = $details['sender_email'] ?? $agency->email;
                    $mailContent["agency_name"] = $agency->name;
                    $mailContent["candidate_full_name"] = $request->candidate_name ?? $candidate;
                    $mailContent["tenant_slug"] = $request->subdomain;

                    SendMailGun::sendMail("candidate-completed-recruitment-application-form", $mailContent["agency_name"], $mailContent["sender_email"], $agency->email, $mailContent["title"], $mailContent, null, null, null);
                }

                $user = DB::table('logezycore.users')->where('id', $candidateData->user_id)->select('name', 'email')->first();
                
                $loggedData = [
                    'role'          => 'candidate',
                    'tenant'        => $request->input('subdomain'),
                    'dashboardType' => 'candidate_app',
                    'user_name'     => $user->name,
                    'email'         => $user->email,
                ];
                //Activity Log
                $message = 'Candidate - the application form has been submitted in recruitment.';
                ActivityCustomLogTrait::saveActivityLog('Candidates', 'Update', $message, $candidateData->id, $candidateData->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                [
                    'error' => [
                        'status_code' => 500,
                        'status' => 'Error',
                        'message' => $ex->getMessage(),
                    ],
                ],
                500
            );
        }
    }

    /**
     * uploadComplianceCertificate
     *
     * @param  mixed $request
     *
     * @return void
     */
    public function uploadCandidateCompliance(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'subdomain' => 'required',
            'candidate_id' => 'required',
            'document_id' => 'required',
            'document' => 'nullable|file|mimes:jpg,jpeg,png,pdf,doc,docx',
        ]);

        if ($validator->fails()) {
            if ($validator->errors()->has('subdomain')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('subdomain'),
                        ],
                    ],
                    400
                );
            } elseif ($validator->errors()->has('candidate_id')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('candidate_id'),
                        ],
                    ],
                    400
                );
            } elseif ($validator->errors()->has('document_id')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('document_id'),
                        ],
                    ],
                    400
                );
            } elseif ($validator->errors()->has('document')) {
                return response()->json(
                    [
                        'error' => [
                            'status_code' => 400,
                            'status' => 'Error',
                            'message' => $validator->errors()->first('document'),
                        ],
                    ],
                    400
                );
            }
        }

        try {
            config(['database.connections.mysql.database' => 'logezy_' . $request->subdomain, 'database.default' => 'mysql']);
            DB::reconnect('mysql');

            $fileName = NULL;
            $fileUrl = NULL;
            
            if ($request->has('document') &&  !is_null($request->document)) {
                $file = $request->file('document');
                $domain = $request->input('subdomain');
                $fileName = $file->getClientOriginalName();
                $imageName = $domain . '/' . time() . $fileName;

                Storage::disk('s3')->put($imageName, file_get_contents($request->file('document')), 'public');
                $fileUrl = Storage::disk('s3')->url($imageName);
            }

            CandidateDocument::where(
                [
                    'candidate_id' => $request->candidate_id,
                    'document_id' => $request->document_id
                ]
            )->update(['is_latest' => 0]);

            CandidateDocument::create([
                'document_id' => $request->document_id,
                'issue_date' => $request->issue_date ? Carbon::parse($request->issue_date) : Carbon::now(),
                'expiry_date' => $request->expiry_date ? Carbon::parse($request->expiry_date) : Carbon::now()->addMonths(3),
                'status' => 'active',
                'file' => $fileUrl,
                'candidate_id' => $request->candidate_id,
                'is_latest' => 1,
                'created_by' => Auth::user()->id,
            ]);

            $candidateData = Candidate::where("id", $request->candidate_id)->select('id', 'user_id', 'full_name')->first();
            $candidate = $candidateData->full_name;

            $userIds = RoleUser::whereIn('role_id', [2, 3, 4])->pluck('user_id')->all();
            $userIds = array_unique($userIds);

            $document = Document::where('id', $request->document_id)->pluck('title')->first();
            $success = $candidate . ' has uploaded ' . $document . ' document in recruitment, please check.';
            $notify = [];

            if ($userIds) {

                $notify['title'] = 'Candidate document uploaded'; 
                $notify['type'] = 'candidate_document_uploaded';
                $notify['body'] = $success;

                foreach ($userIds as $userId) {
                    $notification                  = new \App\Tenants\Notification;
                    $notification->type            = 'App\Notifications\HelloNotification';
                    $notification->notifiable_type = 'App\User';
                    $notification->notifiable_id   = $userId;
                    $notification->data            = json_encode($notify);
                    $notification->save();
                }
            }

            $user = DB::table('logezycore.users')->where('id', $candidateData->user_id)->select('id', 'name', 'email')->first();
            $loggedData = [
              'role'          => 'candidate',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'candidate_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];
            //Activity Log
            $message = 'Candidate Document - ' . $document . ' document has been uploaded in recruitment.';
            ActivityCustomLogTrait::saveActivityLog('Candidate Documents', 'Update', $message, $request->document_id, $document, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json(
                [
                    'success'=> [
                    'status_code' => 200,
                    'status' => 'Success', 
                    'message' => "Your document has been uploaded.",
                ]
            ], 200);

        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(
                [
                    'error' => [
                        'status_code' => 500,
                        'status' => 'Error',
                        'message' => $ex->getMessage(),
                    ],
                ],
                500
            );
        }
    }
}
