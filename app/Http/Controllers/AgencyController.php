<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenant;
use App\User;
use App\UserTenant;
use App\Tenants\Candidate;
use App\Tenants\Job;
use App\Tenants\Agency;
use App\Tenants\Vacancy;
use App\Tenants\RoleUser;
use App\Tenants\EmploymentType;
use App\Tenants\AgencySetting;
use App\Tenants\MobileAppSetting;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;

class AgencyController extends BaseController
{

  public function get_branches (Request $request) 
  {

    $validator = Validator::make(
      array(
        'subdomain' => $request->subdomain
      ), array(
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {

      $tenant_id = Tenant::where('slug', $request->subdomain)->first()->id;

      if ($tenant_id) {

        $tenants = Tenant::where('parent_id', $tenant_id)->orWhere('id', $tenant_id)->select('name', 'slug')->get();

        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'data' => $tenants
          ]
        ],200);

      } else {

        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => "No agency found !"
          ]
        ],400);

      }
    }
    
  }

  public function get_admins (Request $request) 
  {

    $validator = Validator::make(
      array(
        'subdomain' => $request->subdomain
      ), array(
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {

      config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
      DB::reconnect('mysql');

      $userIds = RoleUser::whereIn('role_id', [2, 3, 4])->pluck('user_id')->all();

      if ($userIds) {

        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'data' => $userIds
          ]
        ],200);

      } else {

        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => "No admins added !"
          ]
        ],400);

      }
    }
    
  }

  public function all_tenants (Request $request) 
  {
      $tenants = Tenant::where('parent_id', 0)->where('is_white_label', 0)->select('id', 'name','slug as subdomain')->get();

      $tenantsArr = [];

      if ( count($tenants) > 0 ) {
        foreach ($tenants as $tenant) {
          $branches = Tenant::where('parent_id', $tenant->id)->select('name', 'slug')->get();
          $tenantsArr[] = array(
            'name' => $tenant->name, 
            'subdomain' => $tenant->subdomain, 
            'isBranchExist' => count($branches)>0 ? true : false, 
          );
        }

      }

        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'data' => $tenantsArr
          ]
        ],200);
    
  }

  public function tenantsByUserEmail (Request $request) 
  {
      $validator = Validator::make(
                        array(
                        'email' => $request->email, 
                        ), array(
                        'email' => 'required|email', 
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
      } else {
        $user_id = User::where('email', $request->input('email'))->pluck('id');

        if (count($user_id) > 0) {

          $user_tenants = UserTenant::where('user_id', $user_id)->pluck('tenant_id')->all();
          $tenantIds = array_unique($user_tenants);
          if (isset($request->is_white_label) && $request->is_white_label == 1) {
            if(isset($request->subdomain) && $request->subdomain != "") {
              $parentTenantId = Tenant::where('slug', $request->subdomain)->pluck('id');
              $tenantBranches = Tenant::where('id', $parentTenantId)->orWhere('parent_id', $parentTenantId)->pluck('id')->all();
              $tenants = Tenant::whereIn('id', $tenantIds)->whereIn('id', $tenantBranches)->select('id','name','slug as subdomain')->get();
            } else {
              $tenants = Tenant::whereIn('id', $tenantIds)->select('id','name','slug as subdomain')->get();
            }
          } else {
            // $tenants = Tenant::whereIn('id', $tenantIds)->where('parent_id', 0)->where('is_white_label', 0)->select('id','name','slug as subdomain')->get();
            $tenants = Tenant::whereIn('id', $tenantIds)->select('id','name','slug as subdomain')->get();
          }
          
          $tenantsArr = [];

          if ( count($tenants) > 0 ) {
            foreach ($tenants as $tenant) {
              $branches = Tenant::where('parent_id', $tenant->id)->select('name', 'slug')->get();
              //reconnecting database
              config(['database.connections.mysql.database' => 'logezy_'.$tenant->subdomain,'database.default'=>'mysql']);
              DB::reconnect('mysql');
              $roleUser  = RoleUser::where('role_id', 6)->where('user_id', $user_id)->first();
              if ( $roleUser ) {
                $tenantsArr[] = array(
                  'name' => $tenant->name, 
                  'subdomain' => $tenant->subdomain, 
                  'isBranchExist' => count($branches)>0 ? true : false, 
                );
              }
              //reconnecting database
              config(['database.connections.mysql.database' => 'logezycore','database.default'=>'mysql']);
              DB::reconnect('mysql');
            }

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $tenantsArr
              ]
            ],200);

          } else {
          
            return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Unauthorized', 
                      'message' => "You don't have access to continue."
                    ]
                  ],401);
          }

        } else {
          
          return response()->json([
                  'error'=>[
                    'status_code' => 404,
                    'status' => 'Not Found', 
                    'message' => "No user found on this email address."
                  ]
                ],404);
        }


      } 
  }

  public function tenantsByEmailGeneralPurpose (Request $request) 
  {
      $validator = Validator::make(
                        array(
                        'email' => $request->email, 
                        ), array(
                        'email' => 'required|email', 
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
      } else {
        $user_id = User::where('email', $request->input('email'))->pluck('id');

        if (count($user_id) > 0) {

          $user_tenants = UserTenant::where('user_id', $user_id)->pluck('tenant_id')->all();
          $tenantIds = array_unique($user_tenants);
          $tenants = Tenant::whereIn('id', $tenantIds)->select('id','name','slug as subdomain')->get();

          $tenantsArr = [];

          if ( count($tenants) > 0 ) {
            foreach ($tenants as $tenant) {
              $branches = Tenant::where('parent_id', $tenant->id)->select('name', 'slug')->get();
              //reconnecting database
              config(['database.connections.mysql.database' => 'logezy_'.$tenant->subdomain,'database.default'=>'mysql']);
              DB::reconnect('mysql');
              $roleUser  = RoleUser::where('role_id', 6)->where('user_id', $user_id)->first();
              if ( $roleUser ) {
                $tenantsArr[] = array(
                  'name' => $tenant->name, 
                  'subdomain' => $tenant->subdomain, 
                  'isBranchExist' => count($branches)>0 ? true : false, 
                );
              }
            }

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $tenantsArr
              ]
            ],200);

          } else {
          
            return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Unauthorized', 
                      'message' => "You don't have access to continue."
                    ]
                  ],401);
          }

        } else {
          
          return response()->json([
                  'error'=>[
                    'status_code' => 404,
                    'status' => 'Not Found', 
                    'message' => "No user found on this email address."
                  ]
                ],404);
        }


      } 
  }

  public function agencyTenantsByUserEmail (Request $request) 
  {
      $validator = Validator::make(
                        array(
                        'email' => $request->email, 
                        ), array(
                        'email' => 'required|email', 
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
      } else {
        $user_id = User::where('email', $request->input('email'))->pluck('id');

        if (count($user_id) > 0) {

          $user_tenants = UserTenant::where('user_id', $user_id)->pluck('tenant_id')->all();
          $tenantIds = array_unique($user_tenants);
          $tenants = Tenant::whereIn('id', $tenantIds)->select('id','name','slug as subdomain')->get();

          $tenantsArr = [];

          if ( count($tenants) > 0 ) {
            foreach ($tenants as $tenant) {
              //reconnecting database
              config(['database.connections.mysql.database' => 'logezy_'.$tenant->subdomain,'database.default'=>'mysql']);
              DB::reconnect('mysql');
              $roleUsers  = RoleUser::whereIn('role_id', [2,3,4])->where('user_id', $user_id)->get();
              if ( count($roleUsers) > 0 ) {
                $tenantsArr[] = array(
                  'name' => $tenant->name, 
                  'subdomain' => $tenant->subdomain, 
                );
              }
            }

            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $tenantsArr
              ]
            ],200);

          } else {
          
            return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Unauthorized', 
                      'message' => "You don't have access to continue."
                    ]
                  ],401);
          }

        } else {
          
          return response()->json([
                  'error'=>[
                    'status_code' => 404,
                    'status' => 'Not Found', 
                    'message' => "No user found on this email address."
                  ]
                ],404);
        }


      } 
  }

  public function get_jobs (Request $request) 
  {

    $validator = Validator::make(
      array(
        'subdomain' => $request->subdomain
      ), array(
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {


      config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
      DB::reconnect('mysql');

      $jobs = Job::where('is_deleted', 0)->where('status', 'active')->orderBy('name', 'asc')->get();

      return response()->json([
        'success'=>[
          'status_code' => 200,
          'status' => 'Success', 
          'data' => $jobs
        ]
      ],200);
    }
    
  }
  public function get_employment_types (Request $request) 
  {

    $validator = Validator::make(
      array(
        'subdomain' => $request->subdomain
      ), array(
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {


      config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
      DB::reconnect('mysql');

      $employmentTypes = EmploymentType::where('is_deleted', 0)->where('status', 'active')->get();

      return response()->json([
        'success'=>[
          'status_code' => 200,
          'status' => 'Success', 
          'data' => $employmentTypes
        ]
      ],200);
    }
    
  }

  public function update_firebase_server_key_white_label_app(Request $request)
  {
    $validator = Validator::make(
      array(
        'firebase_server_key' => $request->firebase_server_key,
        'subdomain' => $request->subdomain
      ), array(
        'firebase_server_key' => 'required',
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('firebase_server_key')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('firebase_server_key')
          ]
        ],400);
      }
      else if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {

      $tenant_id = Tenant::where('slug', $request->subdomain)->first()->id;

      $tenants = Tenant::where('parent_id', $tenant_id)->orWhere('id', $tenant_id)->select('id', 'name', 'slug')->get();

      if ( count($tenants) > 0) {
          foreach ($tenants as $tenant) {
                $tenant->is_white_label  = 1;
                $tenant->save();
          }
      }

      if ( count($tenants) > 0) {

          foreach ($tenants as $tenant) {
              config(['database.connections.mysql.database' => 'logezy_'.$tenant->slug,'database.default'=>'mysql']);
              DB::reconnect('mysql');

              $agencySettingsCheck = AgencySetting::where('settings_key', 'firebase_server_key')->first();

              if ($agencySettingsCheck) {
                
                $agencySettingsCheck->settings_value  = $request->input('firebase_server_key');
                $agencySettingsCheck->save();

              } else {

                $agencySettings = new AgencySetting;
                $agencySettings->settings_key    = 'firebase_server_key';
                $agencySettings->settings_value  = $request->input('firebase_server_key');
                $agencySettings->save();

              }
          }

      }

      return response()->json([
        'success'=>[
          'status_code' => 200,
          'status' => 'Success', 
          'message' => "Firebase server key updated successfully."
        ]
      ],200);
    }

  }

  public function update_firebase_server_key_non_white_label_app(Request $request)
  {
    $validator = Validator::make(
      array(
        'firebase_server_key' => $request->firebase_server_key,
      ), array(
        'firebase_server_key' => 'required',
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('firebase_server_key')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('firebase_server_key')
          ]
        ],400);
      }
      // else if ($messages->has('subdomain')) {
      //           //Show custom message
      //   return response()->json([
      //     'error'=>[
      //       'status_code' => 400,
      //       'status' => 'Error', 
      //       'message' => $messages->first('subdomain')
      //     ]
      //   ],400);
      // }


    } else {

      $tenants = Tenant::where('is_white_label', 0)->get();


      if ( count($tenants) > 0) {

          foreach ($tenants as $tenant) {
            $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
            $db = DB::select($query, [$tenant->db_name]);

            if( !empty($db) ){
              config(['database.connections.mysql.database' => 'logezy_'.$tenant->slug,'database.default'=>'mysql']);
              DB::reconnect('mysql');

              $agencySettingsCheck = AgencySetting::where('settings_key', 'firebase_server_key')->first();

              if ($agencySettingsCheck) {
                
                $agencySettingsCheck->settings_value  = $request->input('firebase_server_key');
                $agencySettingsCheck->save();

              } else {

                $agencySettings = new AgencySetting;
                $agencySettings->settings_key    = 'firebase_server_key';
                $agencySettings->settings_value  = $request->input('firebase_server_key');
                $agencySettings->save();

              }
            }
          }

      }

      return response()->json([
        'success'=>[
          'status_code' => 200,
          'status' => 'Success', 
          'message' => "Firebase server key updated successfully."
        ]
      ],200);
    }

  }

  public function update_firebase_server_key_manager_app(Request $request)
  {
    $validator = Validator::make(
      array(
        'managerapp_firebase_server_key' => $request->managerapp_firebase_server_key,
        // 'subdomain' => $request->subdomain
      ), array(
        'managerapp_firebase_server_key' => 'required',
        // 'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('managerapp_firebase_server_key')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('managerapp_firebase_server_key')
          ]
        ],400);
      }
      // else if ($messages->has('subdomain')) {
      //           //Show custom message
      //   return response()->json([
      //     'error'=>[
      //       'status_code' => 400,
      //       'status' => 'Error', 
      //       'message' => $messages->first('subdomain')
      //     ]
      //   ],400);
      // }


    } else {

      $tenants = Tenant::all();

      if ( count($tenants) > 0) {

          foreach ($tenants as $tenant) {
            $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";
            $db = DB::select($query, [$tenant->db_name]);

            if( !empty($db) ){
              config(['database.connections.mysql.database' => 'logezy_'.$tenant->slug,'database.default'=>'mysql']);
              DB::reconnect('mysql');

              $agencySettingsCheck = AgencySetting::where('settings_key', 'managerapp_firebase_server_key')->first();

              if ($agencySettingsCheck) {
                
                $agencySettingsCheck->settings_value  = $request->input('managerapp_firebase_server_key');
                $agencySettingsCheck->save();

              } else {

                $agencySettings = new AgencySetting;
                $agencySettings->settings_key    = 'managerapp_firebase_server_key';
                $agencySettings->settings_value  = $request->input('managerapp_firebase_server_key');
                $agencySettings->save();

              }
            }
          }

      }

      // config(['database.connections.mysql.database' => 'logezy_'.$request->subdomain,'database.default'=>'mysql']);
      // DB::reconnect('mysql');

      // $agencySettingsCheck = AgencySetting::where('settings_key', 'managerapp_firebase_server_key')->first();

      // if ($agencySettingsCheck) {
        
      //   $agencySettingsCheck->settings_value  = $request->input('managerapp_firebase_server_key');
      //   $agencySettingsCheck->save();

      // } else {

      //   $agencySettings = new AgencySetting;
      //   $agencySettings->settings_key    = 'managerapp_firebase_server_key';
      //   $agencySettings->settings_value  = $request->input('managerapp_firebase_server_key');
      //   $agencySettings->save();

      // }

      return response()->json([
        'success'=>[
          'status_code' => 200,
          'status' => 'Success', 
          'message' => "Firebase server key updated successfully."
        ]
      ],200);
    }

  }

  public function update_firebase_uin(Request $request)
  {
      $validator = Validator::make(
                      array(
                      'user_id' => $request->user_id,
                      'firebase_uin' => $request->firebase_uin,
                      'player_id' => $request->player_id,
                      'subdomain' => $request->subdomain
                      ), array(
                      'user_id' => 'required',
                      'firebase_uin' => 'nullable',
                      'player_id' => 'nullable',
                      'subdomain' => 'required'
                  )
              );

    if ($validator->fails()) {

          $messages = $validator->errors();
          //Determining If Messages Exist For A Field
          if ($messages->has('user_id')) {
              //Show custom message
              return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => $messages->first('user_id')
                ]
              ],400);
          }
          else if ($messages->has('subdomain')) {
              //Show custom message
              return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => $messages->first('subdomain')
                ]
              ],400);
          }


    } else {

          $user_id = $request->input('user_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');
          //This api is used to update both candidate and manager user UIN
          if(isset($request->role) && $request->role == "candidate") {
            $user = RoleUser::where('user_id', $user_id)->where('role_id', 6)->first();
          } else {
            $user = RoleUser::where('user_id', $user_id)->whereIn('role_id', [2,3,4])->first();
          }
          if($user) {
            $user->firebase_uin = isset($request->firebase_uin) ? $request->firebase_uin : '';
            $user->player_id = isset($request->player_id) ? $request->player_id : '';
            $user->save();

            return response()->json([
                  'success'=>[
                    'status_code' => 200,
                    'status' => 'Success', 
                    'message' => "UIN updated successfully."
                  ]
                ],200);
          } else {

            return response()->json(
              [
                'error' => [
               'status_code' => 400,
               'status' => 'Error',
               'message' => "No user found !", 
              ]
            ], 400); 
          }
    }

  }

  public function getAgency(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'subdomain' => 'required',
    ]);

    if ($validator->fails()) {

      if ($validator->errors()->has('subdomain')) {
        return response()->json(
          ['error' => [
           'status_code' => 400,
           'status' => 'Error',
           'message' => $validator->errors()->first('subdomain'), ]], 400); 
      }

    } else {
      try {
        config(['database.connections.mysql.database' => 'logezy_' . $request->subdomain, 'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $agency = Agency::first();
        $agency['app_recruitment_about_content'] = AgencySetting::where('settings_key', 'app_recruitment_about_content')->first()->settings_value ?? '';

        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'agency' => $agency
          ]
        ],200);
      } catch (\Illuminate\Database\QueryException $ex) {
        return response()->json(
          ['error' => [
           'status_code' => 500,
           'status' => 'Error',
           'message' => $ex->getMessage(), ]], 500);
      }
    }
  }

  public function get_signedtimesheet_declaration(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'subdomain' => 'required',
    ]);

    if ($validator->fails()) {

      if ($validator->errors()->has('subdomain')) {
        return response()->json(
          ['error' => [
           'status_code' => 400,
           'status' => 'Error',
           'message' => $validator->errors()->first('subdomain'), ]], 400); 
      }

    } else {
      try {
        config(['database.connections.mysql.database' => 'logezy_' . $request->subdomain, 'database.default'=>'mysql']);
        DB::reconnect('mysql');
        $agencySettings = AgencySetting::where('settings_key', 'signed_timesheet_declaration')->first();
        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'agency' =>  $agencySettings ? $agencySettings->settings_value : NULL,
          ]
        ],200);
      } catch (\Illuminate\Database\QueryException $ex) {
        return response()->json(
          ['error' => [
           'status_code' => 500,
           'status' => 'Error',
           'message' => $ex->getMessage(), ]], 500);
      }
    }

  }

  public function agency_settings(Request $request){
    $validator = Validator::make($request->all(), [
      'subdomain' => 'required',
      'settings_key' => 'required',
    ]);

    if ($validator->fails()) {

      if ($validator->errors()->has('subdomain')) {
        return response()->json(
          ['error' => [
           'status_code' => 400,
           'status' => 'Error',
           'message' => $validator->errors()->first('subdomain'), ]], 400); 
      } else if ($validator->errors()->has('settings_key')) {
        return response()->json(
          ['error' => [
           'status_code' => 400,
           'status' => 'Error',
           'message' => $validator->errors()->first('settings_key'), ]], 400); 
      }

    } else {
      try {
        config(['database.connections.mysql.database' => 'logezy_' . $request->subdomain, 'database.default'=>'mysql']);
        DB::reconnect('mysql');


      $agencySetting = DB::table('agency_settings')->where('settings_key', $request->settings_key)->first(); 
      if ($agencySetting) {
          return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            $request->settings_key =>  $agencySetting->settings_value,
            'data' => $agencySetting->details
          ]
        ],200);
      } else {
          return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            $request->settings_key => NULL,
            'data' => []
          ]
        ],200);
      }
        
      } catch (\Illuminate\Database\QueryException $ex) {
        return response()->json(
          ['error' => [
           'status_code' => 500,
           'status' => 'Error',
           'message' => $ex->getMessage(), ]], 500);
      }
    }
  }

  public function get_integration(Request $request){

    $validator = Validator::make($request->all(), [
      'slug' => 'required',
    ]);

    if ($validator->fails()) {

      if ($validator->errors()->has('slug')) {
        return response()->json(
          ['error' => [
           'status_code' => 400,
           'status' => 'Error',
           'message' => $validator->errors()->first('slug'), ]], 400); 
      }

    } else {
      try {
        config(['database.connections.mysql.database' => 'logezycore', 'database.default'=>'mysql']);
        DB::reconnect('mysql');


      $integration = DB::table('integrations')->where('slug', $request->slug)->first(); 
      if ($integration) {
          return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'data' => $integration->access_token,
            'additional_data' => $integration
          ]
        ],200);
      } else {
          return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'data' => NULL,
            'data' => [],
            'additional_data' => NULL
          ]
        ],200);
      }
        
      } catch (\Illuminate\Database\QueryException $ex) {
        return response()->json(
          ['error' => [
           'status_code' => 500,
           'status' => 'Error',
           'message' => $ex->getMessage(), ]], 500);
      }
    }
  }
  
  public function mobile_app_settings(Request $request){
    $validator = Validator::make($request->all(), [
      'subdomain' => 'required',
    ]);

    if ($validator->fails()) {

      if ($validator->errors()->has('subdomain')) {
        return response()->json(
          ['error' => [
           'status_code' => 400,
           'status' => 'Error',
           'message' => $validator->errors()->first('subdomain'), ]], 400); 
      }

    } else {
      try {
        config(['database.connections.mysql.database' => 'logezy_' . $request->subdomain, 'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $mobileAppSetting = MobileAppSetting::get(['name', 'key', 'value']); 
        if ($mobileAppSetting) {
            return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success', 
              'data' => $mobileAppSetting
            ]
          ],200);
        } else {
            return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success',
              'data' => []
            ]
          ],200);
        }
        
      } catch (\Illuminate\Database\QueryException $ex) {
        return response()->json(
          ['error' => [
           'status_code' => 500,
           'status' => 'Error',
           'message' => $ex->getMessage(), ]], 500);
      }
    }
  }
}
