<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\AssignVacancy;
use App\Tenants\Helper;
use App\Tenants\Timeclock;
use App\Tenants\TimeclockDetail;
use App\Tenants\TimeclockDetailLocation;
use App\Tenants\RoleUser;
use App\Tenants\Agency;
use App\Traits\PushNotificationTrait;
use App\Tenants\Notification;
use App\Traits\ActivityLog\ActivityCustomLogTrait;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use DateTime;
use Illuminate\Database\DatabaseManager;

class TimeclockController extends BaseController
{

  /**
   * store the clock in time
   *
   * @return     <type>  The schedule.
   */
  public function timeclock(Request $request) {

      $validator = Validator::make(
                        array(
                        'assigned_vacancy_id' => $request->assigned_vacancy_id, 
                        'user_id' => $request->user_id, 
                        'type' => $request->type, 
                        'clock_time' => $request->clock_time, 
                        'latitude' => $request->latitude, 
                        'longitude' => $request->longitude, 
                        'address_line' => $request->address_line, 
                        'subdomain' => $request->subdomain
                        ), array(
                        'assigned_vacancy_id' => 'required', 
                        'user_id' => 'required', 
                        'type' => 'required', 
                        'clock_time' => 'required', 
                        'latitude' => 'required', 
                        'longitude' => 'required', 
                        'address_line' => 'required', 
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('assigned_vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('assigned_vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('user_id')
                  ]
                ],400);
            }
            else if ($messages->has('type')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('type')
                  ]
                ],400);
            }
            else if ($messages->has('clock_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('clock_time')
                  ]
                ],400);
            }
            else if ($messages->has('latitude')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('latitude')
                  ]
                ],400);
            }
            else if ($messages->has('longitude')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('longitude')
                  ]
                ],400);
            }
            else if ($messages->has('address_line')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('address_line')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        $assigned_vacancy_id = $request->assigned_vacancy_id; 

        //rounding of the given clock time
        $dateTime = new DateTime($request->clock_time);
        $roundedDateTime = $this->roundMinuteInterval($dateTime);
        $clock_time = $roundedDateTime->format("Y-m-d H:i:s");

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');
        //checking intial validation
        $agencySetting = DB::table('agency_settings')->where('settings_key', 'timeclock_status')->first(); 

        if(!$agencySetting) {
          return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'error', 
                'message' => "You don't have any access to perform this action"
              ]
            ],400);
        }

        if($agencySetting && $agencySetting->settings_value != "true") {
          return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'error', 
                'message' => "You don't have any access to perform this action"
              ]
            ],400);
        }

        $assign = AssignVacancy::find($assigned_vacancy_id);

        $agencySettingDetails = (array)json_decode($agencySetting->details, true);

        $candidate = DB::table('candidates')->where('id', $assign->candidate_id)->select('id', 'full_name')->first();
        $user = DB::table('logezycore.users')->where('id', $request->user_id)->select('id', 'name', 'email')->first();

        $loggedData = [
          'role'          => 'candidate',
          'tenant'        => $request->input('subdomain'),
          'dashboardType' => 'candidate_app',
          'user_name'     => $user->name,
          'email'         => $user->email,
        ];

        if($request->type == "clock_in"){

          // if($agencySettingDetails['clock_in']['status'] == true) {
          //   if($agencySettingDetails['clock_in']['clockin_at_shift_start']==1) {
          //     if(strtotime($assign->start_time) > strtotime(date("Y-m-d H:i"))) {

          //       return response()->json([
          //         'error'=>[
          //           'status_code' => 400,
          //           'status' => 'error', 
          //           'message' => "You cannot clock in as the shift isn't started yet."
          //         ]
          //       ],400);

          //     } else if(strtotime($assign->start_time) < strtotime(date("Y-m-d H:i"))) {

          //       return response()->json([
          //         'error'=>[
          //           'status_code' => 400,
          //           'status' => 'error', 
          //           'message' => "You cannot clock in as the shift is already started."
          //         ]
          //       ],400);

          //     }
          //   } else if ($agencySettingDetails['clock_in']['relaxed_time']['before']!='' && $agencySettingDetails['clock_in']['relaxed_time']['before']>0) {
          //       if(strtotime(date("Y-m-d H:i")) < strtotime($assign->start_time) && strtotime(date("Y-m-d H:i")) < strtotime('-'.$agencySettingDetails['clock_in']['relaxed_time']['before'].' minutes',strtotime($assign->start_time))) {
          //         return response()->json([
          //           'error'=>[
          //             'status_code' => 400,
          //             'status' => 'error', 
          //             'message' => "You are not in relaxed time before the shift start."
          //           ]
          //         ],400);
                  
          //       }

          //   } else if ($agencySettingDetails['clock_in']['relaxed_time']['after']!='' && $agencySettingDetails['clock_in']['relaxed_time']['after']>0) {
          //       if(strtotime(date("Y-m-d H:i")) > strtotime($assign->start_time) || strtotime(date("Y-m-d H:i")) > strtotime('+'.$agencySettingDetails['clock_in']['relaxed_time']['before'].' minutes',strtotime($assign->start_time))) {
          //         return response()->json([
          //           'error'=>[
          //             'status_code' => 400,
          //             'status' => 'error', 
          //             'message' => "You are not in relaxed time after the shift start."
          //           ]
          //         ],400);
                  
          //       }

          //   }
          // }

          $timeclock = Timeclock::where('assigned_vacancy_id', $assigned_vacancy_id)->where('status', 'pending')->first();
          if($timeclock) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'error', 
                'message' => "You've already started the shift"
              ]
            ],400);
          }
          //inserting timeclock table
          $timeclock = new Timeclock;
          $timeclock->assigned_vacancy_id = $request->assigned_vacancy_id;
          $timeclock->vacancy_id = $assign->vacancy_id;
          $timeclock->candidate_id = $assign->candidate_id;
          $timeclock->clock_in = date("Y-m-d H:i:s", strtotime($clock_time));
          $timeclock->clock_out = NULL;
          $timeclock->break = 0;
          $timeclock->status = "pending";
          $timeclock->status_by = $request->user_id;
          $timeclock->created_by = $request->user_id;
          $timeclock->status_at   = date('Y-m-d H:i:s');
          $timeclock->save();

          //inserting timeclock details
          $timeclockDetail        = new TimeclockDetail;
          $timeclockDetail->timeclock_id  = $timeclock->id;
          $timeclockDetail->clock_in    = date("Y-m-d H:i:s", strtotime($clock_time));
          $timeclockDetail->clock_out   = NULL;
          $timeclockDetail->created_by  = Auth::id();

          if($timeclockDetail->save()){
            //Inserting timeclock locations
            $timeclockDetailLocation = new TimeclockDetailLocation;       
            $timeclockDetailLocation->timeclock_detail_id = $timeclockDetail->id;
            $timeclockDetailLocation->latitude = $request->latitude;
            $timeclockDetailLocation->longitude = $request->longitude;
            $timeclockDetailLocation->address_line = $request->address_line;
            $timeclockDetailLocation->type = "clock_in";
            $timeclockDetailLocation->created_by = $request->user_id;
            $timeclockDetailLocation->save();
          }

          //Activity Log
          $message = 'Candidate has been started the shift with booking code '.$assign->code;
          ActivityCustomLogTrait::saveActivityLog('Time Clock', 'Update', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData 

          return response()->json([
            'success'=>[
              'status_code' => 201,
              'status' => 'Success', 
              'data' => $timeclock,
              'message' => "You've started the shift"
            ]
          ],201);
        } else if($request->type == "break_start"){

          $timeclock = Timeclock::where('assigned_vacancy_id', $assigned_vacancy_id)->where('status', 'pending')->first();
          if(!$timeclock) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'error', 
                'message' => "You haven't started the shift yet"
              ]
            ],400);
          }
          //inserting timeclock details
          $timeclockDetail        = TimeclockDetail::where('timeclock_id', $timeclock->id)->whereNotNull('clock_in')->whereNull('clock_out')->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
          if($timeclockDetail) {
            $timeclockDetail->clock_out    = date("Y-m-d H:i:s", strtotime($clock_time));

            if($timeclockDetail->save()){
              //Inserting timeclock locations
              $timeclockDetailLocation = new TimeclockDetailLocation;       
              $timeclockDetailLocation->timeclock_detail_id = $timeclockDetail->id;
              $timeclockDetailLocation->latitude = $request->latitude;
              $timeclockDetailLocation->longitude = $request->longitude;
              $timeclockDetailLocation->address_line = $request->address_line;
              $timeclockDetailLocation->type = "break_start";
              $timeclockDetailLocation->created_by = $request->user_id;
              $timeclockDetailLocation->save();
            }

            //Activity Log
            $message = 'Candidate has been started the break time with booking code '.$assign->code;
            ActivityCustomLogTrait::saveActivityLog('Time Clock', 'Update', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData 

            return response()->json([
              'success'=>[
                'status_code' => 201,
                'status' => 'Success', 
                'data' => $timeclock,
                'message' => "You've started the break time"
              ]
            ],201);
          } else {
            return response()->json([
              'success'=>[
                'status_code' => 201,
                'status' => 'Success', 
                'data' => $timeclock,
                'message' => "You are in break now"
              ]
            ],201);
          }
        } else if($request->type == "break_end"){

          $timeclock = Timeclock::where('assigned_vacancy_id', $assigned_vacancy_id)->where('status', 'pending')->first();
          if(!$timeclock) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'error', 
                'message' => "You haven't started the shift yet"
              ]
            ],400);
          }
          //update or inserting timeclock details
          $timeclockDetail = TimeclockDetail::where('timeclock_id', $timeclock->id)->whereNotNull('clock_in')->whereNull('clock_out')->orderBy('id', 'DESC')->first();
          if($timeclockDetail){
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'error', 
                'message' => "You have already resumed your shift"
              ]
            ],400);
          }
          $timeclockDetail        = new TimeclockDetail;
          $timeclockDetail->timeclock_id  = $timeclock->id;
          $timeclockDetail->clock_in    = date("Y-m-d H:i:s", strtotime($clock_time));
          $timeclockDetail->clock_out   = NULL;
          $timeclockDetail->created_by  = Auth::id();

          if($timeclockDetail->save()){
            //calculating break
            $timeclockLatestBreakStart = TimeclockDetail::where('timeclock_id', $timeclock->id)->whereNotNull('clock_in')->whereNotNull('clock_out')->orderBy('id', 'DESC')->first();
            $clock_out = $timeclockLatestBreakStart->clock_out;
            $clock_in = $timeclockDetail->clock_in;
            $timeDifference = \Carbon\Carbon::parse($clock_out)->diffInMinutes(\Carbon\Carbon::parse($clock_in));
            $break = $timeclock->break + $timeDifference; 

            $timeclock->break = $break;
            $timeclock->save();


            //Inserting timeclock locations
            $timeclockDetailLocation = new TimeclockDetailLocation;       
            $timeclockDetailLocation->timeclock_detail_id = $timeclockDetail->id;
            $timeclockDetailLocation->latitude = $request->latitude;
            $timeclockDetailLocation->longitude = $request->longitude;
            $timeclockDetailLocation->address_line = $request->address_line;
            $timeclockDetailLocation->type = "break_end";
            $timeclockDetailLocation->created_by = $request->user_id;
            $timeclockDetailLocation->save();
          }
          //Activity Log
          $message = 'Candidate has been resumed the shift with booking code '.$assign->code;
          ActivityCustomLogTrait::saveActivityLog('Time Clock', 'Update', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData 

          return response()->json([
            'success'=>[
              'status_code' => 201,
              'status' => 'Success', 
              'data' => $timeclock,
              'message' => "You've resumed the shift"
            ]
          ],201);
        } else if($request->type == "clock_out"){

          $timeclock = Timeclock::where('assigned_vacancy_id', $assigned_vacancy_id)->where('status', 'pending')->orderBy('id', 'DESC')->first();

          if(!$timeclock) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'error', 
                'message' => "You haven't started the shift yet"
              ]
            ],400);
          }
          //calculate total hours
          $inputs = array(
              'on_date'    => date('Y-m-d', strtotime($timeclock->clock_in)),
              'start_time' => date("H:i", strtotime($timeclock->clock_in)),
              'end_time'   => date("H:i", strtotime($clock_time)),
              'break'      => $timeclock->break,
          );
          $totalHours = Helper::getTotalHours($inputs);
          $timeclock->clock_out = date("Y-m-d H:i:s", strtotime($clock_time));
          $timeclock->total_hours = $totalHours;
          $timeclock->notes       = $request->notes??NULL;
          $timeclock->save();

          //updating timeclock details
          $timeclockDetail        = TimeclockDetail::where('timeclock_id', $timeclock->id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
          $timeclockDetail->clock_out    = date("Y-m-d H:i:s", strtotime($clock_time));

          if($timeclockDetail->save()){
            //update or inserting timeclock locations
            $timeclockDetailLocation = TimeclockDetailLocation::where('timeclock_detail_id', $timeclockDetail->id)->where('type', 'clock_out')->orderBy('id', 'DESC')->first();
            if(!$timeclockDetailLocation){
              $timeclockDetailLocation        = new TimeclockDetailLocation;
            } 
            $timeclockDetailLocation->timeclock_detail_id = $timeclockDetail->id;
            $timeclockDetailLocation->latitude = $request->latitude;
            $timeclockDetailLocation->longitude = $request->longitude;
            $timeclockDetailLocation->address_line = $request->address_line;
            $timeclockDetailLocation->type = "clock_out";
            $timeclockDetailLocation->created_by = $request->user_id;
            $timeclockDetailLocation->save();
          }

          //Activity Log
          $message = 'Candidate has been finished their timeclock for the shift with booking code '.$assign->code;
          ActivityCustomLogTrait::saveActivityLog('Time Clock', 'Update', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData 

          //updating notification
          $candidate_name  = DB::table('candidates')->find($timeclock->candidate_id)->full_name;
          $agency_email    = Agency::first()->email;
          $user_ids        = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
          $user_ids        = array_unique($user_ids);

          $notify_array = array(
              'title' => $candidate_name,
              'type' => "timeclock",
              'date' => date("d-m-Y",strtotime($assign->on_date)),
              'body' => "has finished their timeclock for the shift (".$assign->code.") on ".date("d-m-Y",strtotime($assign->on_date)),
               );
          if ($user_ids) {
              foreach ($user_ids as $user_id) {
                  $notification = new Notification;
                  $notification->type             = 'App\Notifications\HelloNotification';
                  $notification->notifiable_type  = 'App\User';
                  $notification->notifiable_id    = $user_id;
                  $notification->object_id        = $timeclock->id;
                  $notification->data             = json_encode($notify_array);
                  $notification->resource_ids     = $timeclock->id;
                  $notification->save();
              }
          }
          //send push notifications to admins and managers
          // $player_ids = RoleUser::whereIn('role_id', [2,3,4])->whereNotNull('player_id')->where('player_id', '!=', '')->pluck('player_id')->all();
          // $player_ids = array_unique($player_ids);
          // $template = $candidate_name." has finished their timeclock for the shift (".$assign->code.") on ".date("d-m-Y",strtotime($assign->on_date));
          // PushNotificationTrait::oneSignalPushMessage($player_ids, $template, , array("data_type" => "timeclock"));

          return response()->json([
            'success'=>[
              'status_code' => 201,
              'status' => 'Success', 
              'data' => $timeclock,
              'message' => "You've finished the shift"
            ]
          ],201);
        } else{
          return response()->json([
            'error'=>[
              'status_code' => 400,
              'status' => 'error', 
              'message' => "Something went wrong"
            ]
          ],400);
        }
        
      }
  }


  /**
   * Round up minutes to the nearest upper interval of a DateTime object.
   * 
   * @param \DateTime $dateTime
   * @param int $minuteInterval
   * @return \DateTime
   */
  public function roundMinuteInterval(\DateTime $dateTime, $minuteInterval = 5)
  {
    //Round to Nearest minutes
    return $dateTime->setTime(
        $dateTime->format('H'),
        round($dateTime->format('i') / $minuteInterval) * $minuteInterval,
        0
    );
    //Round to highest interval
    return $dateTime->setTime(
        $dateTime->format('H'),
        ceil($dateTime->format('i') / $minuteInterval) * $minuteInterval,
        0
    );
    //Round to lowest interval
    return $dateTime->setTime(
      $dateTime->format('H'),
      floor($dateTime->format('i') / $minuteInterval) * $minuteInterval,
      0
    );
  }

  /**
   * store the clock out time
   *
   * @return     <type>  The schedule.
   */
  public function clock_out(Request $request) {

      $validator = Validator::make(
                        array(
                        'timeclock_id' => $request->timeclock_id, 
                        'user_id' => $request->user_id, 
                        'clock_out' => $request->clock_out, 
                        'latitude' => $request->latitude, 
                        'longitude' => $request->longitude, 
                        'address_line' => $request->address_line, 
                        'notes' => $request->notes, 
                        'break' => $request->break, 
                        'subdomain' => $request->subdomain
                        ), array(
                        'timeclock_id' => 'required', 
                        'user_id' => 'required', 
                        'clock_out' => 'required', 
                        'latitude' => 'required', 
                        'longitude' => 'required', 
                        'address_line' => 'required', 
                        'notes' => 'nullable', 
                        'break' => 'nullable', 
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('timeclock_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('timeclock_id')
                  ]
                ],400);
            }
            else if ($messages->has('user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('user_id')
                  ]
                ],400);
            }
            else if ($messages->has('clock_out')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('clock_out')
                  ]
                ],400);
            }
            else if ($messages->has('latitude')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('latitude')
                  ]
                ],400);
            }
            else if ($messages->has('longitude')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('longitude')
                  ]
                ],400);
            }
            else if ($messages->has('address_line')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('address_line')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {


        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        // $assign = AssignVacancy::find($assigned_vacancy_id);

        //updating timeclock table
        $timeclock = Timeclock::find($request->timeclock_id);
        if ($timeclock) {

          $inputs = array(
              'start_time' => strtotime($timeclock->clock_in),
              'end_time'   => strtotime($request->clock_out),
              'break'      => $request->break ?? 0,
          );
          $total_hours = calcTotalHours($inputs);

          $timeclock->clock_out = date("Y-m-d H:i:s", strtotime($request->clock_out));
          $timeclock->created_by = $request->user_id;
          $timeclock->notes = $request->notes;
          $timeclock->break = $request->break;
          $timeclock->total_hours = $total_hours;
          $timeclock->save();

          $assigned_vacancy_id = $timeclock->assigned_vacancy_id; 

          $timeclockDetail = TimeclockDetail::where('timeclock_id', $request->timeclock_id)->first();
          if($timeclockDetail) {
            $timeclockDetail->clock_time = date("Y-m-d H:i:s", strtotime($request->clock_out));
            $timeclockDetail->latitude = $request->latitude;
            $timeclockDetail->longitude = $request->longitude;
            $timeclockDetail->address_line = $request->address_line;
            $timeclockDetail->type = "clock_out";
            $timeclockDetail->created_by = $request->user_id;
            $timeclockDetail->save();

          }

          return response()->json([
            'success'=>[
              'status_code' => 201,
              'status' => 'Success', 
              'data' => $timeclock,
              'message' => "You've finished the shift"
            ]
          ],201);
        } else {

          return response()->json([
            'error'=>[
              'status_code' => 400,
              'status' => 'error', 
              'message' => "There is no timeclock started. Please start your shift."
            ]
          ],400);
        }
      }
  }


}
