<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Tenant;
use App\UserTenant;
use App\UserRole;
use App\Tenants\Agency;
use App\Tenants\Candidate;
use App\Tenants\DocumentCategory;
use App\Tenants\CandidateDocument;
use App\Tenants\DocumentHiddenCandidate;
use App\Tenants\DocumentExcludedCandidate;
use App\Tenants\CandidateJob;
use App\Tenants\Document;
use App\Tenants\Role;
use App\Tenants\RoleUser;
use App\Tenants\CandidateAvailability;
use App\Tenants\CandidateUploadedDocument;
use App\Traits\ActivityLog\ActivityCustomLogTrait;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Intervention\Image\Facades\Image as Image;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\DatabaseManager;
use App\Helpers\SendMailGun;
use Carbon\Carbon;

class CandidateController extends BaseController
{

    public function candidate_sign_up(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'email' => $request->email,
                        'employment_type_id' => $request->employment_type_id,
                        'job_ids' => $request->job_ids,
                        'password' => $request->password,
                        'subdomain' => $request->subdomain
                        ), array(
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required',
                        'employment_type_id' => 'required',
                        'job_ids' => 'required',
                        'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('first_name')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('first_name')
                  ]
                ],400);
            }
            else if ($messages->has('last_name')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('last_name')
                  ]
                ],400);
            }
            else if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }
            else if ($messages->has('password')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => "The password must contain at least eight characters, including uppercase, lowercase letters, special characters and numbers."
                  ]
                ],400);
            }
            else if ($messages->has('employment_type_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('employment_type_id')
                  ]
                ],400);
            }
            else if ($messages->has('job_ids')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_ids')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            $checkUser = User::where('email', $request->email)->first();

            if ( $checkUser  ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 403,
                    'status' => 'Error', 
                    'message' => "An account already exist with this email address. Please try with another !"
                  ]
                ],403);
            }

            $subdomain = $request->subdomain;
            $tenant = Tenant::where('slug', $subdomain)->first();

            if ($checkUser && $tenant) {

                $user_tenant = UserTenant::where('user_id', $checkUser->id)->where('tenant_id', $tenant->id)->first();
            
                if ( $user_tenant  ) {
                    return response()->json([
                      'error'=>[
                        'status_code' => 403,
                        'status' => 'Error', 
                        'message' => "An account already exist with this email address. Please try with another !"
                      ]
                    ],403);
                }
            }
            /** Adding new user */
            $user           = new User();
            $user->name     = $request->first_name.' '.$request->last_name;
            $user->email    = $request->email;
            // $password       = 'Emp'.substr($request->first_name, 0, 2).substr($request->last_name, 0, 2).'@'.substr($subdomain, 0, 2);
            $user->password = Hash::make($request->password);
            $user->status   = 'inactive';
            $user->save();

            $user_tenant = new UserTenant();
            $user_tenant->user_id = $user->id;
            $user_tenant->tenant_id = $tenant->id;
            $user_tenant->save();

            if (UserRole::where('user_id', $user->id)->first() == '') {
                $tenent_role = new UserRole();
                $tenent_role->user_id = $user->id;
                $tenent_role->role_id = 2;
                $tenent_role->save();
            }


            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            /** Fetch candidate role */
            $roles = Role::where('name', 'candidate')->first();

            /**
             * Attach roles.
             */
            $user_role = new RoleUser();
            $user_role->user_id = $user->id;
            $user_role->role_id = $roles->id;
            $user_role->save();

            /* Adding candidate primary details */
            if (Candidate::where('user_id', $user->id)->first()) {
                return response()->json([
                  'error'=>[
                    'status_code' => 403,
                    'status' => 'Error', 
                    'message' => "An account already exist with this email address. Please try with another !"
                  ]
                ],403);
            }

            $candidate                      = new Candidate();
            $candidate->user_id             = $user->id;
            $candidate->first_name          = $request->first_name;
            $candidate->last_name           = $request->last_name;
            $candidate->full_name           = $request->first_name.' '.$request->last_name;
            $candidate->employment_types_id = $request->employment_type_id;
            $candidate->candidate_status_id = 2;
            $candidate->has_access          = 0;
            $candidate->created_by          = $user->id;
            $candidate->save();

            // $user->roles()->attach($roles);

            /** Attaching the jobs*/
            $jobs = explode(',', $request->job_ids);
            $candidate_id = $candidate->id;
            foreach ($jobs as $job) {
                $user_job = new CandidateJob();
                $user_job->candidate_id = $candidate_id;
                $user_job->job_id = $job;
                $user_job->status = 'active';
                $user_job->save();
            }

            return response()->json([
                  'success'=>[
                    'status_code' => 201,
                    'status' => 'Success', 
                    // 'message' => "Thank you for registration, your account requires approval before you can login."
                    'message' => "Thank you for the registration."
                  ]
                ],201);
      }

    }

    public function candidate_profile(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            $candidate_id = $request->input('candidate_id');

            $tenant = Tenant::where('slug', $request->input('subdomain'))->first();

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');


            $candidate = Candidate::where('id', $candidate_id)->with('jobs')->with('employment_type')->first();
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 
            $role_id = Role::where('name', 'candidate')->pluck('id')->first();
            $roleUser = RoleUser::where('user_id', $candidate->user_id)->where('role_id',$role_id)->first();

            return response()->json([
                  'success'=>[
                    'status_code' => 200,
                    'status' => 'Success', 
                    'tenant_id' => $tenant->id,
                    'subdomain' => $request->input('subdomain'),
                    'role_user_id' => $roleUser->id,
                    'agency_email' => Agency::first()->email,
                    'candidate_data' => $candidate
                  ]
                ],200);
      }

    }

    public function getMoreCandidateDetails(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            $candidate_id = $request->input('candidate_id');

            // $tenant = Tenant::where('slug', $request->input('subdomain'))->first();

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $candidate = Candidate::where('id', $candidate_id)->with('jobs')->first();

            return response()->json([
                  'success'=>[
                    'status_code' => 200,
                    'status' => 'Success',
                    'candidate_data' => $candidate
                  ]
                ],200);
      }

    }

    public function update_profile(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'profile_img' => $request->profile_img,
                        'subdomain' => $request->subdomain,
                        // 'job_ids' => $request->job_ids,
                        'dob' => $request->dob,
                        'gender' => $request->gender,
                        'nationality' => $request->nationality,
                        'station' => $request->station,
                        ), array(
                        'candidate_id' => 'required',
                        'profile_img' => 'nullable|mimes:jpeg,png,jpg,gif,svg,pdf|max:2048',
                        'subdomain' => 'required',
                        // 'job_ids' => 'nullable',
                        'dob' => 'nullable',
                        'gender' => 'nullable',
                        'nationality' => 'nullable',
                        'station' => 'nullable',
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('profile_img')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 413,
                    'status' => 'Error', 
                    'message' => "Please check the image size and try again."
                  ]
                ],413);
            }


      } else {

            $candidate_id = $request->input('candidate_id');

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $candidate = Candidate::find($request->candidate_id);
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 

            //image updation 
            if ($request->has('profile_img') && ($request->profile_img != '' || $request->profile_img != null) ) {

                $file                = $request->file('profile_img');
                $domain              = $request->input('subdomain');

                //delete the existing image
                if ($candidate->profile_img != '') {

                    $splitUrl              = explode('/', $candidate->profile_img);
                    $array_reverse         = array_reverse($splitUrl);
                    $profile_img_url       = $array_reverse[0];

                    if(Storage::disk('s3')->exists($domain.'/'.$profile_img_url)) {
                        Storage::disk('s3')->delete($domain.'/'.$profile_img_url);
                    }
                }
                // inserting new image
                $file_name = time().$file->getClientOriginalName(); 

                $image_name = $domain.'/'.$file_name;
                // $image_thumb = Image::make($file)->fit(220);
                // $image_thumb = $image_thumb->stream();
                // $path = Storage::disk('s3')->put($image_name, $image_thumb->__toString(), 'public');

                $path = Storage::disk('s3')->put(
                            $image_name, 
                            file_get_contents($request->file('profile_img')), 
                           'public');

                $image_url = Storage::disk('s3')->url($image_name);

                $candidate->profile_img = $image_url;

            }
            //details updation
            if (isset($request->gender)) {
                $candidate->gender      = $request->gender;
            }
            if (isset($request->nationality)) {
                $candidate->nationality      = $request->nationality;
            }
            if (isset($request->station)) {
                $candidate->place      = $request->station;
            }
            if (isset($request->dob)) {
                $candidate->dob      = $request->dob;
            }
            // $candidate->nationality = isset($request->nationality) ? $request->nationality : '';
            // $candidate->place       = isset($request->station) ? $request->station : '';
            // $candidate->dob         = isset($request->dob) ? $request->dob : '';
            $candidate->save();

            // designation updation
            /*if ($request->job_ids) {

                // fetching job with candidate_id
                $CandidateJobs = CandidateJob::where('candidate_id', $candidate_id)->get();
                // deleting jobs with candidate_id
                foreach($CandidateJobs as $job) {
                    $job->delete();
                }

                $jobs = explode(',', $request->job_ids);
                foreach($jobs as $job) {
                    $CandidateJob                   = new CandidateJob;
                    $CandidateJob->candidate_id     = $candidate_id;
                    $CandidateJob->job_id           = $job;
                    $CandidateJob->created_by       = $candidate_id;
                    $CandidateJob->save();
                }
            }*/

            return response()->json([
                  'success'=>[
                    'status_code' => 200,
                    'status' => 'Success', 
                    'message' => "Details are updated successfully."
                  ]
                ],200);
      }

    }

    public function update_firebase_uin(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'firebase_uin' => $request->firebase_uin,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'firebase_uin' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('firebase_uin')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('firebase_uin')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            $candidate_id = $request->input('candidate_id');

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $candidate = Candidate::find($candidate_id);

            $candidate->firebase_uin      = isset($request->firebase_uin) ? $request->firebase_uin : '';
            $candidate->save();

            return response()->json([
                  'success'=>[
                    'status_code' => 200,
                    'status' => 'Success', 
                    'message' => "UIN updated successfully."
                  ]
                ],200);
      }

    }

    public function candidate_documents(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        $candidate_id = $request->input('candidate_id');

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $candidate = Candidate::find($request->candidate_id);
        //checking for candidate status, restrict if candidate is deleted
        if ( $candidate && $candidate->is_deleted == 1 ) {
            return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
        } 

        // $job_array  = CandidateJob::where('candidate_id',$candidate_id)->pluck('job_id');
        // $documents = DocumentCategory::with('documents')
        //         ->with(['documents.jobs' => function ($query) use($job_array){
        //             $query->whereIn('job_id',$job_array);
        //         }])
        //         ->with(['documents.candidate_documents' => function ($query) use($candidate_id){
        //             $query->where('candidate_id', $candidate_id);
        //         }])
        //         ->where('is_deleted',0)->get();

        $documentCategories = DocumentCategory::with(['documents' => function ($query) use($candidate_id) {
                        $query->whereHas('documentJob', function($query) use($candidate_id) {
                            $query->whereIn('job_id', CandidateJob::where('candidate_id', $candidate_id)->pluck('job_id'));
                        });
                        $query->with(['candidate_document' => function($query) use($candidate_id) {
                            $query->where('candidate_id', $candidate_id);
                            $query->where(['is_latest' => 1, 'is_deleted' => 0]);
                        }]);
                        $query->whereNotIn('id', DocumentExcludedCandidate::where('candidate_id', $candidate_id)->pluck('document_id'));
                        $query->whereNull('subject_candidate_id')->orWhere('subject_candidate_id', $candidate_id);  
                        $query->where('is_deleted', 0);
                        $query->orderBy('position', 'ASC');
                    }])->orderBy('position', 'ASC')->where('is_deleted', 0)->get();

        $documentCategoryArray = [];
        // $docArrCollection = [];
        // $FinaldocArr = [];

        // $docsIds = DB::table('document_jobs')->whereIn('job_id',$job_array)->pluck('document_id')->all();
        // $docs = DB::table('documents')->whereIn('id',$docsIds)->where('is_deleted',0)->get();
        // $collectedDocs = collect($docs);
        // $groupedDocs = $collectedDocs->groupBy('document_category_id');
        // $docsArr = [];
        if ( count($documentCategories) > 0 ) {
            foreach ($documentCategories as $documentCategory) {
                if ($documentCategory) {
                    $documentArray = [];
                    foreach ($documentCategory->documents as $document) {
                        $CandidateDocument = [];
                        if ($document && $document->candidate_document) {
                            $CandidateDocument[] = [
                                "id" => $document->candidate_document->id,
                                "title" => $document->title,
                                "description" => $document->candidate_document->description,
                                "has_profile_view" => $document->has_profile_view,
                                "is_mandatory" => $document->is_mandatory,
                                "has_issue_date" => $document->has_issue_date,
                                "has_expiry_date" => $document->has_expiry_date,
                                "is_deleted" => $document->is_deleted,
                                "status" => $document->status,
                                'issue_date' => $document->candidate_document->issue_date,
                                'expiry_date' => $document->candidate_document->expiry_date,
                                "file" => $document->candidate_document->file,
                                "is_completed" => $document->candidate_document->is_completed,
                                "is_expired" => $document->candidate_document->is_expired
                            ];
                        } else {
                            $CandidateDocument[] = [
                                "id" => '',
                                "title" => $document->title,
                                "description" => $document->description,
                                "has_profile_view" => $document->has_profile_view,
                                "is_mandatory" => $document->is_mandatory,
                                "has_issue_date" => $document->has_issue_date,
                                "has_expiry_date" => $document->has_expiry_date,
                                "is_deleted" => $document->is_deleted,
                                "status" => $document->status,
                                'issue_date' => '',
                                'expiry_date' => '',
                                "file" => null,
                                "is_completed" => false,
                                "is_expired" => false
                            ];
                        }
                        $documentArray[] = [
                            "id" => $document->id,
                            "title" => $document->title,
                            "description" => $document->description,
                            "has_profile_view" => $document->has_profile_view,
                            "is_mandatory" => $document->is_mandatory,
                            "has_issue_date" => $document->has_issue_date,
                            "has_expiry_date" => $document->has_expiry_date,
                            "candidate_documents" => $CandidateDocument
                        ];
                    }
    
                    $documentCategoryArray[] = [
                        "id" => $documentCategory->id,
                        "title" => $documentCategory->title,
                        "description" => $documentCategory->description,
                        "has_profile_view" => $documentCategory->has_profile_view,
                        "is_mandatory" => $documentCategory->is_mandatory,
                        "is_deleted" => $documentCategory->is_deleted,
                        "status" => $documentCategory->status,
                        "documents" => $documentArray
                    ];
                }
            }
        }

        if (env('APP_ENV')== "dev") {
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $documentCategoryArray
              ]
            ],200);
        } else {
            return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success', 
                'data' => $documentCategoryArray
              ]
            ],200);
        }


      }
    }

    public function upload_documents(Request $request)
    {
        $validator = Validator::make(
            array(
                'candidate_id' => $request->candidate_id,
                'document_id' => $request->document_id,
                'issue_date' => $request->issue_date,
                'expiry_date' => $request->expiry_date,
                'document' => $request->document,
                'description' => $request->description,
                'subdomain' => $request->subdomain
            ), array(
                'candidate_id' => 'required',
                'document_id' => 'required',
                'issue_date' => 'nullable|date|before:now',
                'expiry_date' => 'nullable|date|after:issue_date',
                'document' => 'nullable|file|mimes:jpg,jpeg,png,pdf,doc,docx',
                'description' => 'nullable',
                'subdomain' => 'required'
            ), array(
                'expiry_date.after' => "The expiry date must be greater than issue date."
            )
        );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            if ($messages->has('document_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('document_id')
                  ]
                ],400);
            }
            if ($messages->has('issue_date')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('issue_date')
                  ]
                ],400);
            }
            if ($messages->has('expiry_date')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('expiry_date')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        $candidate_id = $request->input('candidate_id');

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $candidate = Candidate::find($request->candidate_id);
        //checking for candidate status, restrict if candidate is deleted
        if ( $candidate && $candidate->is_deleted == 1 ) {
            return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
        } 
        //updating all other documents with latest '0'
        CandidateDocument::where([
            'document_id' => $request->document_id,
            'candidate_id' => $request->candidate_id,
            ])->update(['is_latest' => 0]);

        $candidate_doc_insert                 = new CandidateDocument;

        if ($request->has('document') &&  !is_null($request->document)) {
            $file = $request->file('document');
            $domain = $request->input('subdomain');
            $fileName = $file->getClientOriginalName();
            $imageName = $domain . '/' . time() . $fileName;

            Storage::disk('s3')->put($imageName, file_get_contents($request->file('document')), 'public');
            $fileUrl = Storage::disk('s3')->url($imageName);
            $candidate_doc_insert->file = $fileUrl; 
        }


        $candidate_doc_insert->document_id    = $request->document_id;
        $candidate_doc_insert->issue_date     = $request->issue_date;
        $candidate_doc_insert->expiry_date    = $request->expiry_date;
        $candidate_doc_insert->candidate_id   = $request->candidate_id;
        $candidate_doc_insert->description    = $request->description;
        $candidate_doc_insert->is_latest      = 1;
        $candidate_doc_insert->save();

        if ($this->checkCompleted($candidate_doc_insert)) {
            $candidate_doc_insert->is_optional = 1;
            $candidate_doc_insert->save();
        } else{
            $candidate_doc_insert->is_optional = 0;
            $candidate_doc_insert->save();
        }


        $candidate = Candidate::find($request->candidate_id);
        /* checking candidate status [1=>active, 2=> pending] */
        if ($this->check_candidate_documents_completed( $request->candidate_id, $request->input('subdomain') ) == 'true') {
            
            /* check candidate status is pending and need to be active */
            if ($candidate->candidate_status_id == 2) {
                $candidate->candidate_status_id = 1;
                $candidate->save();
            }
            
        } else {
            /* check candidate status is active and need to be pending */
            if ($candidate->candidate_status_id == 1) {
                $candidate->candidate_status_id = 2;
                $candidate->save();
            }
        }

        $user = DB::table('logezycore.users')->where('id', Auth::id())->select('name', 'email')->first();
        $loggedData = [
          'role'          => 'manager',
          'tenant'        => $request->input('subdomain'),
          'dashboardType' => 'manager_app',
          'user_name'     => $user->name,
          'email'         => $user->email,
        ];
        $document = DB::table('documents')->where('id', $request->document_id)->select('id', 'title')->first();
        //Activity Log
        $message = 'Candidate Document - '.$candidate->full_name ."'s ".$document->title.' document has been uploaded.';
        ActivityCustomLogTrait::saveActivityLog('Candidate Documents', 'Update', $message, $document->id, $document->title, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

        return response()->json([
              'success'=>[
                'status_code' => 201,
                'status' => 'Success', 
                'message' => "Document updated successfully"
              ]
            ],201);


      }
    }

    public function compliant_documents(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'candidate_id' => $request->candidate_id,
                        'document_id' => $request->document_id,
                        'is_completed' => $request->is_completed,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_id' => 'required',
                        'document_id' => 'required',
                        'is_completed' => 'required|boolean',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('document_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('document_id')
                  ]
                ],400);
            }
            else if ($messages->has('is_completed')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('is_completed')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        // $candidate_doc = CandidateDocument::where('document_id', $request->document_id)->where('candidate_id', $request->candidate_id)->where('is_latest',1)->where('is_deleted',0)->first();

        // if ($candidate_doc) {
        //     $candidate_doc->is_optional = $request->is_completed;
        //     $candidate_doc->save();
        // } else {
        //     $candidate_doc_insert                 = new CandidateDocument;
        //     $candidate_doc_insert->document_id    = $request->document_id;
        //     // $candidate_doc_insert->issue_date     = '';
        //     // $candidate_doc_insert->expiry_date    = '';
        //     // $candidate_doc_insert->file           = '';
        //     $candidate_doc_insert->candidate_id   = $request->candidate_id;
        //     $candidate_doc_insert->is_optional    = $request->is_completed;
        //     $candidate_doc_insert->save();
        // }

        if($user_document = CandidateDocument::where('document_id', $request->document_id)->where('candidate_id', $request->candidate_id)->where('is_latest',1)->where('is_deleted',0)->first()){

        } else {
            //updating other documents
            DB::table('candidate_documents')
            ->where(['document_id' => $request->document_id, 'candidate_id' => $request->candidate_id])
            ->update(['is_latest' => 0]);
            $user_document              = new CandidateDocument;
        }
        
        $user_document->document_id  = $request->document_id;
        $user_document->candidate_id = $request->candidate_id;
        $user_document->is_optional  = $request->is_completed;
        $user_document->save();

        if($this->checkCompleted($user_document) && $request->is_completed == 0){
            $user_document->is_optional = 1;
            $user_document->save();
        }

        $candidate = Candidate::find($request->candidate_id);
        /* checking candidate status [1=>active, 2=> pending] */
        if ($this->check_candidate_documents_completed( $request->candidate_id, $request->input('subdomain') ) == 'true') {
            
            /* check candidate status is pending and need to be active */
            if ($candidate->candidate_status_id == 2) {
                $candidate->candidate_status_id = 1;
                $candidate->save();
            }
            
        } else {
            /* check candidate status is active and need to be pending */
            if ($candidate->candidate_status_id == 1) {
                $candidate->candidate_status_id = 2;
                $candidate->save();
            }
        }

        $user = DB::table('logezycore.users')->where('id', Auth::id())->select('name', 'email')->first();
        $loggedData = [
          'role'          => 'manager',
          'tenant'        => $request->input('subdomain'),
          'dashboardType' => 'manager_app',
          'user_name'     => $user->name,
          'email'         => $user->email,
        ];
        $document = DB::table('documents')->where('id', $request->document_id)->select('id', 'title')->first();

        if ($user_document->is_optional == 0) {
          //Activity Log
          $message = 'Candidate Document - '.$candidate->full_name ."'s ".$document->title.' document has not been complaint.';
          ActivityCustomLogTrait::saveActivityLog('Candidate Documents', 'Update', $message, $document->id, $document->title, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json([
              'success'=>[
                'status_code' => 201,
                'status' => 'Success', 
                'message' => "Document not compliant"
              ]
            ],201);

        } else {
          //Activity Log
          $message = 'Candidate Document - '.$candidate->full_name ."'s ".$document->title.' document has been complaint.';
          ActivityCustomLogTrait::saveActivityLog('Candidate Documents', 'Update', $message, $document->id, $document->title, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json([
              'success'=>[
                'status_code' => 201,
                'status' => 'Success', 
                'message' => "Document successfully compliant"
              ]
            ],201);

        }

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserDocument  $userDocument
     * @return \Illuminate\Http\Response
     */
    public function deleteDocument(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'document_id' => $request->document_id,
                        'candidate_document_id' => $request->candidate_document_id,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'document_id' => 'required',
                        'candidate_document_id' => 'nullable',
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_document_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_document_id')
                  ]
                ],400);
            }
            else if ($messages->has('document_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('document_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {
            config(['database.connections.mysql.database' => 'logezy_'.$request->subdomain,'database.default'=>'mysql']);
            DB::reconnect('mysql');


            $document = Document::find($request->document_id);
            $document_id = $document->id;
            $document_title = $document->title;

            if ($document) {

                if ($request->candidate_document_id) {
                    $candidate_document = CandidateDocument::find($request->candidate_document_id);
                    if ($candidate_document) {
                        $candidate_document->is_deleted = 1;
                        $candidate_document->save();
                        // $candidate_document->update(['is_deleted' => 1]);
                    }
                }

                if (!is_null($document->subject_candidate_id)) {
                    $document->delete();
                } else {
                    $model = new DocumentExcludedCandidate;
                    $model->candidate_id = $request->candidate_id;
                    $model->document_id  = $request->document_id;
                    $model->created_by = Auth::id();
                    $model->save();
                }

                $candidate_name = DB::table('candidates')->where('id', $request->candidate_id)->pluck('full_name')->first();
                $user = DB::table('logezycore.users')->where('id', Auth::id())->select('name', 'email')->first();
                $loggedData = [
                  'role'          => 'manager',
                  'tenant'        => $request->input('subdomain'),
                  'dashboardType' => 'manager_app',
                  'user_name'     => $user->name,
                  'email'         => $user->email,
                ];
                //Activity Log
                $message = 'Document - '.$candidate_name ."'s ".$document_title.' document has been deleted.';
                ActivityCustomLogTrait::saveActivityLog('Candidate Documents', 'Delete', $message, $document_id, $document_title, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

                return response()->json([
                      'success'=>[
                        'status_code' => 201,
                        'status' => 'Success', 
                        'message' => "Document deleted successfully"
                      ]
                    ],201);
                
            } else {

                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'error', 
                    'message' => "No documents found !"
                  ]
                ],400);

            }
        }
    }
    
    /**
     * clear_document
     *
     * @param  mixed $request
     * @param  mixed $subdomain
     * @param  mixed $id
     * @return void
     */
    public function clearDocument(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'candidate_document_id' => $request->candidate_document_id,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'candidate_document_id' => 'required',
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

        if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('candidate_document_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_document_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }

        } else {
            config(['database.connections.mysql.database' => 'logezy_'.$request->subdomain,'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $candidateDocument = CandidateDocument::find($request->candidate_document_id);
            // if (!is_null($candidateDocument->file)) {
            //     //clear document from s3 bucket
            //     $splitUrl              = explode('/', $candidateDocument->file);
            //     $array_reverse         = array_reverse($splitUrl);
            //     $profile_img_url       = $array_reverse[0];
            //     if(Storage::disk('s3')->exists($request->subdomain.'/'.$profile_img_url)) {
            //         Storage::disk('s3')->delete($request->subdomain.'/'.$profile_img_url);
            //     }
            // }
            // $candidateDocument->delete();
            if ($candidateDocument) {
                $candidateDocument->is_deleted = 1;
                $candidateDocument->save();
                // $candidateDocument->update(['is_deleted' => 1]);
            }

            $candidate_name = DB::table('candidates')->where('id', $request->candidate_id)->pluck('full_name')->first();
            $user = DB::table('logezycore.users')->where('id', Auth::id())->select('name', 'email')->first();
            $loggedData = [
              'role'          => 'manager',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'manager_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];
            //Activity Log
            $message = 'Candidate Document - '.$candidate_name ."'s ".$document->title.' document data has been cleared.';
            ActivityCustomLogTrait::saveActivityLog('Candidate Documents', 'Delete', $message, $document->id, $document->title, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json([
                  'success'=>[
                    'status_code' => 201,
                    'status' => 'Success', 
                    'message' => "Document data cleared successfully"
                  ]
                ],201);
        }
    }

    public function checkCompleted($user_document){
        $has_expiry = Document::with(['category' => function($query) {
            $query->where(['is_deleted' => 0]);
        }])->where('id', $user_document->document_id)->pluck('has_expiry_date')->first();
        $has_issue = Document::with(['category' => function($query) {
            $query->where(['is_deleted' => 0]);
        }])->where('id', $user_document->document_id)->pluck('has_issue_date')->first();
        
        if ($has_expiry == 1) {
            if (!$user_document->expiry_date) {
                return false;
            }

            if ($user_document->expiry_date < Carbon::now()->format('Y-m-d')) {
                return false;
            }
        }
        if ($has_issue == 1) {
            if (!$user_document->issue_date) {
                return false;
            }
        }
        return true;
    }

    /**
     * check all the documents of candidate is completed
     * @param mixed $id [candidate id]
     * 
     * @return void [true/false]
     */
    public function check_candidate_documents_completed($id, $subdomain)
    {
        $candidate_id = $id;

        config(['database.connections.mysql.database' => 'logezy_'.$subdomain,'database.default'=>'mysql']);
        DB::reconnect('mysql');
        
        $job_array  = CandidateJob::where('candidate_id',$candidate_id)->pluck('job_id');

        // $documents = Document::whereHas('jobs' , function ($query) use($job_array){
        //     $query->whereIn('job_id',$job_array);
        // })
        // ->with(['uploaded_candidate_documents' => function ($query) use($candidate_id){
        //     $query->where('candidate_id', $candidate_id);
        // }])
        // ->has('active_category')
        // ->where('is_deleted',0)
        // ->where('is_mandatory',1)->get();

        $documents = Document::whereHas('jobs' , function ($query) use($job_array){
            $query->whereIn('job_id', $job_array);
        })->whereDoesntHave('candidate_documents_excluded' , function ($query) use($candidate_id){
            $query->where('candidate_id', $candidate_id);
        })
        ->with(['uploaded_candidate_documents' => function ($query) use($candidate_id){
            $query->where('is_latest', 1);
            $query->where('candidate_id', $candidate_id);
        }])
        ->has('active_category')
        ->where('is_deleted', 0)
        ->where(function ($query) use ($candidate_id) {
          $query->whereNull('subject_candidate_id')
            ->orWhere('subject_candidate_id', $candidate_id);
        })
        // ->whereNull('subject_candidate_id')->orWhere('subject_candidate_id', $candidate_id)
        ->where('is_mandatory', 1)->get();

        foreach ($documents as $document) {
            if ($document->is_mandatory == 1) {
                if ($document->uploaded_candidate_documents) {
                    // if( $document->has_expiry_date == 1 && $document->uploaded_candidate_documents->expiry_date < \Carbon\Carbon::today() ){
                    //     return 'false';
                    // } else 
                    if ($document->uploaded_candidate_documents->is_completed != 'true') {
                        return 'false';
                    }
                } else {
                    return 'false';
                }
            }
        }
        return 'true';
    }
}
