<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\Day;
use App\Tenants\CustomHoliday;
use App\Tenants\Holiday;
use App\Tenants\CandidateVacancy;
use App\Tenants\Vacancy;
use App\Tenants\RoleUser;
use App\Tenants\Agency;
use App\Tenants\Helper;
use App\Tenants\AssignVacancy;
use App\Tenants\CandidateRateCard;
use App\Tenants\EmploymentType;
use App\Tenants\VacancyDetail;
use App\Tenants\VacancyStaffRate;
use App\Jobs\PublishAssignedShiftsJob;
use App\Traits\ActivityLog\ActivityCustomLogTrait;
use App\Traits\PushNotificationTrait;
use App\Traits\SMSTraits;
use App\Traits\EmailTraits;
use App\Traits\UserTraits;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;
use App\Tenants\Notification;

class VacancyController extends BaseController
{

    /**
     * Store new vacancy
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function addVacancies(Request $request) {

      $validator = Validator::make(
                        array(
                        'business_unit_id' => $request->business_unit_id,
                        'job_id' => $request->job_id,
                        'dates' => $request->dates,
                        'shift_id' => $request->shift_id,
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_numbers' => $request->total_numbers,
                        'subdomain' => $request->subdomain
                        ), array(
                        'business_unit_id' => 'required',
                        'job_id' => 'required',
                        'dates' => 'required',
                        'shift_id' => 'required',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'notes' => 'nullable',
                        'total_numbers' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('business_unit_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('job_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_id')
                  ]
                ],400);
            }
            else if ($messages->has('dates')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('dates')
                  ]
                ],400);
            }
            else if ($messages->has('shift_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('shift_id')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_numbers')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_numbers')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
        try {
          $datesArr = explode(",", $request->dates);

          if ($datesArr) {
            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');
            foreach ($datesArr as $date) {
              $request->request->add(['on_date' => $date]);
              Vacancy::addVacancy($request);
            }
          }

          return response()->json([
            'success'=>[
              'status_code' => 201,
              'status' => 'Success', 
              'message' => "Vacancy created successfully"
            ]
          ],201);
        } catch (\Illuminate\Database\QueryException $ex) {

            return response()->json([
              'Error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => $ex->getMessage()
              ]
            ],400);
        }
      }

    }



    /**
     * Show the resource
     *
     * @param      \App\Models\Vacancy  $vacancy  The vacancy
     *
     * @return     \App\Models\Vacancy  ( description_of_the_return_value )
     */
    public function show($subdomain, $vacancy_id) {
        return $vacancy = Vacancy::find($vacancy_id);
    }


    /**
     * Update vacancy
     *
     * @param      \Illuminate\Http\Request  $request  The request
     * @param      \App\Models\Vacancy       $vacancy  The vacancy
     *
     * @return     <type>                    ( description_of_the_return_value )
     */
    public function update_vacancy(Request $request) {

      $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id,
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_hours' => $request->total_hours,
                        'subdomain' => $request->subdomain
                        ), array(
                        'vacancy_id' => 'required',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'notes' => 'nullable',
                        'total_hours' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_hours')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_hours')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $vacancy = Vacancy::find($request->vacancy_id);
        //checking for night shift
        $start = date("H:i", strtotime($request->start_time));
        $end = date("H:i", strtotime($request->end_time));
        if ($start >= $end) {
            $endDate = date('Y-m-d', strtotime('+1 day', strtotime($vacancy->on_date)));
        } else {
            $endDate = $vacancy->on_date;
        }
        //calculate total hours
        $inputs = array(
            'on_date'    => $vacancy->on_date,
            'start_time' => strtotime($vacancy->on_date. ' '.$request->start_time),
            'end_time'   => strtotime($endDate. ' '.$request->end_time) ,
            'break'      => $request->break ?? 0,
        );
        $totalHours = calcTotalHours($inputs);

        $logArray = [];

        if(strcmp($vacancy->start_time, (isset($request->start_time) ? date( "Y-m-d H:i", strtotime($vacancy->on_date. ' '.$request->start_time)) : null)) != 0){
          array_push($logArray, ['key' => 'start time', 'old_value' => date( "H:i:s", strtotime($vacancy->start_time)), 'new_value' => isset($request->start_time) ? date( "H:i:s", strtotime($vacancy->on_date. ' '.$request->start_time)) : null]);
        }
        if(strcmp($vacancy->end_time, (isset($request->end_time) ? date( "Y-m-d H:i", strtotime($endDate. ' '.$request->end_time)) : null)) != 0){
          array_push($logArray, ['key' => 'end time', 'old_value' => date( "H:i:s", strtotime($vacancy->end_time)), 'new_value' => isset($request->end_time) ? date( "H:i:s", strtotime($endDate. ' '.$request->end_time)) : null]);
        }
        if(strcmp($vacancy->break, (isset($request->break) ? $request->break : 0)) != 0){
          array_push($logArray, ['key' => 'break', 'old_value' => $vacancy->break.' min', 'new_value' => (isset($request->break) ? $request->break : 0).' min']);
        }
        if(strcmp($vacancy->total_hours, ($totalHours ?? 0)) != 0){
          array_push($logArray, ['key' => 'total hours', 'old_value' => $vacancy->total_hours. ' hrs', 'new_value' => ($totalHours ?? 0). ' hrs']);
        }
        if(strcmp($vacancy->notes, (isset($request->notes) ? $request->notes : null)) != 0){
          array_push($logArray, ['key' => 'notes', 'old_value' => $vacancy->notes, 'new_value' => isset($request->notes) ? $request->notes : null]);
        }
        //fetching rates
        // $inputArr = array(
        //     'business_unit_id'  => $vacancy->business_unit_id,
        //     'job_id'            => $vacancy->job_id,
        //     'on_date'           => $vacancy->on_date,
        //     'shift_id'          => $vacancy->shift_id,
        // );
        // $rates = Helper::getClientStaffRates($inputArr);

        // if( isset($request->client_rate) && ( $request->client_rate == null || $request->client_rate == "0.00") ) {
        //     $clientRate = !empty($rates->client_rate) ? $rates->client_rate : 0;
        // } else {
        //     $clientRate = $request->client_rate;
        // }
        // $staffRates  = [];
        // if( $rates ) {
        //     $staffRates  = $rates->staff_rate_cards ?? [];
        // }
        // $vacancy->shift_id         = isset($request->shift_id) ? $request->shift_id : null;
        $vacancy->start_time       = isset($request->start_time) ? date( "Y-m-d H:i:s", strtotime($vacancy->on_date. ' '.$request->start_time)) : null;
        $vacancy->end_time         = isset($request->end_time) ? date( "Y-m-d H:i:s", strtotime($endDate. ' '.$request->end_time)) : null;
        // $vacancy->on_date          = isset($request->on_date) ? date( "Y-m-d", strtotime($request->on_date) ) : null;
        $vacancy->break            = isset($request->break) ? $request->break : 0;
        $vacancy->total_hours      = $totalHours ?? 0;
        $vacancy->client_rate      = 0;
        $vacancy->notes            = isset($request->notes) ? $request->notes : null;
        if (isset($request->space_left) ) {
            $space = (int)$vacancy->total_numbers - (int)$vacancy->used_count;
            if($space != $request->space_left) {
                $new_space = ((int)$request->space_left - (int)$space)+(int)$vacancy->total_numbers;
                $vacancy->total_numbers    = $new_space;
            }
        }
        $oldVacancy = DB::table('vacancies')->find($request->vacancy_id);
        //Populating vacancy staff rates
        if($vacancy->save()) {
          if ( ($oldVacancy->break != $request->break) || (strtotime($oldVacancy->on_date. ' '.$request->start_time) == strtotime($oldVacancy->start_time) && strtotime($endDate. ' '.$request->end_time) == strtotime($oldVacancy->end_time)) ) {

                    $request->request->add(['on_date' => date('Y-m-d', strtotime($oldVacancy->on_date))]);
                    $request->request->add(['business_unit_id' => $vacancy->business_unit_id]);
                    $request->request->add(['job_id' => $vacancy->job_id]);
                    $request->request->add(['shift_id' => $vacancy->shift_id]);
                    //find matching ratecards
                    $rateCards = splitTimeRangeIntoSlots($request);
                    // dd($rateCards);
                    //delete entries on vacancy id if exists
                    $detailIDs = VacancyDetail::where('vacancy_id', $vacancy->id)->pluck('id')->all();
                    if ($detailIDs) VacancyDetail::destroy($detailIDs);
                    $staffrateIDs = VacancyStaffRate::where('vacancy_id', $vacancy->id)->pluck('id')->all();
                    if ($staffrateIDs) VacancyStaffRate::destroy($staffrateIDs);

                    //adding vacancy details
                    foreach ($rateCards as $rateCard) {
                        $vacancyDetail                   = new VacancyDetail;
                        $vacancyDetail->vacancy_id       = $vacancy->id;
                        $vacancyDetail->day_id           = $rateCard['day_id'];
                        $vacancyDetail->shift_id         = $rateCard['shift_id'];
                        $vacancyDetail->rate_type        = $rateCard['rate_type'];
                        $vacancyDetail->start_time       = date( "Y-m-d H:i:s", strtotime($rateCard['start_time'])) ?? null;
                        $vacancyDetail->end_time         = date( "Y-m-d H:i:s", strtotime($rateCard['end_time'])) ?? null;
                        $vacancyDetail->break            = $rateCard['break'] ?? 0;
                        $vacancyDetail->total_hours      = $rateCard['total_hours'] ?? 0;
                        $vacancyDetail->client_rate      = $rateCard['client_rate'] ?? 0;
                        $vacancyDetail->save();
                        //Populating vacancy staff rates
                        if( $rateCard['staff_rates'] ) {
                            foreach($rateCard['staff_rates'] as $staffRate) {
                                // if(isset($staffRate['value'])){
                                    $vacancy_staff_rate                     = new VacancyStaffRate;
                                    $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                    $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                    $vacancy_staff_rate->employment_type_id = $staffRate->employment_type_id;
                                    $vacancy_staff_rate->staff_rate         = $staffRate->staff_rate;
                                    $vacancy_staff_rate->save();
                                // }
                            }
                        } else {
                            $staffRates = EmploymentType::where('status','active')->where('is_deleted',0)->get();
                            foreach ($staffRates as $staffRate) {
                                // if(isset($staffRate['value'])){
                                    $vacancy_staff_rate                     = new VacancyStaffRate;
                                    $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                    $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                    $vacancy_staff_rate->employment_type_id = $staffRate->id;
                                    $vacancy_staff_rate->staff_rate         = $staffRate->value;
                                    $vacancy_staff_rate->save();
                                // }
                            }
                        }
                    }
                }


                $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
                $loggedData = [
                    'role'          => 'manager',
                    'tenant'        => $request->input('subdomain'),
                    'dashboardType' => 'manager_app',
                    'user_name'     => $user->name,
                    'email'         => $user->email,
                ];
                //Activity Log
                foreach($logArray as $value){
                  $message = 'Vacancy - ' . $vacancy->code."'s " .$value['key'] .' has been updated from '. ($value['old_value'] ? $value['old_value'] : 'null') .' to '. ($value['new_value'] ? $value['new_value'] : 'null'). '.';
                  ActivityCustomLogTrait::saveActivityLog('Vacancies', 'Update', $message, $vacancy->id, $vacancy->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
                }

            // $staffRateArr    = isset($request->staff_rates) ? $request->staff_rates : [];
            // if($staffRateArr) {
            //     $checkEmpty = [];
            //     foreach($staffRateArr as $staffRate) {
            //         if( VacancyStaffRate::find($staffRate['id']) && $staffRate['staff_rate'] != '0.00' ){
            //             $checkEmpty [] = $staffRate;
            //             $vacancy_staff_rate                     = VacancyStaffRate::find($staffRate['id']);
            //             $vacancy_staff_rate->vacancy_id         = $id;
            //             $vacancy_staff_rate->employment_type_id = $staffRate['employment_type_id'];
            //             $vacancy_staff_rate->staff_rate         = $staffRate['staff_rate'];
            //             $vacancy_staff_rate->save();
            //         }
            //     }
            //     if (!$checkEmpty) {
            //         if( $staffRates ) {
            //             foreach($staffRateArr as $staffRate) {
            //                 $vacancy_staff_rate = VacancyStaffRate::find($staffRate['id']);
            //                 $vacancy_staff_rate->delete();
            //             }
            //             foreach($staffRates as $staffRate) {
            //                 // if(isset($staffRate['value'])){
            //                     $vacancy_staff_rate                     = new VacancyStaffRate;
            //                     $vacancy_staff_rate->vacancy_id         = $vacancy->id;
            //                     $vacancy_staff_rate->employment_type_id = $staffRate['id'];
            //                     $vacancy_staff_rate->staff_rate         = $staffRate['value'];
            //                     $vacancy_staff_rate->save();
            //                 // }
            //             }
            //         }
            //     }
            // }
        }

        return response()->json([
          'success'=>[
            'status_code' => 201,
            'status' => 'Success', 
            'message' => "Vacancy updated successfully"
          ]
        ],201);
      }

    }
    /**
     * Assign candidate on multiple dates
     *
     * @param \App\Models\AssignVacancy $assignVacancy
     *
     * @return \Illuminate\Http\Response
     */
    public function assign_multiple_dates(Request $request)
    {

      $validator = Validator::make(
                        array(
                        'business_unit_id' => $request->business_unit_id,
                        'job_id' => $request->job_id,
                        'candidate_id' => $request->candidate_id,
                        'dates' => $request->dates,
                        'shift_id' => $request->shift_id,
                        'start_time' => $request->start_time,
                        'end_time' => $request->end_time,
                        'break' => $request->break,
                        'notes' => $request->notes,
                        'total_hours' => $request->total_hours,
                        'subdomain' => $request->subdomain,
                        'is_publish' => $request->is_publish,
                        'text' => $request->text,
                        'push' => $request->push,
                        'email' => $request->email,
                        ), array(
                        'business_unit_id' => 'required|numeric',
                        'job_id' => 'required|numeric',
                        'candidate_id' => 'required|numeric',
                        'dates' => 'required',
                        'shift_id' => 'required|numeric',
                        'start_time' => 'required',
                        'end_time' => 'required',
                        'break' => 'nullable',
                        'notes' => 'nullable',
                        'total_hours' => 'required',
                        'subdomain' => 'required',
                        'is_publish' => 'nullable|boolean',
                        'text' => 'nullable|boolean',
                        'push' => 'nullable|boolean',
                        'email' => 'nullable|boolean',
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('business_unit_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('business_unit_id')
                  ]
                ],400);
            }
            else if ($messages->has('job_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('job_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('dates')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('dates')
                  ]
                ],400);
            }
            else if ($messages->has('shift_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('shift_id')
                  ]
                ],400);
            }
            else if ($messages->has('start_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('start_time')
                  ]
                ],400);
            }
            else if ($messages->has('end_time')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('end_time')
                  ]
                ],400);
            }
            else if ($messages->has('total_hours')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('total_hours')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('is_publish')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('is_publish')
                  ]
                ],400);
            }
            else if ($messages->has('text')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('text')
                  ]
                ],400);
            }
            else if ($messages->has('push')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('push')
                  ]
                ],400);
            }
            else if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }


      } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $errResponse = [];
        $sucResponse = [];
        $assignDates = [];
        $shift       = [];
        $notify_data = [];
        $publishedShifts = [];
        $candidateDetails = Candidate::find($request->candidate_id);

        $datesArr = explode(",", $request->dates);
        $bookingCodes = [];
        if ($datesArr) {
          foreach( $datesArr as $date){
              //checking for night shift
              $start = date("H:i", strtotime($request->start_time));
              $end = date("H:i", strtotime($request->end_time));
              $startDate = $date;
              if ($start >= $end) {
                  $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
              } else {
                  $endDate = $date;
              }

              //Checking before assigning candidate
              $inputArr = array(
                  'candidate_id'      => $request->candidate_id,
                  'business_unit_id'  => $request->business_unit_id,
                  'job_id'            => $request->job_id,
                  'on_date'           => $date,
                  'startDate'         => $startDate,
                  'endDate'           => $endDate,
                  'shift_id'          => $request->shift_id,
                  'start_time'        => $start,
                  'end_time'          => $end,
                  'total_hours'       => $request->total_hours,
              );
              $assignValidations = AssignVacancy::assignValidations($inputArr);

              if ($assignValidations != "false") {
                  array_push($errResponse, $assignValidations);
              } else {
                  $request->request->add(['on_date' => $date]);
                  $request->request->add(['total_numbers' => 1]);
                  $vacancy_id = Vacancy::addVacancy($request);
                  if ($vacancy_id != '') {
                      $request->request->add(['vacancy_id' => $vacancy_id]);
                      $request->request->add(['status' => 'pending']);
                      $assignDetails = AssignVacancy::assign_candidate($request);
                      if ($assignDetails) {
                          array_push($bookingCodes, $assignDetails->code);
                          array_push($assignDates, date("d-m-Y", strtotime($date)));
                          
                          if(isset($request->is_publish) && $request->is_publish == 1 ) {
                            $vacancy = Vacancy::find($vacancy_id);
                            Notification::candidate_shift_assign($candidateDetails, $assignDetails, $vacancy);

                            $notification_services['email'] = 0; 
                            $notification_services['text']  = 0; 
                            $notification_services['push']  = 0; 
        
                            // Check if push notification is selected
                            if($request->push == 1){ 
                                $notification_services['push'] = 1; 
                                PushNotificationTrait::shiftAssign($assignDetails, $candidateDetails);
                            }
                            // Check if text notification is selected
                            if($request->text == 1){ 
                                $notification_services['text']  = 1; 
                                SMSTraits::vacancyAssign($assignDetails, $candidateDetails, $request->subdomain);
                            }
                            // Check if text notification is selected
                            if($request->email == 1){ 
                                $notification_services['email']  = 1;
                                $candidateEmail = RoleUser::where('role_id', 6)->where('user_id', $candidateDetails->user_id)->first()->email;
                                $notification_services['email']  = 1;
                                $assign_mail_variables  = [
                                    '[candidate_name]'  => $assignDetails->candidate_name,
                                    '[client]'          => $assignDetails->vacancy->client,
                                    '[business_unit]'   => $assignDetails->business_unit,
                                    '[address]'         => $assignDetails->vacancy->address_line,
                                    '[date]'            => $assignDetails->on_date,
                                    '[shift_start]'     => $assignDetails->start_time,
                                    '[shift_end]'       => $assignDetails->end_time,
                                    '[detail]'          => $assignDetails->notes, 
                                    '[booking_code]'    => $assignDetails->code,
                                    '[agency_name]'     => $assignDetails->vacancy->agency,
                                ];
                                $agency = Agency::first();
                                $mail_content                 = EmailTraits::getMessegeConetent($assign_mail_variables,'Assign Vacancy');
                                $mail_content['agency_email'] = $agency->email;
                                $mail_content['agency_name']  = $agency->name;
                                $mail_content['agency_logo']  = $agency->agency_logo;
                                
                                // dispatch(new PublishAssignedShiftsJob($mail_content));
                            }
                          }
                          
                          
                          // array_push($shift, json_decode(json_encode($assignDetails), true));

                          // if ( ( isset($request->is_publish) && $request->is_publish == 1 ) && isset($request->notify) ) {
          
                          //     array_push($publishedShifts, $assignDetails);
          
                          //     $notify = Notification::AddPublishedShiftNotication($assignDetails);
                              
                          // }

                      } else {
                          array_push($errResponse, 'Can not assign '.$candidateDetails->full_name.'. Something went wrong!.');
                      }
                  } else {
                      array_push($errResponse, 'Can not assign '.$candidateDetails->full_name.'. Something went wrong!.');
                  }
              }
          }
        }

        if ($assignDates) {
          array_push($sucResponse, $candidateDetails->full_name.'  Successfully  assigned on '.implode(", ",$assignDates));
        }

        if(count($bookingCodes) > 0){
          $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
          $loggedData = [
              'role'          => 'manager',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'manager_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
          ];
          //Activity Log
          $message = 'New booking(s) '. (count($bookingCodes) > 1 ? ' have ' : ' has '). 'been created with code(s) ' . implode(', ', $bookingCodes) . '.';
          ActivityCustomLogTrait::saveActivityLog('Bookings', 'Create', $message, '', $bookingCodes, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
        }

      return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $errResponse
          ],
          'success'=>[
            'status_code' => 201,
            'status' => 'Success', 
            'message' => $sucResponse
          ],
        ]);
        // return ['error' => $errResponse,'success' => $sucResponse];
      }
    }

    /**
     * Publish vacancy
     *
     * @param \App\Models\AssignVacancy $assignVacancy
     *
     * @return \Illuminate\Http\Response
     */
    public function publish_vacancy(Request $request)
    {

      $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id,
                        'candidate_ids' => $request->candidate_ids,
                        'subdomain' => $request->subdomain,
                        'is_publish' => $request->is_publish,
                        'text' => $request->text,
                        'push' => $request->push,
                        'email' => $request->email,
                        ), array(
                        'vacancy_id' => 'required|numeric',
                        'candidate_ids' => 'required',
                        'subdomain' => 'required',
                        'is_publish' => 'nullable|boolean',
                        'text' => 'nullable|boolean',
                        'push' => 'nullable|boolean',
                        'email' => 'nullable|boolean',
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_ids')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_ids')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('is_publish')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('is_publish')
                  ]
                ],400);
            }
            else if ($messages->has('text')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('text')
                  ]
                ],400);
            }
            else if ($messages->has('push')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('push')
                  ]
                ],400);
            }
            else if ($messages->has('email')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('email')
                  ]
                ],400);
            }


      } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');
        
        $candidates = explode(",", $request->candidate_ids);
        $id = $request->vacancy_id;
        $vacancy                 = Vacancy::find($id);
        $vacancy->is_publish     = 1;
        //updating published candidates
        if ($vacancy->published_candidates != NULL) {
            $candIds = array_merge($vacancy->published_candidates, $candidates);
        } else {
            $candIds = $candidates;
        }
        $vacancy->published_ids  = implode(",", array_values(array_unique($candIds)) );
        $vacancy->published_at   = date("Y-m-d H:i:s");
        $vacancy->save();
        
        $candidates = DB::table('candidates')->whereIn('id', $candidates)->get();
        
        $notify_array    = array(
          'title'      => '',
          'type'       => "candidate_vacancy_created",
          'date'       => date("d-m-Y",strtotime($vacancy['on_date'])),
          'body'       => "A new vacancy has been added on ".date("d-m-Y",strtotime($vacancy['on_date']))." with Ref.No: ".$vacancy['code'].", in ".$vacancy['business_unit_name'].", ".$vacancy['address_line']
        );

        if ($candidates) {
          foreach($candidates as $candidate){            
            $notification                   = new Notification;
            $notification->type             = 'App\Notifications\HelloNotification';
            $notification->notifiable_type  = 'App\User';
            $notification->notifiable_id    = $candidate->user_id;
            $notification->role_id          = 6;
            $notification->object_id        = $vacancy->id;
            $notification->data             = json_encode($notify_array);
            $notification->save();
              
          }
        }

        $notiArray = [];
        /* Check if push notification is selected  */
        if($request->push == 1){
            //checking for onesignal token
            $oneSignalAppId = DB::table('mobile_app_settings')->where('key', 'oneSignalAppId')->whereNotNull('value')->pluck('value')->first();
            if(empty($oneSignalAppId)) {
                $candidateUins = DB::table('candidates')->whereIn('id', array_unique($candIds))->where('firebase_uin', '!=' , '')->pluck('firebase_uin')->all();
            } else {
                $candidateUsers = DB::table('candidates')->whereIn('id', array_unique($candIds))->pluck('user_id')->all();
                $candidateUins = DB::table('role_users')->whereIn('user_id', $candidateUsers)
                                ->where('role_id', 6)
                                ->whereNotNull('player_id')->where('player_id', '!=' , '')->pluck('player_id')->all();                                
            }
            $request->request->add(['candidateUins' => $candidateUins]);
            $result = PushNotificationTrait::vacancyPublish($request);
            array_push($notiArray, 'push');
        }
        /* Check if text notification is selected  */
        if($request->text == 1){
            // $request->request->add([$request->input('subdomain')]);
            // $request->request->add(['vacancy' => $vacancy]);
            // $request->request->add(['candidates' => $candidates]);
            // $result = SMSTraits::vacancyPublish($request);
            array_push($notiArray, 'text');
        }
        /* Check if text notification is selected  */
        if($request->email == 1){
            // event(new VacancyCreated($candidates,$vacancy->id));
            array_push($notiArray, 'email');
        }

        $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
        $loggedData = [
            'role'          => 'manager',
            'tenant'        => $request->input('subdomain'),
            'dashboardType' => 'manager_app',
            'user_name'     => $user->name,
            'email'         => $user->email,
        ];
        
        $candidateNames = $candidates->pluck('full_name')->all();
        
        //Activity Log
        $message = 'Vacancy - '. $vacancy->code.' has been published for the candidate(s): '. implode(',', $candidateNames). ' via '. implode(', ', $notiArray).'.';
        ActivityCustomLogTrait::saveActivityLog('Vacancies', 'Update', $message, $vacancy->id, $vacancy->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

        return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success', 
              'message' => "Vacancy published successfully"
            ]
          ], 200);
      }
    }

    /**
     * Vacancy dates with open and assigned
     * */
    public function vacancy_dates (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

          $date_from = $request->input('date_from');
          $date_to = $request->input('date_to');
          $candidate_id = $request->input('candidate_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');

            $candidate = Candidate::find($request->candidate_id);
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 

            if ( $candidate->candidate_status_id != 1 ) {
                return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => "Please check and fill your profile to get full access."
                    ]
                  ],400);
            }
            //getting candidate job id's
            $job_ids = CandidateJob::where('candidate_id', $candidate_id)->pluck('job_id')->all();

            $openVacancies = DB::table('vacancies')
                    ->whereBetween('on_date', [$date_from, $date_to])
                    ->whereIn('job_id', $job_ids)
                    ->whereRaw("find_in_set($candidate_id, published_ids)")
                    ->orderBy('on_date', 'asc')->where('is_publish', 1)
                    ->pluck('on_date')->all();

            $assignedVacancies = DB::table('assign_vacancies')
                            ->whereBetween('on_date', [$date_from, $date_to])
                            ->where('candidate_id', $candidate_id)
                            ->where('is_publish', 1)
                            ->whereIn('status',['pending', 'invoiced', 'approved'])
                            ->orderBy('on_date', 'asc')
                            ->where('is_deleted', 0)
                            ->pluck('on_date')->all();

            return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success',
              'data' => [
                'open' => $openVacancies,
                'assigned' => $assignedVacancies
              ]
            ]
            ],200);

      }
    }

    public function vacancy_list (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        // 'date' => $request->date, 
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'subdomain' => $request->subdomain,
                        'applied' => $request->applied
                        ), array(
                        // 'date' => 'required', 
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'subdomain' => 'required',
                        'applied' => 'nullable|boolean'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            // if ($messages->has('date')) {
            //     //Show custom message
            //     return response()->json([
            //       'error'=>[
            //         'status_code' => 400,
            //         'status' => 'Error', 
            //         'message' => $messages->first('date')
            //       ]
            //     ],400);
            // }
            // else 
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }
            else if ($messages->has('applied')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('applied')
                  ]
                ],400);
            }


      } else {

          // $date = $request->input('date');
          $date_from = $request->input('date_from');
          $date_to = $request->input('date_to');
          $datesArr = [];
          if (strtotime($date_from)>strtotime($date_to)) {
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => "Please select the dates correctly"
                  ]
                ],400);
          }
          for( $i= strtotime($date_from); $i<= strtotime($date_to); $i = strtotime("+1 days", $i) ){
            $datesArr[] = date ("Y-m-d", $i);
          }

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');
          //search variables
          $shift_id = !empty($request->shift) ? Shift::whereIn("shift_name", $request->shift)->pluck('id') : '';
          $business_unit_id = !empty($request->business_unit_id) ? $request->business_unit_id : '';
          $job_id = !empty($request->job_id) ? $request->job_id : '';
          $publish = !empty($request->publish) ? $request->publish : '';

          //time format
          // $agency = DB::table('agencies')->first();
          //     if ($agency->time_format == 24){
          //   $time_format = "%H:%i";
          // } else {
          //   $time_format = "%h:%i %p";
          // }
          $open_shifts = [];

          

          sort($datesArr);

          $vacancies = DB::table('vacancies')
          ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
          ->leftJoin('assign_vacancies', function($join)
          // ->join('assign_vacancies', function($join)
            {
              $join->on('vacancies.id', '=', 'assign_vacancies.vacancy_id');
              $join->where('assign_vacancies.status', 'applied');
              $join->where('assign_vacancies.is_deleted', 0);
            })
          ->join('clients', 'business_units.client_id', '=', 'clients.id')
          ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
          ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
          ->whereIn('vacancies.on_date', $datesArr)
          ->where(function($query) use ($shift_id, $business_unit_id, $job_id, $publish, $request)
            {
              if ($shift_id) {
                $query->where('vacancies.shift_id', $shift_id)->get();
              }
              if ($business_unit_id) {
                $query->where('vacancies.business_unit_id', $business_unit_id)->get();
              }
              if ($job_id) {
                $query->where('vacancies.job_id', $job_id)->get();
              }
              if ($publish) {
                $query->where('vacancies.is_publish', $publish)->get();
              }
              if (isset($request->applied) && $request->applied == 1) {
                $query->where('vacancies.applied_count', '>', 0);
              }
            })
          ->whereRaw('vacancies.total_numbers > vacancies.used_count')
          ->select('vacancies.id', 'vacancies.code', 'business_units.client_id', 'clients.client_name', 'business_units.business_unit_name', 'jobs.id AS job_id', 'jobs.name AS job_name', 'jobs.color', 'vacancies.total_numbers AS total_space', 'shifts.shift_name', 'vacancies.on_date', DB::raw('DATE_FORMAT(vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  DB::raw('DATE_FORMAT(vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 'vacancies.total_hours', 'vacancies.break', 'vacancies.total_numbers', 'vacancies.used_count', 'vacancies.used_count as assigned_count', 'vacancies.notes', 'vacancies.is_publish', DB::raw("count(assign_vacancies.id) as applied_count"))
          ->orderBy('vacancies.on_date')
          ->groupBy('vacancies.id')
          ->paginate(20);

          $collection = collect($vacancies->getCollection());
            $groupedCollections = $collection->groupBy('on_date');
            //checking empty date set
            foreach ($groupedCollections as $key => $value) {
                $open_shifts[] = array(
                    'date' => $key, 
                    'vacancies' => isset($groupedCollections[$key]) ? $groupedCollections[$key] : []
                );
            }
          
          // if ($datesArr) {
          //   foreach($datesArr as $date) { 

          //         $vacancyArr = [];
          
          //         foreach($vacancies as $vacancy){  
          //           $used_count = DB::table('assign_vacancies')->where('vacancy_id', $vacancy->id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');
          //           // $assignedDeleted = DB::table('assign_vacancies')->where('vacancy_id', $vacancy->id)->where('is_deleted', 1)->where('is_moved_to_vacancies', 0)->where('status', 'pending')->count('id');
          //           // $used_count = (int)$assigned + (int)$assignedDeleted;  
          //           $vacancy->used_count = (int)$used_count;
          //           $vacancy->assigned_count = (int)$used_count; 
                    
          //           if($vacancy->total_numbers > $used_count) {
          //             $vacancy->space_left = (int)$vacancy->total_numbers-(int)$used_count;
          //             $vacancyArr[] =  $vacancy;
          //           }
          //         }
          //       $open_shifts[$date] = $vacancyArr;
          //   }
          // }

          // $vacancy_list = [];
          // if ($open_shifts) {
          //   foreach($open_shifts as $key => $value) { 
          //     $vacancy_list[] = array(
          //       'date' => $key, 
          //       'vacancies' => $value 
          //     );
          //   }
          // }


              return response()->json([
                'success'=>[
                  'status_code' => 200,
                  'status' => 'Success',
                  'time_format' => DB::table('agencies')->first()->time_format,
                  'data' => $open_shifts
                ]
              ],200);

      }
    }


    public function vacancy (Request $request) 
    {

      $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'vacancy_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
          $vacancy_id = $request->input('vacancy_id');

          config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
          DB::reconnect('mysql');
          $vacancy = Vacancy::find($vacancy_id);

          $agency = DB::table('agencies')->first();
          if ($agency->time_format == 24){
              $vacancy->start_time = date("Y-m-d H:i", strtotime($vacancy->start_time));
              $vacancy->end_time   = date("Y-m-d H:i", strtotime($vacancy->end_time));
          } else {
              $vacancy->start_time = date("Y-m-d h:i A", strtotime($vacancy->start_time));
              $vacancy->end_time   = date("Y-m-d h:i A", strtotime($vacancy->end_time));
          }



          return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success',
              'time_format' => DB::table('agencies')->first()->time_format,
              'data' => $vacancy
            ]
          ],200);

      }
    }

    /**
     * delete vacancy
     *
     * @param \App\Models\AssignVacancy $assignVacancy
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_vacancy(Request $request)
    {

      $validator = Validator::make(
                        array(
                        'vacancy_id' => $request->vacancy_id,
                        'reason' => $request->reason,
                        'subdomain' => $request->subdomain,
                        ), array(
                        'vacancy_id' => 'required|numeric',
                        'reason' => 'required',
                        'subdomain' => 'required',
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('vacancy_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('vacancy_id')
                  ]
                ],400);
            }
            else if ($messages->has('reason')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('reason')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $id = $request->vacancy_id;
        $vacancy                 = Vacancy::find($id);

        $space_left = 0;
        $used_count = DB::table('assign_vacancies')->where('vacancy_id', $id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');

        if((int)$vacancy->total_numbers > (int)$used_count){
            $space_left = (int)$vacancy->total_numbers-(int)$used_count;
            $vacancy->used_count     = (int)$used_count;
            $vacancy->total_numbers  = (int)$used_count;
            $vacancy->deleted_space  = $space_left;
            $vacancy->reason         = isset($request->reason) ? $request->reason : NULL;
            $vacancy->deleted_by     = Auth::id();
            $vacancy->save();

            $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
            $loggedData = [
                'role'          => 'manager',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'manager_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
            ];
            //Activity Log
            $message = 'Vacancy - '. $vacancy->code .' has been deleted.';
            ActivityCustomLogTrait::saveActivityLog('Vacancies', 'Delete', $message, $vacancy->id, $vacancy->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData
        }
        
        return response()->json([
            'success'=>[
              'status_code' => 200,
              'status' => 'Success', 
              'message' => "Vacancy deleted successfully"
            ]
          ], 200);
      }
    }
    
}
