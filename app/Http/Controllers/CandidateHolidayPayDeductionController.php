<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Tenants\Agency;
use App\Tenants\RoleUser;
use App\Tenants\Candidate;
use App\Tenants\Notification;
use App\Tenants\CandidateHolidayPayDeduction;
use App\Helpers\SendMailGun;
use App\Traits\ActivityLog\ActivityCustomLogTrait;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;

class CandidateHolidayPayDeductionController extends BaseController
{


  public function request_holiday_pay (Request $request) 
  {

    $validator = Validator::make(
      array(
        'candidate_id' => $request->candidate_id,
        'requested_amount' => $request->requested_amount,
        'subdomain' => $request->subdomain
      ), array(
        'candidate_id' => 'required',
        'requested_amount' => 'required',
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('candidate_id')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('candidate_id')
          ]
        ],400);
      }
      else if ($messages->has('requested_amount')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('requested_amount')
          ]
        ],400);
      }
      else if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $holiday_pay = DB::table('candidate_holiday_pays')
                        ->where('candidate_id', $request->candidate_id)->first();
        if($request->requested_amount<=0) {
          return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Please enter a valid amount."
              ]
            ],400);
        }
        if(!$holiday_pay) {
          return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "There is no amount added. Please contact your agency."
              ]
            ],400);
        }
        if($holiday_pay->total_amount < $request->requested_amount) {
            return response()->json([
              'error'=>[
                'status_code' => 400,
                'status' => 'Error', 
                'message' => "Your requested amount must be lesser than balance amount"
              ]
            ],400);
        }

        try {
            // $candidate  = Candidate::find($request->candidate_id);
            //checking for candidate access, restrict if candidate has no accesss
            $candidate = DB::table('candidates')->where('id', $request->candidate_id)->select('id', 'full_name', 'has_access','is_deleted', 'employment_types_id', 'user_id')->first();
            if ( $candidate && $candidate->has_access == 0 ) {
                return response()->json([
                      'error'=>[
                        'status_code' => 401,
                        'status' => 'Error', 
                        'message' =>  'You have no access to use this account. Please contact the agency.',
                      ]
                    ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'Unauthorized',
                      'isDeleted'=> true,
                      'isUnauthorized'=> true
                    ]
                  ],401);
            } 

            $input = [
              'type' => "deduct",
              'candidate_id' => $request->candidate_id,
              'requested_amount' => $request->requested_amount,
              'amount_when_deducting' => $holiday_pay ? $holiday_pay->total_amount : 0,
              'status' => "pending",
              'initiated_by' => $candidate->user_id,
            ];
            //inserting deduction details
            CandidateHolidayPayDeduction::create($input);

            //updating notification
            $user_ids        = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
            $user_ids        = array_unique($user_ids);
            $notify_array = array(
                'title' => $candidate->full_name,
                'type' => "holidaypay_request",
                'body' => "has requested holiday pay of amount £".$request->requested_amount,
                 );
            $success = $candidate->full_name . ' has requested holiday pay of amount £' . $request->requested_amount;
            if ($user_ids) {
                foreach ($user_ids as $user_id) {
                    $notification = new Notification;
                    $notification->type             = 'App\Notifications\HelloNotification';
                    $notification->notifiable_type  = 'App\User';
                    $notification->notifiable_id    = $user_id;
                    $notification->data             = json_encode($notify_array);
                    $notification->resource_ids     = $request->candidate_id;
                    $notification->save();
                }
            }
            //get agency details
            $agency = Agency::first();
            //sending request email to the agency
            $mail_content['candidate_name'] = '';
            $mail_content['agency_email']   = $agency->email;
            $mail_content['agency_name']    = $agency->name;
            $mail_content['agency_logo']    = $agency->agency_logo;
            $mail_content['title']          = 'Holiday Pay Request';
            $mail_content['body']           = $candidate->full_name . " has requested holiday pay of amount £".$request->requested_amount . ". Kindly review and process the request accordingly.";
            $mail_content['tenant_slug']    = $request->input('subdomain');

            SendMailGun::sendMail('notify_new_message', $mail_content['agency_name'], $mail_content['agency_email'], $mail_content["agency_email"], $mail_content['title'], $mail_content, null, null, null);

            $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('name', 'email')->first();
            $loggedData = [
              'role'          => 'candidate',
              'tenant'        => $request->input('subdomain'),
              'dashboardType' => 'candidate_app',
              'user_name'     => $user->name,
              'email'         => $user->email,
            ];
            //Activity Log
            $message = 'Candidate - '. $candidate->full_name .' has requested holiday pay of amount £' . $request->requested_amount . '.';
            $message = 'Candidate - holiday pay of amount £' . $request->requested_amount . ' has been requested.';
            ActivityCustomLogTrait::saveActivityLog('Candidates', 'Update', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

            return response()->json([
                'success' => [
                    'status_code' => 201,
                    'status' => 'Success',
                    'message' => "Request has been made successfully",
                    'success' => $success,
                    'notify' => $notify_array
                  ],
                ], 201);

        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json([
                'error' => [
                    'status_code' => 500,
                    'status' => 'Error',
                    'message' => $ex->getMessage(),
                ],
            ], 500);
        }
    }
    
  }
  
  public function get_holiday_pay_requests (Request $request) 
  {

    $validator = Validator::make(
      array(
        'candidate_id' => $request->candidate_id,
        'date_from' => $request->date_from,
        'date_to' => $request->date_to,
        'page' => $request->page,
        'total' => $request->total,
        'subdomain' => $request->subdomain
      ), array(
        'candidate_id' => 'required',
        'date_from' => 'required',
        'date_to' => 'required',
        'page' => 'required',
        'total' => 'nullable',
        'subdomain' => 'required'
      )
    );

    if ($validator->fails()) {

      $messages = $validator->errors();
            //Determining If Messages Exist For A Field
      if ($messages->has('candidate_id')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('candidate_id')
          ]
        ],400);
      }
      else if ($messages->has('date_from')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('date_from')
          ]
        ],400);
      }
      else if ($messages->has('date_to')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('date_to')
          ]
        ],400);
      }
      else if ($messages->has('page')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('page')
          ]
        ],400);
      }
      else if ($messages->has('subdomain')) {
                //Show custom message
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('subdomain')
          ]
        ],400);
      }


    } else {

      config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
      DB::reconnect('mysql');

      $holiday_pay = DB::table('candidate_holiday_pays')
                        ->where('candidate_id', $request->candidate_id)->first();
      $user_id = DB::table('candidates')->where('id', $request->candidate_id)->first()->user_id;

      $total = !empty($request->total) ? $request->total : 5;
      $holidayPayRequests = CandidateHolidayPayDeduction::where([
                'candidate_id' => $request->candidate_id,
                'type' => "deduct",
                'initiated_by' => $user_id
                ])->where(function($query) use ($request) {
                    if ($request->date_from && $request->date_to) {
                        $query->whereBetween('created_at', [$request->date_from, $request->date_to]);
                    }
                })->orderBy('id', 'desc')->paginate($total);


        return response()->json([
          'success'=>[
            'status_code' => 200,
            'status' => 'Success', 
            'balance_amount' => $holiday_pay ? $holiday_pay->total_amount : 0, 
            'data' => $holidayPayRequests
          ]
        ],200);

    }
    
  }
  
}
