<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Helpers\SendMailGun;

class EmailController extends BaseController
{

  public function forward_msg_to_email (Request $request) {
    $validator = Validator::make(
      array('email' => $request->email,'message' => $request->message, 'mobile_no' => $request->mobile_no),
      array('email' => 'required','message' => 'required', 'mobile_no' => 'required')
    );
    if ($validator->fails()) {
      $messages = $validator->errors();
      if ($messages->has('email')) {
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('email')
          ]
        ],400);
      } else if ($messages->has('message')) {
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('message')
          ]
        ],400);
      } else if ($messages->has('mobile_no')) {
        return response()->json([
          'error'=>[
            'status_code' => 400,
            'status' => 'Error', 
            'message' => $messages->first('mobile_no')
          ]
        ],400);
      }
    } else {
      $message = $request->message;
      $email = $request->email;
      $mobile_no = $request->mobile_no;
      SendMailGun::sendMail('candidate-reply-from-sms-app', 'SMS App', 'info@logezy.co.uk', $email, 'New Message', ['message' => $message, 'mobile_no' => $mobile_no], null, null, null);
      return true;
    }
  }

}