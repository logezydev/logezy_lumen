<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Tenants\RoleUser;
use App\Tenants\Agency;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\Notification;
use App\Tenants\CandidateAvailability;
use App\Traits\PushNotificationTrait;
use App\Traits\ActivityLog\ActivityCustomLogTrait;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Illuminate\Database\DatabaseManager;

class CandidateAvailabilityController extends BaseController
{
    public function get_availability(Request $request)
    {

        $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'candidate_id' => $request->candidate_id,
                        'subdomain' => $request->subdomain
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'candidate_id' => 'required',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            $date_from = $request->input('date_from');
            $date_to = $request->input('date_to');
            $candidate_id = $request->input('candidate_id');

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            $candidate = Candidate::find($request->candidate_id);
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 
            
            $allcandidate = CandidateAvailability::where(function($query)use($date_from, $date_to, $candidate_id){
                                if ($date_from != '' && $date_to != ''){
                                  $query->whereBetween('date', [$date_from, $date_to]);
                                }
                                if($candidate_id){
                                  $query->where('candidate_id',$candidate_id);
                                }
                            })->get();

            $candidates = collect($allcandidate);
            $grouped = $candidates->groupBy('date');
            $datearray = array();
                if (count($grouped)>0) {
                    
                    foreach ($grouped as  $key => $value) {
                        $availability = array();
                        foreach ($value as  $val) {
                          $availability[] = $val->availability;
                        }
                        $candidate_availability[] = array('date' => $key, 'availabilities' => $availability);
                        
                    }
                }else{
                    $candidate_availability = array();
                }

                return response()->json([
                  'success'=>[
                    'status_code' => 200,
                    'status' => 'Success', 
                    'data' => $candidate_availability
                  ]
                ],200);
      }
        
    }

    public function add_availability(Request $request)
    {

        $validator = Validator::make(
                        array(
                        'date_from' => $request->date_from, 
                        'date_to' => $request->date_to,
                        'candidate_id' => $request->candidate_id,
                        'availability' => $request->availability,
                        'subdomain' => $request->subdomain
                        ), array(
                        'date_from' => 'required', 
                        'date_to' => 'required',
                        'candidate_id' => 'required',
                        'availability' => 'nullable',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date_from')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_from')
                  ]
                ],400);
            }
            else if ($messages->has('date_to')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date_to')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('availability')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('availability')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {

            $date_from = $request->input('date_from');
            $date_to = $request->input('date_to');
            $candidate_id = $request->input('candidate_id');
            $availability = $request->input('availability');

            config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
            DB::reconnect('mysql');

            // $candidate = Candidate::find($request->candidate_id);
            $candidate = DB::table('candidates')->where('id', $candidate_id)->select('id','has_access','is_deleted','employment_types_id', 'user_id', 'candidate_status_id', 'full_name')->first();
            //checking for candidate access, restrict if candidate has no accesss
            if ( $candidate && $candidate->has_access == 0 ) {
              return response()->json([
                    'error'=>[
                      'status_code' => 401,
                      'status' => 'Error', 
                      'message' =>  'You have no access to use this account. Please contact the agency.',
                    ]
                  ],401);
            } 
            //checking for candidate status, restrict if candidate is deleted
            if ( $candidate && $candidate->is_deleted == 1 ) {
                return response()->json([
                  'error'=>[
                    'status_code' => 401,
                    'status' => 'Error', 
                    'message' =>  'Unauthorized',
                    'isDeleted'=> true,
                    'isUnauthorized'=> true
                  ]
                ],401);
            } 

            if ( $candidate->candidate_status_id != 1 ) {
                return response()->json([
                    'error'=>[
                      'status_code' => 400,
                      'status' => 'Error', 
                      'message' => "Please check and fill your profile to get full access."
                    ]
                  ],400);
            }
            
            if ($request->has('availability')) {

                $dateArray = [];

                if (count($availability) > 0) {
                
                    foreach ($availability as $available) {
                        foreach ($available['availabilities'] as $shift) {

                            $dateArray[] = date("Y-m-d", strtotime($available['date']));
                            //delete availabilities with the date
                            CandidateAvailability::whereNotIn('availability', $available['availabilities'])->whereNotIn('availability', ['Other'])->where('candidate_id', $candidate_id)->whereIn('date', [date("Y-m-d", strtotime($available['date']))])->whereBetween('date', [$date_from, $date_to])->delete();
                        }

                        //delete availabilities not within the date
                        CandidateAvailability::where('candidate_id', $candidate_id)->whereNotIn('availability', ['Other'])->whereNotIn('date', $dateArray)->whereBetween('date', [$date_from, $date_to])->delete();
                    } 
                    
                } else {

                    //delete availabilities not within the date range
                    CandidateAvailability::where('candidate_id', $candidate_id)->whereNotIn('availability', ['Other'])->whereBetween('date', [$date_from, $date_to])->delete();

                }
            }

            $date_array = [];
            $error_dates = [];
            $resMessage = '';
            $availabilityActionsEnabled = DB::table('agencies')->pluck('allow_availability_actions')->first();
            if ($request->has('availability')) {

                $date_array = [];
                $error_dates = [];
                foreach ($availability as $available) {

                  foreach ($available['availabilities'] as $shift) {
                    if($availabilityActionsEnabled) {
                      $assignVacancy = DB::table('assign_vacancies')->where('candidate_id', $candidate_id)->where('on_date', $available['date'])->whereIn('status', ['pending', 'approved', 'invoiced'])->where('is_deleted', 0)->count();
                    } else {
                      $assignVacancy = 0;
                    }
                    if($assignVacancy == 0) {
                      if ($shift == "E" || $shift == "L") {
                          $shift_id = 1;
                      } else if ($shift == "N") {
                          $shift_id = 2;
                      } else {
                          $shift_id = 0;
                      }

                      $exists = CandidateAvailability::where('candidate_id', $candidate_id)->where('availability', $shift)->where('date', $available['date'])->count();

                      if ($exists == 0) {

                          $candidateAvailability                   = new CandidateAvailability;
                          $candidateAvailability->candidate_id     = $candidate_id;
                          $candidateAvailability->shift_id         = $shift_id;
                          $candidateAvailability->created_by       = $candidate->user_id;
                          $candidateAvailability->availability     = $shift;
                          $candidateAvailability->date             = $available['date'];
                          $candidateAvailability->save();

                          $date_array[] = date("d-m-Y", strtotime($available['date']));
                      }
                    } else {
                      $error_dates[] = date("d-m-Y", strtotime($available['date']));
                    }
                  }
                    
                } 
            }
            if(count($date_array) > 0) {
              //updating notification
              $candidate_name  = Candidate::find($candidate_id)->full_name;
              $agency_email    = Agency::first()->email;
              $date_array      = array_unique($date_array);
              $dates           = implode(',' , $date_array);
              $user_ids        = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
              $user_ids        = array_unique($user_ids);
              $notify_array = array(
                  'title' => $candidate_name,
                  'type' => "availability",
                  'body' => "modified their availability on ".$dates.".",
                  );
              if ($user_ids) {
                  foreach ($user_ids as $user_id) {
                      $notification = new Notification;
                      $notification->type             = 'App\Notifications\HelloNotification';
                      $notification->notifiable_type  = 'App\User';
                      $notification->notifiable_id    = $user_id;
                      $notification->data             = json_encode($notify_array);
                      $notification->resource_ids     = $candidate_id;
                      $notification->save();
                  }
              }
              //send push notifications to admins and managers
              $player_ids = RoleUser::whereIn('role_id', [2,3,4])->whereNotNull('player_id')->where('player_id', '!=', '')->pluck('player_id')->all();
              $player_ids = array_unique($player_ids);
              $template = $candidate_name." modified their availability on ".$dates.".";
              PushNotificationTrait::oneSignalPushMessage($player_ids, $template, array("data_type" => "availability"));
              $resMessage = "Your availability modified successfully.";
            }

            if(count($error_dates) > 0) {
              $error_dates      = array_unique($error_dates);
              $dates           = implode(',' , $error_dates);
              $resMessage .= "Due to already allocated shifts, please note that the availability cannot be changed on (".$dates.").";
            }

            return response()->json([
              'success'=>[
                'status_code' => 201,
                'status' => 'Success', 
                'message' => $resMessage
              ]
            ],201);
            
      }
        
    }

    public function addBulkAvailability(Request $request)
    {
        $validator = Validator::make(
                        array(
                        'date' => $request->date, 
                        'candidate_id' => $request->candidate_id,
                        'user_id' => $request->user_id,
                        'availabilities' => $request->availabilities,
                        'notes' => $request->notes,
                        'subdomain' => $request->subdomain
                        ), array(
                        'date' => 'required', 
                        'candidate_id' => 'required',
                        'user_id' => 'required',
                        'availabilities' => 'required',
                        'notes' => 'nullable',
                        'subdomain' => 'required'
                    )
                );

      if ($validator->fails()) {

            $messages = $validator->errors();
            //Determining If Messages Exist For A Field
            if ($messages->has('date')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('date')
                  ]
                ],400);
            }
            else if ($messages->has('user_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('user_id')
                  ]
                ],400);
            }
            else if ($messages->has('candidate_id')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('candidate_id')
                  ]
                ],400);
            }
            else if ($messages->has('availabilities')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('availabilities')
                  ]
                ],400);
            }
            else if ($messages->has('subdomain')) {
                //Show custom message
                return response()->json([
                  'error'=>[
                    'status_code' => 400,
                    'status' => 'Error', 
                    'message' => $messages->first('subdomain')
                  ]
                ],400);
            }


      } else {
        //addBulkAvailability
        // dd($request->all());
        $dates = explode(",",  $request->date);
        $candidate = $request->candidate_id;
        $availabilities = $request->availabilities;
        //change DB
        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');
        $availabilityActionsEnabled = DB::table('agencies')->pluck('allow_availability_actions')->first();
        $data_to_insert = [];
        $date_array = [];
        $error_dates = [];
        $sucMessage = '';
        foreach ($dates as $date) {
            foreach ($availabilities as $availability) {
              if($availabilityActionsEnabled) {
                $assignVacancy = DB::table('assign_vacancies')->where('candidate_id', $candidate)->where('on_date', $date)->whereIn('status', ['pending', 'approved', 'invoiced'])->where('is_deleted', 0)->count();
              } else {
                $assignVacancy = 0;
              }
              if($assignVacancy == 0) {
                $shift_id = 0;
                switch($availability) {
                    case 'E':
                    case 'L':
                        $shift_id = 1;
                        break;
                    case 'N':
                        $shift_id = 2;
                        break;

                }
                array_push($data_to_insert, [
                    'candidate_id' => $candidate,
                    'shift_id' => $shift_id,
                    'availability' => $availability,
                    'date' => $date,
                    'notes' => isset($request->notes)&&!empty($request->notes) ? $request->notes : NULL,
                    'created_by' => $request->user_id
                ]);
                $date_array[] = date("d-m-Y", strtotime($date));
              } else {
                $error_dates[] = date("d-m-Y", strtotime($date));
              }
            }
        }
        
        if(count($date_array) > 0) {
          //delete old datta
          CandidateAvailability::where('candidate_id', $candidate)->whereIn('date', $dates)->delete();
          //bulk insert
          CandidateAvailability::insert($data_to_insert);

          //updating notification
          $candidate_name  = Candidate::find($candidate)->full_name;
          // $agency_email    = Agency::first()->email;
          $date_array      = array_unique($date_array);
          $dates           = implode(',' , $date_array);
          $user_ids        = RoleUser::whereIn('role_id', [2,3,4])->pluck('user_id')->all();
          $user_ids        = array_unique($user_ids);
          $notify_array = array(
              'title' => $candidate_name,
              'type' => "availability",
              'body' => "modified their availability on ".$dates.".",
              );
          if ($user_ids) {
              foreach ($user_ids as $user_id) {
                  $notification = new Notification;
                  $notification->type             = 'App\Notifications\HelloNotification';
                  $notification->notifiable_type  = 'App\User';
                  $notification->notifiable_id    = $user_id;
                  $notification->data             = json_encode($notify_array);
                  $notification->resource_ids     = $candidate;
                  $notification->save();
              }
          }
          //send push notifications to admins and managers
          $player_ids = RoleUser::whereIn('role_id', [2,3,4])->whereNotNull('player_id')->where('player_id', '!=', '')->pluck('player_id')->all();
          $player_ids = array_unique($player_ids);
          $template = $candidate_name." modified their availability on ".$dates.".";
          PushNotificationTrait::oneSignalPushMessage($player_ids, $template, array("data_type" => "availability"));
          $sucMessage = "Your availability modified successfully.";
        }
        if(count($error_dates) > 0) {
          $error_dates      = array_unique($error_dates);
          $dates           = implode(',' , $error_dates);
          $sucMessage .= "Due to already allocated shifts, please note that the availability cannot be changed on (".$dates.").";
        }

        $user = DB::table('logezycore.users')->where('id', $candidate->user_id)->select('name', 'email')->first();
        $loggedData = [
          'role'          => 'candidate',
          'tenant'        => $request->input('subdomain'),
          'dashboardType' => 'candidate_app',
          'user_name'     => $user->name,
          'email'         => $user->email,
        ];
        //Activity Log
        $message = 'Availability - modified their availability on ' . $dates . '.';
        ActivityCustomLogTrait::saveActivityLog('Availability', 'Update', $message, $candidate->id, $candidate->full_name, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

          return response()->json([
            'success'=>[
              'status_code' => 201,
              'status' => 'Success', 
              'message' => $sucMessage
            ]
          ],201);
    }
    
    }

    
}
