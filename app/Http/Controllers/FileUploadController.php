<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    public function upload(Request $request)
    {
        // return $request->all();
    	$this->validate($request, [
            'file'   => 'required|mimes:jpeg,png,jpg,gif,svg,pdf|max:2048',
            'domain' => 'required'
        ]);

    	$domain    = $request->domain;
    	$file      = $request->file('file');
		return $file_name = $file->getClientOriginalName(); 

    	$image_name = $domain.'/'.$file_name;
    	$path = Storage::disk('s3')->put(
                    $image_name, 
                    file_get_contents($request->file('file')), 
                   'public');

        $image_url = Storage::disk('s3')->url($image_name);


    	return $image_url;
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'file_url'   => 'required',
            'domain'     => 'required'
        ]);

        $domain          = $request->domain;
        $file_path       = $request->file_url;
        

        if(Storage::disk('s3')->exists($domain.'/'.$file_path)) {

            Storage::disk('s3')->delete($domain.'/'.$file_path);
        }
        return 'True';


        
    }
}
