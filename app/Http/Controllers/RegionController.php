<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Auth;
use Artisan;
use Config;
use Schema;
use Session;
use Carbon\Carbon;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\Storage;

class RegionController extends BaseController
{
  public function getRegions (Request $request) {

    $validator = Validator::make(
                      array(
                      'subdomain' => $request->subdomain
                      ), array(
                      "subdomain" => "required"
                  )
              );

    if ($validator->fails()) {

          $messages = $validator->errors();
          //Determining If Messages Exist For A Field
          if ($messages->has('subdomain')) {
              //Show custom message
              return response()->json([
                'error'=>[
                  'status_code' => 400,
                  'status' => 'Error', 
                  'message' => $messages->first('subdomain')
                ]
              ],400);
          }


    } else {

        config(['database.connections.mysql.database' => 'logezy_'.$request->input('subdomain'),'database.default'=>'mysql']);
        DB::reconnect('mysql');

        $regions = DB::table('regions')->where('status','active')->where('is_deleted',0)->orderBy('name', 'asc')->get(); 
        $agencySetting = DB::table('agency_settings')->where('settings_key', 'region_status')->first(); 

        return response()->json([
              'success'=>[
                'status_code' => 200,
                'status' => 'Success',
                'region_status' => $agencySetting->settings_value == "true" ? true : false,
                'data' => $regions
              ]
            ],200);
    }

  }

}
