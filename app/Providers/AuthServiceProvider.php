<?php

namespace App\Providers;

use DB;
use App\User;
use App\UserToken;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
    }

    /**
     * Boot the authentication services for the application.
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->header('authorization')) :
                // return User::where('remember_token', explode(' ', $request->header('Authorization'))[1])->first();
                //Update Auth provider
                $userID = DB::table('user_tokens')->where('remember_token', explode(' ', $request->header('Authorization'))[1])->first('user_id');
                // dd($userID);
                if($userID) {
                    // return User::find($userID->user_id);
                    $user = User::where('status', 'active')->where('id', $userID->user_id)->first();
                    if(!$user) {
                        abort(401, 'Unauthorized.');
                    } 
                    return $user;
                } else {
                    abort(401, 'Unauthorized..');
                }
            endif;
            abort(401, 'Unauthorized !');
        });

    }
}
