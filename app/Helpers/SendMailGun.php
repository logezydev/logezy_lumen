<?php

namespace App\Helpers;
use Mailgun\Mailgun;
use App\Tenants\EmailReport;
use App\Helpers\MicrosoftEmail;
use DB;
class SendMailGun {

  protected static $microsoftEmail;

  public static function sendMail($template, $sender_name, $from_email, $to_email, $subject, $data, $cc, $bcc, $attachments, $email_type='') {
    if ($template == 'password-reset' && $template == 'candidate-reply-from-sms-app' && $template == 'otp-sending') {
      self::sendUsingMailGun($to_email, $subject, $data, $template, $sender_name, $from_email, $cc, $bcc, $attachments, $email_type);
      return true;
    }
    $graphAPI = env('MIX_MICROSOFT_GRAPH_URL');
    if (isset($data['tenant_slug'])) {
      config(['database.connections.mysql.database' => 'logezy_'.$data['tenant_slug'],'database.default'=>'mysql']);
      DB::reconnect('mysql');
      self::$microsoftEmail = new MicrosoftEmail($graphAPI, null);
      if (self::$microsoftEmail->integration && self::$microsoftEmail->integration->is_active) {
        self::sendUsingMicrosoft($to_email, $subject, $data, $template, $cc, $bcc, $attachments);
      } else {
        self::sendUsingMailGun($to_email, $subject, $data, $template, $sender_name, $from_email, $cc, $bcc, $attachments, $email_type);
      }
    } else {
      self::sendUsingMailGun($to_email, $subject, $data, $template, $sender_name, $from_email, $cc, $bcc, $attachments, $email_type);
    }
  }

  public static function sendUsingMailGun($to_email, $subject, $data, $template, $sender_name, $from_email, $cc, $bcc, $attachments, $email_type) {
    $domain = env('MAILGUN_DOMAIN');
    if (is_array($to_email)) {
      $to_email = implode(",",$to_email);
    }
    if ($template != 'password-reset' && $template != 'candidate-reply-from-sms-app' && $template != 'otp-sending') {
      config(['database.connections.mysql.database' => 'logezy_'.$data['tenant_slug'],'database.default'=>'mysql']);
      DB::reconnect('mysql');
      $mail_report = EmailReport::create(['parent_id' => null, 'from' => '', 'sender' => $sender_name .' <'.$from_email.'>', 'recipient' => $to_email, 'status' => 'send', 'subject' => $subject, 'recipient-domain' => '', 'time' => time()]);
      $data['parent_id']    = $mail_report->id;
    }
    $data['email_type'] = $email_type;
    $params = array(
      'from' => $sender_name .' <'.$from_email.'>',
      'to' => $to_email,
      'subject' => $subject,
      'template' => $template,
      'h:X-Mailgun-Variables' => json_encode($data),
      't:text' => 'yes',
      'o:tracking' => 'yes'
    );
    if ($cc)
      $params['cc'] = $cc;
    if ($bcc)
      $params['bcc'] = $bcc;
    if ($attachments && count($attachments) > 0) {
      $params['attachment'] = $attachments;
    }
    $mgClient = Mailgun::create(env('MAILGUN_SENDING_KEY'),env('MAILGUN_API_URL'));
   
    $mgClient->messages()->send($domain, $params);
  }

  public static function sendUsingMicrosoft($to_email, $subject, $data, $template, $cc, $bcc, $attachments) {
    if (!is_array($to_email)) {
      $to_email = array($to_email);
    }
    if (!is_array($cc)) {
      $cc = array($cc);
    }
    if (!is_array($bcc)) {
      $bcc = array($bcc);
    }
    self::$microsoftEmail->sendEmail($to_email, $subject, $data, $template, $cc, $bcc, $attachments);
  }
}
