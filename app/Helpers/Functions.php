<?php

use Brick\PhoneNumber\PhoneNumber;
use App\Tenants\AgencySetting;
use App\Tenants\Client;
use App\Tenants\Day;
use App\Tenants\Shift;
use App\Tenants\BusinessUnit;
use App\Tenants\Helper;
use App\Tenants\Holiday;
use App\Tenants\RateCard;
use App\Tenants\CustomHoliday;
use App\Tenants\EmploymentType;


/*
 * Find Country code using phone number
*/
if (!function_exists('countryCode')) {
    function countryCode($phone)
    {
        $number = PhoneNumber::parse($phone);

        return $number->getRegionCode();
    }
    function smsIntegrationAccount()
    {
        return AgencySetting::getTextIntegrationDetails();
    }
}

if (!function_exists('candidateCode')) {
    function candidateCode($agency_name, $tenant_id, $candidate_id)
    {
        $agency = str_replace('_', '',substr(strtoupper($agency_name), 0, 3));
        if(strlen($agency)<3) {
            $agency = str_replace('_', '',substr(strtoupper($agency_name), 0, 4));
        }
        return $agency.$tenant_id.'CA'.$candidate_id;
    }
}



/*****************************************Text message*********************************************/

if (!function_exists('clickSendText')) {
    /**
     * clickSendText.
     *
     * @param mixed $apiUsername
     * @param mixed $apiKey
     * @param mixed $fields
     */
    function clickSendText($apiUsername = '', $apiKey = '', $candidateMobile = '', $agencyMobile = '', $message = '')
    {
        if (empty($apiUsername) && empty($apiKey) && empty($candidateMobile) && empty($agencyMobile) && empty($message)) {
            return;
        }

        $headers = [
            'Content-Type: application/json',
            'Authorization: Basic '.base64_encode($apiUsername.':'.$apiKey),
        ];

        $body = [
            'messages' => [[
                'source' => 'php',
                'body' => $message,
                'to' => $candidateMobile,
                'from' => $agencyMobile,
            ]],
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, 'https://rest.clicksend.com/v3/sms/send');
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}

if (!function_exists('localSendText')) {
    /**
     * localSendText.
     *
     * @param mixed $agencyMobile
     * @param mixed $candidateMobile
     * @param mixed $message
     */
    function localSendText($agencyMobile = '', $candidateMobile = '', $message = '') // formatted mobile number required
    {
        if (empty($agencyMobile) && empty($candidateMobile) && empty($message)) {
            return;
        }

        $fcm_token = env('SMS_FCM');
        $regNo = \App\SmsDevice::where('mobile_number', $agencyMobile)->first(); // fetch device

        if ($regNo->device_token ?? false) {
            $fcmData = [
                'data' => [
                    'mobileNo' => $candidateMobile,
                    'message' => $message,
                ],
                'to' => $regNo->device_token,
                'priority' => 'high',
                'restricted_package_name' => '',
            ];

            $headers = [
                'Authorization: '.$fcm_token.'',
                'Content-Type: application/json',
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmData));
            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        }
    }
}

/*****************************************Push notification*********************************************/

if (!function_exists('pushNotification')) {
    /**
     * pushNotification.
     *
     * @param mixed $firebaseServerKey
     * @param mixed $candidateUin
     * @param mixed $template
     */
    function pushNotification($candidateUin = '', $template = '', $notification_type)
    {
        $firebaseServerKey = \App\Models\AgencySetting::where('settings_key', 'firebase_server_key')->first();

        if (empty($firebaseServerKey) && empty($candidateUin)) {
            return;
        }

        $notification = [
                'body' => $template,
                'notification_type' => $notification_type,
                'sound' => true,
                ];

        $extraNotificationData = ['message' => $notification, 'moredata' => 'dd'];

        $fcmNotification = [
                'to' => $candidateUin,
                'notification' => $notification,
                'data' => $extraNotificationData,
                ];

        $headers = [
                'Authorization: key='.$firebaseServerKey->settings_value.'',
                'Content-Type: application/json',
                ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}
/***************************************** Client Dashboard *********************************************/    

/*
 * get Client id using user id
*/
if (!function_exists('clientId')) {
    function clientId($user_id)
    {
        return Client::where('user_id', $user_id)->first()->id; 
    }
}
/*
 * get Client id using user id
*/
if (!function_exists('domainUrl')) {

    function domainUrl()
    {
        $urlParts = explode('.', $_SERVER['HTTP_HOST']);

        $appEnv = env('APP_ENV');

        switch($appEnv){

            case 'local' : 
                $domain = 'http://'.$urlParts[1]; break;

            case 'staging' : 
                $domain = 'https://'.$urlParts[1].'.co.uk'; break;

            case 'production' : 
                $domain = 'https://'.$urlParts[1].'.co'; break;            
        }

        return $domain;
    }
}
/*
 * get Business unit id in session
*/
if (!function_exists('businessUnitId')) {
    function businessUnitId()
    {
       return session::get('business_unit_id') ?? '';
    }
}
/*
 * get Client id in session
*/
if (!function_exists('businessClientId')) {
    function businessClientId()
    {
        $business_unit_id = session::get('business_unit_id') ?? '';

        return BusinessUnit::find($business_unit_id)->client_id ?? '';
    }
}
/*
 * get Holiday Pay by Bu and Job
*/
if (!function_exists('getHolidayPayByBuAndJob')) {
    function getHolidayPayByBuAndJob($payload)
    {
        $business_unit_id = $payload->business_unit_id;
        $job_id = $payload->job_id;

        $client_id = DB::table('business_units')->find($business_unit_id)->client_id;
        $clientJob = DB::table('client_jobs')->where('client_id', $client_id)->where('job_id', $job_id)
                    ->select('holiday_pay_percentage', 'holiday_pay_frequency', 'holiday_pay_amount')
                    ->first();
        if($clientJob) {
            return $clientJob;
        }
        return [];
    }
}
/*
 * Date format
*/
if (!function_exists('dateFormat')) {
    function dateFormat()
    {
        return "%d-%m-%Y";
    }
}
/*
 * Time format
*/
if (!function_exists('timeFormat')) {
    function timeFormat()
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            $time_format = "%H:%i";
        } else {
            $time_format = "%h:%i %p";
        }
        
        return $time_format;
    }
}
/*
 * Time format
*/
if (!function_exists('dateTimeFormat')) {
    function dateTimeFormat()
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            $time_format = "%Y-%m-%d %H:%i";
        } else {
            $time_format = "%Y-%m-%d %h:%i %p";
        }
        
        return $time_format;
    }
}
    

/*
 * Calculate total hours with datetime format
*/
if (!function_exists('calcTotalHours')) {
    function calcTotalHours($input)
    {
        if (isset($input['start_time']) && isset($input['end_time']) ) {
            $startTime = $input['start_time'];
            $endTime = $input['end_time'];
            $break = isset($input['break']) ? $input['break'] : 0 ;
            //calculating difference
            $time_diff = round(abs($endTime - $startTime) / 60,2);

            if(!empty($break) && (number_format($time_diff) == number_format($break) )){
                return 0;
            } else if(!empty($break) && ($time_diff > $break)){
                $time_diff_minutes = $time_diff - $break;
                $hours = floor($time_diff_minutes / 60);
                $hours = (int)$hours;
                $minutes = ($time_diff_minutes % 60);
                $minutes = ($minutes/60);
                $minutes = round($minutes, 2);
                return $hoursMinutes = $hours + $minutes;
            } else {
                $hours = floor($time_diff / 60);
                $hours = (int)$hours;
                $minutes = ($time_diff % 60);
                $minutes = ($minutes/60);
                $minutes = round($minutes, 2);
                return $hoursMinutes = $hours + $minutes;
            }//end if
        }//end if
        return 0;
    }
}



if (!function_exists('getContinousTimeslots')) {
    function getContinousTimeslots($finalRateCards) {
        if (count($finalRateCards)>1) {
            usort($finalRateCards, function($a, $b) {
                if ($a&&$b)
                return $a->start_time <=> $b->start_time;
            });
        }
        $validateArr = [];
        $validateArr = array_values($finalRateCards);
        for ( $i = 0; $i < count($validateArr)-1; $i = $i + 1) {
            if ( $validateArr[$i+1] && $validateArr[$i] ){
                if ( $validateArr[$i+1]->start_time == $validateArr[$i]->start_time && $validateArr[$i]->end_time == $validateArr[$i+1]->end_time ){
                    if($validateArr[$i]->day_id==8) {
                        // unset($validateArr[$i+1]);
                        // $validateArr = array_values($validateArr);
                        $validateArr[$i+1] = [];
                        continue;
                    } else if($validateArr[$i+1]->day_id==8) {
                        // unset($validateArr[$i]);
                        // $validateArr = array_values($validateArr);
                        $validateArr[$i] = [];
                        continue;
                    }
                }
                if ( $validateArr[$i+1] && $validateArr[$i] ){
                    if($validateArr[$i+1]->start_time == $validateArr[$i+1]->end_time) {
                        // unset($validateArr[$i+1]);
                                        // $validateArr = array_values($validateArr);
                        $validateArr[$i+1] = [];
                        continue;
                    } else if($validateArr[$i]->start_time == $validateArr[$i]->end_time) {
                        // unset($validateArr[$i]);
                                        // $validateArr = array_values($validateArr);
                        $validateArr[$i] = [];
                        continue;
                    }
                }
                
                if ( $validateArr[$i+1] && $validateArr[$i] ){
                    if ( $validateArr[$i+1]->start_time < $validateArr[$i]->end_time && $validateArr[$i]->start_time < $validateArr[$i+1]->end_time ){
                        if ( $validateArr[$i]->day_id==8) {
                            if ($validateArr[$i]->end_time > $validateArr[$i+1]->start_time) {
                                $validateArr[$i+1]->start_time = $validateArr[$i]->end_time;
                                $temp = $validateArr[$i+1];
                                $validateArr[$i+1] = [];
                                $validateArr[$i+1] =$temp;

                            } 
                            // unset($finalRateCards[$i+1]);
                            continue;
                        } else if ( $validateArr[$i+1]->day_id==8) {
                            if ($validateArr[$i]->end_time > $validateArr[$i+1]->start_time) {
                                $validateArr[$i]->end_time = $validateArr[$i+1]->start_time;
                                $temp = $validateArr[$i];
                                $validateArr[$i] = [];
                                $validateArr[$i] = $temp;
                                continue;
                            }
                        } else if ($validateArr[$i]->end_time > $validateArr[$i+1]->start_time) {
                            $validateArr[$i+1]->start_time = $validateArr[$i]->end_time;
                            $temp = $validateArr[$i+1];
                            $validateArr[$i+1] = [];
                            $validateArr[$i+1] = $temp;
                            continue;
                        }
                        getContinousTimeslots($validateArr);
                    }
                }
            }
        }
        return $validateArr;
    }
}
/*
 * Calculate total hours with datetime format
*/
if (!function_exists('splitTimeRangeIntoSlots')) {
    function splitTimeRangeIntoSlots($request) {
        //checks for mandatory values
        if (isset($request->business_unit_id) && isset($request->job_id) && isset($request->shift_id) && isset($request->start_time) && isset($request->end_time) && isset($request->on_date) ) {

            $start_time = date('H:i:s', strtotime($request->start_time));
            $end_time = date('H:i:s', strtotime($request->end_time));

            $f_start = date("H.i", strtotime($request->start_time));
            $f_end = date("H.i", strtotime($request->end_time));

            $input_start = date("H:i", strtotime($request->start_time));
            $input_end = date("H:i", strtotime($request->end_time));

            $dayID = Day::where('day', date("l", strtotime($request->on_date)))->first()->id;

            if ($start_time >= $end_time) {
                $startDateTime = strtotime($request->on_date.' '.$start_time);
                $endDateTime = strtotime(date('Y-m-d', strtotime('+1 day', strtotime($request->on_date))).' '.$end_time);
                $startDate= date('Y-m-d', strtotime($request->on_date));
                $endDate = date('Y-m-d', strtotime('+1 day', strtotime($request->on_date)));
            } else {
                $startDateTime = strtotime($request->on_date.' '.$start_time);
                $endDateTime = strtotime($request->on_date.' '.$end_time);
                $startDate = date('Y-m-d', strtotime($request->on_date));
                $endDate = date('Y-m-d', strtotime($request->on_date));
            }
            
            $dateArray = [];
            //check the start time is in previous day
            $shiftCheck = Shift::find($request->shift_id);
            $shiftStartTime = $shiftCheck ? strtotime($request->on_date.' '.$shiftCheck->start_time): '';
            if ($shiftStartTime != '' && $shiftStartTime > $startDateTime) {
                $previousDate = date('Y-m-d', strtotime('-1 day', strtotime($startDate)));
                array_push($dateArray, $previousDate);
            } else {
                $previousDate = date('Y-m-d', strtotime('-1 day', strtotime($startDate)));
                array_push($dateArray, $previousDate);
            }
            if($startDate == $endDate) {
                array_push($dateArray, $startDate);
            } else {
                array_push($dateArray, $startDate);
                array_push($dateArray, $endDate);
            }
            //Holiday check for split rate
            //#check for custom_holiday or holiday
            $custom_holidayCheck = DB::table('custom_holidays')->where('business_unit_id',$request->business_unit_id)->whereIn('holiday_date', [$startDate, $endDate])->where('is_deleted',0)->exists();
            $holidayCheck = DB::table('holidays')->whereIn('on_date', [$startDate, $endDate])->where('is_deleted',0)->exists();
            $hasHolidays = $custom_holidayCheck || $holidayCheck ? 1 : 0;

            //initialising ratecard array
            $rateCardArrays = [];
            //check the business unit has split rate or not
            $businessUnitData = DB::table('business_units')->find($request->business_unit_id);
            // Lookup table for split rate calculation
            $lookup = ['000' => 0,'100' => 1,'010' => 0,'110' => 1,'001' => 0,'101' => 1,'011' => 1,'111' => 1,];
            // Concatenate the binary values of the three variables
            $key = $businessUnitData->has_split_rate . $businessUnitData->holiday_split_rate . $hasHolidays;
            // Use the lookup table to get the corresponding split rate value
            $splitRate = isset($lookup[$key]) ? $lookup[$key] : 0;
            //if doesn't have split rate, then normal calculation
            if ( $splitRate == 0 ) {
                //fetching rates
                $inputArr = array(
                    'business_unit_id'  => $request->business_unit_id,
                    'job_id'            => $request->job_id,
                    'on_date'           => $request->on_date,
                    'shift_id'          => $request->shift_id,
                );
                $rates = Helper::getClientStaffRates($inputArr);
                //client rate and staff rate assigning
                if( $rates ) {
                    $rateID = $rates->id;
                    $clientRate = !empty($rates->client_rate) ? $rates->client_rate : 0;
                    $staffRates  = $rates->staffratecard;
                    $rate_type  = $rates->rate_type;
                } else {
                    $rateID = 0;
                    $clientRate = 0;
                    $staffRates  = EmploymentType::where('status','active')->where('is_deleted',0)->get();
                    $rate_type = "hourly_rate";
                }
                $inputs = array(
                    'start_time' => $startDateTime,
                    'end_time'   => $endDateTime,
                    'break'      => $request->break ?? 0,
                );
                $rateCardArrays[] = array(
                    'id' => $rateID,
                    'day_id' => $dayID,
                    'shift_id' => $request->shift_id,
                    'rate_type' => $rate_type,
                    'start_time' => date("Y-m-d H:i:s", $startDateTime),
                    'end_time' => date("Y-m-d H:i:s", $endDateTime),
                    'break'      => $request->break ?? 0,
                    'total_hours' => calcTotalHours($inputs),
                    'client_rate' => $clientRate,
                    'staff_rates' => $staffRates,
                );
                return $rateCardArrays;

            } else {
                //else has split rate, then split rate calculation
                // dd($dateArray);
                foreach ($dateArray as $date) {
                    $day_id = [];
                    //#check for custom_holiday
                    $custom_holiday = DB::table('custom_holidays')->where('business_unit_id',$request->business_unit_id)->where('holiday_date', $date)->where('is_deleted',0)->first();
                    //#check for holiday
                    $holiday = DB::table('holidays')->where('on_date', $date)->where('is_deleted',0)->first();
                    // if ($holiday || $custom_holiday)  { $day_id[] = 8;}
                    if ($custom_holiday)  { $day_id[] = 8;}
                    $day_id[] = Day::where('day', date("l", strtotime($date)))->first()->id;
                    //check Custom Holiday Has Rates
                    if ($custom_holiday) {
                        $checkCustomHolidayHasRates = DB::table('rate_cards')
                            ->where('job_id', $request->job_id)
                            ->whereIn('day_id', $day_id)
                            ->where('business_unit_id', $request->business_unit_id)
                            ->where('custom_holiday_id', $custom_holiday->id)->first();
                        if(empty($checkCustomHolidayHasRates)) {
                            $custom_holiday = [];
                        }
                    }
                    $customHolidayrateCards = [];
                    if ($custom_holiday){
                        $day_id = [];
                        $day_id[] = Day::where('day', date("l", strtotime($date)))->first()->id;
                        $customHolidayrateCards = RateCard::with('staffratecard')->where('business_unit_id', $request->business_unit_id)
                                ->where('job_id', $request->job_id)
                                ->where('day_id', 8)
                                ->where('custom_holiday_id', '!=', 0)
                                ->where('custom_holiday_id', $custom_holiday->id)
                                ->where('status', 'active')
                                ->where('is_deleted', 0)
                                ->orderBy('start_time', 'asc')
                                ->orderBy('day_id', 'desc')
                                ->get();
                    }
                    //fetch rate cards
                    $normalrateCards = RateCard::with('staffratecard')->where('business_unit_id', $request->business_unit_id)
                                ->where('job_id', $request->job_id)
                                ->whereIn('day_id', $day_id)
                                ->where('custom_holiday_id', 0)
                                ->where('status', 'active')
                                ->where('is_deleted', 0)
                                ->orderBy('start_time', 'asc')
                                ->orderBy('day_id', 'desc')
                                ->get();
                    $rateCards = $customHolidayrateCards ? $normalrateCards->merge($customHolidayrateCards) : $normalrateCards;
                    foreach ($rateCards as $rateCard) {
                        $start = date("H:i", strtotime($rateCard->start_time));
                        $end = date("H:i", strtotime($rateCard->end_time));
                        
                        if ($start >= $end) {
                            $startDate = $date;
                            $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                        }
                        else {
                            $startDate = $date;
                            $endDate = $date;
                        }
                        if ( strtotime($startDate.' '.$rateCard->start_time) < $endDateTime && $startDateTime < strtotime($endDate.' '.$rateCard->end_time) ) {
                            
                            $rateCardArrays[] = array(
                                'id' => $rateCard->id,
                                'day_id' => $rateCard->day_id,
                                'shift_id' => $rateCard->shift_id,
                                'rate_type' => $rateCard->rate_type,
                                // 'formated_start_time' => $startDate.' '.$rateCard->start_time,
                                // 'formated_end_time' => $endDate.' '.$rateCard->end_time,
                                'start_time' => strtotime($startDate.' '.$rateCard->start_time),
                                'end_time' => strtotime($endDate.' '.$rateCard->end_time),
                                'client_rate' => $rateCard->client_rate,
                                'staff_rates' => $rateCard->staffratecard,
                            );
                        }
                    }
                }
                // dd($rateCardArrays);
                // if ($rateCardArrays) {
                //     usort($rateCardArrays, function($a, $b) {
                //         return [$a['start_time'], $a['end_time']] <=> [$b['start_time'], $b['end_time']];
                //     });
                // }
                // dd($rateCardArrays);
                //Check the ratecard array is empty
                if (count($rateCardArrays)>0) {
                    usort($rateCardArrays, function($a, $b) {
                        return [$a['start_time'], $a['end_time']] <=> [$b['start_time'], $b['end_time']];
                    });
                    $eliminated = [];
                    //removing rates after comparing slots
                    foreach ($rateCardArrays as $firstKey => $firstValue) {
                        foreach ($rateCardArrays as $secondKey => $secondValue) {
                            if ($firstKey != $secondKey) {
                                if ( $secondValue['start_time'] < $firstValue['end_time'] && $firstValue['start_time'] < $secondValue['end_time'] ) {
                                    // $firstStart = Carbon::parse($firstValue['formated_start_time']);
                                    // $firstEnd = Carbon::parse( $firstValue['formated_end_time']);
                                    $startCheck = $firstValue['start_time'] <= $secondValue['start_time'] && $secondValue['start_time'] <= $firstValue['end_time'] ? true : false;
                                    $endCheck = $firstValue['start_time'] <= $secondValue['end_time'] && $secondValue['end_time'] <= $firstValue['end_time'] ? true : false;
                                    // dd($endCheck);
                                    if ( $startCheck && $endCheck ) {
                                            if ($firstValue['day_id']==8) {
                                                $startHolidayCheck = $firstValue['start_time'] <= $secondValue['start_time'] ? true : false;
                                                $endHolidayCheck = $secondValue['end_time'] <= $firstValue['end_time'] ? true : false;
                                                if ( $startHolidayCheck && $endHolidayCheck ) {
                                                    $key = array_search($secondValue['id'], array_column($eliminated, 'id'));
                                                    if ($key == false){
                                                        $eliminated[$firstKey.'-'.$secondKey] = $secondValue;
                                                    }
                                                }
                                            } else if ($secondValue['day_id']==8) {
                                                $startHolidayCheck = $secondValue['start_time'] <= $firstValue['start_time'] ? true : false;
                                                $endHolidayCheck = $firstValue['end_time'] <= $secondValue['end_time'] ? true : false;
                                                if ( $startHolidayCheck && $endHolidayCheck ) {
                                                    $key = array_search($firstValue['id'], array_column($eliminated, 'id'));
                                                    if ($key == false){
                                                        $eliminated[$firstKey.'-'.$secondKey] = $firstValue;
                                                    }
                                                }
                                            } 
                                    // else {
                                    //     $key = array_search($firstValue['id'], array_column($eliminated, 'id'));
                                    //     if ($key == false){
                                    //         $eliminated[] = $firstValue;
                                    //     }
                                    // }
                                    }
                                }
                            }
                        }
                    }
                    // dd($eliminated);
                    // Compare all values by a json_encode
                    $diff = array_diff(array_map('json_encode', $rateCardArrays), array_map('json_encode', $eliminated));
                    // Json decode the result
                    $finalRateCards = array_map('json_decode', $diff);
                    // dd($finalRateCards);
                    $firstIteratedRateCards = [];
                    if (count($finalRateCards)>0) {
                        $firstIteratedRateCards = getContinousTimeslots($finalRateCards);
                    }
                    $secondIteratedRateCards = [];
                    if (count($firstIteratedRateCards)>0) {
                        $secondIteratedRateCards = getContinousTimeslots($firstIteratedRateCards);
                    }
                    // dd($secondIteratedRateCards);
                    $filteredRateCards = [];
                    $maxArr = [];
                    if (count($secondIteratedRateCards)>0) {
                        foreach ($secondIteratedRateCards as $rateCard) {
                            if ($rateCard) {
                                if ( $rateCard->start_time < $endDateTime && $startDateTime < $rateCard->end_time ) {
                                    if($startDateTime <= $rateCard->start_time) {
                                        $finalST = $rateCard->start_time;
                                    } else {
                                        $finalST = $startDateTime;
                                    }
                                    if($endDateTime >= $rateCard->end_time) {
                                        $finalET = $rateCard->end_time;
                                    } else {
                                        $finalET = $endDateTime;
                                    }
                                    $inputs = array(
                                        'start_time' => $finalST,
                                        'end_time' => $finalET,
                                        'break'      => 0,
                                    );
                                    // if ($finalRateCard['day_id'] != 8) {
                                        // $maxArr[$rateCard->id] = calcTotalHours($inputs);
                                    // }
                                    $filteredRateCards[] = array(
                                        'id' => $rateCard->id,
                                        'day_id' => $rateCard->day_id,
                                        'shift_id' => $rateCard->shift_id,
                                        'rate_type' => $rateCard->rate_type,
                                        // 'formated_start_time' => date("Y-m-d H:i:s", $finalST),
                                        // 'formated_end_time' => date("Y-m-d H:i:s", $finalET),
                                        'start_time' => $finalST,
                                        'end_time' => $finalET,
                                        'total_hours' => calcTotalHours($inputs),
                                        'client_rate' => $rateCard->client_rate,
                                        'staff_rates' => $rateCard->staff_rates,
                                    );
                                }
                            }
                        }
                    }
                    // dd($filteredRateCards);
                    // $max = array_keys($maxArr, max($maxArr));
                    // dd($max[0]);
                    $max = '';
                    $index = '';
                    if (count($filteredRateCards)>0) {
                        $max = max(array_column($filteredRateCards, 'total_hours'));
                        $index = array_search($max, array_column($filteredRateCards, 'total_hours'));
                    }
                    
                    $lastRateCards = [];
                    foreach ($filteredRateCards as $key => $filteredRateCard) {
                        if ($filteredRateCard) {
                            if($filteredRateCard['start_time'] != $filteredRateCard['end_time']) {
                                $break = 0;
                                if (isset($request->break)) {
                                    $break = ($index == $key) ? $request->break : 0; 
                                }
                                $inputs = array(
                                    'start_time' => $filteredRateCard['start_time'],
                                    'end_time'   => $filteredRateCard['end_time'],
                                    'break'      => $break,
                                );
                                $lastRateCards[] = array(
                                    'id' => $filteredRateCard['id'],
                                    'day_id' => $filteredRateCard['day_id'],
                                    'shift_id' => $filteredRateCard['shift_id'],
                                    'rate_type' => $filteredRateCard['rate_type'],
                                    // 'formated_start_time' => date("Y-m-d H:i:s", $filteredRateCard['start_time']),
                                    // 'formated_end_time' => date("Y-m-d H:i:s", $filteredRateCard['end_time']),
                                    'start_time' => $filteredRateCard['start_time'],
                                    'end_time' => $filteredRateCard['end_time'],
                                    'break'      => $break,
                                    'total_hours' => calcTotalHours($inputs),
                                    'client_rate' => $filteredRateCard['client_rate'],
                                    'staff_rates' => $filteredRateCard['staff_rates'],
                                );
                            }
                        }
                    }
                    // dd($lastRateCards);
                    $endRateCards = [];
                    if(count($lastRateCards)==1) {
                            $endRateCards[] = $lastRateCards[0];
                    } else if(count($lastRateCards)>1) {
                        for ( $i = 0; $i < count($lastRateCards)-1; $i = $i + 1) {
                            if ( $lastRateCards[$i] && $lastRateCards[$i+1] ){
                                if($lastRateCards[$i+1]['start_time'] == $lastRateCards[$i]['end_time']) {
                                    $firstKey = array_search($lastRateCards[$i]['id'], array_column($endRateCards, 'id'));
                                    if ($firstKey == false){
                                        $endRateCards[] = $lastRateCards[$i];
                                    }
                                    $secondKey = array_search($lastRateCards[$i+1]['id'], array_column($endRateCards, 'id'));
                                    if ($secondKey == false){
                                        $endRateCards[] = $lastRateCards[$i+1];
                                    }
                                } else if ($lastRateCards[$i+1]['start_time'] > $lastRateCards[$i]['end_time']) {
                                    $inputs = array(
                                        'start_time' => $lastRateCards[$i]['end_time'],
                                        'end_time'   => $lastRateCards[$i+1]['start_time'],
                                        'break'      => 0,
                                    );
                                    $endRateCards[] = array(
                                        'id' => 0,
                                        'day_id' => $dayID,
                                        'shift_id' => $request->shift_id,
                                        'rate_type' => 'hourly_rate',
                                        // 'formated_end_start_time' => date("Y-m-d H:i:s", $lastRateCards[$i]['end_time']),
                                        // 'formated_end_end_time' => date("Y-m-d H:i:s", $lastRateCards[$i+1]['start_time']),
                                        'start_time' => $lastRateCards[$i]['end_time'],
                                        'end_time' => $lastRateCards[$i+1]['start_time'],
                                        'break'      => 0,
                                        'total_hours' => calcTotalHours($inputs),
                                        'client_rate' => 0,
                                        'staff_rates' => EmploymentType::where('status','active')->where('is_deleted',0)->get(),
                                    );
                                    $firstKey = array_search($lastRateCards[$i]['id'], array_column($endRateCards, 'id'));
                                    if ($firstKey == false){
                                        $endRateCards[] = $lastRateCards[$i];
                                    }
                                    $secondKey = array_search($lastRateCards[$i+1]['id'], array_column($endRateCards, 'id'));
                                    if ($secondKey == false){
                                        $endRateCards[] = $lastRateCards[$i+1];
                                    }
                                } 
                                
                            }
                        }
                    }
                    // dd($endRateCards);
                    uasort($endRateCards, function($a, $b) {
                        return strcmp($a['start_time'], $b['start_time']);
                    });
                    $sequentialRateCards = [];
                    foreach ($endRateCards as $rateCard) {
                        if ($rateCard) {
                            if ( $rateCard['start_time'] < $endDateTime && $startDateTime < $rateCard['end_time'] ) {
                                
                                if ( $rateCard === reset( $endRateCards ) ) { 
                                    if ($startDateTime != $rateCard['start_time'] && $startDateTime < $rateCard['start_time']) {
                                        $inputs = array(
                                            'start_time' => $startDateTime,
                                            'end_time'   => $rateCard['start_time'],
                                            'break'      => 0,
                                        );
                                        $sequentialRateCards[] = array(
                                            'id' => 0,
                                            'day_id' => $dayID,
                                            'shift_id' => $request->shift_id,
                                            'rate_type' => 'hourly_rate',
                                            'start_time' => date("Y-m-d H:i:s", $startDateTime),
                                            'end_time' => date("Y-m-d H:i:s", $rateCard['start_time']),
                                            'break'      => 0,
                                            'total_hours' => calcTotalHours($inputs),
                                            'client_rate' => 0,
                                            'staff_rates' => $rateCard['staff_rates'],
                                        );
                                    }
                                }
                                $sequentialRateCards[] = array(
                                    'id' => $rateCard['id'],
                                    'day_id' => $rateCard['day_id'],
                                    'shift_id' => $rateCard['shift_id'],
                                    'rate_type' => $rateCard['rate_type'],
                                    'start_time' => date("Y-m-d H:i:s", $rateCard['start_time']),
                                    'end_time' => date("Y-m-d H:i:s", $rateCard['end_time']),
                                    'break'      => $rateCard['break'],
                                    'total_hours' => $rateCard['total_hours'],
                                    'client_rate' => $rateCard['client_rate'],
                                    'staff_rates' => $rateCard['staff_rates'],
                                );
                                if( $rateCard === end( $endRateCards ) ) { 
                                    if ($endDateTime != $rateCard['end_time'] && $endDateTime > $rateCard['end_time']) {
                                        $inputs = array(
                                            'start_time' => $rateCard['end_time'],
                                            'end_time'   => $endDateTime,
                                            'break'      => 0,
                                        );
                                        $sequentialRateCards[] = array(
                                            'id' => 0,
                                            'day_id' => $dayID,
                                            'shift_id' => $request->shift_id,
                                            'rate_type' => 'hourly_rate',
                                            'start_time' => date("Y-m-d H:i:s", $rateCard['end_time']),
                                            'end_time' => date("Y-m-d H:i:s", $endDateTime),
                                            'break'      => 0,
                                            'total_hours' => calcTotalHours($inputs),
                                            'client_rate' => 0,
                                            'staff_rates' => $rateCard['staff_rates'],
                                        );
                                        
                                    }
                                }
                            }
                        }
                    }
                    // dd($sequentialRateCards);
                    uasort($sequentialRateCards, function($a, $b) {
                        return strcmp($a['start_time'], $b['start_time']);
                    });
                    // uasort($endRateCards, function($a, $b) {
                    //     return strcmp($a['end_time'], $b['end_time']);
                    // });
                    if (count($sequentialRateCards) == 0) {
                        $rateCardArrays = [];
                        $inputs = array(
                            'start_time' => $startDateTime,
                            'end_time'   => $endDateTime,
                            'break'      => $request->break ?? 0,
                        );
                        $rateCardArrays[] = array(
                            'id' => 0,
                            'day_id' => $dayID,
                            'shift_id' => $request->shift_id,
                            'rate_type' => 'hourly_rate',
                            'start_time' => date("Y-m-d H:i:s", $startDateTime),
                            'end_time' => date("Y-m-d H:i:s", $endDateTime),
                            'break'      => $request->break ?? 0,
                            'total_hours' => calcTotalHours($inputs),
                            'client_rate' => 0,
                            'staff_rates' => EmploymentType::where('status','active')->where('is_deleted',0)->get(),
                        );
                        return $rateCardArrays;

                    } else {
                        return $sequentialRateCards;
                    }
                } else {
                    $inputs = array(
                        'start_time' => $startDateTime,
                        'end_time'   => $endDateTime,
                        'break'      => $request->break ?? 0,
                    );
                    $rateCardArrays[] = array(
                        'id' => 0,
                        'day_id' => $dayID,
                        'shift_id' => $request->shift_id,
                        'rate_type' => 'hourly_rate',
                        'start_time' => date("Y-m-d H:i:s", $startDateTime),
                        'end_time' => date("Y-m-d H:i:s", $endDateTime),
                        'break'      => $request->break ?? 0,
                        'total_hours' => calcTotalHours($inputs),
                        'client_rate' => 0,
                        'staff_rates' => EmploymentType::where('status','active')->where('is_deleted',0)->get(),
                    );
                    return $rateCardArrays;
                }
            }
        }
        return [];
    }
}
