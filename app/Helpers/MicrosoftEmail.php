<?php 

namespace App\Helpers;
use Config;
use DB;
use Auth;
use App\Tenants\Integration;
use App\Tenants\UserIntegration;
use Symfony\Component\Debug\Exception\FatalThrowableError;

/**
 * Microsoft Email library to full fill Logezy needs
 */

class MicrosoftEmail{

  const VERSION = '0.0.1';
  
  protected $apiVersion;

  protected $graphAPI;

  protected $userId;

  public $integration;

  public function __construct($graphAPI, $userId=null)
	{
    $this->apiVersion = "/v1.0";
		$this->integration = $this->getIntegration("microsoft-365", $userId);

    $this->graphAPI = $graphAPI;
    $this->userId =$userId;
	}

  public function requestMicrosoft($body = '', $url = '', $method = 'POST'){
    try {
      $accessToken = $this->integration->access_token;
      $header = array("Authorization: Bearer $accessToken");
      if ($method == "POST" || $method == "PUT") {
        array_push($header, "Content-Type: application/json");
      }
      $ch = curl_init($this->graphAPI.$url);
      if ($body) {
        $body = json_encode($body);
      }
      curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

      $response = curl_exec($ch);
      if ($method == "POST" || $method == "PUT") {
        $response = json_decode($response);
      }

      curl_close($ch);
      return $response;
    } catch (\Exception $ex) {
      dd($ex);
      return $ex->getMessage();
    }
  }

  public function requestNewAccessToken(){
    try {
      $clientId = env('MIX_MICROSOFT_CLIENT_ID');
      $clientSecret = env('MIX_MICROSOFT_CLIENT_SECRET');
      $redirectURL = env('MIX_MICROSOFT_REDIRECT_URI');
      $authAPI = env('MIX_MICROSOFT_AUTH_URL');
      $scope = urldecode(env("MIX_MICROSOFT_SCOPE"));
      $url = $authAPI."/common/oauth2/v2.0/token";
      $body = ['refresh_token' => $this->integration->refresh_token, 'client_id' => $clientId, 'client_secret' => $clientSecret, 'redirect_uri' => $redirectURL, 'grant_type' => 'refresh_token', 'scope' => $scope];
      
      $apiResponce = json_decode(Integration::requestAPI($body, $url, 'POST'));
      if (!isset($apiResponce->error)) {
        $userIntegration = UserIntegration::find($this->integration->id);
        $userIntegration->update(['refresh_token' => $apiResponce->refresh_token, 'access_token' => $apiResponce->access_token]);
        $this->integration = $userIntegration;
        return true;
      }
      return false;
    } catch (\Exception $ex) {
      dd($ex);
      return $ex->getMessage();
    }
  }


  public function getIntegration($slug, $userId) {
    $userIngegration = null;
    $integration = Integration::where('slug', $slug)->where('is_active', 1)->first();
    if ($integration) {
      if (!$userId) {
        $userId = null;
      }
      if($userId) {
        // Get logged in user microsoft 365 configuration
        $userIngegration = UserIntegration::where('integration_id', $integration->id)->where('user_id', $userId)->first();
        if (!$userIngegration) {
          // Emails triggered from client, business unit or candidate portal. They dont have microsoft 365 configuration so taking configuration from agency
          $userIngegration = UserIntegration::where('integration_id', $integration->id)->first();
        }
      } else {
        // Emails triggered from cron job or laravel job. They dont have microsoft 365 configuration so taking configuration from agency
        $userIngegration = UserIntegration::where('integration_id', $integration->id)->first();
      }
      if ($userIngegration) {
        $this->userId = $userIngegration->user_id;
        return $userIngegration;
      }
      return false;
    }
    return false;
  }

  /**
   * [Get logged using microsft graph API]
   * @param  $status    [Status of the customer to retrive, All, Active, Inactive, Gapps, Crm, NonSubscribers, PortalEnabled, PortalDisabled]
   * @return [array]    [Customers data from zoho]
   */
  public function getUserDetails() {
    try {
      $requestURL = $this->apiVersion."/me";
      $response = $this->requestMicrosoft("", $requestURL, "GET");
      if (isset($response->error) && $response->error->code === "InvalidAuthenticationToken") {
        if($this->requestNewAccessToken()) {
          return $this->requestMicrosoft("", $requestURL, "GET");
        }
      }
      return $response;
    } catch (\Exception $ex) {
      return $ex->getMessage();
    }
  }

  /**
   * [Send email using microsft graph API]
   * @return [array]    [Customers data from zoho]
   */
  public function sendEmail($to_emails, $subject, $mail_data, $template, $cc_emails=null, $bcc_emails=null, $attachments=null) {
    try {
      $toEmail = [];
      $bccEmail = [];
      $ccEmail = [];
      $mailAttachments = [];
      if (!$to_emails) {
        throw new \Exception('To email address is required');
      }
      if (!$subject) {
        throw new \Exception('Subject is required');
      }
      if (!$mail_data) {
        throw new \Exception('Body data is required');
      }
      if (!is_array($to_emails)) {
        throw new \Exception('To emails address should be array');
      }
      if ($bcc_emails) {
        if (!is_array($bcc_emails)) {
          throw new \Exception('Bcc emails should be array');
        }
        foreach ($bcc_emails as $key => $bcc_email) {
          if ($bcc_email) array_push($bccEmail, array("emailAddress" => array("address" => $bcc_email)));
        }
      }

      if ($cc_emails) {
        if (!is_array($cc_emails)) {
          throw new \Exception('CC emails should be array');
        }
        foreach ($cc_emails as $key => $cc_email) {
          if ($cc_email) array_push($ccEmail, array("emailAddress" => array("address" => $cc_email)));
        }
      }

      if ($attachments) {
        if (!is_array($attachments)) {
          throw new \Exception('Attachments should be array');
        }
        foreach ($attachments as $key => $attachment) {
          $file = \File::get($attachment['filePath']);
          $contentType = \File::mimeType($attachment['filePath']);
          $tmp = array(
            "@odata.type" => "#microsoft.graph.fileAttachment",
            "name" => $attachment['filename'],
            "contentType" => $contentType,
            "contentBytes" => base64_encode($file)
          );
          array_push($mailAttachments, $tmp);
        }
      }
      $body = $this->renderMailBodyContent($template, $mail_data);

      $payload = [
        "message" => array(
          "subject" => $subject,
          "body" => array(
            "contentType" => 'HTML',
            "content"     => $body
          )
        )
      ];
      foreach ($to_emails as $key => $to_email) {
        array_push($toEmail, array("emailAddress" => array("address" => $to_email)));
      }
      $payload["message"]["toRecipients"] = $toEmail;
      if(count($ccEmail) > 0) $payload["message"]["ccRecipients"] = $ccEmail;
      if(count($bccEmail) > 0) $payload["message"]["bccRecipients"] = $bccEmail;
      if(count($mailAttachments) > 0) $payload["message"]["attachments"] = $mailAttachments;

      $requestURL = $this->apiVersion."/me/sendMail";
      $response = $this->requestMicrosoft($payload, $requestURL, "POST");
      if (isset($response->error) && $response->error->code === "InvalidAuthenticationToken") {
        if($this->requestNewAccessToken()) {
          $response = $this->requestMicrosoft($payload, $requestURL, "POST");
        }
      }
      return $response;
    } catch (\Exception $ex) {
      dd($ex);
      return $ex;
    } 
  }

  public function renderMailBodyContent($template, $mail_data) {
    $body = "";
    switch ($template) {

      case 'candidate-reply-from-sms-app':
        $body = view('mail.v1.candidate_reply_from_sms_app', compact('mail_data'))->render();
        break;

      case 'mobile-app-change-password':
        $body = view('mail.v1.mobile_app_change_password', compact('mail_data'))->render();
        break;

      case 'notify-agency-candidate-registration':
        $body = view('mail.v1.notify_agency_candidate_registration', compact('mail_data'))->render();
        break;

      case 'otp-sending':
        $body = view('mail.v1.otp_sending', compact('mail_data'))->render();
        break;

      case 'recruitment-candidate-signup':
        $body = view('mail.v1.recruitment_candidate_signup', compact('mail_data'))->render();
        break;

      default:
        break;
    }
    return $body;
  }
}