<?php
namespace App\Traits;

use App\Tenants\Agency;
use App\Tenants\Candidate;
use App\Tenants\AgencySetting;
use App\Tenants\PushNotificationTemplate;
use GuzzleHttp;
use DB;
use Illuminate\Http\Request;

trait PushNotificationTrait{


    /*
    *  Vacancy Publish text notification
    */
    public static function vacancyPublish($request){

        $vacancyCreationTemplate = PushNotificationTemplate::where('template_name','Vacancy creation')->pluck('template')->first();

        // $candidateUin = [];
        // dd($request->candidateUin);
        // if($request->candidates) {
        //     foreach($request->candidates as $candidate){
        //         $candidate = Candidate::find($candidate);
        //         if (!empty($candidate->firebase_uin)) {
        //             $candidateUin[] = $candidate->firebase_uin;
        //         }
        //     }
        // }
        
        /* Push message to Candidates */
        if ($request->candidateUins) {
            $oneSignalAppId = DB::table('mobile_app_settings')->where('key', 'oneSignalAppId')->whereNotNull('value')->pluck('value')->first();
            if(empty($oneSignalAppId)) {
                PushNotificationTrait::pushMessage($request->candidateUins, $vacancyCreationTemplate, 'vacancy_added');
            } else {
                PushNotificationTrait::dynamicOneSignalPushMessage($request->candidateUins, $vacancyCreationTemplate, 'vacancy_added');
            }
        }
    }


    /*
    *  Assign Vacancy multiple publish
    */
    public static function assignPublishonMultipleDates($request){

        // $vacancyCreationTemplate = PushNotificationTemplate::where('template_name','Vacancy creation')->pluck('template')->first();
        $AssignMultipleDateTemplate = "You have been assigned on ".implode(",", $request['assignDates']).". check the App for more information.";
        
        /* Push message to Candidates */
        if ($request['candidateUin']) {
            $oneSignalAppId = DB::table('mobile_app_settings')->where('key', 'oneSignalAppId')->whereNotNull('value')->pluck('value')->first();
            if(empty($oneSignalAppId)) {
                PushNotificationTrait::pushMessage($request['candidateUin'], $AssignMultipleDateTemplate, 'vacancy_assigned');
            } else {
                PushNotificationTrait::dynamicOneSignalPushMessage($request['candidateUin'], $AssignMultipleDateTemplate, 'vacancy_assigned');
            }
        }
    }
    /*
    *  Shift Assign Publish Push notification
    */
    public static function shiftAssign($request, $candidate){

        $assignVacancyTemplate = PushNotificationTemplate::where('template_name','Assign Vacancy')->pluck('template')->first();

        $candidateUin = [];
        $oneSignalAppId = DB::table('mobile_app_settings')->where('key', 'oneSignalAppId')->whereNotNull('value')->pluck('value')->first();

        if($request->candidate_id) {
            $candidate = Candidate::find($request->candidate_id);
            if(empty($oneSignalAppId)) {
                if (!empty($candidate->firebase_uin)) {
                    $candidateUin[] = $candidate->firebase_uin;
                }
            } else {
                $candidate_player_id = DB::table('role_users')->where('user_id', $candidate->user_id)
                                ->where('role_id', 6)
                                ->whereNotNull('player_id')->where('player_id', '!=' , '')->first();
                if ($candidate_player_id) {
                    $candidateUin[] = $candidate_player_id->player_id;
                }
            }
        }
        
        /* Push message to Candidates */
        if ($candidateUin) {
            if(empty($oneSignalAppId)) {
                PushNotificationTrait::pushMessage($candidateUin, $assignVacancyTemplate, 'vacancy_assigned');
            } else {
                PushNotificationTrait::dynamicOneSignalPushMessage($candidateUin, $assignVacancyTemplate, 'vacancy_assigned');
            }
        }
    }

    /*
    *  Shift Assign Cancel Push notification
    */
    public static function shiftCancel($request){

        $shiftCancelTemplate = PushNotificationTemplate::where('template_name','Remove Assigned')->pluck('template')->first();

        $candidateUin = [];
        $oneSignalAppId = DB::table('mobile_app_settings')->where('key', 'oneSignalAppId')->whereNotNull('value')->pluck('value')->first();

        if($request->candidate_id) {
            $candidate = Candidate::find($request->candidate_id);
            if(empty($oneSignalAppId)) {
                if (!empty($candidate->firebase_uin)) {
                    $candidateUin[] = $candidate->firebase_uin;
                }
            } else {
                $candidate_player_id = DB::table('role_users')->where('user_id', $candidate->user_id)
                                ->where('role_id', 6)
                                ->whereNotNull('player_id')->where('player_id', '!=' , '')->first();
                if ($candidate_player_id) {
                    $candidateUin[] = $candidate_player_id->player_id;
                }
            }
        }
        
        /* Push message to Candidates */
        if ($candidateUin) {
            if(empty($oneSignalAppId)) {
                PushNotificationTrait::pushMessage($candidateUin, $shiftCancelTemplate, 'assign_vacancy_removed');
            } else {
                PushNotificationTrait::dynamicOneSignalPushMessage($candidateUin, $shiftCancelTemplate, 'assign_vacancy_removed');
            }
        }
    }

    //send push notification
    public static function pushMessage($candidateUin,$template, $notify_type){
        if( !empty($template) && !empty($candidateUin) ) {
            //getting agency firebase key
            $agencySetting = AgencySetting::where('settings_key', 'firebase_server_key')->first();
            $firebase_server_key = $agencySetting ? $agencySetting->settings_value : '';

                if( empty($firebase_server_key) ){
                    // dd("not registered");
                }else{
                
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                //$token='fFAAHLuaIWI:APA91bGCF7Mj5wq3TFlIvYfQ5C_qKXb0ytGdeQo0pdJ9R3gD82L8mGUN-qPc9qaM2JOfRoX9c7haBcp5aUXQMudvQv4x1a29_dRzLPl8qL9hEFtGgDAmsf_KAkUbClR0ZgxF3NN_cLTL';
                
                $tokenList = array_values($candidateUin);
                
                //print_r($tokenList);exit;
                
                $notification = [
                'body' => $template,
                'notification_type' => $notify_type,
                'sound' => true,
                ];
                
                $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];
                
                $fcmNotification = [
                'registration_ids' => $tokenList, //multple token array
                //'to' => $token, //single token
                'notification' => $notification,
                'data' => $extraNotificationData
                ];
                
                $headers = [
                'Authorization: key='.$firebase_server_key.'',
                'Content-Type: application/json'
                ];
                
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                
                
                return $result = json_decode($result);
                
                // return $result->success;
                }
        
        }
    
    }

    //send push notification
    public static function pushMessageToAdminManager($firebaseUin,$template){
        if( !empty($template) && !empty($firebaseUin) ) {
            //getting agency firebase key
            $agencySetting = AgencySetting::where('settings_key', 'managerapp_firebase_server_key')->first();
            $firebase_server_key = $agencySetting ? $agencySetting->settings_value : '';

                if( empty($firebase_server_key) ){
                    // dd("not registered");
                }else{
                
                $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                //$token='fFAAHLuaIWI:APA91bGCF7Mj5wq3TFlIvYfQ5C_qKXb0ytGdeQo0pdJ9R3gD82L8mGUN-qPc9qaM2JOfRoX9c7haBcp5aUXQMudvQv4x1a29_dRzLPl8qL9hEFtGgDAmsf_KAkUbClR0ZgxF3NN_cLTL';
                
                $tokenList = array_values($firebaseUin);
                
                //print_r($tokenList);exit;
                
                $notification = [
                'body' => $template,
                // 'notification_type' => $notify_type,
                'sound' => true,
                ];
                
                $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];
                
                $fcmNotification = [
                'registration_ids' => $tokenList, //multple token array
                //'to' => $token, //single token
                'notification' => $notification,
                'data' => $extraNotificationData
                ];
                
                $headers = [
                'Authorization: key='.$firebase_server_key.'',
                'Content-Type: application/json'
                ];
                
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                $result = curl_exec($ch);
                curl_close($ch);
                
                
                return $result = json_decode($result);
                
                // return $result->success;
                }
        
        }
    
    }

    //send One Signal Push notification static app id
    public static function oneSignalPushMessage($playersId, $message, $additionalData){

        if(empty($playersId)) {
            return [];
        }
        
        $content      = array(
            "en" => $message
        );
        // $hashes_array = array();
        // array_push($hashes_array, array(
        //     "id" => "like-button",
        //     "text" => "Like",
        //     "icon" => "http://i.imgur.com/N8SN8ZS.png",
        //     "url" => "https://yoursite.com"
        // ));
        // array_push($hashes_array, array(
        //     "id" => "like-button-2",
        //     "text" => "Like2",
        //     "icon" => "http://i.imgur.com/N8SN8ZS.png",
        //     "url" => "https://yoursite.com"
        // ));
        
        $fields = array(
            'app_id' => "7612a6af-9de8-4b2a-9c55-0abd50f79daf",
            'include_player_ids' => $playersId,
            'data' => $additionalData,
            'contents' => $content,
            // 'web_buttons' => $hashes_array
        );
        
        $fields = json_encode($fields);
        // print("\nJSON sent:\n");
        // print($fields);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            // 'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($ch);
        curl_close($ch);
        
        return $response;
    
    }

    //send One Signal Push notification dynamic app id
    public static function dynamicOneSignalPushMessage($playersId, $message, $additionalData){

        if(empty($playersId)) {
            return [];
        }
        
        $content      = array(
            "en" => $message
        );

        $additionalData = array("data_type" => $additionalData);
        // $hashes_array = array();
        // array_push($hashes_array, array(
        //     "id" => "like-button",
        //     "text" => "Like",
        //     "icon" => "http://i.imgur.com/N8SN8ZS.png",
        //     "url" => "https://yoursite.com"
        // ));
        // array_push($hashes_array, array(
        //     "id" => "like-button-2",
        //     "text" => "Like2",
        //     "icon" => "http://i.imgur.com/N8SN8ZS.png",
        //     "url" => "https://yoursite.com"
        // ));

        $oneSignalAppId = DB::table('mobile_app_settings')->where('key', 'oneSignalAppId')->whereNotNull('value')->pluck('value')->first();

        if($oneSignalAppId) {
            $fields = array(
                'app_id' => $oneSignalAppId,
                'include_player_ids' => $playersId,
                'data' => $additionalData,
                'contents' => $content,
                // 'web_buttons' => $hashes_array
            );
            
            $fields = json_encode($fields);
            // print("\nJSON sent:\n");
            // print($fields);
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                // 'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'
            ));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            
            $response = curl_exec($ch);
            curl_close($ch);
            
            return $response;
        }
    
    }



}

