<?php
namespace App\Traits;

use Illuminate\Http\Request;
use App\Tenants\AuditEventModel;
use App\Tenants\RoleUser;
use App\Tenants\UserJob;
use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Helpers\GlobalAuth as Auth;
use App\Tenants\Agency;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\Pages;
use App\Tenants\Permissions;
use App\Tenants\UserPermission;
use App\Tenants\BusinessUnit;
use App\Tenant;
use App\UserTenant;

trait UserTraits{

    
    /**
     * get user list by job ids
     * @param mixed $jobArray
     * 
     * @return void
     */
    public function GetUserByJobId($jobArray)
    {
        $candidate =Candidate::whereIn('id',CandidateJob::whereIn('job_id',$jobArray)->pluck('candidate_id'))->get();
        return $candidate;
    }

    /**
     * get agency name
     * @return void
     */
    public static function GetAgencyName()
    {
        $agency = Agency::first();
        return isset($agency->name)?$agency->name:'Agency';
    }

    /**
     * get agency email
     * @return void
     */
    public static function GetAgencyEmail()
    {
        $agency = Agency::first();
        return isset($agency->email)?$agency->email:'noreply@reply.com';
    }

    /**
     * get agency name
     * @return void
     */
    public static function GetAgencyLogo()
    {
        $logo = Agency::pluck('agency_logo')->first();
        $agency_logo = $logo != ''?$logo:'https://logezy.co/assets/img/logo.png';
        return $agency_logo;
    }

     /**
     * get agency name
     * @return void
     */
    public function GetAgencyAndroidAppUrl()
    {
        $android_app_url = Agency::pluck('android_app_url')->first();
        return $android_app_url != '' ? $android_app_url : 'https://play.google.com/store/apps/details?id=com.mbtechpro.logezy&hl=en_IN' ;
    }
     /**
     * get agency name
     * @return void
     */
    public function GetAgencyIosAppUrl()
    {
        $ios_app_url = Agency::pluck('ios_app_url')->first();
        return $ios_app_url != '' ? $ios_app_url : 'https://play.google.com/store/apps/details?id=com.mbtechpro.logezy&hl=en_IN' ;
        
    }

    /**
     * get agency id
     * @return void
     */
    public function GetAgencyId()
    {
        return session(session('subdomain'))? session(session('subdomain'))->id:0;
    }

    
    /**
     * get tenant role
     * @param mixed $db_name
     * 
     * @return void
     */
    public function GetTenentRoles($db_name)
    {
        Config::set('database.connections.mysql.database', $db_name);
        Config::set('database.default', 'mysql');
        DB::reconnect('mysql');
        return RoleUser::where('user_id',Auth::id())->get();
    }
    
    /**
     * get url of the manager to be redircted
     * @param mixed $id
     * 
     * @return void
     */
    public function get_route_of_the_manager_to_be_redirected($id)
    {
        $page_id = Permissions::where('id',UserPermission::where('user_id',$id)->pluck('permission_id')->first())->pluck('page_id')->first();
        
        $page = Pages::where('id',$page_id)->pluck('url')->first();

        if($page == '#'){
            $page = Pages::where('parent_id',$page_id)->pluck('url')->first();
        }
       return $page;
    }  
    
    /**
     * get url of the agency to be redircted
     * @param mixed $id
     * 
     * @return void
     */
    public function getFirstRouteOfAdmin()
    {
        
        $page = Pages::where('status', 'active')->pluck('url')->first();

        if($page == '#'){
            $page_id = Pages::where('status', 'active')->pluck('id')->first();
            $page = Pages::where('parent_id', $page_id)->pluck('url')->first();
        }
       return $page;
    }  

    /**
     * get agency logo of the user
     * @param mixed $id
     * 
     * @return void
     */
    public function get_auth_agency_logo()
    {
        
       return Agency::pluck('agency_logo')->first();
    }  

    /**
     * get agency name of the user
     * @param mixed $id
     * 
     * @return void
     */
    public function get_agency()
    {
       return Agency::first();
    } 

    public function addTenant($old_user_id, $user_id,$slug)
    {
        Config::set('database.connections.mysql.database', 'logezycore');
        Config::set('database.default', 'mysql');
        DB::reconnect('mysql');
        
            $tenant = Tenant::where('slug', $slug)->first();
            /* Adding new user tenant */
            $user_tenant = UserTenant::where('user_id', $old_user_id)->where('tenant_id', $tenant->id)->first();
            $user_tenant->user_id = $user_id;
            $user_tenant->save();

        Config::set('database.connections.mysql.database', 'logezy_'.$slug);
        Config::set('database.default', 'mysql');
        DB::reconnect('mysql');
    }

    /**
     * get business units 
     * @param mixed $db_name
     * 
     * @return void
     */
    public function getBusinessunits($db_name)
    {
        Config::set('database.connections.mysql.database', $db_name);
        Config::set('database.default', 'mysql');
        DB::reconnect('mysql');
        return BusinessUnit::where('user_id',Auth::id())->get();
    }

}

