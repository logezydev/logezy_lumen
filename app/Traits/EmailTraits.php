<?php
namespace App\Traits;

use App\Tenants\EmailTemplate;
use Illuminate\Support\Str;
use App\Tenants\Agency;
use App\Tenants\RecruitmentEmailSettings;

trait EmailTraits{

    public static function getMessegeConetent($content_array, $template_type)
    {
        $content_array;
        $template            = EmailTemplate::where('template_name',$template_type)->first();
        $body                = $template->template;
        $title               = $template->title;
        // return $template->title;
        $replaced_body = $body;
        $replaced_title = $title;
        foreach($content_array as $key=>$value){
            $replaced_body =  Str::replaceArray($key, [$value], $replaced_body);
            $replaced_title =  Str::replaceArray($key, [$value], $replaced_title);
        }
        return ['body'=>$replaced_body,'title'=>$replaced_title];
    }

    public static function getEmailDetails($template_name) {
        $template = EmailTemplate::where('template_name', $template_name)->first();
        $agency = Agency::first(['email', 'is_generic_email']);
        if($template) {            
            if(!$agency->is_generic_email){
                $from_email = $template->from_email ?? $agency->email;
            } else {
                $from_email = $agency->email;
            }
            $is_enabled = $template->is_enabled;
        } else {
            $from_email = $agency->email;
            $is_enabled = 1;
        }
		
		return ['from_email' => $from_email, 'enabled' => $is_enabled];
	}

    public static function getRecruitmentEmailDetails($template_name) {
        $template = RecruitmentEmailSettings::where('template', $template_name)->first();
        $agency = Agency::first(['email', 'is_generic_email']);
        if($template) {            
            if(!$agency->is_generic_email){
                $sender_email = $template->sender_email ?? $agency->email;
            } else {
                $sender_email = $agency->email;
            }
            $is_enabled = $template->status == 'active' ? 1 : 0;
        } else {
            $sender_email = $agency->email;
            $is_enabled = 1;
        }
		
		return ['sender_email' => $sender_email, 'enabled' => $is_enabled];
	}
}

