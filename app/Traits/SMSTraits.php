<?php

namespace App\Traits;
use App\Helpers\ClickSendConf;
use App\Tenants\AgencySetting;
use App\Tenants\TextTemplate;
use App\Tenants\Candidate;
use App\Tenants\Agency;
use App\SmsDevice;
use App\Jobs\SMSJobs;
use Brick\PhoneNumber\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberParseException;
use DB;


trait SMSTraits {

	/*
    *  Create Sub Account
    */
    public static function createResellerAccount($request){

        if (env('NOTIFICATION')) {

            /* get agency details */ 
            $agency = Agency::getAgencyDetails();  

            /* get country code */
            $countryCode = countryCode($request->mobile);

            $username = preg_replace("/[^a-zA-Z]+/", "", $agency->name);
            $username = 'logezy_'.strtolower($username).'_'.substr($request->mobile, -3);
            
            $body = [

                "username"        => $username,
                "user_email"      => $request->email,
                "password"        => 'Log1@'.$username,
                "user_first_name" => $agency->name,
                "user_last_name"  => "logezy",
                "user_phone"      => $request->mobile,
                "company"         => $agency->name,
                "account_name"    => $agency->name,
                "country"         => $countryCode,
                "active"          => 1

            ];

            $url = "https://rest.clicksend.com/v3/reseller/accounts";

            try{
                return ClickSendConf::createAccount($body,$url,'POST');
            } catch(Exception $e) {
                return 1;
            }
    
        } 
    
    }
    /*
    *  Send SMS
    */
    public static function sendSms($phone,$message) {

        if (env('NOTIFICATION')) {

            $agency = AgencySetting::getTextIntegrationDetails(); 

            if(!empty(json_decode($agency))){

                $agency_mobile = json_decode($agency)->phone_number; 

                $body = [

                    'messages' => [[
                        'source' => 'php',
                        'body'   => $message,
                        'to'     => $phone,
                        'from'   => $agency_mobile
                    ]]

                ];

                $url = "https://rest.clicksend.com/v3/sms/send";

                try{
                    $config = ClickSendConf::clicksend($body,$url); 
                }catch(Exception $e){
                    return false;
                }
            }    
        }
    }
    /*
    *  Get Account Details
    */
    public static function getAccountDetails() {

        if (env('NOTIFICATION')) {
            $body = '';
            $url = "https://rest.clicksend.com/v3/account";

            try{
               return ClickSendConf::clicksend($body,$url,'GET'); 
            }catch(Exception $e){
               return false;
            }
        }    
    }
    /*
    *  Add Credit/Debit Card
    */
    public static function addCreditCard($request) {
        if (env('NOTIFICATION')) {

            $body = [

                "number"       => $request['number'],
                "expiry_month" => $request['expiry_month'],
                "expiry_year"  => $request['expiry_year'],
                "cvc"          => $request['cvc'],
                "name"         => $request['name'],
                "bank_name"    => $request['bank'],
            ];

            $url = "https://rest.clicksend.com/v3/recharge/credit-card";

            try{
               return ClickSendConf::clicksend($body,$url,'PUT'); 
            }catch(Exception $e){
               return false;
            }
        }    
    }
    /*
    *  Get Credit/Debit Details
    */
    public static function getCardDetails() {
        if (env('NOTIFICATION')) {

            $body = "";

            $url = "https://rest.clicksend.com/v3/recharge/credit-card";

            try{
               return ClickSendConf::clicksend($body,$url,'GET'); 
            }catch(Exception $e){
               return false;
            }
        }    
    }
    /*
    *  Get Recharge Packs
    */
    public static function getRechargePacks($country) { 
        if (env('NOTIFICATION')) {

            $body = $country;

            $url = "https://rest.clicksend.com/v3/recharge/packages";

            try{
               return ClickSendConf::clicksend($body,$url,'GET'); 
            }catch(Exception $e){
               return false;
            }
        }    
    }
    /*
    *  Recharge Account
    */
    public static function rechargeAccount($package_id) {
        if (env('NOTIFICATION')) { 

            $body = '';   

            $url = "https://rest.clicksend.com/v3/recharge/purchase/".$package_id;

            try{
               return ClickSendConf::clicksend($body,$url,'PUT'); 
            }catch(Exception $e){
               return false;
            }
        }    
    }
    /*
    *  Vacancy Publish text notification
    */
    public static function vacancyPublish($request){
        $subdomain = $request[0];
        $vacancy = $request->vacancy;
        $vacancyCreation = TextTemplate::where('template_name','Vacancy creation')->pluck('template')->first();
        $searchVal = array("[date]", "[client]", "[business_unit]", "[address]", "[shift_start]", "[shift_end]", "[detail]", "[reference_number]");
        $replaceVal = array($vacancy['on_date'], $vacancy['client'], $vacancy['business_unit_name'], $vacancy['address_line'], $vacancy['start_time'], $vacancy['end_time'], $vacancy['notes'], $vacancy['code']);
        $message = str_replace($searchVal, $replaceVal, $vacancyCreation);
    
        // if(smsIntegrationAccount()){ 
            /* CLICKSEND SMS to Candidates */
            // SMSTraits::SMStoCandidates($request->candidates,$message);
        // } else {
            /* LOGEZY SMS to Candidates */
            $regNumberToken = SMSTraits::checkingAgencyRegisteredOrNot($subdomain);
            if ($regNumberToken == null){
                return 'false';
            }
            if ($request->candidates) {
                // dd($candidates);
                foreach($request->candidates as $val){
                    $candidate = Candidate::find($val);
                    $phone = $candidate->formatted_mobile_number;
                    if ($phone){
                        SMSTraits::LogezySMStoCandidatePhone($phone,$message, $regNumberToken, $subdomain);
                    }
                }
            }
            return 'true';
        // }
    }
    /*
    *  Multiple Vacancy Publish text notification
    */
    public static function multipleVacancyPublish($request){

        $subdomain = $request['subdomain'];
        // if(smsIntegrationAccount()){ 
        //     /* CLICKSEND SMS to Candidates */
        //     SMSTraits::SMStoCandidates($request->candidates,$message);
        // }
        
        /* LOGEZY SMS to Candidates */
        $regNumberToken = SMSTraits::checkingAgencyRegisteredOrNot();
        if ($regNumberToken == null){
            return 'false';
        }
        if ($request['data']) {
            // dd($candidates);
            foreach($request['data'] as $key => $value){
                $candidate = Candidate::find($key);
                $phone = $candidate->formatted_mobile_number;
                $message = '';
                $vacancies = [];
                if (count($value)>1) {
                    foreach($value as $val){
                        $vacancies[] = date("d-m-Y", strtotime($val['vacancy']['on_date']));
                    }
                    $message = "New vacancies have been added on ".implode(",", $vacancies).". To apply for these vacancies please login to your app.";
                } else {
                    $vacancy = $value[0]['vacancy'];
                    $vacancyCreation = TextTemplate::where('template_name','Vacancy creation')->pluck('template')->first();
                    $searchVal = array("[date]", "[client]", "[business_unit]", "[address]", "[shift_start]", "[shift_end]", "[detail]", "[reference_number]");
                    $replaceVal = array(date("d-m-Y", strtotime($vacancy['on_date'])), $vacancy['client'], $vacancy['business_unit'], $vacancy['address_line'], $vacancy['start_time'], $vacancy['end_time'], $vacancy['notes'], $vacancy['code']);
                    $message = str_replace($searchVal, $replaceVal, $vacancyCreation);
                }
                if ($phone){
                    SMSTraits::LogezySMStoCandidatePhone($phone, $message, $regNumberToken, $subdomain);
                }
            }
        }
        return 'true';
        
    }

    /*
    *  Multiple Vacancy Publish text notification
    */
    public static function assignPublishonMultipleDates($request){

        $subdomain  = $request['subdomain'];
        $phone      = $request['phone'];
        
        $AssignMultipleDateTemplate = "You have been assigned on ".implode(",", $request['assignDates']).". check the App for more information.";

        $regNumberToken = SMSTraits::checkingAgencyRegisteredOrNot();
        if ($regNumberToken == null){
            return 'false';
        }

        if ($phone){
            SMSTraits::LogezySMStoCandidatePhone($phone, $AssignMultipleDateTemplate, $regNumberToken, $subdomain);
        }
        return 'true';
        
    }
    /*
    *  Vacancy assign text notification
    */
    public static function vacancyAssign($assign, $candidate, $subdomain){
    
        $vacancyAssign = TextTemplate::where('template_name','Assign Vacancy')->pluck('template')->first();
        $searchVal = array("[date]", "[client]", "[business_unit]", "[address]", "[shift_start]", "[shift_end]", "[detail]", "[booking_code]");
        $replaceVal = array(date('d-m-Y',strtotime($assign->on_date)), $assign->client, $assign->business_unit_name, $assign->address_line, $assign->start_time, $assign->end_time, $assign->notes, $assign->code);
        $message = str_replace($searchVal, $replaceVal, $vacancyAssign);
        // $candidate = Candidate::find($assign->candidate_id);
        $phone = $candidate->formatted_mobile_number;

        // if(smsIntegrationAccount()){
            /* Send sms using Jobs queue */
        //     dispatch(new SMSJobs($phone,$message));
        // } else {

            $regNumberToken = SMSTraits::checkingAgencyRegisteredOrNot($subdomain);
            if ($regNumberToken == null){
                return 'false';
            }
            /* LOGEZY SMS to Candidates */
            return SMSTraits::LogezySMStoCandidatePhone($phone, $message, $regNumberToken, $subdomain);
        // } 
    }
    /*
    *  Vacancy assign remove text notification
    */
    public static function vacancyAssignRemove($assignVacancy, $subdomain){

        $vacancyRemove = TextTemplate::where('template_name','Remove Assigned')->pluck('template')->first();
        $searchVal = array("[date]", "[business_unit]", "[shift_start]", "[shift_end]", "[booking_code]");
        $replaceVal = array(date('d-m-Y',strtotime($assignVacancy->on_date)), $assignVacancy->business_unit_name, $assignVacancy->start_time, $assignVacancy->end_time, $assignVacancy->code);
        $message = str_replace($searchVal, $replaceVal, $vacancyRemove);
        $candidate = Candidate::find($assignVacancy->candidate_id);
        $phone = $candidate->formatted_mobile_number;
        
        // if(smsIntegrationAccount()){

            /* Send sms using Jobs queue */
        //     dispatch(new SMSJobs($phone,$message));
        // } else {

            $regNumberToken = SMSTraits::checkingAgencyRegisteredOrNot($subdomain);
            if ($regNumberToken == null){
                return 'false';
            }
            /* LOGEZY SMS to Candidates */
            return SMSTraits::LogezySMStoCandidatePhone($phone, $message, $regNumberToken, $subdomain);
        // }   
    }
    /*
    *  CLicksend SMS to Candidates
    */
    public static function SMStoCandidates($candidates,$message){

        if (env('NOTIFICATION')) {
            foreach($candidates as $val){
                $candidate = Candidate::find($val);
                $phone = $candidate->formatted_mobile_number;

                /* Send sms using Jobs queue */
                dispatch(new SMSJobs($phone,$message));

            }
        }
    } 

    /*
    *  Logezy SMS to Candidates
    */
    public static function checkingAgencyRegisteredOrNot($subdomain){

        $agency_mobile = Agency::first()->mobile_number;
        
        if (!$agency_mobile) {
            return null;
        }

        config(['database.connections.mysql.database' => 'logezycore','database.default'=>'mysql']);
        DB::reconnect('mysql');

        $regNo = DB::table('sms_devices')->where('mobile_number', $agency_mobile)->first();

        config(['database.connections.mysql.database' => 'logezy_'.$subdomain,'database.default'=>'mysql']);
        DB::reconnect('mysql');
    
        if (!$regNo) {
            return null;
        }
        if (!$regNo->device_token) {
            return null;
        }
            
        return  $regNo->device_token ?? null;
    }

    /*
    *  Logezy SMS to Candidates
    */
    public static function LogezySMStoCandidatePhone($phone, $message, $regNumberToken, $subdomain){

        $fcm_token = env('SMS_FCM');

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $notification = [
            "title" => "Ionic 4 Notification",
            "body" => "This notification sent from POSTMAN using Firebase HTTP protocol",
            "sound" => "default",
            "click_action" => "FCM_PLUGIN_ACTIVITY",
            "icon" => "fcm_push_icon"
        ];

        $data = [
            "mobileNo" => $phone,
            "message" => $message
        ];

        $fcmData = [
            // "notification" => $notification,
            "data" => $data,
            "to" => $regNumberToken,
            "priority" => "high",
            "restricted_package_name" => ""
        ];

        $headers = [
            'Authorization: ' . $fcm_token . '',
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmData));
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);
        // dd($result);
        if ($result->success == 1) {
            return "Message send successfully";
        } else {
            return "Something went wrong";
        }
             
    } 

    /**
     * @param mixed $request
     * 
     * @return [type]
     */
    public static function sendCustomSMS($phone_number, $message){
        $subdomain = '';
        if(smsIntegrationAccount()){
            /* Send sms using Jobs queue */
            dispatch(new SMSJobs($phone_number,$message));
        } else {

            $regNumberToken = SMSTraits::checkingAgencyRegisteredOrNot();
            if ($regNumberToken == null){
                return 'false';
            }
            /* LOGEZY SMS to Candidates */
            return SMSTraits::LogezySMStoCandidatePhone($phone_number,$message, $regNumberToken,$subdomain);
        } 
    }
	
}



