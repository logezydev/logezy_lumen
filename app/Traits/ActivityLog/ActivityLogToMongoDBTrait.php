<?php
namespace App\Traits\ActivityLog;
use Illuminate\Support\Collection;
use DB;
use Carbon\Carbon;
use Request;

trait ActivityLogToMongoDBTrait {

    public static function saveActivityLog($logDatas, $integration, $role, $tenant, $email, $fullName) {

        try {
            $header = array(
              "Authorization: Bearer ".$integration->access_token,
              "Content-Type: application/json;charset=UTF-8",
              "TenantName: $tenant",
              "Role: $role",
              "Email: $email",
              "FullName: $fullName"
            );
            $ch = curl_init($integration->api_url.'/activity-log');
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($logDatas));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            $response = curl_exec($ch);
            curl_close($ch);
            return true;
        } catch(\Exception $ex) {
            return $ex->getMessage();
        }
    }
}