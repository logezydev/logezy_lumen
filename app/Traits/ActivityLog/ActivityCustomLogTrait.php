<?php
namespace App\Traits\ActivityLog;
use Illuminate\Support\Collection;
use App\Traits\ActivityLog\ActivityLogToMongoDBTrait;
use Carbon\Carbon;
use Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait ActivityCustomLogTrait {

    public static function saveActivityLog($entity, $operation, $message, $subjectId, $objectId = null, $loggedData = null) { 
        //dd(Auth::id());
        $request = app('request');
        $activityLogIntegration = DB::table('logezycore.integrations')->where('slug', 'logezy-activity-log')->where('is_deleted', 0)->select('is_integrated', 'is_enabled', 'api_url', 'access_token')->first();
        if ($activityLogIntegration && $activityLogIntegration->is_integrated && $activityLogIntegration->is_enabled) {
            //create activity log
            ActivityLogToMongoDBTrait::saveActivityLog(
                [
                    [   
                        'entity' => $entity,
                        'operation' => $operation,
                        'role' => $loggedData['role'],
                        'tenant' => $loggedData['tenant'],
                        'message' => $message,
                        'method' => $request->method(),
                        'url' => $request->fullUrl(),
                        'subjectId' => $subjectId,
                        'objectId' => $objectId,
                        'dashboardType' => $loggedData['dashboardType'],
                        'ip' => $request->ip(),
                        'agent' => $request->header('user-agent'),
                        'activityAt' => Carbon::now()->toDateTimeString()
                    ]
                ],

                $activityLogIntegration, 
                $loggedData['role'],
                $loggedData['tenant'],
                $loggedData['email'],
                $loggedData['user_name']
            );
        }
    }
}