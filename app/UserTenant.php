<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTenant extends Model
{
    protected $table = 'user_tenants';

    protected $fillable = [
        'user_id', 'tenant_id',
    ];

    /**
     * user
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * tenant
     *
     * @return void
     */
    public function tenant()
    {
        return $this->belongsTo('App\Tenant', 'tenant_id');
    }
}
