<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class RecruitmentBuiltForm extends Model
{
    protected $table = 'recruitment_built_forms';

    protected $fillable = ['job_id', 'title', 'description', 'elements', 'template_id', 'status', 'is_deleted', 'created_by'];

    /**
     * recruitmentJobsForm.
     */
    public function recruitmentJobsForm()
    {
        return $this->hasMany('App\Tenants\RecruitmentJobsForm', 'recruitment_built_form_id');
    }
}
