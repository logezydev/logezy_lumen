<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{

	protected $table = 'work_experiences';

	protected $fillable =
	[
		'candidate_id', 'company_name', 'address_line1', 'designation', 'start_date', 'end_date', 'desc', 'status', 'created_by'
	];

	/**
	 * user
	 * relation
	 * @return void
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/**
	 * candidate
	 * relation
	 * @return void
	 */
	public function candidate()
	{
		return $this->belongsTo('App\Tenants\Candidate', 'candidate_id');
	}
}
