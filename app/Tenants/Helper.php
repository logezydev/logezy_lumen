<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Tenants\Day;
use App\Tenants\Shift;
use App\Tenants\Holiday;
use App\Tenants\Vacancy;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\BusinessUnit;
use App\Tenants\CandidateAvailability;
use App\Tenants\AssignVacancy;
use App\Tenants\RateCard;
use App\Tenants\VacancyStaffRate;


class Helper extends Model
{

 	public static function getTotalHours($input) {

 		if (isset($input['on_date']) && isset($input['start_time']) && isset($input['end_time']) ) {

			$start = date("H:i", strtotime($input['start_time']));
			$end = date("H:i", strtotime($input['end_time']));

	 		$workingday =  $input['on_date'];
	        $startTime = strtotime($workingday.' '.$input['start_time']);
	        //check for next day
	        $daycheck = date('a', strtotime($input['end_time']));
	         
	        if ($start >= $end) {
				$nextday = date('d-m-Y', strtotime('+1 day', strtotime($workingday)));
				$endTime = strtotime($nextday.' '.$input['end_time']);
			} else {
				$endTime = strtotime($workingday.' '.$input['end_time']);
			}
			
	        $break = isset($input['break']) ? $input['break'] : 0 ;
	        //calculating difference
	        $time_diff = round(abs($endTime - $startTime) / 60,2);

	        if(!empty($break) && (number_format($time_diff) == number_format($break) )){
				return 0;
	        } else if(!empty($break) && ($time_diff > $break)){
	            $time_diff_minutes = $time_diff - $break;
	            $hours = floor($time_diff_minutes / 60);
	            $hours = (int)$hours;
	            $minutes = ($time_diff_minutes % 60);
	            $minutes = ($minutes/60);
	            $minutes = round($minutes, 2);
				return $hoursMinutes = $hours + $minutes;
	            // return number_format(str_replace(',', '',$hoursMinutes),2) ;
	        } else {
	            $hours = floor($time_diff / 60);
	            $hours = (int)$hours;
	            $minutes = ($time_diff % 60);
	            $minutes = ($minutes/60);
	            $minutes = round($minutes, 2);
				return $hoursMinutes = $hours + $minutes;
	            // return number_format(str_replace(',', '',$hoursMinutes),2) ;
	        }
	        
	    }//end if

 	}

 	public static function getClientStaffRates($input) {

 		if (isset($input['business_unit_id']) && isset($input['job_id']) && isset($input['on_date']) && isset($input['shift_id']) ) {

			$date = date("Y-m-d", strtotime($input['on_date']));
			$day = date("l", strtotime($input['on_date']));
			$dayObj = Day::where('day', $day)->first();
			$rates = [];
			$customHolidayRates = [];
			$holidayShiftRates = [];

			//#check for custom holiday
			$custom_holiday = CustomHoliday::where('business_unit_id', $input['business_unit_id'])->where('holiday_date', $date)->where('is_deleted',0)->first();
			if ($custom_holiday) {
				//getting holiday shift name
				$shift = Shift::find($input['shift_id']);
				$holidayShift = Shift::where('business_unit_id',$input['business_unit_id'])->where('shift_name', 'Holiday '.$shift->shift_name)->first();
				//check custom holiday has ratecard?
				$customHolidayRates = RateCard::with('staffratecard')->where('business_unit_id',$input['business_unit_id'])
						->where('job_id',$input['job_id'])
						->where('shift_id',$holidayShift->id)
						->where('custom_holiday_id',$custom_holiday->id)
						->where('day_id',8)
						->where('is_deleted',0)->orderBy('id', 'ASC')->first();
			}

			//#check for holiday
			$holiday = Holiday::where('on_date', $date)->where('is_deleted',0)->first();
			$excludeHoliday = BusinessUnit::find($input['business_unit_id'])->holiday_exclude;
			
			if ($custom_holiday && empty($customHolidayRates)) {
				//getting holiday shift name
				$shift = Shift::find($input['shift_id']);
				$holidayShift = Shift::where('business_unit_id',$input['business_unit_id'])->where('shift_name', 'Holiday '.$shift->shift_name)->first();
				if (!$holidayShift) {
					$holidayShift = Shift::where('business_unit_id', 0)->where('shift_name', 'Holiday '.$shift->shift_name)->first();
				}
				//check holiday has ratecard?
				$holidayShiftRates = RateCard::with('staffratecard')->where('business_unit_id',$input['business_unit_id'])
						->where('job_id',$input['job_id'])
						->where('shift_id',$holidayShift->id)
						->where('custom_holiday_id', 0)
						->where('day_id',8)
						->where('is_deleted',0)->orderBy('id', 'ASC')->first();
			}

			if ($customHolidayRates) {
				$rates = $customHolidayRates;
			} else if ($holidayShiftRates  && $excludeHoliday == 0) {
				$rates = $holidayShiftRates;
			} else {
				$rates = RateCard::with('staffratecard')->where('business_unit_id',$input['business_unit_id'])
						->where('job_id',$input['job_id'])
						->where('shift_id',$input['shift_id'])
						// ->where('custom_holiday_id', 0)
						->where('day_id',$dayObj->id)
						->where('is_deleted',0)->orderBy('id', 'ASC')->first();
			}
			// dd($rates);
			return $rates;
 			
 		}

 	}

 	public static function getCandidatesForVacancyPublish($input) {


 		if (isset($input['business_unit_id']) && isset($input['job_id']) && isset($input['on_date']) && isset($input['shift_id']) && isset($input['id']) ) {

			$business_unit_id 	= $input['business_unit_id'];
			$job_id 			= $input['job_id'];
			$on_date 			= date("Y-m-d", strtotime($input['on_date']));
			$shift_id 			= $input['shift_id'];
			$id     			= $input['id'];
			$candidate_array    = [];
			
			$shift_name = Shift::find($shift_id)->shift_name;
			$shift = [];
			switch ($shift_name) {
				case "Day":
					$shift = ["E", "L", "LD"];
					break;
				case "Night":
					$shift = ["N", "LD"];
					break;
				case "Holiday Day":
					$shift = ["E", "L", "LD"];
					break;
				case "Holiday Night":
					$shift = ["N", "LD"];
					break;
				default:
					
			}

			$restrictedShiftId = Shift::where('shift_name', $shift_name)->where('business_unit_id', 0)->first()->id;

			$candidate_ids = CandidateJob::where('job_id', $job_id)->pluck('candidate_id')->all();
			$candidateIdArr = array_unique(array_values($candidate_ids));
			//## ASSIGNED CANDIDATES
			$vacancy = DB::table('vacancies')->find($input['id']);
			$assigned_array = DB::table('assign_vacancies')
					->whereIn('status', ['pending', 'approved', 'invoiced'])
					->where('is_deleted', 0)
					->where('start_time', '<', $vacancy->end_time)
					->where('end_time', '>', $vacancy->start_time)
					->whereIn('candidate_id', $candidateIdArr)
					->pluck('candidate_id')->all();
			$assignedArr = array_unique(array_values($assigned_array));

			//## CONSECUTIVE CANDIDATES
			$agency = DB::table('agencies')->first(); 
			$oneHourAgo= date('Y-m-d H:i:s', strtotime($vacancy->start_time.' -'.$agency->consecutive_hours.' hour'));
			$oneHourAfter= date('Y-m-d H:i:s', strtotime($vacancy->end_time.' +'.$agency->consecutive_hours.' hour'));
			$vacStartTime= date('Y-m-d H:i:s', strtotime($vacancy->start_time));
			$vacEndTime= date('Y-m-d H:i:s', strtotime($vacancy->end_time));
			$oneHourAgoQuery = DB::table('assign_vacancies')
						->whereIn('status', ['pending', 'approved', 'invoiced'])
						->where('is_deleted', 0)
						->whereBetween('end_time', [$oneHourAgo, $vacStartTime])
						->whereIn('candidate_id', $candidateIdArr)
						->whereNotIn('candidate_id', $assignedArr);
			$oneHourAgoAssignIDs = $oneHourAgoQuery->pluck('id')->all();
			$oneHourAgoArr = $oneHourAgoQuery->pluck('candidate_id')->all();
			$oneHourAfterQuery = DB::table('assign_vacancies')
						->whereIn('status', ['pending', 'approved', 'invoiced'])
						->where('is_deleted', 0)
						->whereBetween('start_time', [$vacEndTime, $oneHourAfter])
						->whereIn('candidate_id', $candidateIdArr)
						->whereNotIn('candidate_id', $assignedArr);
			$oneHourAfterAssignIDs = $oneHourAfterQuery->pluck('id')->all();
			$oneHourAfterArr = $oneHourAfterQuery->pluck('candidate_id')->all();

			$consecutiveArr = array_unique(array_values(array_merge($oneHourAgoArr, $oneHourAfterArr)));
			$consecutiveAssignIDs = array_unique(array_values(array_merge($oneHourAgoAssignIDs, $oneHourAfterAssignIDs)));

			//## AVAILABLE CANDIDATES
			$consecutive = array_unique(array_values(array_merge($assignedArr, $consecutiveArr)));
			$available_array = CandidateAvailability::where('date', $on_date)
					->whereIn('availability', $shift)
					->whereIn('candidate_id', $candidateIdArr)
					->whereNotIn('candidate_id', $consecutive)
					->pluck('candidate_id')->all();
			$availableArr = array_unique(array_values($available_array));

			//## UNAVAILABLE CANDIDATES
			$consecutiveAvailable = array_unique(array_values(array_merge($assignedArr, $consecutiveArr, $availableArr)));
			$unavailable_array = CandidateAvailability::where('date', $on_date)
					->whereIn('availability', ['U'])
					->whereIn('candidate_id', $candidateIdArr)
					->whereNotIn('candidate_id', $consecutiveAvailable)
					->pluck('candidate_id')->all();
			$unavailableArr = array_unique(array_values($unavailable_array));

			//# WORKED CANDIDATES
			$consecutiveAvailableUnavailable = array_unique(array_values(array_merge($assignedArr, $consecutiveArr, $availableArr, $unavailableArr)));
			$worked_array = AssignVacancy::whereHas('vacancy', function ($query) use($business_unit_id, $job_id, $shift_id, $id) {      
							$query->where('business_unit_id', $business_unit_id);
							$query->where('job_id', $job_id);
							$query->where('shift_id', $shift_id);
							$query->where('id', '!=',$id);
					})
					->whereIn('candidate_id', $candidateIdArr)
					->whereNotIn('candidate_id', $consecutiveAvailableUnavailable)
					->whereIn('status', ['pending','approved', 'invoiced'])
					->where('is_deleted', 0)->pluck('candidate_id')->all();
			$workedArr = array_unique(array_values($worked_array));
			
			//## OTHER CANDIDATES
			$consecutiveAvailableUnavailableWorked = array_unique(array_values(array_merge($assignedArr, $consecutiveArr, $availableArr, $unavailableArr, $workedArr)));
			$other_array = array_diff($candidateIdArr, $consecutiveAvailableUnavailableWorked);
			$otherArr = array_unique(array_values($other_array));

			//checking for regions
			// $filtered_regions = FilterLock::where('title', "region")->where('user_id', Auth::id())->where('status', 'active')->first();
			$regions = BusinessunitRegion::where('business_unit_id', $business_unit_id)->pluck('region_id')->all();
			if ($regions) {
				$candidateConsecutiveIDs = CandidateRegion::whereIn('region_id', $regions)->whereIn('candidate_id', $consecutiveArr)->pluck('candidate_id')->all();
				$candidateAvailableIDs = CandidateRegion::whereIn('region_id', $regions)->whereIn('candidate_id', $availableArr)->pluck('candidate_id')->all();
				$candidateUnavailableIDs = CandidateRegion::whereIn('region_id', $regions)->whereIn('candidate_id', $unavailableArr)->pluck('candidate_id')->all();
				$candidateWorkedIDs = CandidateRegion::whereIn('region_id', $regions)->whereIn('candidate_id', $workedArr)->pluck('candidate_id')->all();
				$candidateOtherIDs = CandidateRegion::whereIn('region_id', $regions)->whereIn('candidate_id', $otherArr)->pluck('candidate_id')->all();
				$candidateAssignedIDs = CandidateRegion::whereIn('region_id', $regions)->whereIn('candidate_id', $assignedArr)->pluck('candidate_id')->all();
			} else {
				$candidateConsecutiveIDs = $consecutiveArr;
				$candidateAvailableIDs = $availableArr;
				$candidateUnavailableIDs = $unavailableArr;
				$candidateWorkedIDs = $workedArr;
				$candidateOtherIDs = $otherArr;
				$candidateAssignedIDs = $assignedArr;
			}
			if (isset($input['vacancy_id']) && $input['vacancy_id'] != '') {
				
				$allCanIds = array_unique(array_values(array_merge($candidateConsecutiveIDs, $candidateAvailableIDs, $candidateUnavailableIDs, $candidateWorkedIDs, $candidateOtherIDs)));
				$vacancyId = $input['vacancy_id'];
				$filteredIds = DB::table('candidates')
							->whereIn('id', $allCanIds)
							->where('candidate_status_id', 1)
							->where('is_deleted',0)
							->whereNotIn('id',function($query) use($business_unit_id){
								$query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
							})->whereNotIn('id',function($query) use($restrictedShiftId){
								$query->where('shift_id', $restrictedShiftId)->select('candidate_id')->from('candidate_restricted_shifts');
							})->pluck('id')->all();
				//updating vacancy
				$vacancyData = [
					'id'       		=> $vacancyId,
					'is_publish'    => 1,
					'published_ids' => implode(",", $filteredIds ),
					'published_at'  => date("Y-m-d H:i:s")
				];
				DB::table('vacancies')->where('id', $vacancyId)->update($vacancyData);

				$canVacArr = collect($filteredIds)->map(function ($candidateId) use($vacancyId) {
					return  [
						'vacancy_id'   => $vacancyId,
						'candidate_id' => $candidateId
					];
				});
				return $canVacArr;
			} else {
				
				$candidate_array['consecutive'] = DB::table('assign_vacancies')
												->join('candidates', function($join) {
													$join->on('candidates.id', '=', 'assign_vacancies.candidate_id');
												})->whereRaw("candidates.id IN ('".implode("','",$candidateConsecutiveIDs)."')")
												// ->whereIn('candidates.id', $candidateConsecutiveIDs)
												->whereIn('assign_vacancies.id', $consecutiveAssignIDs)
												->where('candidates.candidate_status_id', 1)
												->where('candidates.is_deleted',0)
												->select(
													'candidates.id', 
													'candidates.full_name',
													// 'assign_vacancies.id',
													'assign_vacancies.code',
													DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateFormat().' '.timeFormat().'") as start_time'), 
													DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateFormat().' '.timeFormat().'") as end_time'), 
													'candidates.candidate_status_id')
												->whereNotIn('candidates.id',function($query) use($business_unit_id){
													$query->where('candidate_restricted_business_units.business_unit_id', $business_unit_id)->select('candidate_restricted_business_units.candidate_id')->from('candidate_restricted_business_units');
												})->whereNotIn('candidates.id',function($query) use($restrictedShiftId){
													$query->where('candidate_restricted_shifts.shift_id', $restrictedShiftId)->select('candidate_restricted_shifts.candidate_id')->from('candidate_restricted_shifts');
												})->orderBy('candidates.first_name', 'ASC')->get();
				
				$candidate_array['available']   = DB::table('candidates')
												->whereRaw("id IN ('".implode("','",$candidateAvailableIDs)."')")
												// ->whereIn('id', $candidateAvailableIDs)
												->where('candidate_status_id', 1)
												->where('is_deleted',0)
												->select('id', 'full_name', 'candidate_status_id')
												->whereNotIn('id',function($query) use($business_unit_id){
													$query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
												})->whereNotIn('id',function($query) use($restrictedShiftId){
													$query->where('shift_id', $restrictedShiftId)->select('candidate_id')->from('candidate_restricted_shifts');
												})->orderBy('first_name', 'ASC')->get();

				$candidate_array['unavailable'] = DB::table('candidates')
												->whereRaw("id IN ('".implode("','",$candidateUnavailableIDs)."')")
												// ->whereIn('id', $candidateUnavailableIDs)
												->where('candidate_status_id', 1)
												->where('is_deleted',0)
												->select('id', 'full_name', 'candidate_status_id')
												->whereNotIn('id',function($query) use($business_unit_id){
													$query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
												})->whereNotIn('id',function($query) use($restrictedShiftId){
													$query->where('shift_id', $restrictedShiftId)->select('candidate_id')->from('candidate_restricted_shifts');
												})->orderBy('first_name', 'ASC')->get();
												
				$candidate_array['other']      = DB::table('candidates')
												->whereRaw("id IN ('".implode("','",$candidateOtherIDs)."')")
												// ->whereIn('id', $candidateOtherIDs)
												->where('candidate_status_id', 1)
												->where('is_deleted',0)
												->select('id', 'full_name', 'candidate_status_id')
												->whereNotIn('id',function($query) use($business_unit_id){
													$query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
												})->whereNotIn('id',function($query) use($restrictedShiftId){
													$query->where('shift_id', $restrictedShiftId)->select('candidate_id')->from('candidate_restricted_shifts');
												})->orderBy('first_name', 'ASC')->get();

				$candidate_array['worked']      = DB::table('candidates')
												->whereRaw("id IN ('".implode("','",$candidateWorkedIDs)."')")
												// ->whereIn('id', $candidateWorkedIDs)
												->where('candidate_status_id', 1)
												->where('is_deleted',0)
												->select('id', 'full_name', 'candidate_status_id')
												->whereNotIn('id',function($query) use($business_unit_id){
													$query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
												})->whereNotIn('id',function($query) use($restrictedShiftId){
													$query->where('shift_id', $restrictedShiftId)->select('candidate_id')->from('candidate_restricted_shifts');
												})->orderBy('first_name', 'ASC')->get();
												
				$candidate_array['assigned']    = DB::table('candidates')
												->whereRaw("id IN ('".implode("','",$candidateAssignedIDs)."')")
												// ->whereIn('id', $candidateAssignedIDs)
												->where('candidate_status_id', 1)
												->where('is_deleted',0)
												->select('id', 'full_name', 'candidate_status_id')
												->whereNotIn('id',function($query) use($business_unit_id){
													$query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
												})->whereNotIn('id',function($query) use($restrictedShiftId){
													$query->where('shift_id', $restrictedShiftId)->select('candidate_id')->from('candidate_restricted_shifts');
												})->orderBy('first_name', 'ASC')->get();

				return $candidate_array;
			}

 		}
 		return [];

 	}
 	
}
