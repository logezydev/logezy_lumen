<?php

namespace App\Tenants;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CandidateDocument extends Model
{
    protected $table = 'candidate_documents';
    protected $fillable = [
        'document_id', 'issue_date', 'expiry_date', 'description', 'status', 'has_profile_view', 'has_access', 'file', 'candidate_id', 'created_by'
    ];
    public $appends = ['has_issue_date','has_expiry_date','document_title', 'document_category', 'is_completed', 'is_expired'];

    /**
     * Setting accessors for start time
     */
    public function getIssueDateAttribute($value)
    {
        if ($value != null){
            return date("d-m-Y", strtotime($value) );
        } else {
            return $value;
        }
    }

    /**
     * Setting accessors for end time
     */
    public function getExpiryDateAttribute($value)
    {
        if ($value != null){
            return date("d-m-Y", strtotime($value) );
        } else {
            return $value;
        }
    }

    public function documents()
    {
        return $this->belongsTo('App\Tenants\Document','document_id');
    }
    
    /* Gets the Candidate */
    public function candidate() {
        return $this->belongsTo('App\Tenants\Candidate','candidate_id');
    }

    /**
     * Gets the has_issue_date.
     */
    public function getHasIssueDateAttribute() {
        return $this->belongsTo('App\Tenants\Document','document_id')->pluck('has_issue_date')->first() ;
    }

    /**
     * Gets the has_expiry_date.
     */
    public function getHasExpiryDateAttribute() {
        return $this->belongsTo('App\Tenants\Document','document_id')->pluck('has_expiry_date')->first() ;
    }

    /**
     * Gets the title.
     */
    public function getDocumentTitleAttribute() {
        return $this->belongsTo('App\Tenants\Document','document_id')->pluck('title')->first() ;
    }

    /**
     * Gets the is completed attribute.
     */
    public function getIsCompletedAttribute()
    {
        $has_expiry = Document::where('id', $this->document_id)->pluck('has_expiry_date')->first();
        $has_issue = Document::where('id', $this->document_id)->pluck('has_issue_date')->first();

        if ($this->is_optional == 1) {
            return true;
        }
        if ($has_expiry == 1) {
            if (!$this->expiry_date) {
                return false;
            }

            if ($this->expiry_date < Carbon::now()->format('Y-m-d')) {
                return false;
            }
        }
        if ($has_issue == 1) {
            if (!$this->issue_date) {
                return false;
            }
        }
        // if(!$this->file){
        //     return false;
        // }

        return true;
    }

    /**
     * Gets the expiry date format.
     */
    public function getIsExpiredAttribute()
    {
        if ( ($this->expiry_date != NULL) && (date('Y-m-d', strtotime($this->expiry_date)) < date('Y-m-d')) ) {
            return true;
        }

        return false;
    }

    /* Gets the Document Category*/
    public function getDocumentCategoryAttribute()
    {
        //Candidate::where('id', $this->candidate_id)->first()->full_name;
        $document_category_id = Document::find($this->document_id)->document_category_id;

        return DocumentCategory::find($document_category_id)->title;
    }
}
