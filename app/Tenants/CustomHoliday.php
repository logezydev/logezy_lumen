<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class CustomHoliday extends Model
{
    protected $table = 'custom_holidays';
}
