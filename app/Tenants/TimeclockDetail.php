<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class TimeclockDetail extends Model
{
    protected $table = 'timeclock_details';
    // protected $casts = [
    //     'start_time' => 'date:h:i A', 'end_time'   => 'date:h:i A'
    // ];
    public $appends = ['clock_in_details', 'clock_out_details'];

    /**
     * Gets the clock_in details attribute.
     *
     * @return     <type>  The ClockInDetails attribute.
     */
    public function getClockInDetailsAttribute() {
        return $timeclockDetailLocation = TimeclockDetailLocation::where('timeclock_detail_id', $this->id)->whereIn('type', ['clock_in', 'break_end'])->first();
    }

    /**
     * Gets the clock_out details attribute.
     *
     * @return     <type>  The ClockOutDetails attribute.
     */
    public function getClockOutDetailsAttribute() {
        return $timeclockDetailLocation = TimeclockDetailLocation::where('timeclock_detail_id', $this->id)->whereIn('type', ['clock_out', 'break_start'])->first();
    }

    /**
     * [timeclock detail location Relation].
     */
    public function locations()
    {
        return $this->hasMany('App\Tenants\TimeclockDetailLocation', 'timeclock_detail_id')->orderBy('id', 'ASC');
    }
}
