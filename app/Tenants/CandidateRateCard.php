<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use App\Models\Candidate;
use App\Models\Day;
use App\Models\Shift;
use App\Models\RateCard;

class CandidateRateCard extends Model
{
    protected $table = 'candidate_rate_cards';

}