<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class RecruitmentCandidate extends Model
{
    protected $table = 'recruitment_candidates';

    protected $fillable = ['candidate_id', 'status', 'is_deleted', 'created_by'];
}