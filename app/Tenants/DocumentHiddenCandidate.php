<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class DocumentHiddenCandidate extends Model
{	
	/**
	 * table
	 *
	 * @var string
	 */
	protected $table = 'document_hidden_candidates';
		
	/**
	 * fillable
	 *
	 * @var array
	 */
	protected $fillable = ['candidate_id', 'document_id', 'status', 'is_deleted', 'created_by'];
	
	/**
	 * candidates
	 *
	 * @return void
	 */
	public function candidates()
	{
		return $this->hasMany('App\Tenants\Candidate', 'id', 'candidate_id');
	}
	
	/**
	 * documents
	 *
	 * @return void
	 */
	public function documents()
	{
		return $this->hasMany('App\Tenants\Document', 'id', 'document_id');
	}
}
