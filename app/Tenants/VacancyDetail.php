<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;

class VacancyDetail extends Model
{
    protected $table = 'vacancy_details';
}
