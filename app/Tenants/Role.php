<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $table = 'roles';
}
