<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Document extends Model
{
    protected $table = 'documents';

    public $appends = ['document_upload_status', 'pending_document'];

    /** User work experience */
    public function candidate_documents()
    {
        return $this->hasMany('App\Tenants\CandidateDocument','document_id');
    }
    /** candidate_documents_excluded
     * @returnvoid
     */
    public function candidate_documents_excluded()
    {
        return $this->hasMany('App\Tenants\DocumentExcludedCandidate','document_id');
    }
    /** candidate_documents_excluded
     * @returnvoid
     */
    public function candidate_hidden_documents()
    {
        return $this->hasMany('App\Tenants\DocumentHiddenCandidate','document_id');
    }
    /**
     * has one candidate document
     * @return void
     */
    public function uploaded_candidate_documents()
    {
        return $this->hasOne('App\Tenants\CandidateDocument','document_id');
    }
    /** User work experience */
    public static function candidateDocuments($id)
    {
        return self::hasMany('App\Tenants\CandidateDocument','document_id')->where('candidate_id',$id);
    }

    /*Jobs of the document */
    public function jobs()
    {
        return $this->belongsToMany('App\Tenants\Job','document_jobs', 'document_id', 'job_id');
    }
    /* Document Category */
    public function category()
    {
        return $this->belongsTo('App\Tenants\DocumentCategory','document_category_id');
    }
    /** Document Category 
     * @return void
    */
    public function active_category()
    {
        return $this->belongsTo('App\Tenants\DocumentCategory','document_category_id')->where('is_deleted',0);
    }

    /**
     * documentJob
     *
     * @return void
     */
    public function documentJob()
    {
        return $this->hasOne('App\Tenants\DocumentJob', 'document_id');
    }

    /**
     * uploadedDocuments
     *
     * @return void
     */
    public function uploadedDocuments()
    {
        return $this->hasOne('App\Tenants\CandidateDocument','document_id');
    }
    
    /**
     * uploadedDocuments
     *
     * @return void
     */
    public function candidate_document()
    {
        return $this->hasOne('App\Tenants\CandidateDocument','document_id');
    }
    /**
     * excludedCandidates
     *
     * @return void
     */
    public function excludedCandidates()
    {
        return $this->belongsTo('App\Tenants\DocumentExcludedCandidate', 'document_id');
    }

    public function getDocumentUploadStatusAttribute()
    {
        return DB::table('candidate_uploaded_documents')
        ->where([
            'candidate_id' => DB::table('candidates')->where('user_id', Auth::id())->pluck('id')->first(),
            'document_id' => $this->id
        ])->pluck('status')->last();
    }

    public function getPendingDocumentAttribute()
    {
        return DB::table('candidate_uploaded_documents')
        ->where([
            'candidate_id' => DB::table('candidates')->where('user_id', Auth::id())->pluck('id')->first(),
            'document_id' => $this->id,
            'status' => 'pending'
        ])->select('id', 'file', 'file_name', 'issue_date', 'expiry_date')->first();
    }
    
}
