<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RecruitmentEmailSettings extends Model
{
    protected $table = 'recruitment_email_settings';
}
