<?php

namespace App\Tenants;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserIntegration extends Model
{
    /**
     * table
     *
     * @var string
     */
    protected $table = 'user_integrations';

    protected $casts = ['is_active'  => 'boolean', 'is_deleted' => 'boolean'];
    
    /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'integration_id', 'access_token', 'refresh_token', 'is_deleted', 'is_active'
    ];
}