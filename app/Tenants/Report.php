<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;

class Report extends Model
{
    

	/**
	 * Get weekly timesheet.
	 */
	public static function getWeeklyReports($weekStartEnd, $candidate_id) {
		$employmentType =	EmploymentType::where('title', 'PAYE')->first();
		$assignedShifts = DB::table('assign_vacancies')
		->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
		->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
		->join('assigned_vacancy_details', 'assigned_vacancy_details.assigned_vacancy_id', '=', 'assign_vacancies.id')
		->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
		->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
		->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
		->where(function ($query) use($weekStartEnd, $candidate_id){
			if ($weekStartEnd)
				$query->whereBetween('assign_vacancies.on_date', $weekStartEnd);

			if ($candidate_id != '')
				$query->where('assign_vacancies.candidate_id', $candidate_id);
		})
		->select('assign_vacancies.id', 
		'candidates.full_name', 
		'candidates.employment_types_id', 
		'business_units.business_unit_name', 
		'jobs.name AS job_name', 
		'shifts.shift_name AS shift', 
		DB::raw('TIME_FORMAT(assign_vacancies.start_time, "'.timeFormat().'") as start_time'), 
		DB::raw('TIME_FORMAT(assign_vacancies.end_time, "'.timeFormat().'") as end_time'), 
		'assign_vacancies.break', 
		'assign_vacancies.on_date', 
		'assign_vacancies.total_hours', 
		'assign_vacancies.holiday_pay_amount', 
		DB::raw('SUM(assigned_vacancy_details.client_pay_amount) as client_pay_amount'), 
		DB::raw('SUM(assigned_vacancy_details.staff_pay_amount) as staff_pay_amount'), 
		'assign_vacancies.is_publish', 
		'assign_vacancies.status')
		->whereIn('assign_vacancies.status', ['approved','pending','invoiced'])
		->where('assign_vacancies.is_deleted', 0)
		->groupBy('assign_vacancies.id')
		->get();

		$results = $assignedShifts->groupBy(['on_date']);
					
		$grand_total_hours  = 0; 
		$grand_client_amount  = 0; 
		$grand_staff_amount  = 0;
		$grand_holidaypay_amount  = 0;

		if (count($results) > 0) {
			$final = [];
			foreach ($results as $key => $values) {
				$total_hours  = 0; 
				$client_amount = 0;
				$staff_amount  = 0;
				$holidaypay_amount  = 0;
				foreach ($values as $value) {
					$final[$key][] = $value;

					$total_hours  += $value->total_hours;
					$client_amount += $value->client_pay_amount;
					$staff_amount  += $value->staff_pay_amount;
					$value->total_amount  = number_format(str_replace(',', '', ($value->staff_pay_amount+$value->holiday_pay_amount)) ,2);

					if ($employmentType) {
						if ($employmentType->id == $value->employment_types_id) {
							$holidaypay_amount  += $value->holiday_pay_amount;
						}
					}
				}

				$grand_total_hours     += $total_hours;
				$grand_client_amount   += $client_amount;
				$grand_staff_amount    += $staff_amount;
				$grand_holidaypay_amount    += $holidaypay_amount;
			}

			return array(
				'grand_total_hours'  => number_format(str_replace(',', '', $grand_total_hours) ,2),
				// 'grand_client_amount' => number_format(str_replace(',', '', $grand_client_amount) ,2),
				'gross_pay'  => number_format(str_replace(',', '', $grand_staff_amount) ,2),
				'holiday_accumulation'  => number_format(str_replace(',', '', $grand_holidaypay_amount) ,2),
				'grand_total_amount'  => number_format(str_replace(',', '', ($grand_staff_amount+$grand_holidaypay_amount)) ,2),
				'timesheets' 		  => $final,
			);
		} else {
			return [];
		}
           
	}
}
