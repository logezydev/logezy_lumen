<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class Timeclock extends Model
{
    protected $table = 'timeclocks';
    // protected $casts = [
    //     'start_time' => 'date:h:i A', 'end_time'   => 'date:h:i A'
    // ];
    

    /**
     * [timeclock Relation].
     */
    public function details()
    {
        return $this->hasMany('App\Tenants\TimeclockDetail', 'timeclock_id')->orderBy('id', 'ASC');
    }
}
