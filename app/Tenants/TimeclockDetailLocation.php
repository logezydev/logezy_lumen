<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class TimeclockDetailLocation extends Model
{
    protected $table = 'timeclock_detail_locations';
    // protected $casts = [
    //     'start_time' => 'date:h:i A', 'end_time'   => 'date:h:i A'
    // ];
}
