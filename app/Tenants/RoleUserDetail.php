<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class RoleUserDetail extends Model
{
    public $table = 'role_users_details';
}
