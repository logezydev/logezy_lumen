<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Carbon\Carbon;
use App\User;
use App\Tenants\CustomHoliday;
use App\Tenants\Holiday;
use App\Tenants\Vacancy;
use App\Tenants\VacancyStaffRate;
use App\Tenants\CandidateRateCard;
use App\Tenants\AssignVacancy;
use App\Tenants\AgencySetting;
use App\Tenants\AssignVacancyDetail;
use App\Tenants\VacancyDetail;
use App\Tenants\BusinessUnit;
use App\Tenants\Candidate;
use App\Tenants\Job;
use App\Tenants\Day;
use App\Tenants\CandidateClocking;
use App\Tenants\CandidateClockingTime;


class AssignVacancy extends Model
{
    
    protected $table = 'assign_vacancies';

    protected $fillable = ['code'];

    public $appends = ['business_unit_id', 'business_unit', 'business_unit_name', 'business_unit_email','job_id', 'on_day', 'client_rate', 'client_pay_amount',  'candidate_name', 'client_id', 'client' , 'job_title', 'shift_type', 'job_color', 'signed_timesheet_id', 'display_staff_rate', 'booked_by' , 'invoice_creation_period', 'business_unit_address', 'clocking_id', 'clocking_log_status'];

    /**
     * Setting accessors for start time
     */
    public function getStartTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("Y-m-d H:i", strtotime($value) );
        } else {
            return date("Y-m-d h:i A", strtotime($value) );
        }
    }

    /**
     * Setting accessors for end time
     */
    public function getEndTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("Y-m-d H:i", strtotime($value) );
        } else {
            return date("Y-m-d h:i A", strtotime($value) );
        }
    }

    /**
     * Setting accessors for notes
     */
    public function getNotesAttribute($value)
    {
        $databaseName = substr(config('database.connections.mysql.database'), 7);
        //enabling BU extra notes for old candidate app
        $enabledBuNotesAgencies = [
            'annicare',
            'medsolve_healthcare'
        ];
        if (in_array($databaseName, $enabledBuNotesAgencies)) {
            if($this->business_unit->description) {
              return strip_tags($this->business_unit->description.' - '.$value);
            }
        }
        return $value;
    }

    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getCandidateNameAttribute() {
        // $candidate = Candidate::find($this->candidate_id);
        // if( !empty($candidate->middle_name) ){
        //     return $candidate->first_name.' '.$candidate->middle_name.' '.$candidate->last_name;
        // }else{
        //     return $candidate->first_name.' '.$candidate->last_name;
        // }
        $candidate = DB::table('candidates')->where('id', $this->candidate_id)->select('id','full_name')->first();
        return $candidate->full_name;
    }

    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitAttribute() {

        $business_unit_id = Vacancy::find($this->vacancy_id)->business_unit_id;
        return BusinessUnit::find($business_unit_id, ['id', 'client_id', 'business_unit_name', 'formatted_number', 'email', 'address_line', 'latitude', 'longitude', 'description']);
    }

    /**
     * Gets the business unit address attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitNameAttribute() {

        $business_unit_id = Vacancy::find($this->vacancy_id)->business_unit_id;
        return BusinessUnit::find($business_unit_id)->business_unit_name;
    }
    public function getBusinessUnitAddressAttribute() {

        $business_unit_id = Vacancy::find($this->vacancy_id)->business_unit_id;
        return BusinessUnit::find($business_unit_id)->address_line;
    }
    public function getBusinessUnitEmailAttribute() {

        $business_unit_id = Vacancy::find($this->vacancy_id)->business_unit_id;
        return BusinessUnit::find($business_unit_id)->email;
    }
    /**
     * Gets the Client attribute.
     *
     * @return     <type>  Client attribute.
     */
    public function getClientAttribute() {
        $business_unit_id = Vacancy::find($this->vacancy_id)->business_unit_id;
        return Client::find(BusinessUnit::find($business_unit_id)->client_id)->client_name ;
    }
    /**
     * Gets the Client attribute.
     *
     * @return     <type>  Client id attribute.
     */
    public function getClientIdAttribute() {
        $business_unit_id = Vacancy::find($this->vacancy_id)->business_unit_id;
        return BusinessUnit::find($business_unit_id)->client_id;
    }
    /**
     * Get the invoice creation period
     */
    public function getInvoiceCreationPeriodAttribute () {

        $business_unit_id = Vacancy::find($this->vacancy_id)->business_unit_id;
        return BusinessUnit::find($business_unit_id)->invoice_creation_period;

    }
    /**
     * Gets the business unit id attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitIdAttribute() {

        return Vacancy::find($this->vacancy_id)->business_unit_id;
    }
    /**
     * Gets the Shift id attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getShiftIdAttribute() {

        return Vacancy::find($this->vacancy_id)->shift_id;
    }

    /**
     * Gets the shift attribute.
     *
     * @return     <type>  The shift attribute.
     */
    public function getShiftTypeAttribute() {
        $shift_id = Vacancy::find($this->vacancy_id)->shift_id;
        return Shift::find($shift_id)->shift_name;
    }
    /**
     * Gets the Job id attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getJobIdAttribute() {

        return Vacancy::find($this->vacancy_id)->job_id;
    }

    /**
    *is_timesheet_assigned?
    */
    public function getSignedTimesheetIDAttribute() {
        $signed = SignedTimesheet::where('assigned_vacancy_id', $this->id)->first();
        if ($signed){
            return $signed->id;
        }
        return '';
    }
    public function getDisplayStaffRateAttribute() {
        $assignedCount = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->count();
        $assignedDetail = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->first();
        return $assignedCount > 1 ? '---' : ($assignedDetail ? $assignedDetail->staff_rate : 0);
    }
    // /**
    //  * Gets the Details attribute.
    //  *
    //  */
    // public function getDetailsAttribute() {
    //     $assignedCount = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->count();
    //     $assignedDetail = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->first();

    //     return array([
    //         "id"=> $assignedDetail ? $assignedDetail->id : 0,
    //         "start_time"=> $this->start_time,
    //         "end_time"=> $this->end_time,
    //         "staff_rate"=> $assignedCount > 1 ? '---' : $assignedDetail ? $assignedDetail->staff_rate : 0,
    //         "is_timesheet_assigned"=> $assignedDetail ? $assignedDetail->is_timesheet_assigned : false
    //     ]);

    // }

    /**
     * Gets the total hours attribute.
     */
    // public function getTotalHoursAttribute()
    // {
    //     $total = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->sum('total_hours');
    //     // $total = 0;
    //     // if ($details) {
    //     //     foreach ($details as $detail) {
    //     //         $total += $detail->total_hours;
    //     //     }
    //     // }

    //     return str_replace(',', '', number_format($total, 2));
    // }

    /**
     * Gets the client rate attribute.
     */
    public function getClientRateAttribute()
    {
        $total = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->sum('client_rate');
        // $total = 0;
        // if ($details) {
        //     foreach ($details as $detail) {
        //         $total += $detail->client_rate;
        //     }
        // }

        return str_replace(',', '', number_format($total, 2));
    }

    /**
     * Gets the client rate attribute.
     */
    public function getStaffRateAttribute()
    {
        $total = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->sum('staff_rate');
        // $total = 0;
        // if ($details) {
        //     foreach ($details as $detail) {
        //         $total += $detail->staff_rate;
        //     }
        // }

        return str_replace(',', '', number_format($total, 2));
    }

    /**
     * Gets the client rate attribute.
     */
    public function getClientPayAmountAttribute()
    {
        $total = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->sum('client_pay_amount');
        // $total = 0;
        // if ($details) {
        //     foreach ($details as $detail) {
        //         $total += $detail->client_pay_amount;
        //     }
        // }

        return str_replace(',', '', number_format($total, 2));
    }

    /**
     * Gets the client rate attribute.
     */
    public function getStaffPayAmountAttribute()
    {
        $total = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->sum('staff_pay_amount');
        // $total = 0;
        // if ($details) {
        //     foreach ($details as $detail) {
        //         $total += $detail->client_pay_amount;
        //     }
        // }

        return str_replace(',', '', number_format($total, 2));
    }

    /**
     * Gets the booked by attribute.
     *
     * @return     <type>  The booked by attribute.
     */
    public function getBookedByAttribute() {
        $user = User::find($this->created_by);
        return $user ? $user->name : '';
    }

    /**
     * Get On Day
     *
     * @return     <type>  On date attribute.
     */
    public function getOnDayAttribute() {
        $date = strtotime($this->on_date);
        return $date = date('l', $date);
    }

    /**
     * Get Job Title
     *
     * @return     <type>  On date attribute.
     */
    public function getJobTitleAttribute() {
        $job_id = Vacancy::find($this->vacancy_id)->job_id;
        return Job::find($job_id)->name;
    }

    /**
     * Get Job color
     *
     * @return     <type>  On date attribute.
     */
    public function getJobColorAttribute() {
        $job_id = Vacancy::find($this->vacancy_id)->job_id;
        return Job::find($job_id)->color;
    }


    /**
     * [vacancy relation]
     */
    public function vacancy() {
        return $this->belongsTo('App\Tenants\Vacancy', 'vacancy_id');
    }

    /**
     * [candidate relation]
     */
    public function candidates() {
        return $this->belongsTo('App\Tenants\Candidate', 'candidate_id');
    }

    /**
     * [assign vacancy details relation].
     */
    public function assignedDetails()
    {
        return $this->hasMany('App\Tenants\AssignVacancyDetail', 'assigned_vacancy_id');
    }

    /**
     * [assign vacancy details relation].
     */
    public function timeclockDetails()
    {
        return $this->hasMany('App\Tenants\Timeclock', 'assigned_vacancy_id');
    }

    // /**
    //  * Gets the start attribute.
    //  *
    //  * @return <type> The start attribute
    //  */
    // public function getStartTimeAttribute()
    // {
    //     $start = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->first()->start_time;

    //     return date('Y-m-d H:i:s', strtotime($start));
    // }

    // /**
    //  * Gets the end attribute.
    //  *
    //  * @return <type> The end attribute
    //  */
    // public function getEndTimeAttribute()
    // {
    //     $end = AssignVacancyDetail::where('assigned_vacancy_id', $this->id)->latest('id')->first()->end_time;

    //     return date('Y-m-d H:i:s', strtotime($end));
    // }
    /**
     * Gets the clocking_id.
     */
    public function getClockingIDAttribute() {
        // $clockingID = CandidateClocking::where('candidate_id', $this->candidate_id)->where('vacancy_id', $this->vacancy_id)->where('assigned_vacancy_id', $this->id)->first();
        // if ($clockingID) {
        //     return $clockingID->id;
        // } else {
            return null;
        // }
        
    }
    /**
     * Gets the clocking_log_status.
     */
    public function getClockingLogStatusAttribute() {
        // $clockingID = CandidateClocking::where('candidate_id', $this->candidate_id)->where('vacancy_id', $this->vacancy_id)->where('assigned_vacancy_id', $this->id)->first();
        // if ($clockingID) {
        //     return CandidateClockingTime::where('candidate_clocking_id', $clockingID->id)->orderBy('clocking_time', 'desc')->first()->log_status;
        // } else {
            return null;
        // }
    }
    
    /**
     * Assign candidate 
     * @param Request $request
     * 
     * @return void
     */
    public static function assign_candidate($request){
        
        $vacancy = DB::table('vacancies')->find($request->vacancy_id);

        //getting staff rate on employment id
        $candidate = Candidate::where('id', $request->candidate_id)->first();
        

        if ($vacancy && ($vacancy->total_numbers >= $vacancy->used_count + 1)) {
            $assign = new AssignVacancy();
            $assign->vacancy_id = $request->vacancy_id;
            $assign->candidate_id = $request->candidate_id;
            if (isset($request->start_time) || isset($request->start_time)) {
                //checking for night shift
                $start = date("H:i", strtotime($request->start_time));
                $end = date("H:i", strtotime($request->end_time));
                $startDate = $request->on_date;
                if ($start >= $end) {
                    $endDate = date('Y-m-d', strtotime('+1 day', strtotime($request->on_date)));
                } else {
                    $endDate = $request->on_date;
                }
                //calculate total hours
                $inputs = array(
                    'on_date'    => $request->on_date,
                    'start_time' => $request->start_time,
                    'end_time'   => $request->end_time,
                    'break'      => isset($request->break) ? $request->break : $vacancy->break,
                );
                $totalHours = Helper::getTotalHours($inputs);
                $assign->start_time          = date( "Y-m-d H:i:s", strtotime($startDate .' '.$request->start_time)) ?? null;
                $assign->end_time            = date( "Y-m-d H:i:s", strtotime($endDate .' '.$request->end_time)) ?? null;
                $assign->total_hours         = $totalHours;
            } else {
                $assign->start_time          = date( "Y-m-d H:i:s", strtotime($vacancy->start_time)) ?? null;
                $assign->end_time            = date( "Y-m-d H:i:s", strtotime($vacancy->end_time)) ?? null;
                $assign->total_hours         = $vacancy->total_hours;
            }
            $assign->break              = isset($request->break) ? $request->break : $vacancy->break;
            $assign->on_date            = date( "Y-m-d", strtotime($vacancy->on_date));
            $assign->status             = $request->status;
            $assign->notes              = $request->notes??$vacancy->notes;
            $assign->created_by         = Auth::id();
            $assign->is_publish         = isset($request->is_publish) ? $request->is_publish : 0;
            $assign->po_number          = $vacancy->po_number ? $vacancy->po_number : '';
            //getting holiday pay by business unit id and job id from vacancy
            $holidayPay = getHolidayPayByBuAndJob($vacancy);
            if($holidayPay){
                $assign->holiday_pay_percentage = $holidayPay->holiday_pay_percentage;
                $assign->holiday_pay_rate       = $holidayPay->holiday_pay_amount;
                $assign->holiday_pay_frequency  = $holidayPay->holiday_pay_frequency;
                //checking frequency
                if ($holidayPay->holiday_pay_frequency == "flat") {
                    $assign->holiday_pay_amount     = $holidayPay->holiday_pay_amount;
                } else {
                    $assign->holiday_pay_amount     = $assign->total_hours * $holidayPay->holiday_pay_amount;
                }
                $assign->holiday_pay_status     = 'due';
                $assign->holiday_pay_updated_at = date("Y-m-d H:i:s");
            }
            
            $assign->save();
            // $booking_code = 'BKIN'.$assign->id;
            //custom booking code
            $booking_code_setting = AgencySetting::where('settings_key', 'booking_code_format')->first();
            $booking_code_format  = 'BKIN';
            if($booking_code_setting){
                $booking_code_format = !empty($booking_code_setting->settings_value) ? $booking_code_setting->settings_value : 'BKIN';
            }
            $booking_code = $booking_code_format.$assign->id;

            $assign->update(['code' => $booking_code]);

            // Update the vacancy table used_count if status is 'pending'
            if ($assign->status == 'pending') {
                $currentVacancy = Vacancy::find($request->vacancy_id);
                if($currentVacancy->total_numbers >= $currentVacancy->used_count+1){
                    $currentVacancy->used_count = $currentVacancy->used_count + 1;
                    $currentVacancy->save();
                }
            }               

            //creating details table
            if (isset($assign->id)) {
                $details = [];
                $details = VacancyDetail::where('vacancy_id', $vacancy->id)->orderBy('id', 'asc')->get();
                if (count($details) > 0) {
                    $previousValue = null;
                    foreach ($details as $detail) {
                        $vacancydetails = DB::table('vacancy_details')->where('id', $detail->id)->first();
                        $staff_rate = VacancyStaffRate::where('employment_type_id', $candidate->employment_types_id)
                                        ->where('vacancy_id', $request->vacancy_id)
                                        ->where('vacancy_detail_id', $detail->id)
                                        ->first();
                        $staff_rate_amount = (!$staff_rate) ? 0 : $staff_rate->staff_rate;
                        $candidate_staff_rate = 0;
                        if ($detail->day_id != 8) {
                            $candidateRateCards = CandidateRateCard::where('business_unit_id', $vacancy->business_unit_id)
                                        ->where('job_id', $vacancy->job_id)
                                        ->where('day_id', $detail->day_id)
                                        ->where('shift_id', $detail->shift_id)
                                        // ->where('start_time', '<', date( "H:i:s", strtotime($vacancydetails->end_time)) )
                                        // ->where('end_time', '>', date( "H:i:s", strtotime($vacancydetails->start_time)) )
                                        ->where('employment_type_id', $candidate->employment_types_id)
                                        ->where('candidate_id', $request->candidate_id)
                                        ->get();  
                            $candidate_rate = '';
                            if (count($candidateRateCards)>0) {
                                foreach ($candidateRateCards as $rateCard) {
                                    $start = date("H:i", strtotime($rateCard->start_time));
                                    $end = date("H:i", strtotime($rateCard->end_time));
                                    // $date = date('Y-m-d', strtotime($vacancy->on_date));
                                    $week = date("W", strtotime($vacancy->on_date));
                                    if($previousValue) {
                                      $week = $previousValue>$rateCard->day_id ? $week+1 : $week;
                                    }
                                    $year = date("Y", strtotime($vacancy->on_date));
                                    $date = date('Y-m-d', strtotime($year."W".$week.$rateCard->day_id));
                                    if ($start >= $end) {
                                        $startDate = $date;
                                        $endDate = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                                    }
                                    else {
                                        $startDate = $date;
                                        $endDate = $date;
                                    }
                                    if ( strtotime($startDate.' '.$rateCard->start_time) < strtotime($vacancydetails->end_time) && strtotime($vacancydetails->start_time) < strtotime($endDate.' '.$rateCard->end_time) ) {
                                        $candidate_rate = $rateCard;
                                    }
                                } 
                            }
                            $candidate_staff_rate = $candidate_rate == '' ? 0 : $candidate_rate->staff_rate;
                            $previousValue = $detail->day_id;
                        }
                        //check whether the rate is flat or hourly
                        if ($detail->rate_type == "flat_rate") {
                            $client_pay_amount   = !empty($detail->client_rate) ? $detail->client_rate : 0;
                            // $staff_pay_amount    = $staff_rate ? $staff_rate->staff_rate : 0;
                            $staff_pay_amount    = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
                        } else {
                            $client_pay_amount   = ($detail->total_hours) * (!empty($detail->client_rate) ? $detail->client_rate : 0);
                            // $staff_pay_amount    = ($detail->total_hours) * ($staff_rate ? $staff_rate->staff_rate : 0);
                            $staff_pay_amount    = ($detail->total_hours) * (($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount);
                        }

                        $assign_details = new AssignVacancyDetail();
                        $assign_details->assigned_vacancy_id = $assign->id;
                        $assign_details->start_time          = date( "Y-m-d H:i:s", strtotime($vacancydetails->start_time)) ?? null;
                        $assign_details->end_time            = date( "Y-m-d H:i:s", strtotime($vacancydetails->end_time)) ?? null;
                        $assign_details->total_hours         = $detail->total_hours;
                        $assign_details->rate_type           = $detail->rate_type;
                        $assign_details->client_rate         = !empty($detail->client_rate) ? $detail->client_rate : 0;
                        $assign_details->staff_rate          = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
                        $assign_details->client_pay_amount   = $client_pay_amount;
                        $assign_details->staff_pay_amount    = $staff_pay_amount;
                        $assign_details->break               = $detail->break;
                        $assign_details->save();
                    }
                } else {
                    $staff_rate = VacancyStaffRate::where('employment_type_id', $candidate->employment_types_id)
                                    ->where('vacancy_id', $request->vacancy_id)
                                    ->first();
                    $staff_rate_amount = (!$staff_rate) ? 0 : $staff_rate->staff_rate;

                    $day_id = Day::where('day', date('l',strtotime($vacancy->on_date)))->first()->id;  
                    //#check for custom holiday
                    $custom_holiday = CustomHoliday::where('business_unit_id', $vacancy->business_unit_id)->where('holiday_date', $vacancy->on_date)->where('is_deleted',0)->first();
                    //#check for holiday
                    $holiday = Holiday::where('on_date', $vacancy->on_date)->where('is_deleted',0)->first();  
                    $candidate_staff_rate = 0;
                    //checking for holidays, if yes then taking holiday rates. No , then taking candidate rates
                    if (!$custom_holiday && !$holiday) {                
                        //getting Individual staff rate
                        $candidate_rate = CandidateRateCard::where('business_unit_id', $vacancy->business_unit_id)
                                            ->where('job_id', $vacancy->job_id)
                                            ->where('day_id', $day_id)
                                            ->where('shift_id', $vacancy->shift_id)
                                            ->where('employment_type_id', $candidate->employment_types_id)
                                            ->where('candidate_id', $request->candidate_id)
                                            ->first();  
                        $candidate_staff_rate = (!$candidate_rate) ? 0 : $candidate_rate->staff_rate;
                    }
                    //check whether the rate is flat or hourly
                    if ($vacancy->rate_type == "flat_rate") {
                        $client_pay_amount   = !empty($vacancy->client_rate) ? $vacancy->client_rate : 0;
                        $staff_pay_amount    = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
                    } else {
                        $client_pay_amount   = ($vacancy->total_hours) * (!empty($vacancy->client_rate) ? $vacancy->client_rate : 0);
                        $staff_pay_amount    = ($vacancy->total_hours) * (($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount);
                    }

                    $assign_details = new AssignVacancyDetail();
                    $assign_details->assigned_vacancy_id = $assign->id;
                    $assign_details->start_time          = $vacancy->start_time;
                    $assign_details->end_time            = $vacancy->end_time;
                    $assign_details->total_hours         = $vacancy->total_hours;
                    $assign_details->rate_type           = $vacancy->rate_type;
                    $assign_details->client_rate         = !empty($vacancy->client_rate) ? $vacancy->client_rate : 0;
                    $assign_details->staff_rate          = ($candidate_staff_rate != 0) ? $candidate_staff_rate : $staff_rate_amount;
                    $assign_details->client_pay_amount   = $client_pay_amount;
                    $assign_details->staff_pay_amount    = $staff_pay_amount;
                    $assign_details->break               = $vacancy->break;
                    $assign_details->save();
                }
                
                //Updating Candidate notification
                $todayDate = date("Y-m-d");
                // if($assign->status == 'pending' && $assign->is_publish == 1 && $vacancy->on_date >= $todayDate){
                //     $exists = Notification::where('notifiable_id', $candidate->user_id)
                //                 ->where('role_id', 6)
                //                 ->where('object_id', $assign->id)
                //                 ->first();
                //     if (empty($exists)){
                //         Notification::candidate_shift_aasign($candidate, $assign, $currentVacancy);
                //     } else if ($exists) {
                //         $data = json_decode($exists->data);
                //         if($data->type != "candidate_shift_assigned") {
                //             Notification::candidate_shift_aasign($candidate, $assign, $currentVacancy);
                //         }
                //     }
                // }
            }

            // if((session('role') == 'candidate') && $assign->status == 'applied'){
        
            //     $candidate_name  = $candidate->full_name;
            //     $agency_email    = Agency::first()->email;
            //     $user_ids        = RoleUser::whereIn('role_id', [3,4])->pluck('user_id')->all();
            //     $user_ids        = array_unique($user_ids);
            //     $notify_array    = array(
            //         'title'      => $candidate_name,
            //         'type'       => "vacancy_applied",
            //         'body'       => "applied for a vacancy on ".date("d-m-Y",strtotime($vacancy->on_date))." with Ref.No: ".$vacancy->code.". in ".$vacancy->business_unit,
            //     );
            //     if ($user_ids) {
            //         foreach ($user_ids as $user_id) {
            //             $notification                   = new Notification;
            //             $notification->type             = 'App\Notifications\HelloNotification';
            //             $notification->notifiable_type  = 'App\User';
            //             $notification->notifiable_id    = $user_id;
            //             $notification->data             = json_encode($notify_array);
            //             $notification->save();
            //         }
            //     }
            // }
            
            return $assign;
        }

        return [];

    }

    /**
     * [Validations before assign candidates].
     */
    public static function assignValidations($inputs) {
        
        $startDate = $inputs['startDate'];
        $endDate   = $inputs['endDate'];

        $candidateDetails = DB::table('candidates')->find($inputs['candidate_id']); 
        $agency = DB::table('agencies')->first(); 

        //Checks for the candidate unavailability
        $unavailability = CandidateAvailability::where('candidate_id', $inputs['candidate_id'])->where('date', $inputs['on_date'])->where('availability', 'U')->first();
        if ($unavailability && $agency->shift_restriction_unavailable_candidate == 1) {
            return $candidateDetails->full_name.' is not available for the shift on '.(date('d-m-Y', strtotime($inputs['on_date'])));
        }

        //Consecutive shift restriction
        if($agency->restrict_consecutive_shift == 1) {
            $start_time = date('Y-m-d H:i:s', strtotime($inputs['startDate'].' '.$inputs['start_time']));
            $end_time = date('Y-m-d H:i:s', strtotime($inputs['endDate'].' '.$inputs['end_time']));

            $oneHourAgo= date('Y-m-d H:i:s', strtotime($start_time.' -'.$agency->consecutive_hours.' hour'));
            $oneHourAfter= date('Y-m-d H:i:s', strtotime($end_time.' +'.$agency->consecutive_hours.' hour'));
            $vacStartTime= date('Y-m-d H:i:s', strtotime($start_time));
            $vacEndTime= date('Y-m-d H:i:s', strtotime($end_time));
            $oneHourAgoShifts = DB::table('assign_vacancies')
                        ->whereIn('status', ['pending', 'approved', 'invoiced'])
                        ->where('is_deleted', 0)
                        ->whereBetween('end_time', [$oneHourAgo, $vacStartTime])
                        ->where('candidate_id', $inputs['candidate_id'])
                        ->pluck('id')->all();
            if ($oneHourAgoShifts) {
                return $candidateDetails->full_name." has consecutive shifts. Please contact the agency";
            }
            $oneHourAfterShifts = DB::table('assign_vacancies')
                        ->whereIn('status', ['pending', 'approved', 'invoiced'])
                        ->where('is_deleted', 0)
                        ->whereBetween('start_time', [$vacEndTime, $oneHourAfter])
                        ->where('candidate_id', $inputs['candidate_id'])
                        ->pluck('id')->all();
            if ($oneHourAfterShifts) {
                return $candidateDetails->full_name." has consecutive shifts. Please contact the agency";
            }
        }
        
        //Shift restriction
        $shift = DB::table('shifts')->where('id', $inputs['shift_id'])->first();
        if($shift) {
            $shiftID = DB::table('shifts')->where('business_unit_id', 0)->where('shift_name', $shift->shift_name)->first()->id;
            if($shiftID) {
                $shiftRestriction = DB::table('candidate_restricted_shifts')->where('candidate_id', $inputs['candidate_id'])->where('shift_id', $shiftID)->first();
                if ($shiftRestriction) {
                    return $candidateDetails->full_name." is restricted for ".$shift->shift_name." shift.";
                }
            }
        }

        //Job match checking
        $candidateJobs = DB::table('candidate_jobs')->where('job_id', $inputs['job_id'])->where('candidate_id', $inputs['candidate_id'])->first();
        if (!$candidateJobs) {
            return $candidateDetails->full_name."'s designations doesn't match with the job.";
        }

        // Checks for restricted business unit and candidate
        $restriction = DB::table('candidate_restricted_business_units')->where('candidate_id', $inputs['candidate_id'])->where('business_unit_id', $inputs['business_unit_id'])->first();
        if ($restriction) {
            return $candidateDetails->full_name." is restricted for the selected business unit.";
        }

        // checks for weekly wroking hours
        $assigned_hours = AssignVacancy::check_weekly_working_hours_exceeds_or_not($inputs['on_date'], $inputs['candidate_id'], $inputs['total_hours']);
        if ($assigned_hours == "true") {
            return "Weekly working hours exceeding for ".$candidateDetails->first_name.", Please check the total working hours.";
        }

        // Check if the candidate is already assigned or not
        $existing = AssignVacancy::check_existing_assign_vacancies($inputs['shift_id'], $inputs['on_date'], $startDate.' '.$inputs['start_time'], $endDate.' '.$inputs['end_time'], $inputs['candidate_id']);
        if ($existing > 0) {
            return $candidateDetails->full_name." is already assigned.";
        }
        
        return "false";
         
    }


    /**
     * [check_existing_assign_vacancies]
     * @param  [type] $status [description]
     * @return [type]         [description]
     */
    public static function check_existing_assign_vacancies($shift, $date, $start, $end, $candidate_id) {
        $start_time = date('Y-m-d H:i:s', strtotime($start));
        $end_time   = date('Y-m-d H:i:s', strtotime($end));
        $date       = date('Y-m-d', strtotime($date));

        return $existings = DB::table('assign_vacancies')
                    // ->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
                    // ->join('assigned_vacancy_details', 'assigned_vacancy_details.assigned_vacancy_id', '=', 'assign_vacancies.id')
                    ->where('candidate_id', $candidate_id)
                    ->whereIn('status', ['pending', 'approved', 'invoiced'])
                    ->where('is_deleted', 0)
                    ->where('start_time', '<', $end_time)
                    ->where('end_time', '>', $start_time)
                    // ->where('vacancies.shift_id', $shift)
                    // ->where('vacancies.on_date', $date)
                    ->count();
    }

    /**
     * [check_weekly working hours exceeds or not]
     * @param  [type] $status [description]
     * @return [type]         [description]
     */
    public static function check_weekly_working_hours_exceeds_or_not($date, $candidate_id, $totalHours) {
        // finding week start and week end from assigning date
        $dateTimestamp = strtotime($date);
        $num = date("N",$dateTimestamp);

        if($num>1){
            $week_start_timestamp = strtotime("last monday",$dateTimestamp);
        }else{
            $week_start_timestamp = strtotime("monday",$dateTimestamp);
        }
        $week_start = date("Y-m-d", $week_start_timestamp);
        $week_end = date("Y-m-d", strtotime('+6 day',$week_start_timestamp));

        $assigned_hours = DB::table('assign_vacancies')
                    ->join('assigned_vacancy_details', 'assigned_vacancy_details.assigned_vacancy_id', '=', 'assign_vacancies.id')
                    ->select(DB::raw("SUM(assigned_vacancy_details.total_hours) as total_weekly_hours"))
                    ->whereBetween('assign_vacancies.on_date', [$week_start, $week_end])
                    ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                    ->where('assign_vacancies.is_deleted', 0)
                    ->where('assign_vacancies.candidate_id', $candidate_id)
                    ->get();
        $candidate_weekly_hours = Candidate::find($candidate_id)->weekly_hours;
        if ($candidate_weekly_hours != null) {
            if ($assigned_hours[0]->total_weekly_hours != null) {
                
                if ($candidate_weekly_hours >= ($assigned_hours[0]->total_weekly_hours+$totalHours) ) {
                    return 'false';
                } else {
                    return 'true';
                }
            }
            return 'false';
        }
        return 'false';
    }

    /**
     * assignVacancyDetail
     *
     * @return void
     */
    public function assignVacancyDetail() {
        return $this->hasMany('App\Tenants\AssignVacancyDetail', 'assigned_vacancy_id');
    }


}
