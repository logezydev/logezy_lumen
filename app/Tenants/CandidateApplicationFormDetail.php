<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CandidateApplicationFormDetail extends Model
{
    protected $table = 'candidate_application_form_data';

    protected $fillable = ['candidate_id', 'form_id', 'form_data', 'form_completed', 'updated_platform', 'created_by'];

    /**
     * boot
     *
     * @return void
     */
    // protected static function boot()
    // {
    //     parent::boot();
    //     static::saving(function (Model $query) {
    //         $query->created_by = Auth::user()->id ?? $query->created_by;
    //     });
    // }

    /**
     * candidate
     *
     * @return void
     */
    public function candidate()
    {
        return $this->belongsTo('App\Tenants\Candidate', 'candidate_id');
    }

    /**
     * form
     *
     * @return void
     */
    public function form()
    {
        return $this->belongsTo('App\Tenants\RecruitmentBuiltForm', 'form_id');
    }

    /**
     * getFormDataAttribute
     *
     * @return void
     */
    public function getFormDataAttribute($value)
    {
        $decode = json_decode($value);

        if (is_array($decode)) {
            if (empty($decode)) {
                return json_encode((object) []);
            }

            return $value;
        }

        return $value;
    }
}
