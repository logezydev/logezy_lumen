<?php

namespace App\Tenants;

use DB;
use Illuminate\Database\Eloquent\Model;

class Scheduler extends Model
{


    /**
     * Get AssignedVacancies
     *
     * @return     <type>  The schedule.
     */
    public static function fetchAssignedVacancies($request) {

        $shifts = isset($request->shift) ? Shift::whereIn("shift_name", $request->shift)->pluck('id')->all() : [];
        $business_units = isset($request->business_unit) ? $request->business_unit : [];
        $jobs = isset($request->job) ? $request->job : [];
        $availabilities = isset($request->availability) ? $request->availability : [];
        $publish = isset($request->publish) ? $request->publish : [];
        $date_from = isset($request->date_from) ? $request->date_from : '';
        $date_to = isset($request->date_to) ? $request->date_to : '';

        $name = isset($request->name) ? $request->name : '';
        
        $candidate_assignedShifts = [];
        $candidateIds = [];
        $sreachFlag = false;

        // $candidateIds = DB::table('candidates')->where('is_deleted',0)->where('candidate_status_id', 1)
        // ->orderBy('first_name')->pluck('id')->all();

        if ( ( !empty($shifts) || !empty($business_units) || !empty($jobs) || !empty($availabilities) || !empty($publish)) ) {
            $candidateIds = self::candidateSearch($request);
            $sreachFlag = true;
        }

        $candidates = DB::table('candidates')->where(function($query) use ($name, $candidateIds, $sreachFlag) {
            if ($sreachFlag) {
                $query->whereIn('candidates.id', $candidateIds )->get();
            }
            if ($name) {
                $query->where('candidates.full_name', 'like',  '%' . $name .'%')->get();
            }
        })
        ->where('candidates.is_deleted',0)->where('candidates.candidate_status_id', 1)
        ->select('candidates.id','candidates.full_name as name', 'candidates.weekly_hours', 'candidates.profile_img', 'candidates.formatted_mobile_number as mobile_number')
        ->orderBy('candidates.first_name')
        ->paginate(20);
        

        //collecting all candidates details like jobs, availabilities, and assigned shifts
		$candidateJobs = DB::table('candidate_jobs')->select('candidate_id', 'job_id')->get();
		$candidatejobNames = DB::table('jobs')->select('id', 'name', 'color')->get();
		$candidateAvailabilities = DB::table('candidate_availabilities')->whereBetween('date', [$date_from, $date_to])->get(['candidate_id', 'date', 'availability']);
		$candidateAssignedShifts = DB::table('assign_vacancies')
            ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
            ->join('vacancies', function($join) use ($shifts, $business_units, $jobs)
            {
                $join->on('vacancies.id', '=', 'assign_vacancies.vacancy_id');
                // if ($shifts) {
                //     $join->whereIn('vacancies.shift_id', $shifts);
                // }
                // if ($business_units) {
                //     $join->whereIn('vacancies.business_unit_id', $business_units);
                // }
                // if ($jobs) {
                //     $join->whereIn('vacancies.job_id', $jobs);
                // }
            })
            ->where(function($query) use ($publish, $name)
                {
                    // if ($publish) {
                    //     $query->whereIn('assign_vacancies.is_publish', $publish)->get();
                    // }
                    // if ($name) {
                    //     $query->where('candidates.full_name', 'like',  '%' . $name .'%')->get();
                    // }
                })
            // ->join('assigned_vacancy_details', 'assigned_vacancy_details.assigned_vacancy_id', '=', 'assign_vacancies.id')
            ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
            ->join('clients', 'business_units.client_id', '=', 'clients.id')
            ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
            ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
            ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id', 'vacancies.business_unit_id', 'vacancies.job_id', 'vacancies.shift_id', 'clients.client_name', 'business_units.business_unit_name', 'jobs.name AS job_name', 'jobs.color', 'shifts.shift_name', 'vacancies.on_date', DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 'assign_vacancies.break', 'assign_vacancies.total_hours', 'assign_vacancies.notes', 'assign_vacancies.is_publish', 'assign_vacancies.is_confirm', 'assign_vacancies.status')
            ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
            ->where('assign_vacancies.is_deleted', 0)
            ->whereBetween('assign_vacancies.on_date', [$date_from, $date_to])
            ->where('candidates.is_deleted',0)
            ->where('candidates.candidate_status_id', 1)
            ->orderBy('assign_vacancies.on_date')
            ->get();
            
        $jobIDs = collect($candidateJobs);
        $jobs = collect($candidatejobNames);
        $availabilities = collect($candidateAvailabilities);
        $assignedShifts = collect($candidateAssignedShifts);

        // $assignedShifts = collect($candidateAssignedShifts->getCollection())->groupBy('on_date');
        // foreach($assignedShifts as $key => $value){

        //     $candidate_assignedShifts[] = [
        //         'date'=> $key,
        //         'data'=> collect($value)->groupBy('candidate_id')->values()
        //     ];
        // }
        // return $candidate_assignedShifts;

        $candidates->map(function ($candidate, $k) use($jobIDs, $jobs, $availabilities, $assignedShifts) { 
			$candidate->job_ids = $jobIDs->whereIn('candidate_id', [$candidate->id])->pluck('job_id');
			$candidate->jobs = $jobs->whereIn('id', $candidate->job_ids)->values();
			$candidate->availabilities = $availabilities->whereIn('candidate_id', [$candidate->id])->values();
			$candidate->assigned_shifts = $assignedShifts->whereIn('candidate_id', [$candidate->id])->values();
			return $candidate; 
        });

        return $candidates;

        // if ($candidates) {
        //     foreach($candidates as $candidate) {
        //         //assigned shifts
        //         $assigned_shifts = DB::table('assign_vacancies')
        //         ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
        //         ->join('vacancies', function($join) use ($shifts, $business_units, $jobs)
        //          {
        //             $join->on('vacancies.id', '=', 'assign_vacancies.vacancy_id');
        //             if ($shifts) {
        //                 $join->whereIn('vacancies.shift_id', $shifts);
        //             }
        //             if ($business_units) {
        //                 $join->whereIn('vacancies.business_unit_id', $business_units);
        //             }
        //             if ($jobs) {
        //                 $join->whereIn('vacancies.job_id', $jobs);
        //             }
        //          })
        //         ->where(function($query) use ($publish)
        //             {
        //                 if ($publish) {
        //                     $query->whereIn('assign_vacancies.is_publish', $publish)->get();
        //                 }
        //             })
        //         ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
        //         ->join('clients', 'business_units.client_id', '=', 'clients.id')
        //         ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
        //         ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
        //         ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id', 'vacancies.business_unit_id', 'vacancies.job_id', 'vacancies.shift_id', 'clients.client_name', 'business_units.business_unit_name', 'jobs.name AS job_name', 'jobs.color', 'shifts.shift_name', 'vacancies.on_date', DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 'assign_vacancies.break', 'assign_vacancies.total_hours', 'assign_vacancies.notes', 'assign_vacancies.is_publish', 'assign_vacancies.is_confirm', 'assign_vacancies.status')
        //         ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
        //         ->where('assign_vacancies.is_deleted', 0)
        //         ->where('vacancies.on_date', $date)
        //         ->where('candidates.is_deleted',0)
        //         ->where('candidates.candidate_status_id', 1)
        //         ->where('assign_vacancies.candidate_id', $candidate->id)
        //         ->get();

        //         //availabilities
        //         $candidate_availabilities = DB::table('candidate_availabilities')
        //                     ->where(function($query) use ($availabilities)
        //                     {
        //                         if ($availabilities) {
        //                             $query->whereIn('availability', $availabilities);
        //                         }
        //                     })->where('date', $date)->where('candidate_id', $candidate->id)->pluck('availability');
        //         //job ids
        //         $jobIds = CandidateJob::where('candidate_id', $candidate->id)->pluck('job_id');
        //         //assigned total hours calculation
        //         $totalHours = 0;
        //         $assignTotalHrs = 0;
        //         //final array
        //         $candidate_assignedShifts[] = array(
        //             'id' => $candidate->id,
        //             'name' => $candidate->first_name.' '.$candidate->last_name,
        //             'weekly_hours' => $candidate->weekly_hours ?? 0,
        //             'mobile_number' => $candidate->formatted_mobile_number ?? '',
        //             'profile_img' => $candidate->profile_img,
        //             'jobs' => DB::table('jobs')->whereIn('id', $jobIds)->select('id', 'name','color')->get() ?? [],
        //             'assigned_shifts' => $assigned_shifts,
        //             'availabilities' => $candidate_availabilities,
        //         );


        //     }
        // }
        
        // return $candidate_assignedShifts;
    }



    /**
     * candidateSearch
     *
     * @return     <type>  candidate ids
     */
    public static function candidateSearch($request) {

        $shifts = isset($request->shift) ? Shift::whereIn("shift_name", explode( ',', $request->shift))->pluck('id')->all() : [];
        $business_units = isset($request->business_unit) ? explode( ',', $request->business_unit) : [];
        $jobs = isset($request->job) ? explode( ',', $request->job) : [];
        $availabilities = isset($request->availability) ? explode( ',', $request->availability) : [];
        $publish = isset($request->publish) ? explode( ',', $request->publish) : [];
        $assigned = isset($request->assigned) ? $request->assigned : '';
        $date_from = isset($request->date_from) ? $request->date_from : '';
        $date_to = isset($request->date_to) ? $request->date_to : '';
        
        // $datesArr = explode(",", $request->dates);
        // sort($datesArr);
        // $firstDate = reset($datesArr);
        // $lastDate = end($datesArr);
        $assigned_candidates = []; $available_candidates = [];

        if ($assigned == 0) {
            //finding unassigned candidates
            $assignedIDs = DB::table('assign_vacancies')
            ->whereIn('on_date', $datesArr)
            ->whereIn('status', ['pending', 'approved', 'invoiced'])
            ->where('is_deleted', 0)->pluck('candidate_id')->all();
            // $diffUnassigned = array_diff($candidates, $assignedIDs);
            // $candidateUnassigned = array_values($diffUnassigned);
            $unassigned_candidates = DB::table('candidates')->whereNotIn('id', $assignedIDs)->where('is_deleted',0)->where('candidate_status_id', 1)->pluck('id')->all();
        } else {
            if ( $shifts || $business_units || $jobs || $publish) {
            $assigned_candidates = DB::table('assign_vacancies')
                    ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                    ->join('vacancies', function($join) use ($shifts, $business_units, $jobs)
                        {
                        $join->on('vacancies.id', '=', 'assign_vacancies.vacancy_id');
                        if ($shifts) {
                            $join->whereIn('vacancies.shift_id', $shifts);
                        }
                        if ($business_units) {
                            $join->whereIn('vacancies.business_unit_id', $business_units);
                        }
                        if ($jobs) {
                            $join->whereIn('vacancies.job_id', $jobs);
                        }
                        })
                    ->where(function($query) use ($publish)
                        {
                            if ($publish) {
                                $query->whereIn('assign_vacancies.is_publish', $publish)->get();
                            }
                        })
                    ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                    ->where('assign_vacancies.is_deleted', 0)
                    ->whereBetween('assign_vacancies.on_date', [$date_from, $date_to])
                    ->where('candidates.is_deleted',0)
                    ->where('candidates.candidate_status_id', 1)
                    // ->where('vacancies.is_deleted', 0)
                    ->pluck('assign_vacancies.candidate_id')->all();
            }
        }
        
        if ( $availabilities ) {
        $available_candidates = DB::table('candidate_availabilities')
                    ->where(function($query) use ($availabilities)
                    {
                        if ($availabilities) {
                            $query->whereIn('availability', $availabilities);
                        }
                    })->whereBetween('date', [$date_from, $date_to])->pluck('candidate_id')->all();
        }

        $resultArr = array_merge($unassigned_candidates, $assigned_candidates, $available_candidates);

        return array_unique($resultArr);
    }


    /**
     *getBookings
     *
     * @return     <type>  The schedule.
     */
    public static function getBookings($request) {

        $shifts = isset($request->shift) ? Shift::whereIn("shift_name", $request->shift)->pluck('id')->all() : [];
        $business_units = isset($request->business_unit) ? $request->business_unit : [];
        $jobs = isset($request->job) ? $request->job : [];
        $availabilities = isset($request->availability) ? $request->availability : [];
        $publish = isset($request->publish) ? $request->publish : [];
        $date = isset($request->date) ? $request->date : '';

        $name = isset($request->name) ? $request->name : '';
        //assigned shifts
        $assigned_shifts = DB::table('assign_vacancies')
        ->join('candidates', function($join) use ($name)
         {
            $join->on('candidates.id', '=', 'assign_vacancies.candidate_id');
            if ($name) {
                $join->where('full_name', 'like',  '%' . $name .'%');
            }
         })
        ->join('vacancies', function($join) use ($shifts, $business_units, $jobs)
         {
            $join->on('vacancies.id', '=', 'assign_vacancies.vacancy_id');
            if ($shifts) {
                $join->whereIn('vacancies.shift_id', $shifts);
            }
            if ($business_units) {
                $join->whereIn('vacancies.business_unit_id', $business_units);
            }
            if ($jobs) {
                $join->whereIn('vacancies.job_id', $jobs);
            }
         })
        ->where(function($query) use ($publish)
            {
                if ($publish) {
                    $query->whereIn('assign_vacancies.is_publish', $publish)->get();
                }
            })
        // ->join('assigned_vacancy_details', 'assigned_vacancy_details.assigned_vacancy_id', '=', 'assign_vacancies.id')
        ->join('business_units', 'vacancies.business_unit_id', '=', 'business_units.id')
        ->join('clients', 'business_units.client_id', '=', 'clients.id')
        ->join('shifts', 'vacancies.shift_id', '=', 'shifts.id')
        ->join('jobs', 'vacancies.job_id', '=', 'jobs.id')
        ->select('assign_vacancies.id AS assign_id', 'candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'vacancies.business_unit_id', 'vacancies.job_id', 'vacancies.shift_id', 'clients.client_name', 'business_units.business_unit_name', 'jobs.name AS job_name', 'jobs.color', 'shifts.shift_name', 'vacancies.on_date', DB::raw('DATE_FORMAT(assign_vacancies.start_time, "'.dateTimeFormat().'") as start_time'),  DB::raw('DATE_FORMAT(assign_vacancies.end_time, "'.dateTimeFormat().'") as end_time'), 'assign_vacancies.break', 'assign_vacancies.total_hours', 'assign_vacancies.notes', 'assign_vacancies.is_publish', 'assign_vacancies.is_confirm', 'assign_vacancies.status')
        ->where('assign_vacancies.status', 'pending')
        ->where('assign_vacancies.is_deleted', 0)
        ->where('vacancies.on_date', '>=', date("Y-m-d"))
        ->where('candidates.is_deleted',0)
        ->where('candidates.candidate_status_id', 1)
        ->orderBy('vacancies.on_date', 'asc')
        ->get();
        
        return $assigned_shifts;
    }

}

