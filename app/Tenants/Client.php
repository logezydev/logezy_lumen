<?php

namespace App\Tenants;

use App\Tenants\BusinessUnit;
use App\Tenants\ClientJob;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    /*public function businessUnits() {
    	return $this->hasMany('App\Models\BusinessUnit');
    }*/

    public $appends = ['jobs', 'active_businessunits', 'inactive_businessunits'];

 	public function getJobsAttribute() {

 		$client_id = $this->id;
 		return ClientJob::where('client_id', $client_id)->pluck('job_id');
 	}

 	public function getActiveBusinessunitsAttribute() {

 		$client_id = $this->id;
 		return BusinessUnit::where('client_id', $client_id)->where('is_deleted', 0)->get();
 	}

 	public function getInactiveBusinessunitsAttribute() {

 		$client_id = $this->id;
 		return BusinessUnit::where('client_id', $client_id)->where('is_deleted', 1)->get();
 	}
}
