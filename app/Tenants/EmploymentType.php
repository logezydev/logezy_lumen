<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class EmploymentType extends Model
{
    protected $table = 'employment_types';
    public $appends = [ 'value', 'staff_rate', 'employment_type_id', 'employment_type'];

    public function getValueAttribute()
    {
        return '0.00';
    }

    public function getStaffRateAttribute()
    {
        return '0.00';
    }

    public function getEmploymentTypeIdAttribute()
    {
        return $this->id;
    }

    public function getEmploymentTypeAttribute()
    {
        return $this->title;
    }
}
