<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class AgencyNotificationSetting extends Model
{
    protected $table = 'agency_notification_settings';
}
