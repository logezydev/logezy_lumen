<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $table = 'candidates';

    protected $fillable = [
        'user_id', 'employment_types_id', 'candidate_status_id', 'first_name', 'middle_name', 'last_name', 'full_name', 'candidate_code', 'dob', 'place_of_birth', 'address_line1', 'address_line2', 'post_code', 'place', 'gender', 'mobile_prefix', 'mobile_number', 'formatted_mobile_number', 'phone_prefix', 'phone_number', 'passport_no', 'issued_country', 'nationality', 'ni_number', 'dbs_pvg_no', 'dbs_exp_date', 'dbs_state', 'nmc_nurse_type', 'nmc_pin_number', 'next_check_date', 'tax_code', 'payroll_id', 'p45_recieved', 'payroll_job_status', 'profile_img', 'candidate_status', 'created_by', 'is_available', 'is_deleted', 'has_access', 'history_logs'
    ];
    public $appends = ['email', 'employment_type_name', 'last_login', 'last_login_time'];

    protected $casts = [
        'history_logs' => 'array',
    ];

    public function getLastLoginAttribute()
    {
        $r_id = Role::where('name', 'candidate')->pluck('id')->first();
        $login = RoleUser::where('user_id', $this->user_id)->where('role_id', $r_id)->pluck('login_ip')->first();

        return $login;
    }

    public function getLastLoginTimeAttribute()
    {
        $r_id = Role::where('name', 'candidate')->pluck('id')->first();
        $login = RoleUser::where('user_id', $this->user_id)->where('role_id', $r_id)->pluck('login_at')->first();

        return $login;
    }

    /**
     * [candidate email].
     */
    public function getEmailAttribute()
    {
        
        $r_id = Role::where('name', 'candidate')->pluck('id')->first();
        $user = RoleUser::where('user_id', $this->user_id)->where('role_id', $r_id)->first();

        return isset($user) ? $user->email : null;
    }
    
    /**
     * getEmploymentTypeAttribute
     *
     * @return void
     */
    public function getEmploymentTypeNameAttribute()
    {
        $type = EmploymentType::where('id', $this->employment_types_id)->first();

        return $type->title ?? null; 
    }

    /*Jobs of the user*/
    public function jobs()
    {
        return $this->belongsToMany('App\Tenants\Job', 'candidate_jobs')
        ->withPivot('candidate_id', 'job_id')
        ->orderBy('name', 'asc');
    }

    /** User employment_type */
    public function employment_type()
    {
        return $this->belongsTo('App\Tenants\EmploymentType', 'employment_types_id');
    }

    /*Status relation*/
    public function status()
    {
        return $this->belongsTo('App\Tenants\CandidateStatus', 'candidate_status_id');
    }

    /*Status relation*/
    public function availabilities()
    {
        return $this->hasMany('App\Tenants\CandidateAvailability', 'candidate_id');
    }

    /**
     * candidate_clocking
     *
     * @return void
     */
    public function candidate_clocking()
    {
        return $this->hasOne('App\Tenants\CandidateClocking', 'id');
    }

    /**
     * candidateDocument
     *
     * @return void
     */
    public function candidateDocument()
    {
        return $this->hasMany('App\Tenants\CandidateDocument', 'candidate_id');
    }

    /**
     * candidateApplicationFormDetail
     *
     * @return void
     */
    public function candidateApplicationFormDetail()
    {
        return $this->hasMany('App\Tenants\CandidateApplicationFormDetail', 'candidate_id');
    }
}
