<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    public $table = 'role_users';

    protected $fillable = [
        'user_id', 'role_id',
    ];


    public $appends = ['role_name'];

    /**
     * Get the role name
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function getRoleNameAttribute() {
        return Role::find($this->role_id)->name;
    }
}
