<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;

class StaffRateCard extends Model
{
    protected $table = 'staff_rate_cards';

    public $appends = [ 'employment_type', 'rate_card'];


    public function ratecard()
    {
        return $this->belongsTO('App\Tenants\RateCard','rate_card_id');
    }
    public function employementtype()
    {
        return $this->belongsTo('App\Tenants\EmploymentType','employment_type_id');
    }
    public function getEmploymentTypeAttribute()
    {
        return EmploymentType::find($this->employment_type_id)->title;
    }
    public function getRateCardAttribute()
    {
        $rate_card_id = StaffRateCard::find($this->id)->rate_card_id;
        return RateCard::find($rate_card_id);
    }
}
