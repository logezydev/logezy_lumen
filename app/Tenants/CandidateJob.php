<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class CandidateJob extends Model
{
    protected $table = 'candidate_jobs';

    protected $fillable = [
        'candidate_id', 'job_id', 'status', 'is_deleted', 'created_by',
    ];
}
