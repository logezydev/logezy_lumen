<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class RecruitmentJob extends Model
{
	protected $table = 'recruitment_jobs';

	protected $fillable = ['job_code', 'job_id', 'title', 'description', 'address_line', 'latitude', 'longitude', 'status', 'is_deleted', 'created_by'];

	/**
     * appends.
     *
     * @var array
     */
    public $appends = ['name'];

	public function Job()
	{
		return $this->belongsTo('App\Tenants\Job', 'job_id');
	}

	/**
     * getNameAttribute.
     */
    public function getNameAttribute()
    {
        return $this->title;
    }
}
