<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;

class CandidateAvailability extends Model
{

    protected $table = 'candidate_availabilities';

	public $appends = ['candidate_name'];

    /**
     * Gets the candidate name.
     */
    public function getCandidateNameAttribute() {

        $candidate = Candidate::where('id',$this->candidate_id)->first();
        return $candidate->full_name;

    }

    /**
     * Gets the shift name.
     */
    // public function getShiftNameAttribute() {
        
    //     return $this->belongsTo('App\Tenants\Shift','shift_id')->pluck('shift_name')->first();
    // }
}
