<?php

namespace App\Tenants;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\Tenants\Candidate;
use App\Tenants\CandidateJob;
use App\Tenants\Vacancy;
use App\Tenants\AssignVacancy;


class Job extends Model
{
    protected $table = 'jobs';	
}

