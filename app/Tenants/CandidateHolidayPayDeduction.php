<?php

namespace App\Tenants;

use DB;
use App\Tenants\RoleUser;
use Illuminate\Database\Eloquent\Model;

class CandidateHolidayPayDeduction extends Model
{
    protected $table = 'candidate_holiday_pay_deductions';

    protected $fillable = ['type', 'candidate_id', 'requested_amount', 'deducted_date', 'amount_when_deducting', 'deducted_by', 'status', 'initiated_by'];

    protected $casts = ['created_at' => 'date:d-m-Y h:i A', 'updated_at' => 'date:d-m-Y h:i A', 'deducted_date' => 'date:d-m-Y h:i A'];

    public $appends = ['status_class'];

    /**
     * Setting accessors for status class
     */
    public function getStatusClassAttribute()
    {
        switch($this->status) {
            case "pending":
                return "badge-warning";
                break;
            case "approved":
                return "badge-success";
                break;
            case "rejected":
                return "badge-danger";
                break;
        }
    }

    /**
     * Setting accessors for DeductedBy
     */
    public function getDeductedByAttribute($value)
    {
        if ($value != NULL) {
            return RoleUser::where('user_id', $value)->first()->user_name;
        }
        return NULL;
    }

    /**
     * Setting accessors for InitiatedBy
     */
    public function getInitiatedByAttribute($value)
    {
        if ($value != NULL) {
            return RoleUser::where('user_id', $value)->first()->user_name;
        }
        return NULL;
    }
}
