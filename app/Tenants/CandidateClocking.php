<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class CandidateClocking extends Model
{

	protected $table = 'candidate_clocking';

	protected $fillable =
	[
		'candidate_id', 'vacancy_id', 'assigned_vacancy_id', 'working_hours', 'total_break', 'clock_in', 'clock_out', 'completed', 'created_by'
	];

	/**
	 * vacancy
	 * relation
	 * @return void
	 */
	public function vacancy()
	{
		return $this->belongsTo('App\Tenants\Vacancy', 'vacancy_id');
	}

	/**
	 * candidate
	 * relation
	 * @return void
	 */
	public function candidate()
	{
		return $this->belongsTo('App\Tenants\Candidate', 'candidate_id');
	}

	/**
	 * candidate_clocking_time
	 * relation
	 * @return void
	 */
	public function candidateClockingTime()
	{
		return $this->hasMany('App\Tenants\CandidateClockingTime', 'candidate_clocking_id');
	}
}
