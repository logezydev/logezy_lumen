<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Carbon\Carbon;
use App\User;
use App\Tenants\Vacancy;
use App\Tenants\AssignVacancy;
use App\Tenants\BusinessUnit;
use App\Tenants\Candidate;
use App\Tenants\Job;

class AssignVacancyDetail extends Model
{
    protected $table = 'assigned_vacancy_details';

    public $appends = ['on_date', 'is_timesheet_assigned'];

    /**
     * Get On Date
     *
     * @return     <type>  On date attribute.
     */
    public function getOnDateAttribute() {
        // $vacancy_id = AssignVacancy::find($this->assigned_vacancy_id)->vacancy_id;
        $date = AssignVacancy::find($this->assigned_vacancy_id)->on_date;
        $date = strtotime($date);
        return date('Y-m-d', $date);
    }

    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitAttribute() {
    	$vacancy_id = AssignVacancy::find($this->assigned_vacancy_id)->vacancy_id;
        $business_unit_id = Vacancy::find($vacancy_id)->business_unit_id;
        return BusinessUnit::find($business_unit_id);
    }
    /**
    *is_timesheet_assigned?
    */
    public function getIsTimesheetAssignedAttribute() {
        // $detailIds = AssignVacancyDetail::where('assigned_vacancy_id', $this->assigned_vacancy_id)->pluck('id')->all();
        // $signed = SignedTimesheet::whereIn('assigned_vacancy_details_id', $detailIds)->get();
        // if (count($signed) > 0){
        // 	return true;
        // } else {
        	return false;
        // }
    }

    /**
     * [Assign Vacancy Relation]
     */
    public function assignvacancy() {
        return $this->belongsTo('App\Tenants\AssignVacancy', 'assigned_vacancy_id');
    }
}
