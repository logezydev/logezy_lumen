<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class CandidateClockingTime extends Model
{

	protected $table = 'candidate_clocking_time';

	protected $fillable =
	[
		'candidate_clocking_id', 'clocking_time', 'log_status', 'created_by'
	];

     /**
	 * candidate_clocking
	 * relation
	 * @return void
	 */
	public function candidateClocking()
	{
		return $this->belongsTo('App\Tenants\CandidateClocking', 'id');
	}
}
