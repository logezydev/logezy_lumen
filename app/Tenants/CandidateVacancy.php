<?php

namespace App\Tenants;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class CandidateVacancy extends Model
{
    
    protected $table = 'vacancies';
    
    protected $fillable = ['business_unit_id', 'code', 'user_id', 
    'start_time', 'end_time', 'job_id', 'shift_id', 'break', 'on_date', 'status', 'total_numbers', 'notes'];


    public $appends = ['color','client','business_unit','business_unit_name','address_line', 'job_title', 'shift_type', 'assigned_count', 'total_space', 'space_left', 'applied', 'applied_count', 'published_candidates'];

    /**
     * Get all assigned vacancies
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function assignedVacancies()
    {
        return $this->hasMany('App\Tenants\AssignVacancy', 'vacancy_id');
    }

    /**
     * Setting accessors for start time
     */
    public function getStartTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("Y-m-d H:i", strtotime($value) );
        } else {
            return date("Y-m-d h:i A", strtotime($value) );
        }
    }

    /**
     * Setting accessors for end time
     */
    public function getEndTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("Y-m-d H:i", strtotime($value) );
        } else {
            return date("Y-m-d h:i A", strtotime($value) );
        }
    }
    
    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitNameAttribute() {

        return BusinessUnit::find($this->business_unit_id)->business_unit_name;
    }
 
    /**
     * Gets the color attribute.
     *
     * @return     <type>  The color attribute.
     */
    public function getColorAttribute() {
        return Job::find($this->job_id)->color;
    }

    /**
     * Gets the Space left attribute.
     *
     * @return     <type>  The shift attribute.
     */
    public function getSpaceLeftAttribute() {
        $assigned = DB::table('assign_vacancies')->where('vacancy_id', $this->id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');
        // $assignedDeleted = DB::table('assign_vacancies')->where('vacancy_id', $this->id)->where('is_deleted', 0)->where('is_moved_to_vacancies', 0)->where('status', 'pending')->count('id');
        // $used_count = (int)$assigned + (int)$assignedDeleted;
        return (int)$this->total_numbers-(int)$assigned;
    }

    /**
     * Gets the Assigned count attribute.
     *
     * @return     <type>  The shift attribute.
     */
    public function getAssignedCountAttribute() {
        $assigned = DB::table('assign_vacancies')->where('vacancy_id', $this->id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');
        // $assignedDeleted = DB::table('assign_vacancies')->where('vacancy_id', $this->id)->where('is_deleted', 0)->where('is_moved_to_vacancies', 0)->where('status', 'pending')->count('id');
        return (int)$assigned;
    }

    /**
     * [getAppliedCandidates]
     */
    public function getAppliedAttribute() {
        return $applied_candidate_array = DB::table('assign_vacancies')
                    ->select('id', 'candidate_id')
                    ->where('status', 'applied')
                    ->where('is_deleted', 0)
                    ->where('vacancy_id', $this->id)
                    ->get();
    }

    /**
     * [getAppliedCountAttribute description]
     * @return [type] [description]
     */
    public function getAppliedCountAttribute() {
        return $applied_array = DB::table('assign_vacancies')
                    ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                    ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
                    ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
                    ->where('assign_vacancies.status', 'applied')
                    ->where('assign_vacancies.is_deleted', 0)
                    ->where('candidates.candidate_status_id', 1)
                    ->where('assign_vacancies.vacancy_id', $this->id)
                    ->groupBy('assign_vacancies.id')
                    ->count();
    }
 
    /**
     * Gets the total_space attribute.
     *
     * @return     <type>  The color attribute.
     */
    public function getTotalSpaceAttribute() {
        return $this->total_numbers;
    }
    
    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitAttribute() {

        return BusinessUnit::find($this->business_unit_id, ['id', 'client_id', 'business_unit_name', 'formatted_number', 'email', 'address_line', 'latitude', 'longitude', 'description']);
    }

    /**
     * Gets the Client attribute.
     *
     * @return     <type>  Client attribute.
     */
    public function getClientAttribute() {

        return Client::find(BusinessUnit::find($this->business_unit_id)->client_id)->client_name ;
    }
    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getAddressLineAttribute() {

        return BusinessUnit::find($this->business_unit_id)->address_line;
    }

    /**
     * Gets the job title attribute.
     *
     * @return     <type>  The job title attribute.
     */
    public function getJobTitleAttribute() {
    	return Job::find($this->job_id)->name;
    }


    /**
     * Gets the shift attribute.
     *
     * @return     <type>  The shift attribute.
     */
    public function getShiftTypeAttribute() {
    	return Shift::find($this->shift_id)->shift_name;
    }


    /**
     * Undocumented function
     *
     * @return void
     */
    public function getBusinessUnitName() {

        return BusinessUnit::find($this->business_unit_id)->business_unit_name;
    }
    
    /**
     * Gets the Published Candidates.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getPublishedCandidatesAttribute() {

        if ($this->published_ids != NULL) {
            return explode(",", $this->published_ids);
        }
        return NULL;
    }


}

