<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;

class RateCard extends Model
{
    protected $table = 'rate_cards';

    public $appends = [ 'business_unit_name', 'client_id', 'client_name', 'day','custom_holiday', 'staff_rate_cards','shift','job'];

    public function jobs()
    {
        return $this->belongsTo('App\Tenants\Job','job_id');
    }

    public function days()
    {
        return $this->belongsTo('App\Tenants\Day','day_id');
    }

    public function shifts()
    {
        return $this->belongsTo('App\Tenants\Shift','shift_id');
    }
    public function businessunit()
    {
        return $this->belongsTo('App\Tenants\BusinessUnit','business_unit_id');
    }
    public function staffratecard()
    {
        return $this->hasMany('App\Tenants\StaffRateCard','rate_card_id');
    }
    
    /**
     * Setting accessors for start time
     */
    public function getStartTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("H:i", strtotime($value) );
        } else {
            return date("h:i A", strtotime($value) );
        }
    }

    /**
     * Setting accessors for end time
     */
    public function getEndTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("H:i", strtotime($value) );
        } else {
            return date("h:i A", strtotime($value) );
        }
    }

    
    /**
     * Gets the business unit name attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitNameAttribute() {

        return BusinessUnit::find($this->business_unit_id)->business_unit_name;
    }

    /**
     * Gets the Client id attribute.
     *
     * @return     <type>  Client attribute.
     */
    public function getClientIdAttribute() {

        return BusinessUnit::find($this->business_unit_id)->client_id ;
    }

    /**
     * Gets the Client name attribute.
     *
     * @return     <type>  Client attribute.
     */
    public function getClientNameAttribute() {

        return Client::find(BusinessUnit::find($this->business_unit_id)->client_id)->client_name ;
    }

    public function getDayAttribute()
    {
        return Day::find($this->day_id)->day;
    }

    public function getCustomHolidayAttribute()
    {
        if(!empty($this->custom_holiday_id)) {
            $custom_holiday = CustomHoliday::find($this->custom_holiday_id);
            return $custom_holiday->description.' ('. date("d-m-Y", strtotime($custom_holiday->holiday_date)).' )';
        }
        return null;
    }

    public function getStaffRateCardsAttribute() {
        
        $rate_card_id = $this->id;
        $staffratecards = StaffRateCard::whereHas('employementtype', function($query){
                            $query->where('is_deleted', 0);
                          })->where('rate_card_id', $rate_card_id)->where('is_deleted', 0)->select('employment_type_id', 'staff_rate')->get();
        $newarr = array();
        $i = 0;
        
        if(count($staffratecards)>0){
            foreach ($staffratecards as $staffratecard) {
                
                $newarr[$i]['id']       = $staffratecard['employment_type_id'];
                $newarr[$i]['title']    = EmploymentType::find($staffratecard['employment_type_id'])->title;
                $newarr[$i]['value']    = !empty($staffratecard['staff_rate']) ? $staffratecard['staff_rate'] : '00.00';
               
                $i++;
            }
            
        } else {
            $newarr = EmploymentType::where('status','active')->where('is_deleted',0)->get();
        }
        return $newarr;
    }
    public function getShiftAttribute() {
    
        return Shift::find($this->shift_id)->shift_name;
    }
    public function getJobAttribute() {
    
        return Job::find($this->job_id)->name;
    }
}
