<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use App\Tenants\Client;
use App\Tenants\Candidate;
use App\Tenants\BusinessUnit;
use App\Tenants\CandidateDocument;
use App\Tenants\Vacancy;
use App\Tenants\AssignVacancy;
use DateTime;
use DB;
use Carbon\Carbon;

class Dashboard extends Model
{   
    /* Get total client */
    public static function getAllClientsCount(){

        return client::count();
    }
     /* Get active client */
    public static function getActiveClientsCount(){

        return client::where('status','active')->where('is_deleted',0)->count();
    }
    /* Get inactive client */
    public static function getInactiveAllClientsCount(){

        return client::where('status','inactive')->where('is_deleted',1)->count();
    }
    /* Get total candidate */
    public static function getAllCandidateCount(){

        return Candidate::where('candidate_status_id', '!=' , 6)->count();
    }
    /* Get active Candidate */
    public static function getActiveCandidateCount(){

        return Candidate::where('candidate_status_id', '!=' , 6)->where('is_deleted', 0)->count();
    }
    /* Get inactive Candidate */
    public static function getInactiveCandidateCount(){

        return Candidate::where('candidate_status_id', '!=' , 6)->where('is_deleted', 1)->count();
    }
    /* Get total Business unit */
    public static function getAllBusinessUnitCount(){

        return BusinessUnit::count();
    }
    /* Get active business unit */
    public static function getActiveBusinessUnitCount(){

        return BusinessUnit::where('is_deleted',0)->count();
    }
    /* Get inactive business unit */
    public static function getInactiveBusinessUnitCount(){

        return BusinessUnit::where('is_deleted',1)->count();
    }
     /* Get total documents */
     public static function getAllDocumentsCount(){

        return CandidateDocument::whereHas('documents', function($query) {
            $query->where('is_deleted',0);
        })->whereHas('documents.category', function($query) {
            $query->where('is_deleted',0);
        })->count();
    }
    /* Get expired documents */
    public static function getExpiredDocumentsCount(){

        $date = date('Y-m-d');
        return CandidateDocument::whereHas('documents', function($query) {
            $query->where('is_deleted',0);
        })->whereHas('documents.category', function($query) {
            $query->where('is_deleted',0);
        })->where('expiry_date','<',$date)->count();
    }
    /* Get expire documents in 15 days */
    public static function getExpireDocumentsWithinDaysCount(){

        $date = date('Y-m-d');
        $expiry  = date("Y-m-d", strtotime("+15 days", strtotime($date)));
        return CandidateDocument::whereHas('documents', function($query) {
            $query->where('is_deleted',0);
        })->whereHas('documents.category', function($query) {
            $query->where('is_deleted',0);
        })->where('expiry_date','>=',$date)->where('expiry_date','<',$expiry)->count();
    }
    /* Get Open Vacancies */
    public static function getOpenVacanciesCount(){

        $currWeek = Dashboard::getCurrentWeek();
        return $vacancies = DB::table('vacancies')
                     ->where('on_date', '>=', $currWeek['monday'])
                     ->where('on_date', '<=', $currWeek['sunday'])
                     ->where('deleted_space', 0)
                     ->sum(DB::raw('total_numbers - used_count'));
                     // ->select('id', 'total_numbers')->get();

        // $openCount = 0; 
        // $totalShiftCount = 0; 
        // foreach($vacancies as $vacancy){	
        //     $totalShiftCount = DB::table('assign_vacancies')
        //                   ->where('vacancy_id', $vacancy->id)
        //                   ->whereIn('status', ['pending', 'approved', 'invoiced'])
        //                   ->where('is_moved_to_vacancies', 0)
        //                   ->count('id');
            
        //     if($vacancy->total_numbers > $totalShiftCount) {
        //         $openCount += $vacancy->total_numbers - $totalShiftCount;
                
        //     }              
        // } 
        // return $openCount;
    }
    /* Get Applied Vacancies */
    public static function getAppliedVacanciesCount(){
        
        $currWeek = Dashboard::getCurrentWeek();
        return DB::table('assign_vacancies')
        ->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
        ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
        ->where('candidates.candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'))
        ->where('assign_vacancies.status', 'applied')
        ->where('assign_vacancies.is_deleted', 0)
        ->whereRaw('vacancies.total_numbers > vacancies.used_count')
        ->where('assign_vacancies.on_date', '>=', $currWeek['monday'])->where('assign_vacancies.on_date', '<=', $currWeek['sunday'])
        ->count();
        // return AssignVacancy::whereHas('vacancy', function($query) use($currWeek){
        //     $query->whereRaw('total_numbers > used_count')->where('on_date', '>=', $currWeek['monday'])->where('on_date', '<=', $currWeek['sunday']);
        // })->where('status','applied')->where('is_deleted',0)->count();
    }
    /* Get Assigned Vacancies */
    public static function getAssignedVacanciesCount(){
        
        $currWeek = Dashboard::getCurrentWeek();
        return DB::table('assign_vacancies')
        ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
        ->where('candidates.candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'))
        ->whereIn('assign_vacancies.status',['pending', 'approved', 'invoiced'])
        ->where('assign_vacancies.is_deleted', 0)
        ->where('assign_vacancies.on_date', '>=', $currWeek['monday'])->where('assign_vacancies.on_date', '<=', $currWeek['sunday'])
        ->count();
        // return AssignVacancy::whereHas('vacancy', function($query) use($currWeek){
        //     $query->where('on_date', '>=', $currWeek['monday'])->where('on_date', '<=', $currWeek['sunday']);
        // })->whereIn('status',['pending', 'approved', 'invoiced'])->where('is_deleted',0)->count();
    }
    /* Get Assigned Vacancies */
    public static function getDeletedShiftsCount(){
        
        $currWeek = Dashboard::getCurrentWeek();
        return DB::table('assign_vacancies')
        ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
        ->where('candidates.candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'))
        ->where('assign_vacancies.is_deleted', 1)
        ->where('assign_vacancies.on_date', '>=', $currWeek['monday'])->where('assign_vacancies.on_date', '<=', $currWeek['sunday'])
        ->count();
        // return AssignVacancy::whereHas('vacancy', function($query) use($currWeek){
        //     $query->where('on_date', '>=', $currWeek['monday'])->where('on_date', '<=', $currWeek['sunday']);
        // })->where('is_deleted',1)->count();
    }
    /* Get Total Timesheet Hours */
    public static function getTotalTimesheetHours(){

        $currWeek = Dashboard::getCurrentWeek();
        return AssignVacancy::whereHas('candidates', function($query) {
            $query->where('candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'));
        })->where('on_date', '>=', $currWeek['monday'])
        ->where('on_date', '<=', $currWeek['sunday'])
        ->where('is_deleted',0)->whereIn('status',['pending','approved', 'invoiced'])->sum('total_hours');  
    }
    /* Get Approved Timesheet Hours */
    public static function getApprovedTimesheetHours(){

        $currWeek = Dashboard::getCurrentWeek();
        return AssignVacancy::whereHas('candidates', function($query) {
            $query->where('candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'));
        })->where('on_date', '>=', $currWeek['monday'])->where('on_date', '<=', $currWeek['sunday'])->where('status','approved')->where('is_deleted',0)->sum('total_hours');
    }
    /* Get Invoiced Timesheet Hours */
    public static function getInvoicedTimesheetHours(){
        $currWeek = Dashboard::getCurrentWeek();
        return AssignVacancy::whereHas('candidates', function($query) {
            $query->where('candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'));
        })->where('on_date', '>=', $currWeek['monday'])->where('on_date', '<=', $currWeek['sunday'])->where('status','invoiced')->where('is_deleted',0)->sum('total_hours');
    }
    /* Get Pending Timesheet Hours */
    public static function getPendingTimesheetHours(){

        $currWeek = Dashboard::getCurrentWeek();
        return AssignVacancy::whereHas('candidates', function($query) {
            $query->where('candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'));
        })->where('on_date', '>=', $currWeek['monday'])->where('on_date', '<=', $currWeek['sunday'])->where('status','pending')->where('is_deleted',0)->sum('total_hours');
    }
    /* Get Total Charge Rate */
    public static function getTotalChargeRate(){
        
        $currWeek = Dashboard::getCurrentWeek();
        return AssignVacancyDetail::whereHas('assignvacancy.candidates', function($query) {
            $query->where('candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'));
        })->whereHas('assignvacancy.vacancy', function($query) use($currWeek){
            $query->where('on_date', '>=', $currWeek['monday'])->where('on_date', '<=', $currWeek['sunday']);
        })->whereHas('assignvacancy', function($query){
            $query->where('is_deleted',0)->whereIn('status',['pending','approved', 'invoiced']);
        })->sum('client_pay_amount');
    }    
    /* Get Total Pay Rate */
    public static function getTotalPayRate(){
        
        $currWeek = Dashboard::getCurrentWeek();
        return AssignVacancyDetail::whereHas('assignvacancy.candidates', function($query) {
            $query->where('candidate_status_id', '<>', DB::table('candidate_statuses')->where('title', 'Subcontractor')->value('id'));
        })->whereHas('assignvacancy.vacancy', function($query) use($currWeek){
            $query->where('on_date', '>=', $currWeek['monday'])->where('on_date', '<=', $currWeek['sunday']);
        })->whereHas('assignvacancy', function($query){
            $query->where('is_deleted',0)->whereIn('status',['pending','approved', 'invoiced']);
        })->sum('staff_pay_amount');
    }
    /* Get Last 12 months Total Invoice */
    public static function getTotalInvoice(){
        $totalInvoice = array();
        $firstmonth = Dashboard::getLastTwelveMonthsDate(); 
        $lastmonth  = Dashboard::getLastdayPreviousMonth(); 
        $totalInvoice['count'] = ClientInvoice::where('created_on', '>=', $firstmonth)->where('created_on', '<=', $lastmonth)->where('is_deleted', 0)->count();
        $totalInvoice['amount'] = ClientInvoice::where('created_on', '>=', $firstmonth)->where('created_on', '<=', $lastmonth)->where('is_deleted', 0)->sum('grand_total');
        return $totalInvoice;
    }
    /* Get Last 12 months Paid Invoice */
    public static function getPaidInvoice(){
        $paidInvoice = array();
        $firstmonth = Dashboard::getLastTwelveMonthsDate();
        $lastmonth = Dashboard::getLastdayPreviousMonth(); 
        $paidInvoice['count'] = ClientInvoice::where('created_on', '>=', $firstmonth)->where('created_on', '<=', $lastmonth)->where('status','paid')->where('is_deleted', 0)->count();
        $paidInvoice['amount'] = ClientInvoice::where('created_on', '>=', $firstmonth)->where('created_on', '<=', $lastmonth)->where('status','paid')->where('is_deleted', 0)->sum('grand_total');
        return $paidInvoice;
    }
    /* Get Last 12 months Invoice Over due < 30 */
    public static function getInvoiceOverDueLessthanThirty(){
        $data = array();
        $today = date('Y-m-d');
        $firstmonth = Dashboard::getLastTwelveMonthsDate();
        $lastmonth  = Dashboard::getLastdayPreviousMonth(); 
        $expiry = Carbon::now()->subDays(30)->toDateString();
        $data['count'] = ClientInvoice::where('created_on', '>=', $firstmonth)->where('created_on', '<=', $lastmonth)->whereBetween('due_date',[$expiry,$today])->where('status', 'over due')->where('is_deleted', 0)->count(); 
        $data['amount'] = ClientInvoice::where('created_on', '>=', $firstmonth)->where('created_on', '<=', $lastmonth)->whereBetween('due_date',[$expiry,$today])->where('status', 'over due')->where('is_deleted', 0)->sum('grand_total');
        return $data;
    }
    /* Get Last 12 months Invoice Over due > 30 */
    public static function getInvoiceOverDueGreaterthanThirty(){
        $data = array();
        $today = date('Y-m-d');
        $firstmonth = Dashboard::getLastTwelveMonthsDate();
        $lastmonth  = Dashboard::getLastdayPreviousMonth(); 
        $expiry  = Carbon::now()->subDays(30)->toDateString();
        $data['count'] = ClientInvoice::where('created_on', '>=', $firstmonth)->where('created_on', '<=', $lastmonth)->where('due_date','<',$expiry)->where('status', 'over due')->where('is_deleted', 0)->count();
        $data['amount'] = ClientInvoice::where('created_on', '>=', $firstmonth)->where('created_on', '<=', $lastmonth)->where('due_date','<',$expiry)->where('status', 'over due')->where('is_deleted', 0)->sum('grand_total');
        return $data;
    }
    /* Get Last 12 months Sales */
    public static function getLastTwelveMonthsSales(){
        $data = array();
        $dataRes = array();
        $res = array(); 
        $firstmonth = Dashboard::getLastTwelveMonthsDate();
        $lastmonth  = Dashboard::getLastdayPreviousMonth(); 
        $result = ClientInvoice::select(DB::raw('year(created_on) year, month(created_on) as month'),DB::raw('sum(grand_total) as grand_total'))
                  ->where('created_on', '>=',$firstmonth)->where('created_on', '<=',$lastmonth)
                  ->groupby('year','month')->orderBy('year')->get();
        for($i=0; $i<=12; $i++){
            if(isset($result[$i]->month)) { 
                $data[$result[$i]->month] = $result[$i]->grand_total;
            } 
        } 
        for($i=1; $i<=12; $i++){
            $dataRes[$i] = isset($data[$i])  ?  number_format((float) $data[$i]/1000, 2, '.', '') : '0' ;
        }
        /* Get last 12 months */
        $months = array();
        $monthFormat = array();
        $now = date('Y-m');
        $y = 0;
        for($x = 12; $x >= 1; $x--) {
            $ym = date('M-y', strtotime($now . " -$x month"));
            $xm = date('m', strtotime($now . " -$x month"));
            $months[$y]      = $ym;
            $monthFormat[$y] = $xm;
            $y++;
        } 
        $sales = array();
        foreach($monthFormat as $key=> $val){
            for($i=1; $i<=12; $i++){
                if($val == $i){
                    $sales[$key] = $dataRes[$i];
                }
            }
        }
        $res['salesMaxValue'] = max($dataRes);
        $res['monthlysales']  = json_encode($sales);
        $res['months']        = json_encode($months);
        return $res;
    }
    /* Get Last 12 months Total Sales Amount*/
    public static function getLastTwelveMonthsTotalSalesAmount(){
        $firstmonth = Dashboard::getLastTwelveMonthsDate(); 
        $lastmonth  = Dashboard::getLastdayPreviousMonth(); 
        return ClientInvoice::where('created_on', '>=',$firstmonth)->where('created_on', '<=',$lastmonth)->sum('grand_total');
    }
    /* Get Last 12 months Sales */
    public static function getLastTwelveMonthsTopCustomers(){
        $data = array();
        $result = array();
        $firstmonth = Dashboard::getLastTwelveMonthsDate();
        $lastmonth  = Dashboard::getLastdayPreviousMonth(); 
        $result = ClientInvoice::select('business_unit_id as customer',DB::raw('sum(grand_total) as grand_total'))
                  ->where('created_on', '>=',$firstmonth)->where('created_on', '<=',$lastmonth)
                  ->groupby('business_unit_id')->orderBy('grand_total','DESC')->take(5)->get();

        $customers = Dashboard::getClientInvoice(); 
        foreach($customers as $key => $value){ 
            if(isset($result[$key]->customer) && $value->business_unit_id == $result[$key]->customer){
                $data[$key] = number_format((float) $result[$key]->grand_total/1000, 2, '.', '');  
            }else{
                $data[$key] = 0;
            }
        }
        $result['topCustomers'] = json_encode($data);
        $result['maxValue']     = count($data) ? max($data) : 0 ;
        return $result;
    }
    /* Get Last 12 months Top Candidates */
    public static function getLastTwelveMonthsTopCandidates(){
        $data = array();
        $firstmonth = Dashboard::getLastTwelveMonthsDate();
        $lastmonth  = Dashboard::getLastdayPreviousMonth(); 
        $result = CandidateInvoice::select('candidate_id',DB::raw('sum(grand_total) as grand_total'))
                  ->where('created_on', '>=',$firstmonth)->where('created_on', '<=',$lastmonth)
                  ->groupby('candidate_id')->orderBy('grand_total','DESC')->take(5)->get();
        
        $candidates = Dashboard::getCandidateInvoice(); 
        foreach($candidates as $key => $val){
            foreach($result as $value){ 
                if(isset($value->candidate_id) && $val->candidate_id == $value->candidate_id){
                    $data[$key] = number_format((float) $value->grand_total/1000, 2, '.', '');  
                break;
                }else{
                    $data[$key] = 0;
                }
            }
        }   
        $result['topCandidates'] = json_encode($data);
        $result['maxValue']      = count($data) ? max($data) : 0 ;
        return $result;
    }
    /* Get Current Week */
    public static function getCurrentWeek(){
        $currWeek = array();
        $monday = strtotime("last monday");
        $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
        
        $sunday = strtotime(date("Y-m-d",$monday)." +6 days");
        
        $currWeek['monday'] = date("Y-m-d",$monday); 
        $currWeek['sunday'] = date("Y-m-d",$sunday); 

        return $currWeek;
    }    
    /* Get Last 12 month Date */
    public static function getLastTwelveMonthsDate(){
        $firstday = date('Y-m-01');
        $date = new DateTime($firstday);  
        return $date->modify("-12 months")->format('Y-m-d 00:00:00');    
    }    
    /* Get Last day of previous month */
    public static function getLastdayPreviousMonth(){
        $date = new DateTime();
        $date->modify("last day of previous month");
        return $date->format("Y-m-d 23:59:59");   
    }    
    /* Get Top Five Customers */
    public static function getTopFiveCustomers(){
        $data = array();
        $customers = Dashboard::getClientInvoice();
        foreach($customers as $i => $value){
           $data[$i] = $value->business_unit;    
        }
        return json_encode($data);
    }
    /* Get Top Five Candidates */
    public static function getTopFiveCandidates(){
        $data = array();
        $candidates = Dashboard::getCandidateInvoice();
        foreach($candidates as $i => $value){
            $data[$i] = $value->candidate_name;    
        }
        return json_encode($data);
    }
    /* Get Client Invoice Group by Business Unit */
    public static function getClientInvoice(){
        return ClientInvoice::orderBy('grand_total','DESC')->groupBy('business_unit_id')->take(5)->get();
    }
    /* Get Candidate Invoice by Candidate */
    public static function getCandidateInvoice(){
        return CandidateInvoice::orderBy('grand_total','DESC')->groupBy('candidate_id')->take(5)->get();
   }
    
} 
