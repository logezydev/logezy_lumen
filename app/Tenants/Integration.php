<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Integration extends Model
{
	protected $table = 'integrations';

	protected $casts = ['is_active'  => 'boolean'];

	protected $fillable = ['title', 'description' , 'is_active', 'slug', 'config', 'category'];

	public function userIntegration()
	{
		return $this->hasOne('App\Models\UserIntegration');
	}

	public static function requestAPI($body = '', $url = '', $method = 'POST'){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

		$response = curl_exec($ch);
		curl_close($ch);
    return $response;
  }

	public static function getIntegrationTokens($slug) {
		$reference = DB::table('integrations')->where('slug', $slug)->where('is_active', 1)->first();
		if($reference) { 
			return DB::table('user_integrations')->where('integration_id', $reference->id)->where('user_id', Auth::id())->select(['access_token', 'refresh_token'])->first();
		}		
		return false;
	}
}