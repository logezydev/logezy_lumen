<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;

class VacancyStaffRate extends Model
{
    protected $table = 'vacancy_staff_rates';

    public $appends = ['employment_type'];

    /**
     * [get employment_type]
     * @return [type] [description]
     */
    public function getEmploymentTypeAttribute() {
        return EmploymentType::find($this->employment_type_id)->title;
    }
}
