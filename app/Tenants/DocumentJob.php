<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class DocumentJob extends Model
{

    protected $table = 'document_jobs';
    
    /**
     * documents
     *
     * @return void
     */
    public function documents()
    {
        return $this->belongsTo('App\Tenants\Document', 'document_id');
    }
}
