<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class CandidateReferral extends Model
{
    protected $table = 'candidate_referrals';

    protected $fillable = [
        'candidate_id', 'referral_code',
    ];

    /**
     * candidate.
     */
    public function candidate()
    {
        return $this->belongsTo('App\Tenants\Candidate', 'candidate_id');
    }
}
