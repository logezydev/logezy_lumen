<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class CandidateRegion extends Model
{
    protected $table = 'candidate_regions';

}
