<?php

namespace App\Tenants;

use App\User;
use App\Tenants\Client;
use Illuminate\Database\Eloquent\Model;

class BusinessUnit extends Model
{
    protected $table = 'business_units';

    public $appends = ['client_name'];

    /**
     * Setting accessors for latitue
     */
    public function getLatitudeAttribute($value)
    {
        if ($value != ''){
            return $value;
        } else {
            return "0.0";
        }
    }

    /**
     * Setting accessors for longitude
     */
    public function getLongitudeAttribute($value)
    {
        if ($value != ''){
            return $value;
        } else {
            return "0.0";
        }
    }


    /**
     * Get Clients Associated
     */
    public function clients()
    {
        return $this->belongsTo('App\Tenants\Client','client_id');
    }
    /**
     * Gets the client name.
     */
    public function getClientNameAttribute() {
        return Client::find($this->client_id)->client_name;
    }
    /**
     * Gets Client Invoice Associated.
     */
    // public function clientinvoice() {
    //     return $this->hasMany('App\Models\ClientInvoice','business_unit_id');
    // }
}
