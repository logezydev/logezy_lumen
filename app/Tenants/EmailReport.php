<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class EmailReport extends Model
{
    protected $table = 'email_reports';

    protected $fillable = ['parent_id', 'sender', 'from', 'recipient', 'status', 'subject', 'recipient-domain', 'time'];
}
