<?php

namespace App\Tenants;

use DB;
use Illuminate\Database\Eloquent\Model;

class SignedTimesheet extends Model
{
    protected $table = 'signed_timesheets';

    /**
     * Setting accessors for start time
     */
    public function getStartTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("Y-m-d H:i", strtotime($value) );
        } else {
            return date("Y-m-d h:i A", strtotime($value) );
        }
    }

    /**
     * Setting accessors for end time
     */
    public function getEndTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("Y-m-d H:i", strtotime($value) );
        } else {
            return date("Y-m-d h:i A", strtotime($value) );
        }
    }
}
