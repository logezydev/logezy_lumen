<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class AgencySetting extends Model
{
    protected $table = 'agency_settings';
}
