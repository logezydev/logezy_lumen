<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    /* Candidate shift assign notifications */
    public static function candidate_shift_assign($candidate, $assign, $vacancy){ 
        //Updating Candidate notification
        $notify_array    = array(
            'title'      => '',
            'type'       => "candidate_shift_assigned",
            'date'       => date("d-m-Y",strtotime($vacancy->on_date)),
            'body'       => "You have been assigned a job  on ".date("d-m-Y",strtotime($vacancy->on_date))." with Ref.No: ".$assign->code.", in ".$vacancy->business_unit_name.", ".$vacancy->address_line
        );

        $notification                   = new Notification;
        $notification->type             = 'App\Notifications\HelloNotification';
        $notification->notifiable_type  = 'App\User';
        $notification->notifiable_id    = $candidate->user_id;
        $notification->role_id          = 6;
        $notification->object_id        = $assign->id;
        $notification->data             = json_encode($notify_array);
        $notification->save();
        return $notification;
            
    }

    /* Candidate shift cancel notifications */
    public static function candidate_shift_cancel($candidate, $assign, $vacancy){ 
        //Updating Candidate notification
            $notify_array    = array(
                'title'      => '',
                'type'       => "candidate_shift_cancel",
                'date'       => date("d-m-Y",strtotime($vacancy->on_date)),
                'body'       => "Sorry for inconvenience, Your job booking on ".date("d-m-Y",strtotime($vacancy->on_date))." with Ref.No: ".$assign->code.", in ".$vacancy->business_unit_name." has been cancelled. Reason: ".$assign->reason_to_delete
            );

            $notification                   = new Notification;
            $notification->type             = 'App\Notifications\HelloNotification';
            $notification->notifiable_type  = 'App\User';
            $notification->notifiable_id    = $candidate->user_id;
            $notification->role_id          = 6;
            $notification->object_id        = 0;
            $notification->data             = json_encode($notify_array);
            $notification->save();
            return $notification;
            
    }
}
