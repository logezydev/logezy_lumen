<?php

namespace App\Tenants;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ActivityLog\ActivityCustomLogTrait;

class Vacancy extends Model
{
    
    protected $table = 'vacancies';
    
    protected $fillable = ['business_unit_id', 'code', 'user_id', 
    'start_time', 'end_time', 'job_id', 'shift_id', 'break', 'on_date', 'status', 'total_numbers', 'notes'];


    public $appends = ['color','client','business_unit','business_unit_name','address_line', 'job_title', 'shift_type', 'assigned_count', 'total_space', 'space_left', 'unavailable', 'available', 'worked', 'applied', 'assigned', 'rejected', 'deleted', 'applied_count', 'candidates_to_publish', 'published_candidates', 'vacancy_staff_rates'];

    /**
     * Get all assigned vacancies
     *
     * @return     <type>  ( description_of_the_return_value )
     */
    public function assignedVacancies()
    {
        return $this->hasMany('App\Tenants\AssignVacancy', 'vacancy_id');
    }

    /**
     * Setting accessors for start time
     */
    public function getStartTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("Y-m-d H:i", strtotime($value) );
        } else {
            return date("Y-m-d h:i A", strtotime($value) );
        }
    }

    /**
     * Setting accessors for end time
     */
    public function getEndTimeAttribute($value)
    {
        $agency = DB::table('agencies')->first();
        if ($agency->time_format == 24){
            return date("Y-m-d H:i", strtotime($value) );
        } else {
            return date("Y-m-d h:i A", strtotime($value) );
        }
    }
    
    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitNameAttribute() {

        return BusinessUnit::find($this->business_unit_id)->business_unit_name;
    }
 
    /**
     * Gets the color attribute.
     *
     * @return     <type>  The color attribute.
     */
    public function getColorAttribute() {
        return Job::find($this->job_id)->color;
    }

    /**
     * Gets the Space left attribute.
     *
     * @return     <type>  The shift attribute.
     */
    public function getSpaceLeftAttribute() {
        $assigned = DB::table('assign_vacancies')->where('vacancy_id', $this->id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');
        // $assignedDeleted = DB::table('assign_vacancies')->where('vacancy_id', $this->id)->where('is_deleted', 0)->where('is_moved_to_vacancies', 0)->where('status', 'pending')->count('id');
        // $used_count = (int)$assigned + (int)$assignedDeleted;
        return (int)$this->total_numbers-(int)$assigned;
    }

    /**
     * Gets the Assigned count attribute.
     *
     * @return     <type>  The shift attribute.
     */
    public function getAssignedCountAttribute() {
        $assigned = DB::table('assign_vacancies')->where('vacancy_id', $this->id)->where('is_moved_to_vacancies', 0)->whereIn('status', ['pending', 'approved', 'invoiced'])->count('id');
        // $assignedDeleted = DB::table('assign_vacancies')->where('vacancy_id', $this->id)->where('is_deleted', 0)->where('is_moved_to_vacancies', 0)->where('status', 'pending')->count('id');
        return (int)$assigned;
    }
 
    /**
     * Gets the total_space attribute.
     *
     * @return     <type>  The color attribute.
     */
    public function getTotalSpaceAttribute() {
        return $this->total_numbers;
    }
    
    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getBusinessUnitAttribute() {

        return BusinessUnit::find($this->business_unit_id, ['id', 'client_id', 'business_unit_name', 'formatted_number', 'email', 'address_line', 'latitude', 'longitude', 'description']);
    }

    /**
     * Gets the Client attribute.
     *
     * @return     <type>  Client attribute.
     */
    public function getClientAttribute() {

        return Client::find(BusinessUnit::find($this->business_unit_id)->client_id)->client_name ;
    }
    /**
     * Gets the business unit attribute.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getAddressLineAttribute() {

        return BusinessUnit::find($this->business_unit_id)->address_line;
    }

    /**
     * Gets the job title attribute.
     *
     * @return     <type>  The job title attribute.
     */
    public function getJobTitleAttribute() {
    	return Job::find($this->job_id)->name;
    }


    /**
     * Gets the shift attribute.
     *
     * @return     <type>  The shift attribute.
     */
    public function getShiftTypeAttribute() {
    	return Shift::find($this->shift_id)->shift_name;
    }


    /**
     * Undocumented function
     *
     * @return void
     */
    public function getBusinessUnitName() {

        return BusinessUnit::find($this->business_unit_id)->business_unit_name;
    }
    
    /**
     * [getAllCandidates from given Ids description]
     * @return [type] [description]
     */
    public function getActiveCandidatesFromIds($candidateIds) {
        $business_unit_id = $this->business_unit_id;
        $regions = BusinessunitRegion::where('business_unit_id', $business_unit_id)->pluck('region_id')->all();
        
        if ($regions) {
            $candidate_IDs = CandidateRegion::whereIn('region_id', $regions)->whereIn('candidate_id', $candidateIds)->pluck('candidate_id')->all();
            return $candidates = DB::table('candidates')
                ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
                ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
                ->select('candidates.id as candidate_id', 'candidates.full_name', 'candidates.profile_img', 'candidates.formatted_mobile_number', 'candidates.firebase_uin', 'candidates.candidate_status_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
                ->whereIn('candidates.id', $candidate_IDs)
                ->where('candidates.candidate_status_id', 1)
                ->where('candidates.is_deleted', 0)
                ->whereNotIn('candidates.id',function($query) use($business_unit_id){
                    $query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
                })
                ->groupBy('candidates.id')
                ->get();
        } else {
            return $candidates = DB::table('candidates')
                ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
                ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
                ->select('candidates.id as candidate_id', 'candidates.full_name', 'candidates.profile_img', 'candidates.formatted_mobile_number', 'candidates.firebase_uin', 'candidates.candidate_status_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
                ->whereIn('candidates.id', $candidateIds)
                ->where('candidates.candidate_status_id', 1)
                ->where('candidates.is_deleted', 0)
                ->whereNotIn('candidates.id',function($query) use($business_unit_id){
                    $query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
                })
                ->groupBy('candidates.id')
                ->get();
        }

        // $business_unit_id = $this->business_unit_id;
        // $regions = BusinessunitRegion::where('business_unit_id', $business_unit_id)->pluck('region_id')->all();
        // $candidateRegionIDs = CandidateRegion::whereIn('region_id', $regions)->pluck('candidate_id')->all();

        // if (count($candidateRegionIDs)>0){
        //     $commonIDs = array_intersect($candidateIds, $candidateRegionIDs);
        //     if (count($commonIDs)>0){
        //         return $candidates = DB::table('candidates')
        //         ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
        //         ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
        //         ->select('candidates.id as candidate_id', 'candidates.full_name', 'candidates.profile_img', 'candidates.formatted_mobile_number', 'candidates.firebase_uin', 'candidates.candidate_status_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
        //         ->whereIn('candidates.id', $commonIDs)
        //         ->where('candidates.candidate_status_id', 1)
        //         ->where('candidates.is_deleted', 0)
        //         ->whereNotIn('candidates.id',function($query) use($business_unit_id){
        //             $query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
        //         })
        //         ->groupBy('candidates.id')
        //         ->get();
        //         // return $candidates = DB::table('candidates')
        //         //     ->select('id as candidate_id', 'full_name', 'candidate_status_id')
        //         //     ->whereIn('id', $commonIDs)
        //         //     ->where('candidate_status_id', 1)
        //         //     ->where('is_deleted', 0)
        //         //     ->whereNotIn('id',function($query) use($business_unit_id){
        //         //         $query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
        //         //     })->get();
        //     } else {
        //         return $candidates = DB::table('candidates')
        //         ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
        //         ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
        //         ->select('candidates.id as candidate_id', 'candidates.full_name', 'candidates.profile_img', 'candidates.formatted_mobile_number', 'candidates.firebase_uin', 'candidates.candidate_status_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
        //         ->whereIn('candidates.id', $candidateIds)
        //         ->where('candidates.candidate_status_id', 1)
        //         ->where('candidates.is_deleted', 0)
        //         ->whereNotIn('candidates.id',function($query) use($business_unit_id){
        //             $query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
        //         })
        //         ->groupBy('candidates.id')
        //         ->get();
        //         // return $candidates = DB::table('candidates')
        //         //     ->select('id as candidate_id', 'full_name', 'candidate_status_id')
        //         //     ->whereIn('id', $candidateIds)
        //         //     ->where('candidate_status_id', 1)
        //         //     ->where('is_deleted', 0)
        //         //     ->whereNotIn('id',function($query) use($business_unit_id){
        //         //         $query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
        //         //     })->get();
        //     }
        // } else {
        //         return $candidates = DB::table('candidates')
        //         ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
        //         ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
        //         ->select('candidates.id as candidate_id', 'candidates.full_name', 'candidates.profile_img', 'candidates.formatted_mobile_number', 'candidates.firebase_uin', 'candidates.candidate_status_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
        //         ->whereIn('candidates.id', $candidateIds)
        //         ->where('candidates.candidate_status_id', 1)
        //         ->where('candidates.is_deleted', 0)
        //         ->whereNotIn('candidates.id',function($query) use($business_unit_id){
        //             $query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
        //         })
        //         ->groupBy('candidates.id')
        //         ->get();
        //     // return $candidates = DB::table('candidates')
        //     //     ->select('id as candidate_id', 'full_name', 'candidate_status_id')
        //     //     ->whereIn('id', $candidateIds)
        //     //     ->where('candidate_status_id', 1)
        //     //     ->where('is_deleted', 0)
        //     //     ->whereNotIn('id',function($query) use($business_unit_id){
        //     //         $query->where('business_unit_id', $business_unit_id)->select('candidate_id')->from('candidate_restricted_business_units');
        //     //     })->get();
        // }
    }
    /**
     * [totalWorkedCandidates]
     */
    public function totalWorkedCandidates() {
        $vacancy_ids = DB::table('vacancies')->where('business_unit_id', $this->business_unit_id)
                     ->where('job_id', $this->job_id)->where('shift_id', $this->shift_id)
                     ->where('on_date', '!=', $this->on_date)
                     ->where('id', '!=',$this->id)->pluck('id');
        //Worked candidates             
        $worked_array = DB::table('assign_vacancies')->whereIn('vacancy_id', $vacancy_ids)->whereIn('status', ['pending','approved', 'invoiced'])->where('is_deleted', 0)->pluck('candidate_id')->all();
        return array_unique($worked_array);
    }
    /**
     * [totalAvailableCandidates]
     */
    public function totalAvailableCandidates() {
        $shift_name = Shift::find($this->shift_id)->shift_name;
        $shift = [];
        switch ($shift_name) {
            case "Day":
                $shift = ["E", "L"];
                break;
            case "Night":
                $shift = ["N"];
                break;
            case "Holiday Day":
                $shift = ["E", "L"];
                break;
            case "Holiday Night":
                $shift = ["N"];
                break;
            default:
                
        }
        //Available candidates
        $candidate_ids = DB::table('candidate_jobs')->where('job_id', $this->job_id)->pluck('candidate_id')->all();
        $available_array = DB::table('candidate_availabilities')->where('date',$this->on_date)->whereIn('availability',$shift)->whereIn('candidate_id', $candidate_ids)->pluck('candidate_id')->all();
        return array_unique($available_array);
    }

    /**
     * [totalAssignedCandidates]
     */
    public function totalAssignedCandidates() {
        $assigned = $this->getCandidatesAssignedOnSameDateWithInTimeslot();
        $assigned_array = $assigned->pluck('id')->all();
        return array_unique($assigned_array);

    }

    /**
     * [totalAppliedCandidates]
     */
    public function totalAppliedCandidates() {
        return $applied_candidate_array = DB::table('assign_vacancies')
                    ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                    ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
                    ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
                    ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.profile_img', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
                    ->where('assign_vacancies.status', 'applied')
                    ->where('assign_vacancies.is_deleted', 0)
                    ->where('candidates.candidate_status_id', 1)
                    ->where('assign_vacancies.vacancy_id', $this->id)
                    ->groupBy('assign_vacancies.id')
                    ->get();
    }
    /**
     * [totalUnavailableCandidates]
     */
    public function totalUnavailableCandidates() {
        $candidate_array = CandidateJob::where('job_id', $this->job_id)->pluck('candidate_id')->all();
        //Available candidates           
        $available_array = $this->totalAvailableCandidates();
        //Worked candidateswithoutIndexcandidateIds
        $worked_array = $this->totalWorkedCandidates();
        //Assigned candidates
        $assigned_array = $this->totalAssignedCandidates();
        //merged candidates
        $merged_candidates = array_merge($available_array, $worked_array, $assigned_array);
        $merged_array = array_unique($merged_candidates);
        //remaining candidates
        $remaining_array = array_diff($candidate_array, $merged_array);
        return array_unique($remaining_array);
    }
    /**
     * [getCandidatesAssignedOnSameDateWithInTimeslot]
     */
    public function getCandidatesAssignedOnSameDateWithInTimeslot() {
        $vacancy = DB::table('vacancies')->find($this->id);
        return $assigned_on_date_candidate_array = DB::table('assign_vacancies')
                    ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                    ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
                    ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
                    ->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
                    ->join('assigned_vacancy_details', 'assigned_vacancy_details.assigned_vacancy_id', '=', 'assign_vacancies.id')
                    ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.profile_img', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
                    ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                    ->where('assign_vacancies.is_deleted', 0)
                    ->where('assigned_vacancy_details.start_time', '<', $vacancy->end_time)
                    ->where('assigned_vacancy_details.end_time', '>', $vacancy->start_time)
                    ->groupBy('assign_vacancies.id')
                    // ->where('vacancies.on_date', $this->on_date)
                    ->get();

    }

    /**
     * [getAllCandidatesAttribute description]
     * @return [type] [description]
     */
    public function getUnavailableAttribute() {
        //Unavailable candidates
        $candidate_ids = $this->totalUnavailableCandidates();
        $withoutIndexcandidateIds = array_values($candidate_ids);

        return $candidate_array = $this->getActiveCandidatesFromIds($withoutIndexcandidateIds);

    }

    /**
     * [getWorkedAttribute description]
     * @return [type] [description]
     */
    public function getWorkedAttribute() {
        
        //Worked candidates             
        $worked_array = $this->totalWorkedCandidates();
        //Assigned candidates
        $assigned_array = $this->totalAssignedCandidates();
        //Available candidates           
        $available_array = $this->totalAvailableCandidates();
        //merged candidates
        $merged_candidates = array_merge($available_array, $assigned_array);
        $merged_array = array_unique($merged_candidates);
        //remaining candidates
        $remaining_array = array_diff($worked_array, $merged_array);
        $withoutIndexcandidateIds = array_values($remaining_array);

        return $candidate_array = $this->getActiveCandidatesFromIds($withoutIndexcandidateIds);
    }

    /**
     * [getAppliedAttribute description]s
     * @return [type] [description]
     */
    public function getAvailableAttribute() {
        //Available candidates
        $available_array = $this->totalAvailableCandidates();
        //Assigned candidates
        $assigned_array = $this->totalAssignedCandidates();
        //remaining candidates
        $remaining_array = array_diff($available_array, $assigned_array);
        $withoutIndexcandidateIds = array_values($remaining_array);
        
        return $candidate_array = $this->getActiveCandidatesFromIds($withoutIndexcandidateIds);
        
    }

    /**
     * [getAppliedAttribute description]
     * @return [type] [description]
     */
    public function getAppliedAttribute() {
        $applied_array = $this->totalAppliedCandidates();
        return $applied_array;   
    }

    /**
     * [getAppliedCountAttribute description]
     * @return [type] [description]
     */
    public function getAppliedCountAttribute() {
        $applied_array = $this->totalAppliedCandidates();
        return count($applied_array);   
    }

    /**
     * [getAssignedAttribute description]
     * @return [type] [description]
     */
    public function getAssignedAttribute() {
        $vacancy = DB::table('vacancies')->find($this->id);
        return $assigned_on_date_candidate_array = DB::table('assign_vacancies')
                    ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                    ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
                    ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
                    ->join('vacancies', 'vacancies.id', '=', 'assign_vacancies.vacancy_id')
                    ->join('assigned_vacancy_details', 'assigned_vacancy_details.assigned_vacancy_id', '=', 'assign_vacancies.id')
                    ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.profile_img', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
                    ->whereIn('assign_vacancies.status', ['pending', 'approved', 'invoiced'])
                    ->where('assign_vacancies.is_deleted', 0)
                    ->where('assign_vacancies.vacancy_id', $this->id)
                    ->where('assigned_vacancy_details.start_time', '<', $vacancy->end_time)
                    ->where('assigned_vacancy_details.end_time', '>', $vacancy->start_time)
                    ->groupBy('assign_vacancies.id')
                    // ->where('vacancies.on_date', $this->on_date)
                    ->get();

    }

    /**
     * [getRejectedAttribute description]
     * @return [type] [description]
     */
    public function getRejectedAttribute() {
        return $rejected_candidate_array = DB::table('assign_vacancies')
                    ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                    ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
                    ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
                    ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.profile_img', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
                    ->where('assign_vacancies.status', 'rejected')
                    ->where('assign_vacancies.is_deleted', 0)
                    ->where('assign_vacancies.vacancy_id', $this->id)
                    ->groupBy('assign_vacancies.id')
                    ->get();
    }

    /**
     * [getDeletedAttribute description]
     * @return [type] [description]
     */
    public function getDeletedAttribute() {
        return $deleted_candidate_array = DB::table('assign_vacancies')
                    ->join('candidates', 'candidates.id', '=', 'assign_vacancies.candidate_id')
                    ->join('candidate_jobs', 'candidates.id', '=', 'candidate_jobs.candidate_id')
                    ->join('jobs', 'jobs.id', '=', 'candidate_jobs.job_id')
                    ->select('candidates.id as candidate_id', 'assign_vacancies.code', 'candidates.full_name', 'candidates.profile_img', 'candidates.candidate_status_id','assign_vacancies.is_publish', 'assign_vacancies.id AS assign_id', DB::raw("GROUP_CONCAT(jobs.name SEPARATOR ',') as `job_names`"))
                    ->where('assign_vacancies.is_deleted', 1)
                    ->where('assign_vacancies.vacancy_id', $this->id)
                    ->groupBy('assign_vacancies.id')
                    ->get();
    }

    /**
     * [getDeletedAttribute description]
     * @return [type] [description]
     */
    public function getVacancyStaffRatesAttribute() {
        $staffrates = VacancyStaffRate::where('vacancy_id', $this->id)->get();
        $staffRateArr = [];
        if($staffrates){
            foreach ($staffrates as $staffrate) {
                $staffRateArr[] = array(
                    'id'=>$staffrate->id, 
                    'employment_type_id'=>$staffrate->employment_type_id,
                    'title'=>EmploymentType::find($staffrate->employment_type_id)->title,
                    'staff_rate'=>$staffrate->staff_rate );
            }
        }
        return $staffRateArr;
    }
    
    /**
     * Gets the Candidates to Publish.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getCandidatesToPublishAttribute() {

        $vacancy = DB::table('vacancies')->find($this->id);

              //Checking before assigning candidate
              $inputArr = array(
                  'id'  => $vacancy->id,
                  'business_unit_id'  => $vacancy->business_unit_id,
                  'job_id'            => $vacancy->job_id,
                  'on_date'           => $vacancy->on_date,
                  'shift_id'          => $vacancy->shift_id,
                  'start_time'        => $vacancy->start_time,
                  'end_time'          => $vacancy->end_time,
              );

        return Helper::getCandidatesForVacancyPublish($inputArr);

        // $candidate_array    = [];

        // //Available candidates
        // $available_array = $this->totalAvailableCandidates();
        // //Assigned candidates
        // $assigned_array = $this->totalAssignedCandidates();
        // //remaining candidates
        // $remaining_array = array_diff($available_array, $assigned_array);
        // $availableCandidateIds = array_values($remaining_array);
        // $candidate_array['available']   = $this->getActiveCandidatesFromIds($availableCandidateIds);

        // //Unavailable candidates
        // $candidate_ids = $this->totalUnavailableCandidates();
        // $unavailableCandidateIds = array_values($candidate_ids);
        // $candidate_array['unavailable'] = $this->getActiveCandidatesFromIds($unavailableCandidateIds);

        
        // //Worked candidates             
        // $worked_array = $this->totalWorkedCandidates();
        // //Assigned candidates
        // $assigned_array = $this->totalAssignedCandidates();
        // //Available candidates           
        // $available_array = $this->totalAvailableCandidates();
        // //merged candidates
        // $merged_candidates = array_merge($available_array, $assigned_array);
        // $merged_array = array_unique($merged_candidates);
        // //remaining candidates
        // $remaining_array = array_diff($worked_array, $merged_array);
        // $workedCandidateIds = array_values($remaining_array);
        // $candidate_array['worked']      = $this->getActiveCandidatesFromIds($workedCandidateIds);
        
        // // $candidate_array['assigned']    = $this->getAssignedAttribute;;

        // return $candidate_array;
        // dd($candidate_array);

    }
    
    /**
     * Gets the Published Candidates.
     *
     * @return     <type>  The business unit attribute.
     */
    public function getPublishedCandidatesAttribute() {

        if ($this->published_ids != NULL) {
            return explode(",", $this->published_ids);
        }
        return NULL;
    }
    
    /**
     * Store thr resource.
     *
     * @return     <type>  The business unit attribute.
     */
    public static function addVacancy($request) {

        //checking for night shift
        $start = date("H:i", strtotime($request->start_time));
        $end = date("H:i", strtotime($request->end_time));
        $daycheck = date('a', strtotime($request->end_time));
        if ($start >= $end) {
            $endDate = date('Y-m-d', strtotime('+1 day', strtotime($request->on_date)));
        } else {
            $endDate = $request->on_date;
        }

        //calculate total hours
        $inputs = array(
            'on_date'    => $request->on_date,
            'start_time' => strtotime($request->on_date. ' '.$request->start_time),
            'end_time'   => strtotime($endDate. ' '.$request->end_time) ,
            'break'      => $request->break ?? 0,
        );
        $totalHours = calcTotalHours($inputs);

        $vacancy                   = new Vacancy;
        // $vacancy->code             = isset($request->code) ? $req$rates =uest->code : null;
        $vacancy->business_unit_id = isset($request->business_unit_id) ? $request->business_unit_id : null;
        $vacancy->job_id           = isset($request->job_id) ? $request->job_id : null;
        $vacancy->shift_id         = isset($request->shift_id) ? $request->shift_id : null;
        $vacancy->status           = 'pending';
        $vacancy->start_time       = isset($request->start_time) ? date( "Y-m-d H:i:s", strtotime($request->on_date. ' '.$request->start_time)) : null;
        $vacancy->end_time         = isset($request->end_time) ? date( "Y-m-d H:i:s", strtotime($endDate. ' '.$request->end_time) ) : null;
        $vacancy->on_date          = isset($request->on_date) ? $request->on_date : null;
        $vacancy->break            = isset($request->break) ? $request->break : 0;
        $vacancy->total_numbers    = isset($request->total_numbers) ? $request->total_numbers : 0;
        $vacancy->is_publish       = isset($request->is_publish) ? $request->is_publish : 0;
        $vacancy->total_hours      = $totalHours ?? 0;
        $vacancy->client_rate      = 0;
        $vacancy->notes            = isset($request->notes) ? $request->notes : null;
        $vacancy->created_by       = Auth::id();



        //fetching rates
        $inputArr = array(
            'business_unit_id'  => $request->business_unit_id,
            'job_id'            => $request->job_id,
            'on_date'           => $request->on_date,
            'shift_id'          => $request->shift_id,
        );
        $rates = Helper::getClientStaffRates($inputArr);
        if( $request->client_rate == null ) {
            $clientRate = !empty($rates->client_rate) ? $rates->client_rate : 0;
        } else {
            $clientRate = $request->client_rate;
        }
        if( $rates ) {
            $staffRate  = $rates->staff_rate_cards;
        } else {
            $staffRate  = $request->staff_rates;
        }
        //calculating total hours
        $input = array(
            'on_date'           => $request->on_date,
            'start_time'        => $request->start_time,
            'end_time'          => $request->end_time,
            'break'             => $request->break,
        );
        $totalHours = Helper::getTotalHours($input);
        
        //checking for night shift
        $start = date("H:i", strtotime($request->start_time));
        $end = date("H:i", strtotime($request->end_time));
        $daycheck = date('a', strtotime($request->end_time));
        if ($start >= $end) {
            $endDate = date('Y-m-d', strtotime('+1 day', strtotime($request->on_date)));
        } else {
            $endDate = $request->on_date;
        }

        $vacancy                   = new Vacancy;
        // $vacancy->code             = isset($request->code) ? $req$rates =uest->code : null;
        $vacancy->business_unit_id = isset($request->business_unit_id) ? $request->business_unit_id : null;
        $vacancy->job_id           = isset($request->job_id) ? $request->job_id : null;
        $vacancy->shift_id         = isset($request->shift_id) ? $request->shift_id : null;
        $vacancy->status           = 'pending';
        $vacancy->start_time       = isset($request->start_time) ? date( "Y-m-d H:i:s", strtotime($request->on_date. ' '.$request->start_time)) : null;
        $vacancy->end_time         = isset($request->end_time) ? date( "Y-m-d H:i:s", strtotime($endDate. ' '.$request->end_time) ) : null;
        $vacancy->on_date          = isset($request->on_date) ? $request->on_date : null;
        $vacancy->break            = isset($request->break) ? $request->break : 0;
        $vacancy->total_numbers    = isset($request->total_numbers) ? $request->total_numbers : 1;
        $vacancy->total_hours      = $totalHours;
        if( $rates ) {
            $vacancy->rate_type    = isset($rates->rate_type) ? $rates->rate_type : 'hourly_rate';
        }
        $vacancy->client_rate      = $clientRate;
        $vacancy->notes            = isset($request->notes) ? $request->notes : null;
        $vacancy->created_by       = Auth::id();

        //Populating vacancy details & staff rates
        if($vacancy->save()) {

            $rateType = 'hourly_rate';
            $rateCards = [];
            $rateCards = splitTimeRangeIntoSlots($request);

            //update vacancy code
            $vacancy_code_setting = AgencySetting::where('settings_key', 'vacancy_code_format')->first();
            $vacancy_code_format = 'VAC';
            if($vacancy_code_setting) {
                $vacancy_code_format = !empty($vacancy_code_setting->settings_value) ? $vacancy_code_setting->settings_value : 'VAC';
            }
            $vacancy_code = $vacancy_code_format.$vacancy->id;
            
            $vacancy->update(['code' => $vacancy_code]);
            if (count($rateCards) > 0) {
                $rateType = $rateCards[0]['rate_type'];
                //adding vacancy details
                foreach ($rateCards as $rateCard) {
                    $vacancyDetail                   = new VacancyDetail;
                    $vacancyDetail->vacancy_id       = $vacancy->id;
                    $vacancyDetail->day_id           = $rateCard['day_id'];
                    $vacancyDetail->shift_id         = $rateCard['shift_id'];
                    $vacancyDetail->rate_type        = $rateCard['rate_type'];
                    $vacancyDetail->start_time       = date( "Y-m-d H:i:s", strtotime($rateCard['start_time'])) ?? null;
                    $vacancyDetail->end_time         = date( "Y-m-d H:i:s", strtotime($rateCard['end_time'])) ?? null;
                    $vacancyDetail->break            = $rateCard['break'] ?? 0;
                    $vacancyDetail->total_hours      = $rateCard['total_hours'] ?? 0;
                    $vacancyDetail->client_rate      = $rateCard['client_rate'] ?? 0;
                    $vacancyDetail->save();

                    //Populating vacancy staff rates
                    if( $rateCard['staff_rates'] ) {
                        foreach($rateCard['staff_rates'] as $staffRate) {
                            // if(isset($staffRate['value'])){
                                $vacancy_staff_rate                     = new VacancyStaffRate;
                                $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                if (is_array($staffRate)) {
                                    $vacancy_staff_rate->employment_type_id = $staffRate['employment_type_id'];
                                    $vacancy_staff_rate->staff_rate         = $staffRate['staff_rate'] ?? 0;
                                } else {
                                    $vacancy_staff_rate->employment_type_id = $staffRate->employment_type_id;
                                    $vacancy_staff_rate->staff_rate         = $staffRate->staff_rate ?? 0;
                                }
                                $vacancy_staff_rate->save();
                            // }
                        }
                    } else {
                        $staffRates = EmploymentType::where('status','active')->where('is_deleted',0)->get();
                        foreach ($staffRates as $staffRate) {
                            // if(isset($staffRate['value'])){
                                $vacancy_staff_rate                     = new VacancyStaffRate;
                                $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                $vacancy_staff_rate->employment_type_id = $staffRate->id;
                                $vacancy_staff_rate->staff_rate         = $staffRate->value ?? 0;
                                $vacancy_staff_rate->save();
                            // }
                        }
                    }
                }
            } else {
                    $vacancyDetail                   = new VacancyDetail;
                    $vacancyDetail->vacancy_id       = $vacancy->id;
                    $vacancyDetail->day_id           = Day::where('day', date("l", strtotime($request->on_date)))->first()->id;
                    $vacancyDetail->shift_id         = isset($request->shift_id) ? $request->shift_id : null;
                    $vacancyDetail->rate_type        = 'hourly_rate';
                    $vacancyDetail->start_time       = isset($request->start_time) ? date( "Y-m-d H:i:s", strtotime($request->on_date. ' '.$request->start_time)) : null;
                    $vacancyDetail->end_time         = isset($request->end_time) ? date( "Y-m-d H:i:s", strtotime($endDate. ' '.$request->end_time) ) : null;
                    $vacancyDetail->on_date          = isset($request->on_date) ? $request->on_date : null;
                    $vacancyDetail->break            = isset($request->break) ? $request->break : 0;
                    $vacancyDetail->total_hours      = $totalHours ?? 0;
                    $vacancyDetail->client_rate      = 0;
                    $vacancyDetail->save();
                    //Populating vacancy staff rates
                    $staffRates = EmploymentType::where('status','active')->where('is_deleted',0)->get();
                    if( $staffRates ) {
                        foreach ($staffRates as $staffRate) {
                            // if(isset($staffRate['value'])){
                                $vacancy_staff_rate                     = new VacancyStaffRate;
                                $vacancy_staff_rate->vacancy_id         = $vacancy->id;
                                $vacancy_staff_rate->vacancy_detail_id  = $vacancyDetail->id;
                                $vacancy_staff_rate->employment_type_id = $staffRate->id;
                                $vacancy_staff_rate->staff_rate         = $staffRate->value;
                                $vacancy_staff_rate->save();
                            // }
                        }
                    } 
            }

            $user = DB::table('logezycore.users')->where('id', Auth::id())->select('id', 'name', 'email')->first();
            $loggedData = [
                'role'          => 'manager',
                'tenant'        => $request->input('subdomain'),
                'dashboardType' => 'manager_app',
                'user_name'     => $user->name,
                'email'         => $user->email,
            ];
            //Activity Log
            $message = 'New vacancy has been created with Ref code ' . $vacancy_code . '.';
            ActivityCustomLogTrait::saveActivityLog('Vacancies', 'Create', $message, $vacancy->id, $vacancy->code, $loggedData); //entity, operation, message, subjectId, ObjectId, loggedData

        }

        return $vacancy->id;

    }


}

