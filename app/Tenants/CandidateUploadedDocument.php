<?php

namespace App\Tenants;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CandidateUploadedDocument extends Model
{
    protected $table = 'candidate_uploaded_documents';

    protected $fillable = ['candidate_id', 'document_id', 'file', 'file_name', 'issue_date', 'expiry_date', 'description', 'status', 'created_by'];

    protected static function boot()
    {
        parent::boot();
        static::saving(function (Model $query) {
            $query->created_by = Auth::id();
            $query->updated_at = Carbon::now()->timestamp;
        });
    }
}
