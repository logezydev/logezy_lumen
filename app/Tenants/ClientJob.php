<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class ClientJob extends Model
{
    protected $table = 'client_jobs';

    public $appends = ['job_name'];

    public function job_details()
    {
        return $this->belongsTo('App\Models\Job','job_id');
    }

    public function getJobNameAttribute() {
        $job_id = ClientJob::find($this->id)->job_id;
    	return Job::find($job_id)->name;
    }
    
    /**
     * Get client jobs 
     */
    public static function getClientJobs($client_id){
        return ClientJob::where('client_id', $client_id)->where('status','active')->where('is_deleted',0)->orderBy('id', 'ASC')->get();
    }

}
