<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class BusinessunitRegion extends Model
{
    protected $table = 'businessunit_regions';

}
