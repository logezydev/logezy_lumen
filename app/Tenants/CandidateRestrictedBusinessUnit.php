<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class CandidateRestrictedBusinessUnit extends Model
{

	protected $table = 'candidate_restricted_business_units';
	
}
