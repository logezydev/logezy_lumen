<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class RecruitmentJobsForm extends Model
{
    protected $table = 'recruitment_jobs_form';

    protected $fillable = ['job_id', 'recruitment_built_form_id', 'status', 'is_deleted', 'created_by'];

    /**
     * recruitmentBuiltForm.
     */
    public function recruitmentBuiltForm()
    {
        return $this->belongsTo('App\Tenants\RecruitmentBuiltForm', 'recruitment_built_form_id');
    }
}
