<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class MobileAppSetting extends Model
{
    public $table = 'mobile_app_settings';

    /**
     * Setting accessors for start time
     */
    public function getValueAttribute($value)
    {
        if ($value == '0'){
            return false;
        } else if ($value == '1'){
            return true;
        } else {
            return $value;
        }
    }
}
