<?php

namespace App\Tenants;

use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{
    protected $table = 'document_categories';
    /**
     * documents
     *
     * @return void
     */
    public function documents()
    {
        return $this->hasMany('App\Tenants\Document','document_category_id')->where('is_deleted', 0)->orderBy('position', 'ASC');
    }
}
