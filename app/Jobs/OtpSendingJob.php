<?php

namespace App\Jobs;

use App\Mail\OtpSendingMail;
use Illuminate\Support\Facades\Mail;

class OtpSendingJob extends Job
{
    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::to($this->data['user_email'])->send(new OtpSendingMail($this->data));
        }catch(\Exception $e){
            \Log::error($e->getMessage());
        }
    }
}
