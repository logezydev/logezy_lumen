<?php

namespace App\Jobs;

use App\Mail\NewCandidateRegistered;
use Illuminate\Support\Facades\Mail;

class NewCandidateRegisterJob extends Job
{
    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->data['candidate_email'])->send(new NewCandidateRegistered($this->data));
    }
}
