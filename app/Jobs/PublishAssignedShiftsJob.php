<?php

namespace App\Jobs;

use App\Mail\NotifyShiftAssigned;
use Illuminate\Support\Facades\Mail;

class PublishAssignedShiftsJob extends Job
{

    public $subdomain;
    public $assign;
    public $notification_services;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mail_content)
    {
        $this->mail_content = $mail_content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Mail::to($this->data['candidate_email'])->send(new NewCandidateRegistered($this->data));

        $mail_content                  = $this->mail_content;
        Mail::to($candidate->email)->send(new NotifyShiftAssigned($mail_content));
    }
}
