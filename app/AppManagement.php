<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppManagement extends Model
{

    protected $table = 'app_managements';

    public $appends = ['tenant_name', 'tenant_slug'];

    /**
     * Gets the tenant_name
     */
    public function getTenantNameAttribute()
    {
        return Tenant::find($this->tenant_id)->name;
    }

    /**
     * Gets the tenant_name
     */
    public function getTenantSlugAttribute()
    {
        return Tenant::find($this->tenant_id)->slug;
    }
}
