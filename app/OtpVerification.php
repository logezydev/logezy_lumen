<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpVerification extends Model
{
    protected $table = 'otp_verification';

    protected $fillable = [
        'user_id', 'otp', 'expired_at'
    ];
}
