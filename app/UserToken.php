<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $table = 'user_tokens';

    protected $fillable = [
        'user_id', 'tenant_role_id', 'tenant_id', 'remember_token',
    ];

    /**
     * user
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * tenant
     *
     * @return void
     */
    public function tenant()
    {
        return $this->belongsTo('App\Tenant', 'tenant_id');
    }
}
