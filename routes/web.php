<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/'], function () use ($router) {

    /*
    |--------------------------------------------------------------------------
    | General API's
    |--------------------------------------------------------------------------
    */

    $router->get('/health-check', function () {
        return response()->json([
            'success' => 'success',
            'message' => 'All Systems Operational'
        ], 200);
    });

    $router->post('/forgot_password', ['uses' => 'UserController@forgot_password']);
    $router->post('/verify_otp', ['as' => 'password.reset', 'uses' => 'UserController@verify_otp']);
    $router->post('/password_reset', ['as' => 'passwordreset', 'uses' => 'UserController@password_reset']);

    $router->post('/tenants', ['uses' => 'AgencyController@all_tenants']);
    $router->post('/tenants_by_email', ['uses' => 'AgencyController@tenantsByUserEmail']);
    $router->post('/all_tenants_by_email', ['uses' => 'AgencyController@tenantsByEmailGeneralPurpose']);
    $router->post('/agency_tenants_by_email', ['uses' => 'AgencyController@agencyTenantsByUserEmail']);
    $router->post('/branches', ['uses' => 'AgencyController@get_branches']);
    $router->post('/jobs', ['uses' => 'AgencyController@get_jobs']);
    $router->post('/employment_types', ['uses' => 'AgencyController@get_employment_types']);
    $router->get('/get_app_version', ['uses' => 'AppController@get_app_version']);
    $router->post('/add_app_version', ['uses' => 'AppController@add_app_version']);
    $router->get('/signedtimesheet_declaration', ['uses' => 'AgencyController@get_signedtimesheet_declaration']);
    $router->post('/forward_msg_to_email', ['uses' => 'EmailController@forward_msg_to_email']);

    $router->post('/mobile_app_settings', ['uses' => 'AgencyController@mobile_app_settings']);


    /*
    |--------------------------------------------------------------------------
    | General Authorized API's
    |--------------------------------------------------------------------------
    */
    $router->group(['middleware' => 'auth:api'], function () use ($router) {
        $router->post('agency_settings', ['as' => 'agency.settings', 'uses' => 'AgencyController@agency_settings']);
        $router->post('change_password', ['as' => 'user.change_password', 'uses' => 'UserController@change_password']);
    });

    /*
    |--------------------------------------------------------------------------
    | Recruitment API's
    |--------------------------------------------------------------------------
    */

    $router->group(['prefix' => 'recruitment/'], function () use ($router) {
        $router->get('/get_general_details', ['uses' => 'RecruitmentCandidateController@getGeneralDetails']);
        $router->post('/register_candidate', ['uses' => 'RecruitmentCandidateController@registerCandidate']);
        
        /*
        |--------------------------------------------------------------------------
        | Authorized Recruitment API's
        |--------------------------------------------------------------------------
        */
        $router->group(['middleware' => 'auth:api'], function () use ($router) {
            $router->get('/get_available_jobs', ['uses' => 'RecruitmentCandidateController@getAvailableJobs']);
            $router->get('/get_built_form', ['uses' => 'RecruitmentCandidateController@getBuiltForm']);
            $router->get('/get_candidate_additional', ['uses' => 'RecruitmentCandidateController@getCandidateAdditional']);
            $router->post('/store_candidate_additional', ['uses' => 'RecruitmentCandidateController@storeCandidateAdditional']);
            $router->post('/upload_candidate_compliance', ['uses' => 'RecruitmentCandidateController@uploadCandidateCompliance']);
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Candidate API's
    |--------------------------------------------------------------------------
    */

    $router->group(['prefix' => 'candidate/'], function () use ($router) {

       $router->post('sign_up/', ['uses' => 'CandidateController@candidate_sign_up']);
       $router->get('login/', ['uses' => 'UserController@authenticate']);
       $router->post('login/', ['uses' => 'UserController@authenticate']);
       $router->post('logout/', ['uses' => 'UserController@logout']);
       $router->post('delete/', ['uses' => 'UserController@deleteCandidateAccount']);

        /*
        |--------------------------------------------------------------------------
        | Authorized Candidate API's
        |--------------------------------------------------------------------------
        */

        $router->group(['middleware' => 'auth:api'], function () use ($router) {

            //Profile
            $router->post('/switch_account', ['uses' => 'UserController@authenticateCandidateWithoutPassword']);
            $router->post('/profile', ['uses' => 'CandidateController@candidate_profile']);
            $router->post('/update_profile', ['uses' => 'CandidateController@update_profile']);
            $router->post('/update_firebase_uin', ['uses' => 'CandidateController@update_firebase_uin']);

            //Candidate Documents
            $router->post('/documents', ['uses' => 'CandidateController@candidate_documents']);
            $router->post('/upload_documents', ['uses' => 'CandidateController@upload_documents']);

            //Vacancies
            $router->post('/vacancy_dates', ['uses' => 'VacancyController@vacancy_dates']);
            // $router->post('/vacancy_list', ['uses' => 'VacancyController@vacancy_list']);
            $router->post('/vacancy_list', ['uses' => 'VacancyController@getVacancies']);
            // $router->post('/vacancy', ['uses' => 'VacancyController@vacancy']);
            $router->post('/vacancy', ['uses' => 'VacancyController@getVacancy']);
            $router->post('/reject_vacancy', ['uses' => 'VacancyController@reject_vacancy']);
            $router->post('/apply_for_vacancy', ['uses' => 'AssignVacancyController@apply_for_vacancy']);

            //Assigned Vacancies
            $router->post('/assigned_vacancies', ['uses' => 'AssignVacancyController@assigned_vacancies']);
            $router->post('/assigned_vacancy', ['uses' => 'AssignVacancyController@assigned_vacancy']);
            $router->post('/assigned_vacancy_confirm', ['uses' => 'AssignVacancyController@assigned_vacancy_confirm']);
            $router->post('/assigned_vacancy_reject', ['uses' => 'AssignVacancyController@assigned_vacancy_reject']);

            //Signed Timesheets
            $router->post('/signed_timesheet', ['uses' => 'AssignVacancyController@get_signed_vacancies_by_id']);
            $router->post('/signed_timesheets', ['uses' => 'AssignVacancyController@get_signed_vacancies']);
            $router->post('/signed_timesheet_update', ['uses' => 'AssignVacancyController@post_signed_vacancies']);
            
            //Availability
            $router->post('/get_availability', ['uses' => 'CandidateAvailabilityController@get_availability']);
            $router->post('/add_availability', ['uses' => 'CandidateAvailabilityController@add_availability']);
            $router->post('/add_bulk_availability', ['uses' => 'CandidateAvailabilityController@addBulkAvailability']);

            //Notifications
            $router->get('/get_notifications', ['uses' => 'NotificationController@get_notifications']);
            $router->post('/view_notification', ['uses' => 'NotificationController@view_notification']);
            $router->post('/delete_notification', ['uses' => 'NotificationController@delete_notification']);
            $router->post('/delete_all_notifications', ['uses' => 'NotificationController@delete_all_notifications']);
            
            //Reports
            $router->post('/payroll_reports', ['uses' => 'ReportController@get_payroll_reports']);

            //Holiday Pays
            $router->post('/request_holiday_pay', ['uses' => 'CandidateHolidayPayDeductionController@request_holiday_pay']);
            $router->post('/get_holiday_pay_requests', ['uses' => 'CandidateHolidayPayDeductionController@get_holiday_pay_requests']);

            //Timeclocks
            $router->post('/timeclock', ['uses' => 'TimeclockController@timeclock']);
            $router->post('/clock_in', ['uses' => 'TimeclockController@clock_in']);
            $router->post('/clock_out', ['uses' => 'TimeclockController@clock_out']);

            //Candidate clocking - OLD
            $router->post('/in_range', ['uses' => 'CandidateClockingController@checkInRange']);
            $router->post('/shift_clock_in', ['uses' => 'CandidateClockingController@shiftClockIn']);
            $router->post('/shift_break', ['uses' => 'CandidateClockingController@shiftBreak']);
            $router->post('/shift_clock_out', ['uses' => 'CandidateClockingController@shiftClockOut']);
        });

        
        //Candidate registration
        $router->group(['prefix' => 'authorizing/'], function () use ($router) {
            $router->post('/get_compliance_documents', ['uses' => 'CandidateRegistrationController@getComplianceDocuments']);
            $router->post('/return_users_by_email', ['uses' => 'CandidateRegistrationController@returnUsersByEmail']);
            $router->post('/get_candidate_by_user_id', ['uses' => 'CandidateRegistrationController@getCandidateByUserId']);

            $router->post('/create_candidate_user', ['uses' => 'CandidateRegistrationController@createCandidateUser']);
            $router->post('/upload_compliance_document', ['uses' => 'CandidateRegistrationController@uploadComplianceDocument']);
            $router->post('/work_experience', ['uses' => 'CandidateRegistrationController@workExperience']);
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Agency API's
    |--------------------------------------------------------------------------
    */

    $router->group(['prefix' => 'agency/'], function () use ($router) {
        $router->post('web_notification_status', ['uses' => 'AgencyNotificationSettingController@getMobileNotifications']);
        $router->post('update_firebase_server_key_white_label_app', ['uses' => 'AgencyController@update_firebase_server_key_white_label_app']);
        $router->post('update_firebase_server_key_non_white_label', ['uses' => 'AgencyController@update_firebase_server_key_non_white_label_app']);
        $router->post('update_firebase_server_key_manager_app', ['uses' => 'AgencyController@update_firebase_server_key_manager_app']);

        $router->post('get_agency', ['uses' => 'AgencyController@getAgency']);
        $router->post('integration', ['uses' => 'AgencyController@get_integration']);

        $router->post('admins', ['uses' => 'AgencyController@get_admins']);
        $router->post('rolesby_email', ['uses' => 'UserController@rolesByUserEmail']);
          
        $router->post('regions', ['uses' => 'RegionController@getRegions']);
        $router->post('business_units', ['uses' => 'HelperController@getBusinesses']);
        $router->post('business_unit_shifts', ['uses' => 'HelperController@getShiftsByBusinessunit']);
        $router->post('business_unit_jobs', ['uses' => 'HelperController@getClientJobs']);
        $router->post('candidate_jobs', ['uses' => 'HelperController@get_candidate_jobs']);
        $router->post('total_hours', ['uses' => 'HelperController@calculateTotalHours']);
        $router->post('get_times', ['uses' => 'HelperController@getTimes']);
        $router->post('businessunits_by_job', ['uses' => 'HelperController@getBuByJobId']);
        $router->post('candidate_list', ['uses' => 'HelperController@getCandidatesByStatus']);
        $router->post('candidates_by_job', ['uses' => 'HelperController@getCandidatesByJob']);
        $router->post('candidate_statuses', ['uses' => 'HelperController@getCandidateStatuses']);
        //For agency app
        $router->get('login/', ['uses' => 'UserController@authenticateAgency']);
        $router->post('login/', ['uses' => 'UserController@authenticateAgency']);
        $router->post('logout/', ['uses' => 'UserController@logout']);
        $router->post('sendOneSignalNotification', ['uses' => 'HelperController@sendOneSignalNotification']);

        /*
        |--------------------------------------------------------------------------
        | Authorized Agency API's
        |--------------------------------------------------------------------------
        */
        $router->group(['middleware' => 'auth:api'], function () use ($router) {

            //Profile
            $router->post('subscription', ['uses' => 'SubscriptionController@getSubscriptionDetails']);
            $router->post('/switch_account', ['uses' => 'UserController@authenticateAgencyWithoutPassword']);
            $router->post('/dashboard', ['uses' => 'DashboardController@getDashboardDetails']);
            $router->post('/update_firebase_uin', ['uses' => 'AgencyController@update_firebase_uin']);

            // Vacancies
            $router->post('/add_vacancy', ['uses' => 'Manager\VacancyController@addVacancies']);
            $router->post('/vacancy_list', ['uses' => 'Manager\VacancyController@vacancy_list']);
            $router->post('/vacancy', ['uses' => 'Manager\VacancyController@vacancy']);
            $router->post('/update_vacancy', ['uses' => 'Manager\VacancyController@update_vacancy']);
            $router->post('/candidates_for_publish_vacancy', ['uses' => 'HelperController@candidates_for_publish_vacancy']);
            $router->post('/publish_vacancy', ['uses' => 'Manager\VacancyController@publish_vacancy']);
            $router->post('/delete_vacancy', ['uses' => 'Manager\VacancyController@delete_vacancy']);

            //Assign vacancies
            $router->post('/assign_multiple_dates', ['uses' => 'Manager\VacancyController@assign_multiple_dates']);
            $router->post('/assigned_shifts', ['uses' => 'Manager\ScheduleController@fetchScheduleSlots']);
            $router->post('/assigned_shift', ['uses' => 'AssignVacancyController@assigned_shift']);
            $router->post('/bookings', ['uses' => 'Manager\ScheduleController@getBookingsFromToday']);
            $router->post('/assign_multiple_candidates', ['uses' => 'AssignVacancyController@assign_multiple_candidates']);
            $router->post('/update_or_reassign_shift', ['uses' => 'AssignVacancyController@update_or_reassign_shift']);
            $router->post('/accept_applied_candidates', ['uses' => 'AssignVacancyController@accept_applied_candidates']);
            $router->post('/delete_shifts', ['uses' => 'AssignVacancyController@delete_shifts']);

            //Candidates
            $router->post('/candidate', ['uses' => 'Manager\CandidateController@getMoreCandidateDetails']);
            $router->post('/candidate_documents', ['uses' => 'Manager\CandidateController@candidate_documents']);
            $router->post('/candidate_upload_documents', ['uses' => 'Manager\CandidateController@upload_documents']);
            $router->post('/compliant_documents', ['uses' => 'Manager\CandidateController@compliant_documents']);
            $router->post('/delete_document', ['uses' => 'Manager\CandidateController@deleteDocument']);
            $router->post('/clear_document', ['uses' => 'Manager\CandidateController@clearDocument']);

            //Notifications
            $router->get('/notifications', ['uses' => 'NotificationController@getAgencyNotifications']);
            $router->post('/view_notification', ['uses' => 'NotificationController@view_notification']);
            $router->post('/delete_notification', ['uses' => 'NotificationController@delete_notification']);
            $router->post('/delete_all_notifications', ['uses' => 'NotificationController@delete_all_notifications_agency']);
        });
    });




    /*
    |--------------------------------------------------------------------------
    | client API's
    |--------------------------------------------------------------------------
    */

    $router->group(['prefix' => 'client/'], function () use ($router) {

       $router->post('sign_up/', ['uses' => 'CandidateController@candidate_sign_up']);
       $router->post('login/', ['uses' => 'UserController@authenticateClientBu']);
       $router->post('logout/', ['uses' => 'UserController@logout']);
       $router->post('business_units', ['uses' => 'HelperController@getBusinessesForClients']);

        /*
        |--------------------------------------------------------------------------
        | Authorized client API's
        |--------------------------------------------------------------------------
        */

        $router->group(['middleware' => 'auth:api'], function () use ($router) {
           $router->post('dashboard/', ['uses' => 'Client\DashboardController@dashboardDetails']);
           $router->post('add_vacancy/', ['uses' => 'Client\VacancyController@addVacancies']);
           $router->post('vacancy_list/', ['uses' => 'Client\VacancyController@vacancy_list']);
           $router->post('vacancy/', ['uses' => 'Client\VacancyController@vacancy']);
           $router->post('assigned_vacancies/', ['uses' => 'Client\AssignVacancyController@assigned_vacancies']);
           $router->post('assigned_vacancy/', ['uses' => 'Client\AssignVacancyController@assigned_vacancy']);
           $router->post('timesheets/', ['uses' => 'Client\AssignVacancyController@timesheets']);
           $router->post('timesheet/', ['uses' => 'Client\AssignVacancyController@timesheet']);
           $router->post('signed_timesheet/', ['uses' => 'Client\AssignVacancyController@signed_timesheet']);
           $router->post('approve_signed_timesheet/', ['uses' => 'Client\AssignVacancyController@approveSignedTimesheet']);
           $router->post('approve_timesheet/', ['uses' => 'Client\AssignVacancyController@approveTimesheet']);
        });
    });

    /*
    |--------------------------------------------------------------------------
    | LOGEZY SMS API's
    |--------------------------------------------------------------------------
    */
    $router->group(['prefix' => 'sms/'], function () use ($router) {

        // $router->group(['middleware' => 'check_method'], function () use ($router) {

        $router->post('register', ['uses' => 'SmsController@register_mobile']);
        $router->post('send', ['uses' => 'SmsController@send_sms']);
        // });

    });

});
